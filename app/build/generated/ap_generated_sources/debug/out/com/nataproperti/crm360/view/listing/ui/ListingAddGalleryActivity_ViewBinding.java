// Generated code from Butter Knife. Do not modify!
package com.nataproperti.crm360.view.listing.ui;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.nataproperti.crm360.nataproperty.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListingAddGalleryActivity_ViewBinding implements Unbinder {
  private ListingAddGalleryActivity target;

  @UiThread
  public ListingAddGalleryActivity_ViewBinding(ListingAddGalleryActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ListingAddGalleryActivity_ViewBinding(ListingAddGalleryActivity target, View source) {
    this.target = target;

    target.btnUpload = Utils.findRequiredViewAsType(source, R.id.btn_upload, "field 'btnUpload'", Button.class);
    target.btnNext = Utils.findRequiredViewAsType(source, R.id.btn_next, "field 'btnNext'", Button.class);
    target.linearLayoutNoData = Utils.findRequiredViewAsType(source, R.id.linear_no_data, "field 'linearLayoutNoData'", LinearLayout.class);
    target.btnFinish = Utils.findRequiredViewAsType(source, R.id.btn_finish, "field 'btnFinish'", Button.class);
    target.coordinatorLayout = Utils.findRequiredViewAsType(source, R.id.coordinatorLayout, "field 'coordinatorLayout'", CoordinatorLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListingAddGalleryActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnUpload = null;
    target.btnNext = null;
    target.linearLayoutNoData = null;
    target.btnFinish = null;
    target.coordinatorLayout = null;
  }
}
