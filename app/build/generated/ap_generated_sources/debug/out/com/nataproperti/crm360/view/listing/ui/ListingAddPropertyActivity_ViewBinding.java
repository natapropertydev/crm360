// Generated code from Butter Knife. Do not modify!
package com.nataproperti.crm360.view.listing.ui;

import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.nataproperti.crm360.nataproperty.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListingAddPropertyActivity_ViewBinding implements Unbinder {
  private ListingAddPropertyActivity target;

  @UiThread
  public ListingAddPropertyActivity_ViewBinding(ListingAddPropertyActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ListingAddPropertyActivity_ViewBinding(ListingAddPropertyActivity target, View source) {
    this.target = target;

    target.spnCountry = Utils.findRequiredViewAsType(source, R.id.spn_country, "field 'spnCountry'", Spinner.class);
    target.spnProvince = Utils.findRequiredViewAsType(source, R.id.spn_province, "field 'spnProvince'", Spinner.class);
    target.spnCity = Utils.findRequiredViewAsType(source, R.id.spn_city, "field 'spnCity'", Spinner.class);
    target.spnSubLocation = Utils.findRequiredViewAsType(source, R.id.spn_sub_location, "field 'spnSubLocation'", Spinner.class);
    target.spnPriceType = Utils.findRequiredViewAsType(source, R.id.spn_price_type, "field 'spnPriceType'", Spinner.class);
    target.spnCertivicate = Utils.findRequiredViewAsType(source, R.id.spn_certificate, "field 'spnCertivicate'", Spinner.class);
    target.edtPropertyName = Utils.findRequiredViewAsType(source, R.id.edt_property_name, "field 'edtPropertyName'", AutoCompleteTextView.class);
    target.edtListingTitle = Utils.findRequiredViewAsType(source, R.id.edt_listing_title, "field 'edtListingTitle'", EditText.class);
    target.edtListingDesc = Utils.findRequiredViewAsType(source, R.id.edt_listing_desc, "field 'edtListingDesc'", EditText.class);
    target.edtPrice = Utils.findRequiredViewAsType(source, R.id.edt_price, "field 'edtPrice'", EditText.class);
    target.edtLand = Utils.findRequiredViewAsType(source, R.id.edt_land, "field 'edtLand'", EditText.class);
    target.edtBuild = Utils.findRequiredViewAsType(source, R.id.edt_build, "field 'edtBuild'", EditText.class);
    target.edtNo = Utils.findRequiredViewAsType(source, R.id.edt_no, "field 'edtNo'", EditText.class);
    target.edtBlock = Utils.findRequiredViewAsType(source, R.id.edt_block, "field 'edtBlock'", EditText.class);
    target.edtAddress = Utils.findRequiredViewAsType(source, R.id.edt_address, "field 'edtAddress'", EditText.class);
    target.edtPostCode = Utils.findRequiredViewAsType(source, R.id.edt_post_code, "field 'edtPostCode'", EditText.class);
    target.edtBuildDimX = Utils.findRequiredViewAsType(source, R.id.edt_buildDimX, "field 'edtBuildDimX'", EditText.class);
    target.edtBuildDimY = Utils.findRequiredViewAsType(source, R.id.edt_buildDimY, "field 'edtBuildDimY'", EditText.class);
    target.edtLandDimX = Utils.findRequiredViewAsType(source, R.id.edt_landDimX, "field 'edtLandDimX'", EditText.class);
    target.edtLandDimY = Utils.findRequiredViewAsType(source, R.id.edt_landDimY, "field 'edtLandDimY'", EditText.class);
    target.btnNext = Utils.findRequiredViewAsType(source, R.id.btn_next, "field 'btnNext'", Button.class);
    target.txtBuild = Utils.findRequiredViewAsType(source, R.id.txt_build, "field 'txtBuild'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListingAddPropertyActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.spnCountry = null;
    target.spnProvince = null;
    target.spnCity = null;
    target.spnSubLocation = null;
    target.spnPriceType = null;
    target.spnCertivicate = null;
    target.edtPropertyName = null;
    target.edtListingTitle = null;
    target.edtListingDesc = null;
    target.edtPrice = null;
    target.edtLand = null;
    target.edtBuild = null;
    target.edtNo = null;
    target.edtBlock = null;
    target.edtAddress = null;
    target.edtPostCode = null;
    target.edtBuildDimX = null;
    target.edtBuildDimY = null;
    target.edtLandDimX = null;
    target.edtLandDimY = null;
    target.btnNext = null;
    target.txtBuild = null;
  }
}
