// Generated code from Butter Knife. Do not modify!
package com.nataproperti.crm360.view.listing.ui;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.nataproperti.crm360.nataproperty.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListingAddPropertyUnitActivity_ViewBinding implements Unbinder {
  private ListingAddPropertyUnitActivity target;

  @UiThread
  public ListingAddPropertyUnitActivity_ViewBinding(ListingAddPropertyUnitActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ListingAddPropertyUnitActivity_ViewBinding(ListingAddPropertyUnitActivity target,
      View source) {
    this.target = target;

    target.spnBRType = Utils.findRequiredViewAsType(source, R.id.spn_br_type, "field 'spnBRType'", Spinner.class);
    target.spnMaidBRType = Utils.findRequiredViewAsType(source, R.id.spn_maidBRType, "field 'spnMaidBRType'", Spinner.class);
    target.spnBathRType = Utils.findRequiredViewAsType(source, R.id.spn_bathRType, "field 'spnBathRType'", Spinner.class);
    target.spnMaidBathRType = Utils.findRequiredViewAsType(source, R.id.spn_maidBathRType, "field 'spnMaidBathRType'", Spinner.class);
    target.spnGarageType = Utils.findRequiredViewAsType(source, R.id.spn_garageType, "field 'spnGarageType'", Spinner.class);
    target.spnCarportType = Utils.findRequiredViewAsType(source, R.id.spn_carportType, "field 'spnCarportType'", Spinner.class);
    target.spnPhoneLine = Utils.findRequiredViewAsType(source, R.id.spn_phoneLine, "field 'spnPhoneLine'", Spinner.class);
    target.spnFurnishType = Utils.findRequiredViewAsType(source, R.id.spn_furnishType, "field 'spnFurnishType'", Spinner.class);
    target.spnElectricType = Utils.findRequiredViewAsType(source, R.id.spn_electricType, "field 'spnElectricType'", Spinner.class);
    target.spnFacingType = Utils.findRequiredViewAsType(source, R.id.spn_facingType, "field 'spnFacingType'", Spinner.class);
    target.spnViewType = Utils.findRequiredViewAsType(source, R.id.spn_viewType, "field 'spnViewType'", Spinner.class);
    target.edtYoutube1 = Utils.findRequiredViewAsType(source, R.id.edt_youtube1, "field 'edtYoutube1'", EditText.class);
    target.edtYoutube2 = Utils.findRequiredViewAsType(source, R.id.edt_youtube2, "field 'edtYoutube2'", EditText.class);
    target.btnSave = Utils.findRequiredViewAsType(source, R.id.btn_save, "field 'btnSave'", Button.class);
    target.btnFinish = Utils.findRequiredViewAsType(source, R.id.btn_finish, "field 'btnFinish'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListingAddPropertyUnitActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.spnBRType = null;
    target.spnMaidBRType = null;
    target.spnBathRType = null;
    target.spnMaidBathRType = null;
    target.spnGarageType = null;
    target.spnCarportType = null;
    target.spnPhoneLine = null;
    target.spnFurnishType = null;
    target.spnElectricType = null;
    target.spnFacingType = null;
    target.spnViewType = null;
    target.edtYoutube1 = null;
    target.edtYoutube2 = null;
    target.btnSave = null;
    target.btnFinish = null;
  }
}
