// Generated code from Butter Knife. Do not modify!
package com.nataproperti.crm360.view.listing.ui;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.nataproperti.crm360.nataproperty.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListingCompanyInfoActivity_ViewBinding implements Unbinder {
  private ListingCompanyInfoActivity target;

  @UiThread
  public ListingCompanyInfoActivity_ViewBinding(ListingCompanyInfoActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ListingCompanyInfoActivity_ViewBinding(ListingCompanyInfoActivity target, View source) {
    this.target = target;

    target.txtAgencyCode = Utils.findRequiredViewAsType(source, R.id.txt_agency_code, "field 'txtAgencyCode'", TextView.class);
    target.txtAgencyName = Utils.findRequiredViewAsType(source, R.id.txt_agency_name, "field 'txtAgencyName'", TextView.class);
    target.txtAgencyEmail = Utils.findRequiredViewAsType(source, R.id.txt_agency_email, "field 'txtAgencyEmail'", TextView.class);
    target.txtAgencyMobile = Utils.findRequiredViewAsType(source, R.id.txt_agency_mobile, "field 'txtAgencyMobile'", TextView.class);
    target.txtAgencyPhone = Utils.findRequiredViewAsType(source, R.id.txt_agency_phone, "field 'txtAgencyPhone'", TextView.class);
    target.txtCompanyCountry = Utils.findRequiredViewAsType(source, R.id.txt_country, "field 'txtCompanyCountry'", TextView.class);
    target.txtCompanyProvince = Utils.findRequiredViewAsType(source, R.id.txt_province, "field 'txtCompanyProvince'", TextView.class);
    target.txtCompanyCity = Utils.findRequiredViewAsType(source, R.id.txt_city, "field 'txtCompanyCity'", TextView.class);
    target.txtCompanyPostCode = Utils.findRequiredViewAsType(source, R.id.txt_post_code, "field 'txtCompanyPostCode'", TextView.class);
    target.txtCompanyAddress = Utils.findRequiredViewAsType(source, R.id.txt_address, "field 'txtCompanyAddress'", TextView.class);
    target.txtPrincipleName = Utils.findRequiredViewAsType(source, R.id.txt_principle_name, "field 'txtPrincipleName'", TextView.class);
    target.txtPrincipleEmail1 = Utils.findRequiredViewAsType(source, R.id.txt_principle_email1, "field 'txtPrincipleEmail1'", TextView.class);
    target.txtPrincipleEmail2 = Utils.findRequiredViewAsType(source, R.id.txt_principle_email2, "field 'txtPrincipleEmail2'", TextView.class);
    target.txtPrincipleHp1 = Utils.findRequiredViewAsType(source, R.id.txt_principle_hp1, "field 'txtPrincipleHp1'", TextView.class);
    target.txtPrincipleHp2 = Utils.findRequiredViewAsType(source, R.id.txt_principle_hp2, "field 'txtPrincipleHp2'", TextView.class);
    target.txtPtName = Utils.findRequiredViewAsType(source, R.id.txt_pt_name, "field 'txtPtName'", TextView.class);
    target.txtNPWP = Utils.findRequiredViewAsType(source, R.id.txt_company_npwp, "field 'txtNPWP'", TextView.class);
    target.txtSPPKP = Utils.findRequiredViewAsType(source, R.id.txt_company_sppkp, "field 'txtSPPKP'", TextView.class);
    target.txtTDP = Utils.findRequiredViewAsType(source, R.id.txt_company_tdp, "field 'txtTDP'", TextView.class);
    target.txtSIUP = Utils.findRequiredViewAsType(source, R.id.txt_company_siup, "field 'txtSIUP'", TextView.class);
    target.txtSkDomisili = Utils.findRequiredViewAsType(source, R.id.txt_company_sk_domisili, "field 'txtSkDomisili'", TextView.class);
    target.txtAccountBank = Utils.findRequiredViewAsType(source, R.id.txt_account_bank, "field 'txtAccountBank'", TextView.class);
    target.txtAccountName = Utils.findRequiredViewAsType(source, R.id.txt_account_name, "field 'txtAccountName'", TextView.class);
    target.txtAccountNomor = Utils.findRequiredViewAsType(source, R.id.txt_account_nomor, "field 'txtAccountNomor'", TextView.class);
    target.txtBankBranch = Utils.findRequiredViewAsType(source, R.id.txt_bank_branch, "field 'txtBankBranch'", TextView.class);
    target.btnChanegAgencyCode = Utils.findRequiredViewAsType(source, R.id.btn_change_agency_code, "field 'btnChanegAgencyCode'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListingCompanyInfoActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtAgencyCode = null;
    target.txtAgencyName = null;
    target.txtAgencyEmail = null;
    target.txtAgencyMobile = null;
    target.txtAgencyPhone = null;
    target.txtCompanyCountry = null;
    target.txtCompanyProvince = null;
    target.txtCompanyCity = null;
    target.txtCompanyPostCode = null;
    target.txtCompanyAddress = null;
    target.txtPrincipleName = null;
    target.txtPrincipleEmail1 = null;
    target.txtPrincipleEmail2 = null;
    target.txtPrincipleHp1 = null;
    target.txtPrincipleHp2 = null;
    target.txtPtName = null;
    target.txtNPWP = null;
    target.txtSPPKP = null;
    target.txtTDP = null;
    target.txtSIUP = null;
    target.txtSkDomisili = null;
    target.txtAccountBank = null;
    target.txtAccountName = null;
    target.txtAccountNomor = null;
    target.txtBankBranch = null;
    target.btnChanegAgencyCode = null;
  }
}
