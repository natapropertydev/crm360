// Generated code from Butter Knife. Do not modify!
package com.nataproperti.crm360.view.listing.ui;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.nataproperty.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListingPropertyDescriptionFragment_ViewBinding implements Unbinder {
  private ListingPropertyDescriptionFragment target;

  @UiThread
  public ListingPropertyDescriptionFragment_ViewBinding(ListingPropertyDescriptionFragment target,
      View source) {
    this.target = target;

    target.txtDesc = Utils.findRequiredViewAsType(source, R.id.txt_desc, "field 'txtDesc'", TextView.class);
    target.txtListingType = Utils.findRequiredViewAsType(source, R.id.txt_listing_type, "field 'txtListingType'", TextView.class);
    target.txtPropertyType = Utils.findRequiredViewAsType(source, R.id.txt_property_type, "field 'txtPropertyType'", TextView.class);
    target.txtPropertyName = Utils.findRequiredViewAsType(source, R.id.txt_property_name, "field 'txtPropertyName'", TextView.class);
    target.txtSubLocationName = Utils.findRequiredViewAsType(source, R.id.txt_sub_location_name, "field 'txtSubLocationName'", TextView.class);
    target.txtAddress = Utils.findRequiredViewAsType(source, R.id.txt_address, "field 'txtAddress'", TextView.class);
    target.txtBlock = Utils.findRequiredViewAsType(source, R.id.txt_block, "field 'txtBlock'", TextView.class);
    target.txtNo = Utils.findRequiredViewAsType(source, R.id.txt_no, "field 'txtNo'", TextView.class);
    target.txtPostCode = Utils.findRequiredViewAsType(source, R.id.txt_post_code, "field 'txtPostCode'", TextView.class);
    target.txtPrice = Utils.findRequiredViewAsType(source, R.id.txt_price, "field 'txtPrice'", TextView.class);
    target.txtPriceTypeName = Utils.findRequiredViewAsType(source, R.id.txt_price_type_name, "field 'txtPriceTypeName'", TextView.class);
    target.txtBuildArea = Utils.findRequiredViewAsType(source, R.id.txt_build_area, "field 'txtBuildArea'", TextView.class);
    target.txtLandArea = Utils.findRequiredViewAsType(source, R.id.txt_land_area, "field 'txtLandArea'", TextView.class);
    target.txtBrTypeName = Utils.findRequiredViewAsType(source, R.id.txt_br_type_name, "field 'txtBrTypeName'", TextView.class);
    target.txtMaidBrTypeName = Utils.findRequiredViewAsType(source, R.id.txt_maid_br_type_name, "field 'txtMaidBrTypeName'", TextView.class);
    target.txtBathRTypeName = Utils.findRequiredViewAsType(source, R.id.txt_bath_r_type_name, "field 'txtBathRTypeName'", TextView.class);
    target.txtMaidBathRTypeName = Utils.findRequiredViewAsType(source, R.id.txt_maid_bath_r_type_name, "field 'txtMaidBathRTypeName'", TextView.class);
    target.txtGarageTypeName = Utils.findRequiredViewAsType(source, R.id.txt_garage_type_name, "field 'txtGarageTypeName'", TextView.class);
    target.txtCarportTypeName = Utils.findRequiredViewAsType(source, R.id.txt_carport_type_name, "field 'txtCarportTypeName'", TextView.class);
    target.txtPhoneLineName = Utils.findRequiredViewAsType(source, R.id.txt_phone_line_name, "field 'txtPhoneLineName'", TextView.class);
    target.txtFurnishTypeName = Utils.findRequiredViewAsType(source, R.id.txt_furnish_type_name, "field 'txtFurnishTypeName'", TextView.class);
    target.txtElectricTypeName = Utils.findRequiredViewAsType(source, R.id.txt_electric_type_name, "field 'txtElectricTypeName'", TextView.class);
    target.txtFacingName = Utils.findRequiredViewAsType(source, R.id.txt_facing_name, "field 'txtFacingName'", TextView.class);
    target.txtViewTypeName = Utils.findRequiredViewAsType(source, R.id.txt_view_type_name, "field 'txtViewTypeName'", TextView.class);
    target.txtFacility = Utils.findRequiredViewAsType(source, R.id.txt_facility, "field 'txtFacility'", TextView.class);
    target.txtMaps = Utils.findRequiredViewAsType(source, R.id.txt_maps, "field 'txtMaps'", MyTextViewLatoReguler.class);
    target.linearLayoutInfo = Utils.findRequiredViewAsType(source, R.id.linear_info, "field 'linearLayoutInfo'", LinearLayout.class);
    target.linearLayoutFacility = Utils.findRequiredViewAsType(source, R.id.linear_facility, "field 'linearLayoutFacility'", LinearLayout.class);
    target.linearLayoutMaps = Utils.findRequiredViewAsType(source, 2131231334, "field 'linearLayoutMaps'", LinearLayout.class);
    target.btnDownload = Utils.findRequiredViewAsType(source, R.id.btn_download, "field 'btnDownload'", Button.class);
    target.btnVideo1 = Utils.findRequiredViewAsType(source, R.id.btn_video1, "field 'btnVideo1'", Button.class);
    target.btnVideo2 = Utils.findRequiredViewAsType(source, R.id.btn_video2, "field 'btnVideo2'", Button.class);
    target.btnCall = Utils.findRequiredViewAsType(source, R.id.btn_call, "field 'btnCall'", ImageView.class);
    target.btnMessage = Utils.findRequiredViewAsType(source, R.id.btn_chat, "field 'btnMessage'", ImageView.class);
    target.btnFavorit = Utils.findRequiredViewAsType(source, R.id.btn_favorite, "field 'btnFavorit'", ImageView.class);
    target.linearLayoutListing = Utils.findRequiredViewAsType(source, R.id.linear_listing, "field 'linearLayoutListing'", LinearLayout.class);
    target.line = Utils.findRequiredView(source, R.id.line, "field 'line'");
  }

  @Override
  @CallSuper
  public void unbind() {
    ListingPropertyDescriptionFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtDesc = null;
    target.txtListingType = null;
    target.txtPropertyType = null;
    target.txtPropertyName = null;
    target.txtSubLocationName = null;
    target.txtAddress = null;
    target.txtBlock = null;
    target.txtNo = null;
    target.txtPostCode = null;
    target.txtPrice = null;
    target.txtPriceTypeName = null;
    target.txtBuildArea = null;
    target.txtLandArea = null;
    target.txtBrTypeName = null;
    target.txtMaidBrTypeName = null;
    target.txtBathRTypeName = null;
    target.txtMaidBathRTypeName = null;
    target.txtGarageTypeName = null;
    target.txtCarportTypeName = null;
    target.txtPhoneLineName = null;
    target.txtFurnishTypeName = null;
    target.txtElectricTypeName = null;
    target.txtFacingName = null;
    target.txtViewTypeName = null;
    target.txtFacility = null;
    target.txtMaps = null;
    target.linearLayoutInfo = null;
    target.linearLayoutFacility = null;
    target.linearLayoutMaps = null;
    target.btnDownload = null;
    target.btnVideo1 = null;
    target.btnVideo2 = null;
    target.btnCall = null;
    target.btnMessage = null;
    target.btnFavorit = null;
    target.linearLayoutListing = null;
    target.line = null;
  }
}
