// Generated code from Butter Knife. Do not modify!
package com.nataproperti.crm360.view.listing.ui;

import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.nataproperti.crm360.nataproperty.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListingPropertyMapsFragment_ViewBinding implements Unbinder {
  private ListingPropertyMapsFragment target;

  @UiThread
  public ListingPropertyMapsFragment_ViewBinding(ListingPropertyMapsFragment target, View source) {
    this.target = target;

    target.txtNoMaps = Utils.findRequiredViewAsType(source, R.id.txt_no_maps, "field 'txtNoMaps'", TextView.class);
    target.btnEdit = Utils.findRequiredViewAsType(source, R.id.btn_edit, "field 'btnEdit'", Button.class);
    target.frameLayoutMaps = Utils.findRequiredViewAsType(source, R.id.maps, "field 'frameLayoutMaps'", FrameLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListingPropertyMapsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtNoMaps = null;
    target.btnEdit = null;
    target.frameLayoutMaps = null;
  }
}
