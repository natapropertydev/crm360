// Generated code from Butter Knife. Do not modify!
package com.nataproperti.crm360.view.profile.ui;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.nataproperti.crm360.nataproperty.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddressActivity_ViewBinding implements Unbinder {
  private AddressActivity target;

  @UiThread
  public AddressActivity_ViewBinding(AddressActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddressActivity_ViewBinding(AddressActivity target, View source) {
    this.target = target;

    target.spnCountry = Utils.findRequiredViewAsType(source, R.id.spn_country, "field 'spnCountry'", Spinner.class);
    target.spnProvince = Utils.findRequiredViewAsType(source, R.id.spn_province, "field 'spnProvince'", Spinner.class);
    target.spnCity = Utils.findRequiredViewAsType(source, R.id.spn_city, "field 'spnCity'", Spinner.class);
    target.spnCorresCountry = Utils.findRequiredViewAsType(source, R.id.spn_corres_country, "field 'spnCorresCountry'", Spinner.class);
    target.spnCorresProvince = Utils.findRequiredViewAsType(source, R.id.spn_corres_province, "field 'spnCorresProvince'", Spinner.class);
    target.spnCorresCity = Utils.findRequiredViewAsType(source, R.id.spn_corrres_city, "field 'spnCorresCity'", Spinner.class);
    target.edtIdAddress = Utils.findRequiredViewAsType(source, R.id.edt_id_address, "field 'edtIdAddress'", EditText.class);
    target.edtIdPostCode = Utils.findRequiredViewAsType(source, R.id.edt_id_post_code, "field 'edtIdPostCode'", EditText.class);
    target.edtCorresAddress = Utils.findRequiredViewAsType(source, R.id.edt_corres_address, "field 'edtCorresAddress'", EditText.class);
    target.edtCorresPostCode = Utils.findRequiredViewAsType(source, R.id.edt_corres_post_code, "field 'edtCorresPostCode'", EditText.class);
    target.checkBoxCorres = Utils.findRequiredViewAsType(source, R.id.cb_corress_address, "field 'checkBoxCorres'", CheckBox.class);
    target.cardViewCorres = Utils.findRequiredViewAsType(source, R.id.cv_corres, "field 'cardViewCorres'", CardView.class);
    target.btnSaveAddress = Utils.findRequiredViewAsType(source, R.id.btnSaveAddress, "field 'btnSaveAddress'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddressActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.spnCountry = null;
    target.spnProvince = null;
    target.spnCity = null;
    target.spnCorresCountry = null;
    target.spnCorresProvince = null;
    target.spnCorresCity = null;
    target.edtIdAddress = null;
    target.edtIdPostCode = null;
    target.edtCorresAddress = null;
    target.edtCorresPostCode = null;
    target.checkBoxCorres = null;
    target.cardViewCorres = null;
    target.btnSaveAddress = null;
  }
}
