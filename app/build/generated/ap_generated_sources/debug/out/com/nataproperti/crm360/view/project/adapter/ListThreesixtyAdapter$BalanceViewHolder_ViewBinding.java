// Generated code from Butter Knife. Do not modify!
package com.nataproperti.crm360.view.project.adapter;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.nataproperti.crm360.nataproperty.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListThreesixtyAdapter$BalanceViewHolder_ViewBinding implements Unbinder {
  private ListThreesixtyAdapter.BalanceViewHolder target;

  @UiThread
  public ListThreesixtyAdapter$BalanceViewHolder_ViewBinding(
      ListThreesixtyAdapter.BalanceViewHolder target, View source) {
    this.target = target;

    target.name = Utils.findRequiredViewAsType(source, R.id.name, "field 'name'", TextView.class);
    target.item = Utils.findRequiredViewAsType(source, R.id.item, "field 'item'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListThreesixtyAdapter.BalanceViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.name = null;
    target.item = null;
  }
}
