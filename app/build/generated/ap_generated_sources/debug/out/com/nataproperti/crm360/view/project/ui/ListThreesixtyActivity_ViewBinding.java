// Generated code from Butter Knife. Do not modify!
package com.nataproperti.crm360.view.project.ui;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.nataproperti.crm360.nataproperty.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ListThreesixtyActivity_ViewBinding implements Unbinder {
  private ListThreesixtyActivity target;

  @UiThread
  public ListThreesixtyActivity_ViewBinding(ListThreesixtyActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ListThreesixtyActivity_ViewBinding(ListThreesixtyActivity target, View source) {
    this.target = target;

    target.list = Utils.findRequiredViewAsType(source, R.id.list, "field 'list'", RecyclerView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.imageView = Utils.findRequiredViewAsType(source, R.id.image_projek, "field 'imageView'", ImageView.class);
    target.rPage = Utils.findRequiredViewAsType(source, R.id.rPage, "field 'rPage'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ListThreesixtyActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.list = null;
    target.toolbar = null;
    target.imageView = null;
    target.rPage = null;
  }
}
