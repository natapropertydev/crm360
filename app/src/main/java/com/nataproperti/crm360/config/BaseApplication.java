package com.nataproperti.crm360.config;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.multidex.MultiDex;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nataproperti.crm360.nataproperty.BuildConfig;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Administrator on 3/31/2016.
 */
public class BaseApplication extends Application {
    private Tracker mTracker;
    private static final int TIMEOUT_MS = 40000;
    private ServiceRetrofit networkService;
    private RequestQueue requestQueue;
    private static BaseApplication instance;
    public static final String TAG = BaseApplication.class.getSimpleName();

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        networkService = new ServiceRetrofit();
        if (BuildConfig.DEBUG) {

        } else {
            Fabric.with(this, new Crashlytics());
        }
        instance = this;
    }

    public ServiceRetrofit getNetworkService() {
        return networkService;
    }

    public static synchronized BaseApplication getInstance() {
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request, String tag) {
        //Utils.TRACE("BaseApplication", "addToRequestQueue : " + request.getUrl());
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        // set retry policy
        request.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        getRequestQueue().add(request);
    }

    public static void showLog(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg);
        }
    }

    /*synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            //mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }*/


}
