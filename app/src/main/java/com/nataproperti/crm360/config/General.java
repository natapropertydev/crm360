package com.nataproperti.crm360.config;

/**
 * Created by User on 5/13/2016.
 */
public class General {

    public static final String DEVELOPER_KEY = "AIzaSyD3APAJoaSZTqBaH57P4OCfrh2rzxxUrsQ";

    public static final String BOOKING_SOURCE = "1";

    //    response error code, variabel ini ditentukan oleh developer.
//    dipanggil ketika error response network
    public static final int NETWROK_ERROR_JSON = 1;
    public static final int NETWORK_ERROR_CONNCECTION = 2;
    public static final int NETWORK_ERROR_PERMISSION_ACCESS = 3;

    //latlong untuk lokasi marketing di contact us
    public static final double LATITUDE_CODE = -6.1767295;
    public static final double LONGITUDE_CODE = 106.7577471;

    //untuk set variable di project menu
    public static final String PROJECT_INFORMASI = "projectInformasi";
    public static final String PROJECT_SEARCH_UNIT = "projectSearchUnit";
    public static final String PROJECT_DOWNLOAD = "projectDownload";
    public static final String PROJECT_NUP = "projectNup";
    public static final String PROJECT_BOOKING = "projectBooking";
    public static final String PROJECT_KOMISI = "projectKomisi";
    public static final String PROJECT_GALLERY = "projectGallery";
    public static final String PROJECT_NEWS = "projectNews";
    public static final String PROJECT_LOOK_BOOK = "projectLookBook";
    public static final String PROJECT_REPORT = "projectReport";
    public static final String PROJECT_THREESIXTY = "projectThreesixty";

    public static final String projectCode =  "23";

    //key value untuk menu
    public static final String MenuAllProject = "AllProject";
    public static final String MenuDownload = "Download";
    public static final String MenuMyListing = "MyProject";
    public static final String MenuNews = "News";
    public static final String MenuCommision = "Commision";
    public static final String MenuMyBooking = "MyBooking";
    public static final String MenuMyNup = "MyNup";
    public static final String MenuEvent = "Event";
    public static final String MenuCobroke = "Cobroke";
    public static final String MenuKalkulator = "Kalkulator";
    public static final String MenuTicket = "Ticet";

    public static final String PREF_NAME = "pref";
    public static final String MEMBER_REF = "memberRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";
    public static final String IMAGE_LOGO = "imageLogo";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String IS_NUP = "isNUP";
    public static final String NUP_AMT = "nupAmt";
    public static final String IS_BOOKING = "isBooking";
    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";
    public static final String TITLE_PRODUCT = "titleProduct";
    public static final String BATHROOM = "bathroom";
    public static final String BEDROOM = "bedroom";
    public static final String CLUSTER_DESCRIPTION = "clusterDescription";

    //set sharedpref
    public static final String IS_WAITING = "isWaiting";
    public static final String IS_JOIN = "isJoin";

    public static final String COUNT_CATEGORY = "countCategory";
    public static final String COUNT_CLUSTER = "countCluster";
    public static final String SALES_STATUS = "salesStatus";
    public static final String URL_VIDEO = "urlVideo";
    public static final String BOOKING_CONTACT = "bookingContact";
    public static final String DOWNLOAD_PROJECT_INFO = "downloadProjectInfo";
    public static final String LINK_DETAIL = "linkDetail";
    public static final String CLUSTER_NAME = "clusterName";
    public static final String PRICE_RANGE = "priceRange";
    public static final String  PROJECT_WA = "projectWA";
    public static final String  PROJECT_EMAIL = "projectEmail";
    public static final String  PROJECT_PHONE = "projectPhone";
}
