package com.nataproperti.crm360.config;

import com.nataproperti.crm360.view.listing.model.ChatHistoryDeleteResponeModel;
import com.nataproperti.crm360.view.listing.model.ChatHistoryModel;
import com.nataproperti.crm360.view.listing.model.UpdatePromote;
import com.nataproperti.crm360.view.listing.model.FindLocation;
import com.nataproperti.crm360.view.listing.model.GooglePlaceResult;
import com.nataproperti.crm360.view.profile.model.BaseData;
import com.nataproperti.crm360.view.project.model.ChatRoomModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by User on 10/27/2016.
 */
public interface NataApi {

    @FormUrlEncoded
    @POST("getListMyAgencyChatInsertSvc")
    Call<List<ChatRoomModel>> postMessaging(
            @Field("memberRefSender") String memberRefSender,
            @Field("memberRefReceiver") String memberRefReceiver,
            @Field("chatMessage") String chatMessage);

    @FormUrlEncoded
    @POST("getListMyAgencyChatHistorySvc")
    Call<List<ChatHistoryModel>> getHistoryMessage(
            @Field("memberRef") String memberRef);

    @FormUrlEncoded
    @POST("getListingMemberInfoKeywordSvc")
    Call<BaseData> getContactMessage(
            @Field("agencyCompanyRef") String agencyCompanyRef,
            @Field("keyword") String keyword,
            @Field("pageNo") String pageNo);

    @FormUrlEncoded
    @POST("veritransBookingSvc")
    Call<BaseData> postVeritransBooking(
            @Field("tokenid") String tokenid,
            @Field("dbMasterRef") String dbMasterRef,
            @Field("projectRef") String projectRef,
            @Field("memberRef") String memberRef,
            @Field("bookingRef") String bookingRef,
            @Field("termRef") String termRef);

    @FormUrlEncoded
    @POST("deleteMyAgencyChatHistorySvc")
    Call<ChatHistoryDeleteResponeModel> deleteMyAgencyChatHistory(
            @Field("chatRef") String chatRef);

    @FormUrlEncoded
    @POST("updatePromotePropertyListingSvc")
    Call<UpdatePromote> updatePromote(
            @Field("listingRef") String listingRef);

    @FormUrlEncoded
    @POST("insertPropertyListingFavoriteSvc")
    Call<UpdatePromote> updateFavorite(
            @Field("listingRef") String listingRef,@Field("memberRef") String memberRef);

    //Call google Service Autocomplete
    @GET("maps/api/place/autocomplete/json?")//key=AIzaSyD-DsgWyVAqNuynLLw5AXrqf3jyFg80JvQ&components=country:id&language=id&types=geocode"
    Call<GooglePlaceResult> findPlaces(@Query("key") String key, @Query("components") String components, @Query("language") String language,
                                       @Query("types") String types, @Query("input") String input);

    @GET("maps/api/place/details/json?")//key=AIzaSyD-DsgWyVAqNuynLLw5AXrqf3jyFg80JvQ&components=country:id&language=id&types=geocode"
    Call<FindLocation> getLocation(@Query("reference") String reference, @Query("sensor") String sensor, @Query("key") String key);


}
