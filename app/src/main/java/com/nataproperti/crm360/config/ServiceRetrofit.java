package com.nataproperti.crm360.config;


import android.util.LruCache;

import com.nataproperti.crm360.view.before_login.model.VersionModel;
import com.nataproperti.crm360.view.booking.model.BookingPaymentMethodModel;
import com.nataproperti.crm360.view.booking.model.DetailBooking;
import com.nataproperti.crm360.view.booking.model.MemberInfoModel;
import com.nataproperti.crm360.view.booking.model.PaymentModel;
import com.nataproperti.crm360.view.booking.model.SaveBookingModel;
import com.nataproperti.crm360.view.booking.model.VeritransBookingVTModel;
import com.nataproperti.crm360.view.commision.model.CommisionDetailModel;
import com.nataproperti.crm360.view.commision.model.CommisionModel;
import com.nataproperti.crm360.view.commision.model.CommisionScheduleModel;
import com.nataproperti.crm360.view.commision.model.CommisionSectionModel;
import com.nataproperti.crm360.view.event.model.EventDetailModel;
import com.nataproperti.crm360.view.event.model.EventModel;
import com.nataproperti.crm360.view.event.model.EventSchaduleModel;
import com.nataproperti.crm360.view.event.model.MyTicketDetailModel;
import com.nataproperti.crm360.view.event.model.MyTicketModel;
import com.nataproperti.crm360.view.event.model.ResponseRsvp;
import com.nataproperti.crm360.view.ilustration.model.BlockMappingModel;
import com.nataproperti.crm360.view.ilustration.model.DiagramColor;
import com.nataproperti.crm360.view.ilustration.model.IlustrationModel;
import com.nataproperti.crm360.view.ilustration.model.LIstBlockDiagramModel;
import com.nataproperti.crm360.view.ilustration.model.ListUnitMappingModel;
import com.nataproperti.crm360.view.ilustration.model.MapingModelNew;
import com.nataproperti.crm360.view.ilustration.model.StatusIlustrationAddKprModel;
import com.nataproperti.crm360.view.kpr.model.CalculationModel;
import com.nataproperti.crm360.view.kpr.model.PaymentCCStatusModel;
import com.nataproperti.crm360.view.kpr.model.PaymentKprStatusModel;
import com.nataproperti.crm360.view.kpr.model.ResponeCalcUserProject;
import com.nataproperti.crm360.view.kpr.model.TemplateModel;
import com.nataproperti.crm360.view.listing.model.ChatHistoryDeleteResponeModel;
import com.nataproperti.crm360.view.listing.model.ChatHistoryModel;
import com.nataproperti.crm360.view.listing.model.ChatSingleModelStatus;
import com.nataproperti.crm360.view.listing.model.ChatStatusModel;
import com.nataproperti.crm360.view.listing.model.IlustrationStatusModel;
import com.nataproperti.crm360.view.listing.model.InfoMapModel;
import com.nataproperti.crm360.view.listing.model.IsCobrokeModel;
import com.nataproperti.crm360.view.listing.model.ListCountChatModel;
import com.nataproperti.crm360.view.listing.model.ListingAgencyInfoModel;
import com.nataproperti.crm360.view.listing.model.ListingPropertyGalleryModel;
import com.nataproperti.crm360.view.listing.model.ListingPropertyStatus;
import com.nataproperti.crm360.view.listing.model.ListingPropertyStatusModel;
import com.nataproperti.crm360.view.listing.model.PropertyDescriptionModel;
import com.nataproperti.crm360.view.listing.model.ResponeListingAddPropertyUnitModel;
import com.nataproperti.crm360.view.listing.model.ResponeListingPropertyModel;
import com.nataproperti.crm360.view.listing.model.ResponeListingPropertyUnitModel;
import com.nataproperti.crm360.view.listing.model.ResponeLookupListingPropertyModel;
import com.nataproperti.crm360.view.listing.model.SaveMapsModel;
import com.nataproperti.crm360.view.listing.model.SendToFriendModel;
import com.nataproperti.crm360.view.menuitem.model.ContactUsModel;
import com.nataproperti.crm360.view.menuitem.model.NotificationDetailModel;
import com.nataproperti.crm360.view.menuitem.model.NotificationModel;
import com.nataproperti.crm360.view.mainmenu.model.StatusTokenGCMModel;
import com.nataproperti.crm360.view.mybooking.model.BookingDetailStatusModel;
import com.nataproperti.crm360.view.mybooking.model.BookingPaymentInfoModel;
import com.nataproperti.crm360.view.mybooking.model.MyBookingModel;
import com.nataproperti.crm360.view.mybooking.model.MyBookingSectionModel;
import com.nataproperti.crm360.view.mybooking.model.PaymentSchaduleModel;
import com.nataproperti.crm360.view.mynup.model.MyNupDetailProjectPsModel;
import com.nataproperti.crm360.view.mynup.model.MyNupProjectPsModel;
import com.nataproperti.crm360.view.mynup.model.MyNupSectionModel;
import com.nataproperti.crm360.view.nup.model.AccountBankModel;
import com.nataproperti.crm360.view.nup.model.BankModel;
import com.nataproperti.crm360.view.nup.model.MouthModel;
import com.nataproperti.crm360.view.nup.model.VeritransNupVTModel;
import com.nataproperti.crm360.view.nup.model.YearModel;
import com.nataproperti.crm360.view.profile.model.BaseData;
import com.nataproperti.crm360.view.profile.model.CityModel;
import com.nataproperti.crm360.view.profile.model.OthersEditModel;
import com.nataproperti.crm360.view.profile.model.ProvinceModel;
import com.nataproperti.crm360.view.profile.model.ResponeModel;
import com.nataproperti.crm360.view.profile.model.SubLocationModel;
import com.nataproperti.crm360.view.project.model.CategoryModel;
import com.nataproperti.crm360.view.project.model.ChatRoomModel;
import com.nataproperti.crm360.view.project.model.CommisionStatusModel;
import com.nataproperti.crm360.view.project.model.DetailStatusModel;
import com.nataproperti.crm360.view.project.model.ListThreesixtyModel;
import com.nataproperti.crm360.view.project.model.LocationModel;
import com.nataproperti.crm360.view.project.model.ProjectModelNew;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by nata on 11/22/2016.
 */

public class ServiceRetrofit {
    private NetworkAPI networkAPI;
    private OkHttpClient okHttpClient;
    private LruCache<Class<?>, Observable<?>> apiObservables;

    public ServiceRetrofit() {
        this(WebService.URL);
    }

    public ServiceRetrofit(String baseUrl) {
        okHttpClient = buildClient();
        apiObservables = new LruCache<>(10);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        networkAPI = retrofit.create(NetworkAPI.class);
    }

    /**
     * Method to return the API interface.
     *
     * @return
     */
    public NetworkAPI getAPI() {
        return networkAPI;
    }


    /**
     * Method to build and return an OkHttpClient so we can set/get
     * headers quickly and efficiently.
     *
     * @return
     */
    public OkHttpClient buildClient() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.readTimeout(60, TimeUnit.SECONDS);
        builder.connectTimeout(60, TimeUnit.SECONDS);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());
                // Do anything with response here
                //if we ant to grab a specific cookie or something..
                return response;
            }
        });

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                //this is where we will add whatever we want to our request headers.
                Request request = chain.request().newBuilder().addHeader("Accept", "application/json").build();
                return chain.proceed(request);
            }
        });

        return builder.build();
    }

    /**
     * Method to clear the entire cache of observables
     */
    public void clearCache() {
        apiObservables.evictAll();
    }


    /**
     * Method to either return a cached observable or prepare a new one.
     *
     * @param unPreparedObservable
     * @param clazz
     * @param cacheObservable
     * @param useCache
     * @return Observable ready to be subscribed to
     */
    public Observable<?> getPreparedObservable(Observable<?> unPreparedObservable, Class<?> clazz, boolean cacheObservable, boolean useCache) {

        Observable<?> preparedObservable = null;

        if (useCache)//this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = apiObservables.get(clazz);

        if (preparedObservable != null)
            return preparedObservable;


        //we are here because we have never created this observable before or we didn't want to use the cache...

        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (cacheObservable) {
            preparedObservable = preparedObservable.cache();
            apiObservables.put(clazz, preparedObservable);
        }


        return preparedObservable;
    }


    /**
     * all the Service alls to use for the retrofit requests.
     */
    public interface NetworkAPI {

        @FormUrlEncoded
        @POST("saveGCMTokenSvc")
        Call<StatusTokenGCMModel> postGCMToken(@Field("GCMToken") String GCMToken, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListingInfoSvc")
        Call<ListingAgencyInfoModel> getAgencyInfo(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("registerAgencyByCode")
        Call<ListingAgencyInfoModel> postRegisterAgency(@Field("memberRef") String memberRef, @Field("keyword") String keyword);

        @FormUrlEncoded
        @POST("getListMonthSvc")
        Call<List<MouthModel>> getMonthList(@Field("urlPostParameter") String urlPostParameter);

        @FormUrlEncoded
        @POST("getListYearSvc")
        Call<List<YearModel>> getYearsList(@Field("urlPostParameter") String urlPostParameter);

        @FormUrlEncoded
        @POST("getIlustrationPaymentSvc")
        Call<PaymentCCStatusModel> getPaymentCC(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef,
                                                @Field("productRef") String productRef, @Field("unitRef") String unitRef, @Field("termRef") String termRef,
                                                @Field("termNo") String termNo);

        @FormUrlEncoded
        @POST("getBookingDetailSvc")
        Call<BookingDetailStatusModel> getBookingDetail(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("bookingRef") String bookingRef);

        @FormUrlEncoded
        @POST("getAppVersionUASvc")
        Call<VersionModel> getNewVersion(@Field("version") String version);

        @FormUrlEncoded
        @POST("getDiagramColorSvc")
        Call<DiagramColor> getDiagramColour(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getDiagramColorNewSvc")
        Call<DiagramColor> getDiagramColour(@Field("memberRef") String memberRef,@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("getListUnitMapingSvc")
        Call<List<MapingModelNew>> getUnitMapping(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("categoryRef") String categoryRef,
                                                  @Field("clusterRef") String clusterRef, @Field("blockName") String blockName);

        @FormUrlEncoded
        @POST("getProjectDetailSvc")
        Call<DetailStatusModel> getProjectDetail(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("getProjectCommisionSvc")
        Call<CommisionStatusModel> getCommision(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetLocationLookupSvc")
        Call<List<LocationModel>> getLocation(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListProjectSvc")
        Call<List<ProjectModelNew>> getListProject(@Field("memberRef") String memberRef, @Field("locationRef") String locationRef);

        @FormUrlEncoded
        @POST("getListUnitSvc")
        Call<List<ListUnitMappingModel>> getListUnitDiagram(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("categoryRef") String categoryRef, @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("getListBlockSvc")
        Call<List<LIstBlockDiagramModel>> getListBlockDiagram(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("categoryRef") String categoryRef, @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("cekMyAgencyChatHistorySvc")
        Call<ListCountChatModel> getCountChat(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListMyAgencyChatHistorySvc")
        Call<List<ChatHistoryModel>> getHistoryMessage(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListingMemberInfoKeywordSvc")
        Call<BaseData> getContactMessage(@Field("agencyCompanyRef") String agencyCompanyRef, @Field("keyword") String keyword, @Field("pageNo") String pageNo);

        @FormUrlEncoded
        @POST("getListingMemberInfoAllSvc")
        Call<BaseData> getContactMessageAll(@Field("agencyCompanyRef") String agencyCompanyRef);

        @FormUrlEncoded
        @POST("getListingMemberInfoSvc")
        Call<BaseData> getContactMessagePage(@Field("agencyCompanyRef") String agencyCompanyRef, @Field("pageNo") String pageNo);

        @FormUrlEncoded
        @POST("getListMyAgencyChatInsertSvc")
        Call<List<ChatRoomModel>> getMessaging(@Field("memberRefSender") String memberRefSender, @Field("memberRefReceiver") String memberRefReceiver, @Field("chatMessage") String chatMessage);

        @FormUrlEncoded
        @POST("getListMyAgencyChatInsertPageSvc")
        Call<ChatStatusModel> postMessaging(@Field("memberRefSender") String memberRefSender, @Field("memberRefReceiver") String memberRefReceiver, @Field("chatMessage") String chatMessage, @Field("pageNo") int pageNo);


        @FormUrlEncoded
        @POST("getClusterInfoSvc")
        Call<IlustrationStatusModel> getClusterInfo(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("getListPaymentIlustrationSvc")
        Call<List<IlustrationModel>> getIlustrationPayment(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef, @Field("keyword") String keyword);

        @FormUrlEncoded
        @POST("getIlustrationPaymentSvc")
        Call<StatusIlustrationAddKprModel> getIlustrationAddKpr(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef
                , @Field("productRef") String productRef, @Field("unitRef") String unitRef, @Field("termRef") String termRef
                , @Field("termNo") String termNo);

        @FormUrlEncoded
        @POST("getListCategorySvc")
        Call<List<CategoryModel>> getIlusCategory(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("getIlustrationPaymentKPRSvc")
        Call<PaymentCCStatusModel> getIlusPaymentKpr(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef
                , @Field("productRef") String productRef, @Field("unitRef") String unitRef, @Field("termRef") String termRef
                , @Field("termNo") String termNo, @Field("KPRYear") String KPRYear);

        @FormUrlEncoded
        @POST("getIlustrationPaymentTermSvc")
        Call<PaymentKprStatusModel> getIlusPaymentTerm(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("clusterRef") String clusterRef
                , @Field("productRef") String productRef, @Field("unitRef") String unitRef);

        @FormUrlEncoded
        @POST("getListBlockSvc")
        Call<List<BlockMappingModel>> getListBlok(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("categoryRef") String categoryRef
                , @Field("clusterRef") String clusterRef);

        @FormUrlEncoded
        @POST("getListCalcUserProject")
        Call<List<CalculationModel>> getListCal(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListBookingSvc")
        Call<List<MyBookingModel>> getListBook(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetSectionMyBooking")
        Call<List<MyBookingSectionModel>> getListBookingSection(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListBookingProjectSvc")
        Call<List<MyBookingModel>> getListBookingProjectPs(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef, @Field("projectPsRef") String projectPsRef);

        @FormUrlEncoded
        @POST("getListTemplateProjectSvc")
        Call<List<TemplateModel>> getListTemplate(@Field("project") String project, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListingPropertyKeywordSvc")
        Call<ListingPropertyStatus> getListingPropertyKey(@Field("agencyCompanyRef") String agencyCompanyRef, @Field("psRef") String psRef,
                                                          @Field("listingStatus") String listingStatus, @Field("pageNo") String pageNo,
                                                          @Field("locationRef") String locationRef, @Field("keyword") String keyword);

        @FormUrlEncoded
        @POST("getProvinceListSvc")
        Call<List<ProvinceModel>> getListProvinsi(@Field("countryCode") String countryCode);

        @FormUrlEncoded
        @POST("getCityListSvc")
        Call<List<CityModel>> getListCity(@Field("countryCode") String countryCode, @Field("provinceCode") String provinceCode);

        @FormUrlEncoded
        @POST("getSubLocationListSvc")
        Call<List<SubLocationModel>> getSubLoc(@Field("countryCode") String countryCode, @Field("provinceCode") String provinceCode, @Field("cityCode") String cityCode);

        @FormUrlEncoded
        @POST("getProvinceListListingSvc")
        Call<List<ProvinceModel>> getListProvinsiListListing(@Field("countryCode") String countryCode);

        @FormUrlEncoded
        @POST("getCityListListingSvc")
        Call<List<CityModel>> getListCityListListing(@Field("countryCode") String countryCode, @Field("provinceCode") String provinceCode);

        @FormUrlEncoded
        @POST("getSubLocationListListingSvc")
        Call<List<SubLocationModel>> getSubLocListListing(@Field("countryCode") String countryCode, @Field("provinceCode") String provinceCode, @Field("cityCode") String cityCode);


        @FormUrlEncoded
        @POST("getListingAddPropertySvc")
        Call<ListingPropertyStatusModel> getListProperty(@Field("countryCode") String countryCode);

        @FormUrlEncoded
        @POST("getListingPropertyFilterSvc")
        Call<ListingPropertyStatus> getListingSearch(@Field("agencyCompanyRef") String agencyCompanyRef, @Field("listingStatus") String listingStatus, @Field("pageNo") String pageNo
                , @Field("provinceCode") String provinceCode, @Field("cityCode") String cityCode, @Field("locationRef") String locationRef, @Field("listingTypeRef") String listingTypeRef
                , @Field("categoryType") String categoryType, @Field("memberRef") String memberRef);

        @GET("getListingAddPropertyUnitSvc")
        Call<ResponeListingAddPropertyUnitModel> getListingPropertyUnit();

        @GET("getLookupListingPropertySvc")
        Call<ResponeLookupListingPropertyModel> getLookupListingProperty();

        //        @Headers( "Content-Type: application/json" )
        @FormUrlEncoded
        @POST("insertPropertyListingUnitSvc")
        Call<ResponeListingPropertyModel> saveInformasiUnit(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("getListingPropertyInfoUnitSvc")
        Call<ResponeListingPropertyUnitModel> getListingPropertyUnitInfo(@Field("listingRef") String listingRef);

        @FormUrlEncoded
        @POST("GetLocationLookupListingSvc")
        Call<List<LocationModel>> getLocationListing(@Field("agencyCompanyRef") String agencyCompanyRef);

        @GET("GetLocationLookupIsCobrokeSvc")
        Call<List<LocationModel>> getLocationListingCobroke();

        @FormUrlEncoded
        @POST("getListingPropertyMemberSvc")
        Call<ListingPropertyStatus> getListingMember(@Field("agencyCompanyRef") String agencyCompanyRef, @Field("memberRef") String memberRef
                , @Field("pageNo") String pageNo, @Field("locationRef") String locationRef);

        @FormUrlEncoded
        @POST("getListingPropertyFavoriteSvc")
        Call<ListingPropertyStatus> getListingBookmark(@Field("agencyCompanyRef") String agencyCompanyRef, @Field("listingStatus") String listingStatus
                , @Field("pageNo") String pageNo, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListingPropertyAllSvc")
        Call<ListingPropertyStatus> getListingAllCoBroke(@Field("agencyCompanyRef") String agencyCompanyRef, @Field("listingStatus") String listingStatus, @Field("pageNo") String pageNo
                , @Field("provinceCode") String provinceCode, @Field("cityCode") String cityCode, @Field("locationRef") String locationRef, @Field("listingTypeRef") String listingTypeRef
                , @Field("categoryType") String categoryType, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("updateIsCobroke")
        Call<IsCobrokeModel> updateIsCobroke(@Field("listingRef") String listingRef, @Field("memberRef") String memberRef, @Field("isCobroke") String isCobroke);

        @FormUrlEncoded
        @POST("getListingPropertyInfoSvc")
        Call<PropertyDescriptionModel> getPropertyDetail(@Field("listingRef") String listingRef, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("sendToFriendListingPropertySvc")
        Call<SendToFriendModel> sendToFriend(@Field("email") String email, @Field("memberRef") String memberRef, @Field("listingRef") String listingRef);

        @FormUrlEncoded
        @POST("sendNotifToAgencyMemberSvc")
        Call<SendToFriendModel> sendNotif(@Field("agencyCompanyRef") String agencyCompanyRef, @Field("listingRef") String listingRef, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListListingPropertyGallerySvc")
        Call<List<ListingPropertyGalleryModel>> getGallery(@Field("listingRef") String listingRef);

        @FormUrlEncoded
        @POST("insertPropertyListingMapsSvc")
        Call<SaveMapsModel> postMaps(@Field("listingRef") String listingRef, @Field("latitude") String latitude, @Field("longitude") String longitude);

        @FormUrlEncoded
        @POST("getListingPropertyInfoMapsSvc")
        Call<InfoMapModel> getMaps(@Field("listingRef") String listingRef);

        @FormUrlEncoded
        @POST("getListMyAgencyChatSingleSvc")
        Call<ChatSingleModelStatus> getMessagingSingle(@Field("memberRefSender") String memberRefSender, @Field("memberRefReceiver") String memberRefReceiver);

        //commision
        @FormUrlEncoded
        @POST("GetSectionCommission")
        Call<List<CommisionSectionModel>> getListCommisionSection(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetListCommissionProjectSvc")
        Call<List<CommisionModel>> GetListCommissionProjectSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef")
                String projectRef, @Field("projectPsRef") String projectPsRef, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetCommissionProjectDetailSvc")
        Call<List<CommisionDetailModel>> GetListCommissionDetailProjectSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef")
                String projectRef, @Field("bookingRef") String bookingRef, @Field("projectPsRef") String projectPsRef);

        @FormUrlEncoded
        @POST("GetListCommissionCustProjectSvc")
        Call<List<CommisionScheduleModel>> GetListCommissionCustProjectSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef")
                String projectRef, @Field("bookingRef") String bookingRef, @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetSectionMyNUPProject")
        Call<List<MyNupSectionModel>> getListNupSection(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetListNUPProjectSVC")
        Call<List<MyNupProjectPsModel>> getListNupProjectPs(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                            @Field("projectPsRef") String projectPsRef, @Field("memberRef") String memberRef,
                                                            @Field("schemeRef") String projectSchemeRef);

        @FormUrlEncoded
        @POST("GetNUPInfoProject")
        Call<MyNupDetailProjectPsModel> GetNUPInfoProject(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                          @Field("projectPsRef") String NUPRef, @Field("NUPRef") String projectPsRef,
                                                          @Field("memberRef") String memberRef, @Field("schemeRef") String projectSchemeRef);

        @FormUrlEncoded
        @POST("getListEventSvc")
        Call<ArrayList<EventModel>> getListEvent(@Field("contentDate") String contentDate,
                                                 @Field("memberRef") String memberRef);
//        @GET("/maps/api/place/autocomplete/json?key=AIzaSyDTkSXoyn7JcS7BTSTY_tSUnV4YCzBFdZw&components=country:id&language=id&types=geocode")
//        public void findPlaces(@Query("input") String input, Callback<GooglePlaceResult> callback);

        @FormUrlEncoded
        @POST("getNewsImageSliderSvc")
        Call<ArrayList<EventDetailModel>> getNewsImageSliderSvc(@Field("contentRef") String contentRef);

        @FormUrlEncoded
        @POST("getListEventScheduleSvc")
        Call<ArrayList<EventSchaduleModel>> getListEventScheduleSvc(@Field("contentRef") String contentRef);

        @FormUrlEncoded
        @POST("getListTiketRsvp")
        Call<ArrayList<MyTicketModel>> getListTiketRsvp(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getListTiketQRCode")
        Call<ArrayList<MyTicketDetailModel>> getListTiketQRCode(@Field("memberRef") String memberRef,
                                                                @Field("eventScheduleRef") String eventScheduleRef);

        @FormUrlEncoded
        @POST("deleteMyAgencyChatHistorySvc")
        Call<ChatHistoryDeleteResponeModel> deleteMyAgencyChatHistory(@Field("chatRef") String chatRef);

        @FormUrlEncoded
        @POST("getListNotificationSSTSvc")
        Call<ArrayList<NotificationModel>> getListNotificationSSTSvc(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getGCMMessageSvc")
        Call<NotificationDetailModel> getGCMMessageSvc(@Field("GCMMessageRef") String GCMMessageRef,
                                                       @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("GetPaymentMethodSvc")
        Call<List<BookingPaymentMethodModel>> GetPaymentMethodSvc(@Field("dbMasterRef") String dbMasterRef,
                                                                  @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("veritransBookingVTSvc")
        Call<VeritransBookingVTModel> veritransBookingVTSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                            @Field("memberRef") String memberRef, @Field("bookingRef") String bookingRef,
                                                            @Field("termRef") String termRef, @Field("paymentType") String paymentType);

        @FormUrlEncoded
        @POST("getIlustrationPaymentSvc")
        Call<PaymentModel> getIlustrationPaymentSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                    @Field("clusterRef") String clusterRef, @Field("productRef") String productRef,
                                                    @Field("unitRef") String unitRef, @Field("termRef") String termRef,
                                                    @Field("termNo") String termNo);

        @FormUrlEncoded
        @POST("getBookingDetailSvc")
        Call<DetailBooking> getBookingDetailSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                @Field("bookingRef") String clusterRef);

        @FormUrlEncoded
        @POST("getListPaymentScheduleSvc")
        Call<ArrayList<PaymentSchaduleModel>> getListPaymentScheduleSvc(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef,
                                                                        @Field("bookingRef") String clusterRef);

        @FormUrlEncoded
        @POST("getBookingPaymentInfoSvc")
        Call<BookingPaymentInfoModel> getBookingPaymentInfoSvc(@Field("bookingRef") String bookingRef);

        @FormUrlEncoded
        @POST("getMemberInfoSvc")
        Call<MemberInfoModel> getMemberInfoSvc(@Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("getProjectAccountBank")
        Call<ArrayList<AccountBankModel>> getProjectAccountBank(@Field("dbMasterRef") String dbMasterRef, @Field("projectRef") String projectRef);

        @GET("getListBankSvc")
        Call<ArrayList<BankModel>> getListBankSvc();

        @FormUrlEncoded
        @POST("savePaymentBookingBankTransferSvc")
        Call<SaveBookingModel> savePaymentBookingBankTransferSvc(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("updatePaymentTypeOrderSvc")
        Call<VeritransNupVTModel> updatePaymentTypeOrderSvc(@Field("nupOrderRef") String nupOrderRef,
                                                            @Field("paymentType") String paymentType,
                                                            @Field("memberRef") String memberRef);

        @FormUrlEncoded
        @POST("updatePaymentTypeOrderVTSvc")
        Call<VeritransNupVTModel> updatePaymentTypeOrderVTSvc(@Field("nupOrderRef") String nupOrderRef,
                                                              @Field("paymentType") String paymentType,
                                                              @Field("dbMasterRef") String dbMasterRef,
                                                              @Field("projectRef") String projectRef,
                                                              @Field("memberRef") String memberRef,
                                                              @Field("nupAmt") String nupAmt,
                                                              @Field("total") String total);

        @FormUrlEncoded
        @POST("savePaymentBankTransferSvc")
        Call<SaveBookingModel> savePaymentBankTransferSvc(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("updateAccountBankSvc")
        Call<ResponeModel> updateAccountBankSvc(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("saveFeedbackSvc")
        Call<ContactUsModel> saveFeedbackUser(@Field("contactName") String contactName,
                                              @Field("contactEmail") String contactEmail,
                                              @Field("contactSubject") String contactSubject,
                                              @Field("contactMessage") String contactMessage);

        @GET("getOtherProfileSvc")
        Call<OthersEditModel> getOtherProfileSvc();

        @FormUrlEncoded
        @POST("updateOtherProfileSvc")
        Call<ResponeModel> updateOtherProfileSvc(@FieldMap Map<String, String> fields);


        @FormUrlEncoded
        @POST("saveNewCalcUserProject")
        Call<ResponeCalcUserProject> saveNewCalcUserProject(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("saveRsvpSvc")
        Call<ResponseRsvp> saveRsvpSvc(@FieldMap Map<String, String> fields);

        @FormUrlEncoded
        @POST("getListCategorySvc")
        Call<List<CategoryModel>> getListCategorySvc(@Field("dbMasterRef") String dbMasterRef,
                                              @Field("projectRef") String projectRef);

        @FormUrlEncoded
        @POST("getListProductThreesixtySvc")
        Call<List<ListThreesixtyModel>> getListProductThreesixtySvc(@Field("dbMasterRef") String dbMasterRef,
                                                                    @Field("projectRef") String projectRef);

    }

}
