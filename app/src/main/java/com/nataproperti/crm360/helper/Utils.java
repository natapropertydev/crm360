package com.nataproperti.crm360.helper;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

/**
 * Created by Nata
 * on Mar 3/21/2017 16:09.
 * Project : wika
 */
public class Utils {
    public static int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
