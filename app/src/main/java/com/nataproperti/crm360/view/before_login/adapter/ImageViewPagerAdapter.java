package com.nataproperti.crm360.view.before_login.adapter;


import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.nataproperti.crm360.view.before_login.ui.ImageFiveFragment;
import com.nataproperti.crm360.view.before_login.ui.ImageFourFragment;
import com.nataproperti.crm360.view.before_login.ui.ImageOneFragment;
import com.nataproperti.crm360.view.before_login.ui.ImageSevenFragment;
import com.nataproperti.crm360.view.before_login.ui.ImageSixFragment;
import com.nataproperti.crm360.view.before_login.ui.ImageThreeFragment;
import com.nataproperti.crm360.view.before_login.ui.ImageTwoFragment;


public class ImageViewPagerAdapter extends FragmentPagerAdapter {
    private Context _context;
    public static int totalPage = 7;

    public ImageViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        _context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();
        switch (position) {
            case 0:
                f = new ImageOneFragment();
                break;
            case 1:
                f = new ImageTwoFragment();
                break;
            case 2:
                f = new ImageThreeFragment();
                break;
            case 3:
                f = new ImageFourFragment();
                break;
            case 4:
                f = new ImageFiveFragment();
                break;
            case 5:
                f = new ImageSixFragment();
                break;
            case 6:
                f = new ImageSevenFragment();
                break;
        }
        return f;
    }

    @Override
    public int getCount() {
        return totalPage;
    }

}

