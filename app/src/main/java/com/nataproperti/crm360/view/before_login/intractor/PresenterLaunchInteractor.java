package com.nataproperti.crm360.view.before_login.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterLaunchInteractor {
    void getVersionApp(String version);
    void rxUnSubscribe();

}
