package com.nataproperti.crm360.view.before_login.model;

/**
 * Created by nata on 11/24/2016.
 */

public class VersionModel {
    int status;
    String message;
    String versionApp;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }
}
