package com.nataproperti.crm360.view.before_login.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.view.before_login.model.VersionModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.before_login.presenter.LaunchPresenter;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;
import com.nataproperti.crm360.view.project.ui.ProjectActivity;

/**
 * Created by User on 4/13/2016.
 */
public class LaunchActivity extends AppCompatActivity {
    public static final String TAG = "LaunchActivity";
    public static final String PREF_NAME = "pref";
    boolean ifNotExists = false;
    private static int splashInterval = 1;
    SharedPreferences sharedPreferences;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    boolean state;
    String versionName = "";
    int versionCode = -1;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private LaunchPresenter presenter;
    ProgressDialog progressDialog;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetector(getApplicationContext());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new LaunchPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        getVersionInfo();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        requestVersion();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    private void requestVersion() {
        presenter.getVersionApp("");

    }

    private void getVersionInfo() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Log.d(TAG, versionName + " " + versionCode);
    }

    public void showVersionAppResults(retrofit2.Response<VersionModel> response) {
        if (response.isSuccessful()){
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            //String message = "55";
            String versionApp = response.body().getVersionApp();
            if (status == 200) {
                if (versionCode < Integer.parseInt(message)) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LaunchActivity.this);
                    alertDialogBuilder
                            .setMessage("Update Available v" + versionApp)
                            .setCancelable(false)
                            .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(WebService.linkAppStore)));
                                }
                            })
                        .setNegativeButton("Exit",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                finish();
                            }
                        });

                    alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    /*if (state) {
                        Intent intent = new Intent(LaunchActivity.this, MainMenuActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(LaunchActivity.this, ViewpagerActivity.class);
                        startActivity(intent);
                        finish();
                    }*/

                    Intent intent = new Intent(LaunchActivity.this, ProjectActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        } else {

        }
    }

    public void showVersionAppFailure(Throwable t) {
        /*if (state) {
            Intent intent = new Intent(LaunchActivity.this, MainMenuActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(LaunchActivity.this, ViewpagerActivity.class);
            startActivity(intent);
            finish();
        }*/

        Intent intent = new Intent(LaunchActivity.this, ProjectActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }
}
