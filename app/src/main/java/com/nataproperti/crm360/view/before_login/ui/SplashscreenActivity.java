package com.nataproperti.crm360.view.before_login.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.nataproperti.crm360.nataproperty.R;

/**
 * Created by User on 4/20/2016. ( Last Update.. )
 */
public class SplashscreenActivity extends Activity {
    private static int splashInterval = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen_activity);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(SplashscreenActivity.this,
                        LaunchActivity
                                .class);
                startActivity(i);
                //jeda selesai Splashscreen
                this.finish();
            }

            private void finish() {

            }
        }, splashInterval);

    }
}
