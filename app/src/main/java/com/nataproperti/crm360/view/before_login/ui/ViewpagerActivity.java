package com.nataproperti.crm360.view.before_login.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.loginregister.ui.LoginActivity;
import com.nataproperti.crm360.view.loginregister.ui.RegisterStepOneActivity;

/**
 * Created by User on 4/27/2016.
 */
public class ViewpagerActivity extends AppCompatActivity {
    private Button btn_login;
    private Button btn_register;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);

        btn_login = (Button) findViewById(R.id.btnLogin);
        btn_register = (Button) findViewById(R.id.btnRegister);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");

        btn_login.setTypeface(font);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewpagerActivity.this, LoginActivity.class));
                finish();
            }
        });

        btn_register.setTypeface(font);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewpagerActivity.this, RegisterStepOneActivity.class));
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
