package com.nataproperti.crm360.view.before_login.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.before_login.adapter.ImageViewPagerAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class ViewpagerFragment extends Fragment {
    private ViewPager _mViewPager;
    private ImageViewPagerAdapter _adapter;
    private ImageView _btn1, _btn2, _btn3, _btn4, _btn5, _btn6, _btn7, logo;
    private TextView txtSmartPropery;

    public ViewpagerFragment() {
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpView();
        setTab();
        onCircleButtonClick();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    private void onCircleButtonClick() {

        _btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btn1.setImageResource(R.drawable.fill_circle);
                _mViewPager.setCurrentItem(0);
                txtSmartPropery.setVisibility(View.GONE);
                logo.setVisibility(View.GONE);
            }
        });

        _btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btn2.setImageResource(R.drawable.fill_circle);
                _mViewPager.setCurrentItem(1);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
            }
        });
        _btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btn3.setImageResource(R.drawable.fill_circle);
                _mViewPager.setCurrentItem(2);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
            }
        });
        _btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btn4.setImageResource(R.drawable.fill_circle);
                _mViewPager.setCurrentItem(3);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
            }
        });
        _btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btn5.setImageResource(R.drawable.fill_circle);
                _mViewPager.setCurrentItem(4);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
            }
        });
        _btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btn6.setImageResource(R.drawable.fill_circle);
                _mViewPager.setCurrentItem(5);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
            }
        });
        _btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btn7.setImageResource(R.drawable.fill_circle);
                _mViewPager.setCurrentItem(6);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
            }
        });

    }

    private void setUpView() {
        logo = (ImageView) getView().findViewById(R.id.logo);
        _mViewPager = (ViewPager) getView().findViewById(R.id.imageviewPager);
        _adapter = new ImageViewPagerAdapter(getActivity(), getFragmentManager());
        _mViewPager.setAdapter(_adapter);
        _mViewPager.setCurrentItem(0);
        initButton();
    }

    private void setTab() {
        _mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int position) {
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                _btn1.setImageResource(R.drawable.holo_circle);
                _btn2.setImageResource(R.drawable.holo_circle);
                _btn3.setImageResource(R.drawable.holo_circle);
                _btn4.setImageResource(R.drawable.holo_circle);
                _btn5.setImageResource(R.drawable.holo_circle);
                _btn6.setImageResource(R.drawable.holo_circle);
                _btn7.setImageResource(R.drawable.holo_circle);
                btnAction(position);
            }

        });

    }

    private void btnAction(int action) {
        switch (action) {
            case 0:
                _btn1.setImageResource(R.drawable.fill_circle);
                txtSmartPropery.setVisibility(View.GONE);
                logo.setVisibility(View.GONE);
                break;

            case 1:
                _btn2.setImageResource(R.drawable.fill_circle);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
                break;
            case 2:
                _btn3.setImageResource(R.drawable.fill_circle);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
                break;

            case 3:
                _btn4.setImageResource(R.drawable.fill_circle);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
                break;

            case 4:
                _btn5.setImageResource(R.drawable.fill_circle);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
                break;

            case 5:
                _btn6.setImageResource(R.drawable.fill_circle);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
                break;

            case 6:
                _btn7.setImageResource(R.drawable.fill_circle);
                txtSmartPropery.setVisibility(View.VISIBLE);
                logo.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void initButton() {
        _btn1 = (ImageView) getView().findViewById(R.id.btn1);
        _btn1.setImageResource(R.drawable.fill_circle);
        _btn2 = (ImageView) getView().findViewById(R.id.btn2);
        _btn3 = (ImageView) getView().findViewById(R.id.btn3);
        _btn4 = (ImageView) getView().findViewById(R.id.btn4);
        _btn5 = (ImageView) getView().findViewById(R.id.btn5);
        _btn6 = (ImageView) getView().findViewById(R.id.btn6);
        _btn7 = (ImageView) getView().findViewById(R.id.btn7);
        txtSmartPropery = (TextView) getView().findViewById(R.id.txt_smart_property);

    }
}
