package com.nataproperti.crm360.view.booking.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterBookingInformasiCustomerInteractor {

    void getMemberInfoSvc(String memberRef);

    void rxUnSubscribe();

}
