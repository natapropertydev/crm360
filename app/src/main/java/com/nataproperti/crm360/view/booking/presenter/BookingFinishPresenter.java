package com.nataproperti.crm360.view.booking.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.booking.intractor.PresenterBookingFinishInteractor;
import com.nataproperti.crm360.view.booking.model.MemberInfoModel;
import com.nataproperti.crm360.view.booking.model.PaymentModel;
import com.nataproperti.crm360.view.booking.ui.BookingFinishActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class BookingFinishPresenter implements PresenterBookingFinishInteractor {
    private BookingFinishActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public BookingFinishPresenter(BookingFinishActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getIlustrationPaymentSvc(String dbMasterRef, String projectRef, String clusterRef, String productRef, String unitRef, String termRef, String termNo) {
        Call<PaymentModel> call = service.getAPI().getIlustrationPaymentSvc(dbMasterRef,projectRef, clusterRef,  productRef,  unitRef,  termRef,  termNo);
        call.enqueue(new Callback<PaymentModel>() {
            @Override
            public void onResponse(Call<PaymentModel> call, Response<PaymentModel> response) {
                view.showPaymentModelResults(response);
            }

            @Override
            public void onFailure(Call<PaymentModel> call, Throwable t) {
                view.showPaymentModelFailure(t);

            }


        });
    }

    @Override
    public void getMemberInfoSvc(String memberRef) {
        Call<MemberInfoModel> call = service.getAPI().getMemberInfoSvc(memberRef);
        call.enqueue(new Callback<MemberInfoModel>() {
            @Override
            public void onResponse(Call<MemberInfoModel> call, Response<MemberInfoModel> response) {
                view.showMemberInfoResults(response);
            }

            @Override
            public void onFailure(Call<MemberInfoModel> call, Throwable t) {
                view.showMemberInfoFailure(t);

            }


        });
    }


    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }


}
