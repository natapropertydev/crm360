package com.nataproperti.crm360.view.booking.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.helper.LoadingBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 5/18/2016.
 */
public class BookingCreateCustomerActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref" ;

    public static final String PROJECT_DESCRIPTION = "projectDescription";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String PROJECT_NAME = "projectName";

    ProgressDialog progressDialog;

    EditText editFullname,editBirthdate,editEmail,editMobil,editPassword,editRetypePassword;
    Button btnRegister;

    String memberCustomerRef;
    String dbMasterRef,projectRef,clusterRef,productRef,unitRef,termRef,termNo,projectName;
    private String projectDescription,nupAmt;
    String message;
    int status;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_create_customer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Register");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        final Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);
        projectDescription = intent.getStringExtra(PROJECT_DESCRIPTION);

        editFullname = (EditText) findViewById(R.id.txt_name);
        editBirthdate = (EditText) findViewById(R.id.edit_birthdate);
        editEmail = (EditText) findViewById(R.id.edit_email);
        editMobil = (EditText) findViewById(R.id.txt_mobile);
        editPassword = (EditText) findViewById(R.id.txt_password);
        editRetypePassword = (EditText) findViewById(R.id.txt_retype_password);

        editBirthdate.setInputType(InputType.TYPE_NULL);
        editBirthdate.setTextIsSelectable(true);
        editBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(),"Date Picker");
            }
        });
        editBirthdate.setFocusable(false);

        btnRegister = (Button) findViewById(R.id.btn_register);
        btnRegister.setTypeface(font);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validate()) {
                    onRegisterFailed();
                    return;
                }

                btnRegister.setEnabled(false);

                String fullname = editFullname.getText().toString().trim();
                String birthdate = editBirthdate.getText().toString().trim();
                String email = editEmail.getText().toString().trim();
                String mobile = editMobil.getText().toString().trim();
                String password = editPassword.getText().toString().trim();
                String retypePassword = editRetypePassword.getText().toString().trim();

                if ((!fullname.isEmpty() && !birthdate.isEmpty() && !email.isEmpty() && !password.isEmpty()
                        && !mobile.isEmpty() && !retypePassword.isEmpty() && password.equals(retypePassword)))
                    registerCustomer();

                else {
                    Toast.makeText(getApplicationContext(), "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }

            }
        });
    }

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }
        public void onDateSet(DatePicker view, int year, int month, int day) {

            EditText editText = (EditText) getActivity().findViewById(R.id.edit_birthdate);

            editText.setText(day+"/"+(month+1)+"/"+year);
        }
    }

    public boolean validate() {
        boolean valid = true;

        String fullname = editFullname.getText().toString().trim();
        String birthdate = editBirthdate.getText().toString().trim();
        String email = editEmail.getText().toString().trim();
        String mobile = editMobil.getText().toString().trim();
        String password = editPassword.getText().toString().trim();
        String retypePassword = editRetypePassword.getText().toString().trim();

        if (fullname.isEmpty()) {
            editFullname.setError("enter a fullname");
            valid = false;
        } else {
            editFullname.setError(null);
        }

        if (birthdate.isEmpty()) {
            editBirthdate.setError("enter a birtdate");
            valid = false;
        } else {
            editBirthdate.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editEmail.setError("enter a valid email address");
            valid = false;
        } else {
            editEmail.setError(null);
        }

        if (mobile.isEmpty()) {
            editMobil.setError("enter a mobile phone");
            valid = false;
        } else {
            editMobil.setError(null);
        }

        if (password.isEmpty()) {
            editPassword.setError("enter a password");
            valid = false;
        } else {
            editPassword.setError(null);
        }

        if (retypePassword.isEmpty()) {
            editRetypePassword.setError("enter a retype password");
            valid = false;
        } else if (!password.equals(retypePassword)){
            editRetypePassword.setError("password don't match");
            valid = false;
        }
        else {
            editRetypePassword.setError(null);
        }

        return valid;
    }

    public void onRegisterFailed() {
        Toast.makeText(getBaseContext(), "Register failed", Toast.LENGTH_LONG).show();

        btnRegister.setEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        btnRegister.setEnabled(true);
    }

    public void registerCustomer() {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getCostumerRegister(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result register", response);
                    status = jo.getInt("status");
                    message = jo.getString("message");

                    if (status==200) {
                        memberCustomerRef = jo.getString("customerMemberRef");
                        Intent intent = new Intent(BookingCreateCustomerActivity.this,BookingInformasiCustomerActivity.class);
                        intent.putExtra(DBMASTER_REF,dbMasterRef);
                        intent.putExtra(PROJECT_REF,projectRef);
                        intent.putExtra(PROJECT_NAME,projectName);
                        intent.putExtra(CLUSTER_REF,clusterRef);
                        intent.putExtra(PRODUCT_REF,productRef);
                        intent.putExtra(UNIT_REF,unitRef);
                        intent.putExtra(TERM_REF,termRef);
                        intent.putExtra(TERM_NO,termNo);

                        SharedPreferences sharedPreferences = BookingCreateCustomerActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("isMemberCostumerRef", memberCustomerRef);
                        editor.commit();
                        intent.putExtra("status","1");
                        startActivity(intent);
                        finish();
                    } else if (status == 202) {
                        btnRegister.setEnabled(true);
                        editEmail.setError(message);

                    } else {
                        String errorMsg = jo.getString("massage");
                        Log.d("status", "" + errorMsg);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(BookingCreateCustomerActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(BookingCreateCustomerActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customerName", editFullname.getText().toString());
                params.put("birthDate", editBirthdate.getText().toString());
                params.put("email", editEmail.getText().toString());
                params.put("mobile", editMobil.getText().toString());
                params.put("password", editPassword.getText().toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "creatcustomer");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
