package com.nataproperti.crm360.view.booking.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.booking.model.MemberInfoModel;
import com.nataproperti.crm360.view.booking.model.PaymentModel;
import com.nataproperti.crm360.view.booking.presenter.BookingFinishPresenter;
import com.nataproperti.crm360.view.mybooking.ui.MyBookingActivity;
import com.nataproperti.crm360.view.projectmenu.ui.ProjectMenuActivity;

import retrofit2.Response;

/**
 * Created by User on 5/21/2016.
 */
public class BookingFinishActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String TERM_REF = "termRef";
    public static final String TERM_NO = "termNo";
    public static final String BOOKING_REF = "bookingRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String STATUS_PAYMENT = "statusPayment";
    private static final String EXTRA_RX = "EXTRA_RX";
    BookingFinishPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;
    SharedPreferences sharedPreferences;
    int typePayment;

    //logo
    ImageView imgLogo;

    ImageView imgProject;
    TextView txtProjectName, txtCostumerName, txtCostumerMobile, txtCostumerPhone, txtCostumerAddress, txtCostumerEmail, txtMemberEmail;
    TextView txtPropertyName, txtCategoryType, txtProduct, txtUnitNo, txtArea, txtEstimate;
    TextView txtPaymentTerms, txtPriceInc, txtDisconutPersen, txtNetPrice, txtdiscount, txtTotal;

    String memberCostumerRef, costumerName, costumerMobile, costumerPhone, costumerEmail, costumerAddress, projectName;
    String email;
    String bookingRef, projectRef, dbMasterRef, clusterRef, productRef, unitRef, termRef, termNo;
    String propertys, category, product, unit, area, priceInc;
    String paymentTerm, priceIncVat, discPercent, discAmt, netPrice, termCondition;
    String statusPayment;

    Toolbar toolbar;
    TextView title;
    Typeface font;
    RelativeLayout rPage;
    Display display;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_finish);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new BookingFinishPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        email = sharedPreferences.getString("isEmail", null);
        memberCostumerRef = sharedPreferences.getString("isMemberCostumerRef", null);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRef = intent.getStringExtra(PRODUCT_REF);
        unitRef = intent.getStringExtra(UNIT_REF);
        termRef = intent.getStringExtra(TERM_REF);
        termNo = intent.getStringExtra(TERM_NO);
        projectName = intent.getStringExtra(PROJECT_NAME);
        bookingRef = intent.getStringExtra(BOOKING_REF);
        statusPayment = intent.getStringExtra(STATUS_PAYMENT);
        typePayment = intent.getIntExtra("typePayment", 0);
        initWidget();

        Log.d("payment", "" + statusPayment);
        Log.d("cek memberCostumer", "" + memberCostumerRef);

        requestPayment();
        requestMemberInfo();
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Booking Berhasil");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //imgProject = (ImageView) findViewById(R.id.image_project);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        /**
         * Property info
         */
        txtPropertyName = (TextView) findViewById(R.id.txt_propertyName);
        txtCategoryType = (TextView) findViewById(R.id.txt_categoryType);
        txtProduct = (TextView) findViewById(R.id.txt_product);
        txtUnitNo = (TextView) findViewById(R.id.txt_unitNo);
        txtArea = (TextView) findViewById(R.id.txt_area);
        txtEstimate = (TextView) findViewById(R.id.txt_estimate);

        /**
         * Price info
         */
        txtPaymentTerms = (TextView) findViewById(R.id.txt_paymentTerms);
        txtPriceInc = (TextView) findViewById(R.id.txt_priceIncVat);
        txtDisconutPersen = (TextView) findViewById(R.id.txt_disconutPersen);
        txtdiscount = (TextView) findViewById(R.id.txt_discount);
        txtNetPrice = (TextView) findViewById(R.id.txt_netPrice);

        /**
         * costumer info
         */
        txtCostumerName = (TextView) findViewById(R.id.txt_customer_name);
        txtCostumerMobile = (TextView) findViewById(R.id.txt_costumer_mobile);
        txtCostumerPhone = (TextView) findViewById(R.id.txt_costumer_phone);
        txtCostumerEmail = (TextView) findViewById(R.id.txt_costumer_email);
        txtCostumerAddress = (TextView) findViewById(R.id.txt_costumer_address);

        txtMemberEmail = (TextView) findViewById(R.id.member_email);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

    }

    private void requestPayment() {
        presenter.getIlustrationPaymentSvc(dbMasterRef, projectRef, clusterRef, productRef, unitRef, termRef, termNo);
    }

    public void showPaymentModelResults(Response<PaymentModel> response) {
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        if (status == 200) {
            paymentTerm = response.body().getDataPrice().getPaymentTerm();
            priceIncVat = response.body().getDataPrice().getPriceInc();
            discPercent = response.body().getDataPrice().getDiscPercent();
            discAmt = response.body().getDataPrice().getDiscAmt();
            netPrice = response.body().getDataPrice().getNetPrice();
            termCondition = response.body().getDataPrice().getTermCondition();

            propertys = response.body().getPropertyInfo().getPropertys();
            category = response.body().getPropertyInfo().getCategory();
            product = response.body().getPropertyInfo().getProduct();
            unit = response.body().getPropertyInfo().getUnit();
            area = response.body().getPropertyInfo().getArea();
            priceInc = response.body().getPropertyInfo().getPriceInc();

            txtPropertyName.setText(propertys);
            txtCategoryType.setText(category);
            txtProduct.setText(product);
            txtUnitNo.setText(unit);
            txtArea.setText(Html.fromHtml(area));
            txtEstimate.setText(priceInc);

            txtPaymentTerms.setText(paymentTerm);
            txtPriceInc.setText(priceIncVat);
            txtDisconutPersen.setText(discPercent);
            txtdiscount.setText(discAmt);
            txtNetPrice.setText(netPrice);

        } else {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    public void showPaymentModelFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void requestMemberInfo() {
        presenter.getMemberInfoSvc(memberCostumerRef);
    }

    public void showMemberInfoResults(Response<MemberInfoModel> response) {
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        if (status == 200) {
            costumerName = response.body().getData().getName();
            costumerMobile = response.body().getData().gethP1();
            costumerPhone = response.body().getData().getPhone1();
            costumerEmail = response.body().getData().getEmail1();
            costumerAddress = response.body().getData().getIdAddr();

            txtCostumerName.setText(costumerName);
            txtCostumerMobile.setText(costumerMobile);
            txtCostumerPhone.setText(costumerPhone);
            txtCostumerEmail.setText(costumerEmail);
            txtCostumerAddress.setText(costumerAddress);

            //dari bank tranfer
            if (statusPayment.equals(WebService.updatePaymentTypeBankTransfer)) {
                String thankYou = txtMemberEmail.getText().toString();
                txtMemberEmail.setText(thankYou + " " + costumerEmail + " dan " + email);
            } else {
                String thankYou = txtMemberEmail.getText().toString();
                txtMemberEmail.setText(thankYou + " " + getResources().getString(R.string.booking_bank_tranfer));
            }

            SharedPreferences sharedPreferences = BookingFinishActivity.this.
                    getSharedPreferences(PREF_NAME, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("isMemberCostumerRef", "");
            editor.commit();

        } else {
            Log.d("error", " " + message);

        }

    }

    public void showMemberInfoFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_top_right:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (typePayment == 0) {
            Intent intent = new Intent(BookingFinishActivity.this, ProjectMenuActivity.class);
            intent.putExtra(PROJECT_REF, projectRef);
            intent.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
            intent.putExtra(PROJECT_NAME, projectName);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(BookingFinishActivity.this, MyBookingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

}
