package com.nataproperti.crm360.view.commision.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterCommisionInteractor {
    void GetListCommissionProjectSvc(String dbMasterRef, String projectRef, String projectPsRef, String memberRef);
    void rxUnSubscribe();

}
