package com.nataproperti.crm360.view.commision.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterCommisionSectionInteractor {
    void getListCommisionSection(String memberRef);
    void rxUnSubscribe();

}
