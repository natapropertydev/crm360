package com.nataproperti.crm360.view.commision.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.commision.intractor.PresenterCommisionDetailInteractor;
import com.nataproperti.crm360.view.commision.model.CommisionDetailModel;
import com.nataproperti.crm360.view.commision.ui.CommisionDetailActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CommisionDetailPresenter implements PresenterCommisionDetailInteractor {
    private CommisionDetailActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public CommisionDetailPresenter(CommisionDetailActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetListCommissionDetailProjectSvc(String dbMasterRef, String projectRef, String bookingRef, String projectPsRef) {
        Call<List<CommisionDetailModel>> call = service.getAPI().GetListCommissionDetailProjectSvc(dbMasterRef,projectRef,bookingRef,projectPsRef);
        call.enqueue(new Callback<List<CommisionDetailModel>>() {
            @Override
            public void onResponse(Call<List<CommisionDetailModel>> call, Response<List<CommisionDetailModel>> response) {
                view.showListCommisionDetailResults(response);
            }

            @Override
            public void onFailure(Call<List<CommisionDetailModel>> call, Throwable t) {
                view.showListCommisionnDetailFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
