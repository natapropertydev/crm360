package com.nataproperti.crm360.view.commision.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.commision.intractor.PresenterCommisionInteractor;
import com.nataproperti.crm360.view.commision.model.CommisionModel;
import com.nataproperti.crm360.view.commision.ui.CommisionActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CommisionPresenter implements PresenterCommisionInteractor {
    private CommisionActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public CommisionPresenter(CommisionActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetListCommissionProjectSvc(String dbMasterRef, String projectRef, String projectPsRef, String memberRef) {
        Call<List<CommisionModel>> call = service.getAPI().GetListCommissionProjectSvc(dbMasterRef,projectRef,projectPsRef,memberRef);
        call.enqueue(new Callback<List<CommisionModel>>() {
            @Override
            public void onResponse(Call<List<CommisionModel>> call, Response<List<CommisionModel>> response) {
                view.showListCommisionResults(response);
            }

            @Override
            public void onFailure(Call<List<CommisionModel>> call, Throwable t) {
                view.showListCommisionnFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
