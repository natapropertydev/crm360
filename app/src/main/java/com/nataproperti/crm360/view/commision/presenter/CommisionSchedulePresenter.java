package com.nataproperti.crm360.view.commision.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.commision.intractor.PresenterCommisionScheduleInteractor;
import com.nataproperti.crm360.view.commision.model.CommisionScheduleModel;
import com.nataproperti.crm360.view.commision.ui.CommisionScheduleActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CommisionSchedulePresenter implements PresenterCommisionScheduleInteractor {
    private CommisionScheduleActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public CommisionSchedulePresenter(CommisionScheduleActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetListCommissionCustProjectSvc(String dbMasterRef, String projectRef, String bookingRef, String memberRef) {
        Call<List<CommisionScheduleModel>> call = service.getAPI().GetListCommissionCustProjectSvc(dbMasterRef,projectRef,bookingRef,memberRef);
        call.enqueue(new Callback<List<CommisionScheduleModel>>() {
            @Override
            public void onResponse(Call<List<CommisionScheduleModel>> call, Response<List<CommisionScheduleModel>> response) {
                view.showListCommisionScheduleResults(response);
            }

            @Override
            public void onFailure(Call<List<CommisionScheduleModel>> call, Throwable t) {
                view.showListCommisionnScheduleFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
