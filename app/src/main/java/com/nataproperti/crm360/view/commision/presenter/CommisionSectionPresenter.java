package com.nataproperti.crm360.view.commision.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.commision.intractor.PresenterCommisionSectionInteractor;
import com.nataproperti.crm360.view.commision.model.CommisionSectionModel;
import com.nataproperti.crm360.view.commision.ui.CommisionSectionActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CommisionSectionPresenter implements PresenterCommisionSectionInteractor {
    private CommisionSectionActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public CommisionSectionPresenter(CommisionSectionActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListCommisionSection(String memberRef) {
        Call<List<CommisionSectionModel>> call = service.getAPI().getListCommisionSection(memberRef);
        call.enqueue(new Callback<List<CommisionSectionModel>>() {
            @Override
            public void onResponse(Call<List<CommisionSectionModel>> call, Response<List<CommisionSectionModel>> response) {
                view.showListCommisionSectionResults(response);
            }

            @Override
            public void onFailure(Call<List<CommisionSectionModel>> call, Throwable t) {
                view.showListCommisionSectionFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
