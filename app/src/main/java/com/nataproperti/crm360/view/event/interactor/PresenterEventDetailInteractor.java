package com.nataproperti.crm360.view.event.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterEventDetailInteractor {
    void getNewsImageSliderSvc(String contentRef);
    void getListEventScheduleSvc(String contentRef);
    void rxUnSubscribe();

}
