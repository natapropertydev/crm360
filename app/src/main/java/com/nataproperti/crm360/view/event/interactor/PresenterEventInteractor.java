package com.nataproperti.crm360.view.event.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterEventInteractor {
    void getListEventSvc(String contentDate, String memberRef);
    void rxUnSubscribe();

}
