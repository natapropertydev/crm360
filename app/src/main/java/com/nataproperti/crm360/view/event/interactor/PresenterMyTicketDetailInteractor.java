package com.nataproperti.crm360.view.event.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyTicketDetailInteractor {
    void getListTiketQRCode(String memberRef, String eventScheduleRef);
    void rxUnSubscribe();

}
