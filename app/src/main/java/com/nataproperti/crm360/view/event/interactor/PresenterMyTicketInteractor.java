package com.nataproperti.crm360.view.event.interactor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyTicketInteractor {
    void getListTiketRsvp(String memberRef);
    void rxUnSubscribe();

}
