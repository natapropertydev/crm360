package com.nataproperti.crm360.view.event.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.event.interactor.PresenterEventDetailInteractor;
import com.nataproperti.crm360.view.event.model.EventDetailModel;
import com.nataproperti.crm360.view.event.model.EventSchaduleModel;
import com.nataproperti.crm360.view.event.ui.EventDetailActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class EventDetailPresenter implements PresenterEventDetailInteractor {
    private EventDetailActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public EventDetailPresenter(EventDetailActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getNewsImageSliderSvc(String contentRef) {
        Call<ArrayList<EventDetailModel>> call = service.getAPI().getNewsImageSliderSvc(contentRef);
        call.enqueue(new Callback<ArrayList<EventDetailModel>>() {
            @Override
            public void onResponse(Call<ArrayList<EventDetailModel>> call, Response<ArrayList<EventDetailModel>> response) {
                view.showListEventImageResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<EventDetailModel>> call, Throwable t) {
                view.showListEventImageFailure(t);

            }

        });
    }

    @Override
    public void getListEventScheduleSvc(String contentRef) {
        Call<ArrayList<EventSchaduleModel>> call = service.getAPI().getListEventScheduleSvc(contentRef);
        call.enqueue(new Callback<ArrayList<EventSchaduleModel>>() {
            @Override
            public void onResponse(Call<ArrayList<EventSchaduleModel>> call, Response<ArrayList<EventSchaduleModel>> response) {
                view.showListEventeScheduleResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<EventSchaduleModel>> call, Throwable t) {
                view.showListEventScheduleFailure(t);

            }

        });
    }


    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }

}
