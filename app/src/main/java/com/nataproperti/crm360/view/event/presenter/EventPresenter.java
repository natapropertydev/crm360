package com.nataproperti.crm360.view.event.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.event.interactor.PresenterEventInteractor;
import com.nataproperti.crm360.view.event.model.EventModel;
import com.nataproperti.crm360.view.event.ui.EventActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class EventPresenter implements PresenterEventInteractor {
    private EventActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public EventPresenter(EventActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListEventSvc(String contentDate, String memberRef) {
        Call<ArrayList<EventModel>> call = service.getAPI().getListEvent(contentDate,memberRef);
        call.enqueue(new Callback<ArrayList<EventModel>>() {
            @Override
            public void onResponse(Call<ArrayList<EventModel>> call, Response<ArrayList<EventModel>> response) {
                view.showListEventResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<EventModel>> call, Throwable t) {
                view.showListEventFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }

}
