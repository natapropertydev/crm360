package com.nataproperti.crm360.view.event.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.event.interactor.EventRsvpInteractor;
import com.nataproperti.crm360.view.event.model.ResponseRsvp;
import com.nataproperti.crm360.view.event.ui.EventRsvpActivity;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class EventRsvpPresenter implements EventRsvpInteractor {
    private EventRsvpActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public EventRsvpPresenter(EventRsvpActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void saveRsvpSvc(Map<String, String> fields) {
        Call<ResponseRsvp> call = service.getAPI().saveRsvpSvc(fields);
        call.enqueue(new Callback<ResponseRsvp>() {
            @Override
            public void onResponse(Call<ResponseRsvp> call, Response<ResponseRsvp> response) {
                view.showResponeResults(response);
            }

            @Override
            public void onFailure(Call<ResponseRsvp> call, Throwable t) {
                view.showResponeFailure(t);
            }
        });
    }


    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
