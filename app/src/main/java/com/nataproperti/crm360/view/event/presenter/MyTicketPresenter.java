package com.nataproperti.crm360.view.event.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.event.interactor.PresenterMyTicketInteractor;
import com.nataproperti.crm360.view.event.model.MyTicketModel;
import com.nataproperti.crm360.view.event.ui.MyTicketActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyTicketPresenter implements PresenterMyTicketInteractor {
    private MyTicketActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public MyTicketPresenter(MyTicketActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListTiketRsvp(String memberRef) {
        Call<ArrayList<MyTicketModel>> call = service.getAPI().getListTiketRsvp(memberRef);
        call.enqueue(new Callback<ArrayList<MyTicketModel>>() {
            @Override
            public void onResponse(Call<ArrayList<MyTicketModel>> call, Response<ArrayList<MyTicketModel>> response) {
                view.showListMyTicketResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<MyTicketModel>> call, Throwable t) {
                view.showListTicketFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }

}
