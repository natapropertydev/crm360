package com.nataproperti.crm360.view.gallery.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.gallery.model.ProjectGalleryModel;
import com.nataproperti.crm360.view.gallery.ui.DetailGalleryActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by User on 5/11/2016.
 */
public class ProjectDetailImageAdapter extends PagerAdapter {
    public static final String TAG = "ProjectDetailImage";

    Context context;
    private List<ProjectGalleryModel> list;

    PhotoViewAttacher attacher;

    public ProjectDetailImageAdapter(Context context, List<ProjectGalleryModel> list) {
        this.context = context;
        this.list = list;
    }

    String projectName, fileName, title;

    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.item_list_gallery_detail_image, container, false);
        final ImageView imageView = (ImageView) viewItem.findViewById(R.id.image_gallery_detail_image);
        ImageView download = (ImageView) viewItem.findViewById(R.id.btn_shared);
        TextView txtTitle = (TextView) viewItem.findViewById(R.id.name);

        ProjectGalleryModel projectGalleryModel = list.get(position);
        projectName = projectGalleryModel.getProjectName();
        fileName = projectGalleryModel.getFileName();
        title = projectGalleryModel.getTitle();

        txtTitle.setText(title);
        Glide.with(context).load(projectGalleryModel.getImgFile()).into(imageView);

        //zoom
        attacher = new PhotoViewAttacher(imageView);
        attacher.update();

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setDrawingCacheEnabled(true);
                Bitmap bitmap = imageView.getDrawingCache();

                String root = Environment.getExternalStorageDirectory().toString();
                File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/nataproperty");
                Log.d("directory", wallpaperDirectory.toString());
                wallpaperDirectory.mkdirs();

                if (checkWriteExternalPermission()) {
                    String fotoname = projectName + "-" + fileName;
                    File file = new File(wallpaperDirectory, fotoname);
                    if (file.exists()) file.delete();
                    try {
                        FileOutputStream out = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                        out.flush();
                        out.close();

                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("image/*");
                        String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                                "/nataproperty/" + projectName + "-" + fileName;
                        File imageFileToShare = new File(imagePath);
                        Uri uri = Uri.fromFile(imageFileToShare);
                        share.putExtra(Intent.EXTRA_STREAM, uri);
                        context.startActivity(Intent.createChooser(share, "Share Image!"));

                    } catch (Exception e) {

                    }
                } else {
                    ((DetailGalleryActivity) context).showDialogCekPermission();
                }

            }

        });

        Log.d(TAG, "" + projectGalleryModel.getImgFile());

        ((ViewPager) container).addView(viewItem);

        return viewItem;
    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public int getCount() {
        return list.size();
    }

    public boolean isViewFromObject(View view, Object object) {

        return view == ((View) object);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }


}
