package com.nataproperti.crm360.view.ilustration.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.kpr.model.DataSchedule;

import java.util.List;

/**
 * Created by User on 5/14/2016.
 */
public class IlustrationPaymentAdapter extends BaseAdapter {
    private Context context;
    private List<DataSchedule> list;
    private ListPaymentHolder holder;
    private Typeface font;

    public IlustrationPaymentAdapter(Context context, List<DataSchedule> list, Typeface font) {
        this.context = context;
        this.list = list;
        this.font = font;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_payment_schedule,null);
            holder = new ListPaymentHolder();
            holder.schedule = (TextView) convertView.findViewById(R.id.txt_schedule);
            holder.dueDate = (TextView) convertView.findViewById(R.id.txt_duedate);
            holder.amount = (TextView) convertView.findViewById(R.id.txt_amount);

            convertView.setTag(holder);
        }else{
            holder = (ListPaymentHolder) convertView.getTag();
        }

        DataSchedule payment = list.get(position);
        holder.schedule.setText(payment.getSchedule());
        holder.dueDate.setText(payment.getDueDate());
        holder.amount.setText(payment.getAmount());
        holder.schedule.setTypeface(font);
        holder.dueDate.setTypeface(font);
        holder.amount.setTypeface(font);
        return convertView;
    }

    private class ListPaymentHolder {
        TextView schedule,dueDate,amount;
    }
}
