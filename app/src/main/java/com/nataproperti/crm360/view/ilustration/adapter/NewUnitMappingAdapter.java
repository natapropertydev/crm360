package com.nataproperti.crm360.view.ilustration.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.view.ilustration.model.UnitMappingModel;
import com.nataproperti.crm360.helper.LoadingBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 5/15/2016.
 */
public class NewUnitMappingAdapter extends BaseAdapter {
    public static final String PREF_NAME = "pref";

    private Context context;
    private List<UnitMappingModel> list;
    private ListUnitMappingHolder holder;

    public NewUnitMappingAdapter(Context context, List<UnitMappingModel> list) {
        this.context = context;
        this.list = list;
    }

    String memberRef, color1, color2, memberType;
    SharedPreferences sharedPreferences;
    UnitMappingModel unit;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_unit_mapping, null);
            holder = new ListUnitMappingHolder();
            holder.unitName = (TextView) convertView.findViewById(R.id.txt_unit_name);
            holder.productName = (TextView) convertView.findViewById(R.id.txt_product_name);
            holder.linearItemUnit = (LinearLayout) convertView.findViewById(R.id.linear_item_unit);

            convertView.setTag(holder);
        } else {
            holder = (ListUnitMappingHolder) convertView.getTag();
        }

        unit = list.get(position);
        holder.unitName.setText(unit.getUnitName());
        holder.productName.setText(unit.getProductName());

        color1 = unit.getColor1();
        color2 = unit.getColor2();

        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberType = sharedPreferences.getString("isMemberType", null);

        //requestDiagramColor();
        if (!color1.equals("") && !color2.equals("")) {
            if (unit.getUnitStatus().equals("A")) {
                holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorPrimarySecond));
            } else {
                holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
            }

        } else {
            if (unit.getIsShowAvailableUnit().equals("1")) {
                if (unit.getUnitStatus().equals("A")) {
                    holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorPrimarySecond));
                } else {
                    holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
                }
            }
        }

        if (!unit.getProductNameInt().equals("")) {
            if (memberType.startsWith("Inhouse")) {
                if (unit.getUnitStatus().equals("A")) {
                    holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorPrimarySecond));
                } else {
                    holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
                }
            } else {
                if (unit.getProductRef().equals(unit.getProductNameInt())) {
                    holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorGray));
                }
            }
        }

        Log.d("Cek GetView unit", "" + unit.getUnitName());

        return convertView;
    }

    private class ListUnitMappingHolder {
        TextView unitName, productName;
        LinearLayout linearItemUnit;
    }

    public void requestDiagramColor() {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getDiagramColorSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //BaseApplication.getInstance().stopLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    color1 = jsonObject.getString("color1");
                    color2 = jsonObject.getString("color2");

                    if (!color1.equals("") && !color2.equals("")) {
                        if (unit.getUnitStatus().equals("A")) {
                            holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorPrimarySecond));
                        } else {
                            holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
                        }

                    } else {
                        if (!unit.getProductNameInt().equals("")) {
                            if (unit.getProductRef().equals(unit.getProductNameInt())) {
                                holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
                            }
                        }

                        if (unit.getIsShowAvailableUnit().equals("1")) {
                            if (unit.getUnitStatus().equals("A")) {
                                holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorPrimarySecond));
                            } else {
                                holder.linearItemUnit.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
                            }
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        Toast.makeText(context, "error connection", Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "color");

    }
}
