package com.nataproperti.crm360.view.ilustration.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterFloorMappingInteractor {
    void getListBlok(String dbMasterRef,String projectRef,String categoryRef,String clusterRef);
    void rxUnSubscribe();

}
