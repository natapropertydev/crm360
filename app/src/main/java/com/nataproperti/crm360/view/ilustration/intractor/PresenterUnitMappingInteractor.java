package com.nataproperti.crm360.view.ilustration.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterUnitMappingInteractor {
    void getDiagramColor(String memberRef, String dbMasterRef, String projectRef);

    void getUnitMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef, String blockName);

    void rxUnSubscribe();

}
