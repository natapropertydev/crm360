package com.nataproperti.crm360.view.ilustration.presenter;


import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.ilustration.intractor.DiagramMaticInflateInteractor;
import com.nataproperti.crm360.view.ilustration.model.DiagramColor;
import com.nataproperti.crm360.view.ilustration.model.ListUnitMappingModel;
import com.nataproperti.crm360.view.ilustration.ui.DiagramMaticInflateActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class DiagramMaticInflatePresenter implements DiagramMaticInflateInteractor {
    private DiagramMaticInflateActivity view;
    private ServiceRetrofit service;

    public DiagramMaticInflatePresenter(DiagramMaticInflateActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }




    @Override
    public void getDiagramColor(String memberRef,String dbMasterRef,String projectRef) {
        Call<DiagramColor> call = service.getAPI().getDiagramColour(memberRef,dbMasterRef,projectRef);
        call.enqueue(new Callback<DiagramColor>() {
            @Override
            public void onResponse(Call<DiagramColor> call, Response<DiagramColor> response) {
                view.showDiagramColorResults(response);
            }

            @Override
            public void onFailure(Call<DiagramColor> call, Throwable t) {
                view.showDiagramColorFailure(t);

            }


        });

    }

    @Override
    public void getUnitMapping(String dbMasterRef, String projectRef, String categoryRef, String clusterRef) {
        Call<List<ListUnitMappingModel>> call = service.getAPI().getListUnitDiagram(dbMasterRef,projectRef,categoryRef,clusterRef);
        call.enqueue(new Callback<List<ListUnitMappingModel>>() {
            @Override
            public void onResponse(Call<List<ListUnitMappingModel>> call, Response<List<ListUnitMappingModel>> response) {
                view.showUnitMappingResults(response);
            }

            @Override
            public void onFailure(Call<List<ListUnitMappingModel>> call, Throwable t) {
                view.showUnitMappingFailure(t);

            }


        });
    }
}
