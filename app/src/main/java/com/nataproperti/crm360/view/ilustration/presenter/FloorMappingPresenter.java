package com.nataproperti.crm360.view.ilustration.presenter;

import com.nataproperti.crm360.view.ilustration.intractor.PresenterFloorMappingInteractor;
import com.nataproperti.crm360.view.ilustration.model.BlockMappingModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.ilustration.ui.FloorMappingActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class FloorMappingPresenter implements PresenterFloorMappingInteractor {
    private FloorMappingActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public FloorMappingPresenter(FloorMappingActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }




    @Override
    public void getListBlok(String dbMasterRef, String projectRef, String categoryRef, String clusterRef) {
        Call<List<BlockMappingModel>> call = service.getAPI().getListBlok(dbMasterRef,projectRef,categoryRef,clusterRef);
        call.enqueue(new Callback<List<BlockMappingModel>>() {
            @Override
            public void onResponse(Call<List<BlockMappingModel>> call, Response<List<BlockMappingModel>> response) {
                view.showListBlokResults(response);
            }

            @Override
            public void onFailure(Call<List<BlockMappingModel>> call, Throwable t) {
                view.showListBlokFailure(t);

            }


        });

    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
