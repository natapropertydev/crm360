package com.nataproperti.crm360.view.ilustration.presenter;

import com.nataproperti.crm360.view.ilustration.intractor.PresenterIlustrationCategoryInteractor;
import com.nataproperti.crm360.view.project.model.CategoryModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.ilustration.ui.IlustrationCategoryActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class IlustrationCategoryPresenter implements PresenterIlustrationCategoryInteractor {
    private IlustrationCategoryActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public IlustrationCategoryPresenter(IlustrationCategoryActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListCategory(String dbMasterRef, String projectRef) {
        Call<List<CategoryModel>> call = service.getAPI().getIlusCategory(dbMasterRef,projectRef);
        call.enqueue(new Callback<List<CategoryModel>>() {
            @Override
            public void onResponse(Call<List<CategoryModel>> call, Response<List<CategoryModel>> response) {
                view.showListCategoryResults(response);
            }

            @Override
            public void onFailure(Call<List<CategoryModel>> call, Throwable t) {
                view.showListCategoryFailure(t);

            }


        });

    }

}
