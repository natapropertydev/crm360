package com.nataproperti.crm360.view.ilustration.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.ilustration.model.DiagramColor;
import com.nataproperti.crm360.view.ilustration.model.ListUnitMappingModel;
import com.nataproperti.crm360.view.ilustration.presenter.DiagramMaticInflatePresenter;
import com.nataproperti.crm360.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

public class DiagramMaticInflateActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String PRODUCT_REF = "productRef";
    public static final String TITLE_PRODUCT = "titleProduct";
    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private DiagramMaticInflatePresenter presenter;
    private List<ListUnitMappingModel> listUnitMappingModels = new ArrayList<>();
    ProgressDialog progressDialog;
    TextView txtProjectName;
    TextView txtNoIsShow, txtIsShow, txtFromProduct;
    RelativeLayout contentDiagram, contentDiagramNoDate;
    String dbMasterRef, projectRef, projectName, isShowAvailableUnit, productRefIntent, titleProduct, categoryRef, clusterRef;
    int lenght;
    SharedPreferences sharedPreferences;
    String memberRef, color1, color2;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagram_matic_inflate);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new DiagramMaticInflatePresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        contentDiagram.addView(new TableMainLayout(this));

        final Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        productRefIntent = intent.getStringExtra(PRODUCT_REF);
        titleProduct = intent.getStringExtra(TITLE_PRODUCT);
        isShowAvailableUnit = intent.getStringExtra(IS_SHOW_AVAILABLE_UNIT);
        title.setText(getResources().getString(R.string.title_diagarammatic) + " " + projectName);
        requestDiagramColor();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtIsShow = (TextView) findViewById(R.id.txt_is_show);
        txtNoIsShow = (TextView) findViewById(R.id.txt_no_is_show);
        txtFromProduct = (TextView) findViewById(R.id.txt_from_product);
        contentDiagram = (RelativeLayout) findViewById(R.id.contentRel);
        contentDiagramNoDate = (RelativeLayout) findViewById(R.id.contentNoDate);
    }

    public void requestDiagramColor() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getDiagramColor(memberRef, dbMasterRef, projectRef);
    }

    public void requestUnitMapping() {
        presenter.getUnitMapping(dbMasterRef, projectRef, categoryRef, clusterRef);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(DiagramMaticInflateActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, Long.parseLong(dbMasterRef));
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public void showDiagramColorResults(retrofit2.Response<DiagramColor> response) {
        progressDialog.dismiss();
        int status = response.body().getStatus();
        color1 = response.body().getColor1();
        color2 = response.body().getColor2();

        Log.d("Color","color1: "+color1+" color2: "+color2);
        if (status == 200) {
            requestUnitMapping();
        }
    }

    public void showDiagramColorFailure(Throwable t) {
        progressDialog.dismiss();

    }

    public void showUnitMappingResults(retrofit2.Response<List<ListUnitMappingModel>> response) {
        listUnitMappingModels = response.body();
        lenght = listUnitMappingModels.size();
        if (lenght != 0) {
            if (!color1.equals("") && !color2.equals("")) {
                txtIsShow.setVisibility(View.VISIBLE);
                txtNoIsShow.setVisibility(View.GONE);
                txtFromProduct.setVisibility(View.GONE);
            } else {
                if (!productRefIntent.equals("")) {
                    if (isShowAvailableUnit.equals("1")) {
                        txtIsShow.setVisibility(View.VISIBLE);
                        txtNoIsShow.setVisibility(View.GONE);
                        txtFromProduct.setVisibility(View.GONE);

                    } else {
                        txtIsShow.setVisibility(View.GONE);
                        txtNoIsShow.setVisibility(View.VISIBLE);
                        txtFromProduct.setVisibility(View.VISIBLE);
                    }

                } else {
                    if (isShowAvailableUnit.equals("1")) {
                        txtIsShow.setVisibility(View.VISIBLE);
                        txtNoIsShow.setVisibility(View.GONE);

                    } else {
                        txtIsShow.setVisibility(View.GONE);
                        txtNoIsShow.setVisibility(View.VISIBLE);

                    }
                }
            }
        } else {
            contentDiagramNoDate.setVisibility(View.VISIBLE);
            txtProjectName.setVisibility(View.GONE);
        }
    }

    public void showUnitMappingFailure(Throwable t) {

    }
}
