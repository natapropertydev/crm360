package com.nataproperti.crm360.view.ilustration.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyListView;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.ilustration.adapter.FloorMappingAdapter;
import com.nataproperti.crm360.view.ilustration.model.BlockMappingModel;
import com.nataproperti.crm360.view.ilustration.presenter.FloorMappingPresenter;

import java.util.ArrayList;
import java.util.List;

public class FloorMappingActivity extends AppCompatActivity {
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String BLOCK_NAME = "blockName";
    public static final String IS_BOOKING = "isBooking";
    public static final String PRODUCT_REF = "productRef";
    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private FloorMappingPresenter presenter;
    ProgressDialog progressDialog;

    private List<BlockMappingModel> listFloorMapping = new ArrayList<>();
    private FloorMappingAdapter adapter;

    ImageView imgLogo;
    TextView txtProjectName, txtFloor;

    String projectRef, dbMasterRef, categoryRef, clusterRef, projectName, blockName, isBooking, keywordFloor, productRef, isShowAvailableUnit;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    Display display;
    Point size;
    Integer width;
    Double result;
    RelativeLayout rPage;
    MyListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor_mapping);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new FloorMappingPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        isBooking = intent.getStringExtra(IS_BOOKING);

        productRef = intent.getStringExtra(PRODUCT_REF);
        isShowAvailableUnit = intent.getStringExtra(IS_SHOW_AVAILABLE_UNIT);

        initWidget();

        Log.d("cek Block", dbMasterRef + " " + projectRef + " " + categoryRef + " " + clusterRef);

        //logo

        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                blockName = listFloorMapping.get(position).getBlockName();
                Intent intentUnit = new Intent(FloorMappingActivity.this, UnitMappingActivity.class);
                intentUnit.putExtra(DBMASTER_REF, dbMasterRef);
                intentUnit.putExtra(PROJECT_REF, projectRef);
                intentUnit.putExtra(CATEGORY_REF, categoryRef);
                intentUnit.putExtra(CLUSTER_REF, clusterRef);
                intentUnit.putExtra(PROJECT_NAME, projectName);
                intentUnit.putExtra(BLOCK_NAME, blockName);
                intentUnit.putExtra(IS_BOOKING, isBooking);
                intentUnit.putExtra(PRODUCT_REF, productRef);
                intentUnit.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                startActivity(intentUnit);
            }
        });

        requestBlockMapping();

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_illustrastion_unit_mapping));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        txtFloor = (TextView) findViewById(R.id.txt_floor);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        listView = (MyListView) findViewById(R.id.list_floor);

    }

    @Override
    protected void onResume() {
        super.onResume();
        //listFloorMapping.clear();

    }

    public void requestBlockMapping() {

        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getListBlok(dbMasterRef, projectRef, categoryRef, clusterRef);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public void showListBlokResults(retrofit2.Response<List<BlockMappingModel>> response) {
        progressDialog.dismiss();
        if (response.isSuccessful())
            listFloorMapping = response.body();
        initAdapter();
    }

    private void initAdapter() {
        adapter = new FloorMappingAdapter(this, listFloorMapping);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    public void showListBlokFailure(Throwable t) {
        progressDialog.dismiss();

    }
}
