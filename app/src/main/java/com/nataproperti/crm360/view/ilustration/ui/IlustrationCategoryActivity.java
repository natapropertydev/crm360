package com.nataproperti.crm360.view.ilustration.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.General;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyGridView;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.before_login.ui.LaunchActivity;
import com.nataproperti.crm360.view.ilustration.presenter.IlustrationCategoryPresenter;
import com.nataproperti.crm360.view.project.adapter.CategoryAdapter;
import com.nataproperti.crm360.view.project.model.CategoryModel;
import com.nataproperti.crm360.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/3/2016.
 */
public class IlustrationCategoryActivity extends AppCompatActivity {
    private ServiceRetrofit service;
    private IlustrationCategoryPresenter presenter;
    ProgressDialog progressDialog;
    private List<CategoryModel> listCategory = new ArrayList<CategoryModel>();
    private CategoryAdapter adapter;
    ImageView imgLogo;
    Button btnNUP;
    TextView txtProjectName;
    long dbMasterRef, categoryRef;
    String projectRef, projectDescription, projectName, isNUP, nupAmt, isBooking ,countCluster, clusterRef,
            isShowAvailableUnit, imageLogo, isSales;
    Display display;
    MyGridView listView;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    RelativeLayout rPage;
    Point size;
    Integer width;
    Double result;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new IlustrationCategoryPresenter(this, service);
        dbMasterRef = getIntent().getLongExtra(General.DBMASTER_REF, 0);
        projectRef = getIntent().getStringExtra(General.PROJECT_REF);
        projectRef = getIntent().getStringExtra(General.PROJECT_REF);
        projectName = getIntent().getStringExtra(General.PROJECT_NAME);
        projectDescription = getIntent().getStringExtra(General.PROJECT_DESCRIPTION);
        isShowAvailableUnit = getIntent().getStringExtra(General.IS_SHOW_AVAILABLE_UNIT);
        isNUP = getIntent().getStringExtra(General.IS_NUP);
        isBooking = getIntent().getStringExtra(General.IS_BOOKING);
        nupAmt = getIntent().getStringExtra(General.NUP_AMT);
        imageLogo = getIntent().getStringExtra(General.IMAGE_LOGO);
        initWidget();

        btnNUP.setVisibility(View.GONE);
        Glide.with(this).load(WebService.getProjectImage() + dbMasterRef +
                "&pr=" + projectRef).into(imgLogo);

        Log.d("screen width", result.toString() + "--" + Math.round(result));

        adapter = new CategoryAdapter(this, listCategory, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                countCluster = listCategory.get(position).getCountCluster();
                categoryRef = listCategory.get(position).getCategoryRef();
                clusterRef = listCategory.get(position).getClusterRef();
                isShowAvailableUnit = listCategory.get(position).getIsShowAvailableUnit();
                isSales = listCategory.get(position).getIsSales();

                if (isSales.equals("1")){
                    if (countCluster.equals("1")) {
                        Intent intentProduct = new Intent(IlustrationCategoryActivity.this, IlustrationProductActivity.class);
                        intentProduct.putExtra(General.PROJECT_REF, projectRef);
                        intentProduct.putExtra(General.DBMASTER_REF, dbMasterRef);
                        intentProduct.putExtra(General.CLUSTER_REF, clusterRef);
                        intentProduct.putExtra(General.CATEGORY_REF, categoryRef);
                        intentProduct.putExtra(General.PROJECT_NAME, projectName);
                        intentProduct.putExtra(General.IS_BOOKING, isBooking);
                        intentProduct.putExtra(General.PRODUCT_REF, "");
                        intentProduct.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                        intentProduct.putExtra(General.IMAGE_LOGO, imageLogo);
                        startActivity(intentProduct);
                    } else {
                        Intent intentCluster = new Intent(IlustrationCategoryActivity.this, IlustrationClusterActivity.class);
                        intentCluster.putExtra(General.PROJECT_REF, projectRef);
                        intentCluster.putExtra(General.DBMASTER_REF, dbMasterRef);
                        intentCluster.putExtra(General.CATEGORY_REF, categoryRef);
                        intentCluster.putExtra(General.PROJECT_NAME, projectName);
                        intentCluster.putExtra(General.IS_BOOKING, isBooking);
                        intentCluster.putExtra(General.IMAGE_LOGO, imageLogo);
                        startActivity(intentCluster);
                    }
                } else {
                    Snackbar snackbar = Snackbar
                            .make(linearLayout, getString(R.string.comingsoon), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }

            }
        });

        requestCategory();
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getString(R.string.categorty));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        btnNUP = (Button) findViewById(R.id.btn_NUP);
        btnNUP.setTypeface(font);
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        txtProjectName.setText(projectName);
        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        listView = (MyGridView) findViewById(R.id.list_category);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
    }

    public void requestCategory() {
        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getListCategory(String.valueOf(dbMasterRef), projectRef);

    }

    private void initListView() {
        adapter = new CategoryAdapter(this, listCategory, display);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(IlustrationCategoryActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(General.PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(General.DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(General.PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public void showListCategoryResults(retrofit2.Response<List<CategoryModel>> response) {
        progressDialog.dismiss();
        listCategory=response.body();
        initListView();

    }

    public void showListCategoryFailure(Throwable t) {
        progressDialog.dismiss();
        Snackbar snackbar = Snackbar
                .make(linearLayout, getString(R.string.offline), Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(IlustrationCategoryActivity.this, LaunchActivity.class));
                        finish();
                    }
                });

        snackbar.setActionTextColor(Color.RED);
        snackbar.show();
    }
}
