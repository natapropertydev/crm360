package com.nataproperti.crm360.view.ilustration.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyListView;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.before_login.ui.LaunchActivity;
import com.nataproperti.crm360.view.ilustration.adapter.IlustrationProductAdapter;
import com.nataproperti.crm360.view.ilustration.model.IlustrationModel;
import com.nataproperti.crm360.view.ilustration.presenter.IlustrationProductPresenter;
import com.nataproperti.crm360.view.listing.model.IlustrationStatusModel;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;
import com.nataproperti.crm360.view.project.model.IlustrationProductModel;
import com.nataproperti.crm360.view.projectmenu.ui.ProjectMenuActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/4/2016.
 */
public class IlustrationProductActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String UNIT_REF = "unitRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String IS_BOOKING = "isBooking";
    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";
    public static final String TITLE_PRODUCT = "titleProduct";
    public static final String IMAGE_LOGO = "imageLogo";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    //    private List<ListCountChatModel> listCountChatModels = new ArrayList<>();
//    private List<ListCountChatModel> listCountChatModels2 = new ArrayList<>();
    private IlustrationProductPresenter presenter;
//    ProgressDialog progressDialog;

    private List<IlustrationModel> listIlustration = new ArrayList<IlustrationModel>();
    private IlustrationProductAdapter ilustrationAdapter;

    ProgressDialog progressDialog;
    AlertDialog b;

    ImageView imgLogo;
    Button btnBlock;
    TextView txtProjectName;

    long dbMasterRef;
    String projectRef, categoryRef, clusterRef, productRef, clusterDescription, projectName, productDescription,
            titleProduct, priceRangeStart, priceRangeEnd, isAllowCalculatePrice, isNUP;
    String newClusterRef;

    TextView clustername, productName;
    EditText edtSearch, edtSearchFloor;
    Button btnSearch, btnDiagramMatic, btnSearchFloor, btnSpecialEnquiries;
    MyListView listView;

    String isBooking;
    long unitRef;
    String isShowAvailableUnit, specialEnquiries;
    String isShowDiagramMaticUnit, imageLogo, isJoin, isWaiting;

    TextView txtChooseIlustration;
    ImageView imgSparator;
    private RelativeLayout snackBarBuatan;
    private TextView retry;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    Intent intent;

    LinearLayout linearNoData, linearLayoutDiagramMatic;
    Display display;
    Point size;
    Integer width;
    Double result;
    RelativeLayout rPage;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilustration_product);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new IlustrationProductPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        intent = getIntent();
        dbMasterRef = intent.getLongExtra(DBMASTER_REF, 0);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        productRef = intent.getStringExtra(PRODUCT_REF);
        titleProduct = intent.getStringExtra(TITLE_PRODUCT);
        isShowAvailableUnit = intent.getStringExtra(IS_SHOW_AVAILABLE_UNIT);
        imageLogo = intent.getStringExtra(IMAGE_LOGO);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        isWaiting = sharedPreferences.getString("isWaiting", "");
        isJoin = sharedPreferences.getString("isJoin", "");
        initWidget();

        Log.d("cek product ilustrasi", dbMasterRef + " " + projectRef + " " + categoryRef + " " + clusterRef + " " + isShowAvailableUnit);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);

        btnDiagramMatic.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnSearchFloor.setOnClickListener(this);
        retry.setOnClickListener(this);
        initAdapter();

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edtSearch.getText().toString().equals("")) {
                        edtSearch.setError("Keyword empty");
                    } else {
                        listIlustration.clear();
                        //ilustrationAdapter.notifyDataSetChanged();
                        Log.d("count", "" + String.valueOf(listIlustration.size()));
                        requestIlustration();

                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
                        return true;
                    }
                }
                return false;
            }
        });

        /**
         * list cari unit
         */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                productRef = listIlustration.get(position).getProductRef();
                unitRef = listIlustration.get(position).getUnitRef();

                Intent intent = new Intent(IlustrationProductActivity.this, IlustrationPaymentTermActivity.class);
                intent.putExtra(DBMASTER_REF, String.valueOf(dbMasterRef));
                intent.putExtra(PROJECT_REF, projectRef);
                intent.putExtra(CATEGORY_REF, categoryRef);
                intent.putExtra(CLUSTER_REF, clusterRef);
                intent.putExtra(PROJECT_NAME, projectName);
                intent.putExtra(PRODUCT_REF, productRef);
                intent.putExtra(UNIT_REF, String.valueOf(unitRef));
                intent.putExtra(IS_BOOKING, isBooking);
                startActivity(intent);

            }
        });

        requestClusterInfo();

    }

    private void initAdapter() {
        ilustrationAdapter = new IlustrationProductAdapter(this, listIlustration);
        listView.setAdapter(ilustrationAdapter);
        listView.setExpanded(true);
    }

    private void initWidget() {
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title.setText(getResources().getString(R.string.title_illustrastion));
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));
        rPage = (RelativeLayout) findViewById(R.id.rPage);
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        edtSearch = (EditText) findViewById(R.id.txt_keyword_search);
        btnSearch = (Button) findViewById(R.id.btn_search);
        btnSearchFloor = (Button) findViewById(R.id.btn_search_lantai);
        imgSparator = (ImageView) findViewById(R.id.imgSparator);
        linearLayoutDiagramMatic = (LinearLayout) findViewById(R.id.linear_diagrammatic);

        btnDiagramMatic = (Button) findViewById(R.id.btn_diagram_matic);
        btnDiagramMatic.setTypeface(font);
        linearNoData = (LinearLayout) findViewById(R.id.linear_no_data);
        listView = (MyListView) findViewById(R.id.list_ilustration);
        btnSearch.setTypeface(font);
        btnSearchFloor.setTypeface(font);
        txtChooseIlustration = (TextView) findViewById(R.id.txt_choose_ilustration);
    }

    public void requestClusterInfo() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getClusterInfo(String.valueOf(dbMasterRef), projectRef, clusterRef);
    }


    public void requestIlustration() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getIlustration(String.valueOf(dbMasterRef), projectRef, clusterRef, edtSearch.getText().toString());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_diagram_matic:
                if (isJoin.equals("0")) {
                    if (isWaiting.equals("1")) {
                        //sudah join tetapi belum di approv
                        dialogNotJoin(getString(R.string.waitingJoinBooking).replace("@projectName", projectName));
                    } else {
                        //belum join
                        dialogNotJoin(getString(R.string.notJoinBooking).replace("@projectName", projectName));
                    }

                } else {
                    //jika dia sudah join
                    Intent intent = new Intent(this, DiagramMaticInflateActivity.class);
                    intent.putExtra(DBMASTER_REF, String.valueOf(dbMasterRef));
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(CATEGORY_REF, categoryRef);
                    intent.putExtra(CLUSTER_REF, clusterRef);
                    intent.putExtra(PROJECT_NAME, projectName);
                    intent.putExtra(PRODUCT_REF, productRef);
                    intent.putExtra(PROJECT_NAME, projectName);
                    intent.putExtra(TITLE_PRODUCT, titleProduct);
                    intent.putExtra(IS_BOOKING, isBooking);
                    intent.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    startActivity(intent);
                }
                break;
            case R.id.btn_search:
                String keyword = edtSearch.getText().toString();
                if (isJoin.equals("0")) {
                    if (isWaiting.equals("1")) {
                        //sudah join tetapi belum di approv
                        dialogNotJoin(getString(R.string.waitingJoinBooking).replace("@projectName", projectName));
                    } else {
                        //belum join
                        dialogNotJoin(getString(R.string.notJoinBooking).replace("@projectName", projectName));
                    }

                } else {
                    //jika dia sudah join
                    if (!keyword.isEmpty()) {
                        listIlustration.clear();
                        //ilustrationAdapter.notifyDataSetChanged();
                        requestIlustration();
                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);

                    } else {
                        edtSearch.setError("Harus diisi.");
                    }
                }

                break;
            case R.id.btn_search_lantai:
                if (isJoin.equals("0")) {
                    if (isWaiting.equals("1")) {
                        //sudah join tetapi belum di approv
                        dialogNotJoin(getString(R.string.waitingJoinBooking).replace("@projectName", projectName));
                    } else {
                        //belum join
                        dialogNotJoin(getString(R.string.notJoinBooking).replace("@projectName", projectName));
                    }

                } else {
                    //jika dia sudah join
                    Intent intentFloor = new Intent(IlustrationProductActivity.this, FloorMappingActivity.class);
                    intentFloor.putExtra(DBMASTER_REF, String.valueOf(dbMasterRef));
                    intentFloor.putExtra(PROJECT_REF, projectRef);
                    intentFloor.putExtra(CATEGORY_REF, categoryRef);
                    intentFloor.putExtra(CLUSTER_REF, clusterRef);
                    intentFloor.putExtra(PROJECT_NAME, projectName);
                    intentFloor.putExtra(IS_BOOKING, isBooking);
                    intentFloor.putExtra(PRODUCT_REF, productRef);
                    intentFloor.putExtra(IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    startActivity(intentFloor);
                }

                break;
            case R.id.main_retry:
                startActivity(new Intent(IlustrationProductActivity.this, LaunchActivity.class));
                finish();
                break;

        }
    }

    private void dialogNotJoin(String notJoint) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(IlustrationProductActivity.this);
        LayoutInflater inflater = IlustrationProductActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
        dialogBuilder.setView(dialogView);

        final TextView textView = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
        dialogBuilder.setMessage("Notifikasi");
        textView.setText(notJoint);
        dialogBuilder.setPositiveButton("GO TO PROJECT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intentProjectMenu = new Intent(IlustrationProductActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                finish();
            }
        });
        dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        b = dialogBuilder.create();
        b.show();
    }


    public void showClusterInfoResults(retrofit2.Response<IlustrationStatusModel> response) {
        progressDialog.dismiss();
        MainMenuActivity.OFF_LINE_MODE = false;
        snackBarBuatan.setVisibility(View.GONE);
        //final DaoSession daoSession = ((BaseApplication)getApplicationContext()).getDaoSession();
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        isShowDiagramMaticUnit = response.body().getData().getIsShowDiagramMaticUnit();

        if (Integer.parseInt(isShowDiagramMaticUnit) == 0) {
            linearLayoutDiagramMatic.setVisibility(View.GONE);
        } else {
            linearLayoutDiagramMatic.setVisibility(View.VISIBLE);
        }
        if (status == 200) {
            IlustrationProductModel ilustrationProductModel = new IlustrationProductModel();
            ilustrationProductModel.setDbMasterRef(response.body().getData().getDbMasterRef());
            ilustrationProductModel.setProjectRef(response.body().getData().getProjectRef());
            ilustrationProductModel.setCategoryRef(response.body().getData().getCategoryRef());
            ilustrationProductModel.setClusterRef(response.body().getData().getClusterRef());
            ilustrationProductModel.setClusterDescription(response.body().getData().getClusterDescription());
            ilustrationProductModel.setPriceRangeStart(response.body().getData().getPriceRangeStart());
            ilustrationProductModel.setPriceRangeEnd(response.body().getData().getPriceRangeEnd());
            ilustrationProductModel.setIsAllowCalculatePrice(response.body().getData().getIsAllowCalculatePrice());
            ilustrationProductModel.setIsNUP(response.body().getData().getIsNUP());
            ilustrationProductModel.setIsBooking(response.body().getData().getIsBooking());
            ilustrationProductModel.setIsShowAvailableUnit(response.body().getData().getIsShowAvailableUnit());
            ilustrationProductModel.setIsShowDiagramMaticUnit(response.body().getData().getIsShowDiagramMaticUnit());

            dbMasterRef = response.body().getData().getDbMasterRef();
            projectRef = response.body().getData().getProjectRef();
            categoryRef = response.body().getData().getCategoryRef();
            clusterRef = response.body().getData().getClusterRef();
            clusterDescription = response.body().getData().getClusterDescription();
            priceRangeStart = response.body().getData().getPriceRangeStart();
            priceRangeEnd = response.body().getData().getPriceRangeEnd();
            isAllowCalculatePrice = response.body().getData().getIsAllowCalculatePrice();
            isNUP = response.body().getData().getIsNUP();
            isBooking = response.body().getData().getIsBooking();
//            IlustrationProductModelDao ilustrationProductModelDao = daoSession.getIlustrationProductModelDao();
//            ilustrationProductModelDao.insertOrReplace(ilustrationProductModel);
        } else {

            //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }
    }

    public void showClusterInfoFailure(Throwable t) {
        progressDialog.dismiss();
        MainMenuActivity.OFF_LINE_MODE = true;
        snackBarBuatan.setVisibility(View.VISIBLE);
//        final DaoSession daoSession = ((BaseApplication)getApplicationContext()).getDaoSession();
//        IlustrationProductModelDao ilustrationProductModelDao = daoSession.getIlustrationProductModelDao();
//        List<IlustrationProductModel> ilustrationProductModels = ilustrationProductModelDao.queryBuilder().where(IlustrationProductModelDao.Properties.DbMasterRef.eq(dbMasterRef))
//                .list();
//        if(ilustrationProductModels.size()==0){
//            finish();
//        }else {
//            dbMasterRef = ilustrationProductModels.get(0).getDbMasterRef();
//            projectRef = ilustrationProductModels.get(0).getProjectRef();
//            categoryRef = ilustrationProductModels.get(0).getCategoryRef();
//            clusterRef = ilustrationProductModels.get(0).getClusterRef();
//            clusterDescription = ilustrationProductModels.get(0).getClusterDescription();
//            priceRangeStart = ilustrationProductModels.get(0).getPriceRangeStart();
//            priceRangeEnd = ilustrationProductModels.get(0).getPriceRangeEnd();
//            isAllowCalculatePrice = ilustrationProductModels.get(0).getIsAllowCalculatePrice();
//            isNUP = ilustrationProductModels.get(0).getIsNUP();
//            isBooking = ilustrationProductModels.get(0).getIsBooking();
//            isShowDiagramMaticUnit = ilustrationProductModels.get(0).getIsShowDiagramMaticUnit();
//        }

    }

    public void showIlustrationResults(retrofit2.Response<List<IlustrationModel>> response) {
        progressDialog.dismiss();
//        final DaoSession daoSession = ((BaseApplication)getApplicationContext()).getDaoSession();
        listIlustration = response.body();
        if (listIlustration.size() != 0) {
            linearNoData.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            txtChooseIlustration.setVisibility(View.VISIBLE);
//            for (int i = 0; i < listIlustration.size(); i++) {
//                IlustrationModel ilustrationModel = new IlustrationModel();
//                ilustrationModel.setUnitRef(listIlustration.get(i).getUnitRef());
//                ilustrationModel.setUnitNo(listIlustration.get(i).getUnitNo());
//                ilustrationModel.setArea(listIlustration.get(i).getArea());
//                ilustrationModel.setPriceInc(listIlustration.get(i).getPriceInc());
//                ilustrationModel.setSalesDisc(listIlustration.get(i).getSalesDisc());
//                ilustrationModel.setNetPrice(listIlustration.get(i).getNetPrice());
//                ilustrationModel.setDbMasterRef(listIlustration.get(i).getDbMasterRef());
//                ilustrationModel.setProjectRef(listIlustration.get(i).getProjectRef());
//                ilustrationModel.setCategoryRef(listIlustration.get(i).getCategoryRef());
//                ilustrationModel.setClusterRef(listIlustration.get(i).getClusterRef());
//                ilustrationModel.setProductRef(listIlustration.get(i).getProductRef());
//                IlustrationModelDao projectModelDao = daoSession.getIlustrationModelDao();
//                projectModelDao.insertOrReplace(ilustrationModel);
//
//            }
            ilustrationAdapter.notifyDataSetChanged();
            initAdapter();

        } else {
            linearNoData.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            txtChooseIlustration.setVisibility(View.VISIBLE);
        }
    }

    public void showIlustrationFailure(Throwable t) {
        progressDialog.dismiss();
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
//        IlustrationModelDao ilustrationModelDao = daoSession.getIlustrationModelDao();
//        List<IlustrationModel> ilustrationModels = ilustrationModelDao.queryBuilder()
//                .list();
//        Log.d("getProjectLocalDB", "" + ilustrationModels.toString());
//        int numData = ilustrationModels.size();
//        if (numData > 0) {
//            for (int i = 0; i < numData; i++) {
//                IlustrationModel ilustrationModel = ilustrationModels.get(i);
//                listIlustration.add(ilustrationModel);
//            }
//        } else {
//            finish();
//        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (b != null && b.isShowing()) {
            b.dismiss();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(IlustrationProductActivity.this, ProjectMenuActivity.class);
                intentProjectMenu.putExtra(PROJECT_REF, projectRef);
                intentProjectMenu.putExtra(DBMASTER_REF, dbMasterRef);
                intentProjectMenu.putExtra(PROJECT_NAME, projectName);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
