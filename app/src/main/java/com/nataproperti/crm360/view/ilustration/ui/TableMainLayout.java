package com.nataproperti.crm360.view.ilustration.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.ilustration.model.DiagramColor;
import com.nataproperti.crm360.view.ilustration.model.LIstBlockDiagramModel;
import com.nataproperti.crm360.view.ilustration.model.ListUnitMappingModel;
import com.nataproperti.crm360.view.ilustration.presenter.TableMainPresenter;

import java.util.ArrayList;
import java.util.List;

public class TableMainLayout extends RelativeLayout {
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String BLOCK_NAME = "blockName";
    public static final String UNIT_REF = "unitRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String IS_BOOKING = "isBooking";
    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private TableMainPresenter presenter;

    final String DBMASTERREF = "dbMasterRef";
    final String PROJECTREF = "projectRef";

    public final String TAG = "TableMainLayout.java";
    private List<ListUnitMappingModel> listUnitMappingModels = new ArrayList<>();
    private List<LIstBlockDiagramModel> lIstBlockDiagramModels = new ArrayList<>();

    String dbMasterRef, projectRef, categoryRef, clusterRef, productRefIntent, unitRef, projectName, isBooking, isShowAvailableUnit;
    ProgressDialog progressDialog;

    TextView textViewHeader;

    SharedPreferences sharedPreferences;
    String memberRef, color1, color2;
    //LIST HEADER BLOCK
    List<String> rowsHeader = new ArrayList<>();
    List<String> unitMapingRows = new ArrayList<>();
    List<String> productRefList = new ArrayList<>();
    List<String> unitStatusList = new ArrayList<>();
    List<String> unitRefList = new ArrayList<>();
    List<String> productNameList = new ArrayList<>();
    List<String> headers = new ArrayList<>();

    public void requestUnitMapping() {
        presenter.getUnitMapping(dbMasterRef, projectRef, categoryRef, clusterRef);
    }

    public void requestBlockMapping() {
        progressDialog = ProgressDialog.show(context, "", "Please Wait...", true);
        presenter.getBlockMapping(dbMasterRef, projectRef, categoryRef, clusterRef);
    }

    public void showUnitMappingResults(retrofit2.Response<List<ListUnitMappingModel>> response) {
        listUnitMappingModels = response.body();
    }

    public void showUnitMappingFailure(Throwable t) {
        Toast.makeText(context, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
    }

    //LIST HEADER UNIT

    protected void initHeader() {
        if (listUnitMappingModels != null) {
            for (int x = 0; x < listUnitMappingModels.size(); x++) {
                headers.add(x, listUnitMappingModels.get(x).getUnitName());
            }
            headerCellsWidth = new int[headers.size()];
        } else {
            Toast.makeText(context, "Terjadi kesalahan", Toast.LENGTH_LONG).show();
        }

    }


    protected void initRows() {
        if (lIstBlockDiagramModels != null) {
            for (int x = 0; x < lIstBlockDiagramModels.size(); x++) {
                rowsHeader.add(x, lIstBlockDiagramModels.get(x).getBlockName());
                unitMapingRows.add(x, lIstBlockDiagramModels.get(x).getUnitMaping());
                productRefList.add(x, lIstBlockDiagramModels.get(x).getProductRefList());
                unitStatusList.add(x, lIstBlockDiagramModels.get(x).getUnitStatusList());
                unitRefList.add(x, lIstBlockDiagramModels.get(x).getUnitRefList());
                productNameList.add(x, lIstBlockDiagramModels.get(x).getProductNameList());
            }
        } else {
            Toast.makeText(context, "Terjadi kesalahan", Toast.LENGTH_LONG).show();
        }
    }

    // set the header titles
    TableLayout tableA, tableX;
    TableLayout tableB;
    TableLayout tableC;
    TableLayout tableD;

    HorizontalScrollView horizontalScrollViewB;
    HorizontalScrollView horizontalScrollViewD;

    ScrollView scrollViewC;
    ScrollView scrollViewD;

    Context context;

    List<SampleObject> sampleObjects;

    int headerCellsWidth[];

    public TableMainLayout(Context context) {
        super(context);

        Intent intent = ((Activity) context).getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTERREF);
        projectRef = intent.getStringExtra(PROJECTREF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        productRefIntent = intent.getStringExtra(PRODUCT_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        isBooking = intent.getStringExtra("isBooking");
        isShowAvailableUnit = intent.getStringExtra(IS_SHOW_AVAILABLE_UNIT);

        Log.d("Diagrammatic", "" + dbMasterRef + "-" + projectRef + "-" + categoryRef + "-" + clusterRef);

        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        service = ((BaseApplication) context.getApplicationContext()).getNetworkService();
        presenter = new TableMainPresenter(this, service);

        /*color*/
        requestDiagramColor();

       /* requestBlockMapping();
        requestUnitMapping();*/

        /*sampleObjects = this.sampleObjects();*/
        /*headerCellsWidth = new int[headers.size()];*/
        this.context = context;
    }

    public void requestDiagramColor() {
        presenter.getDiagramColor(memberRef,dbMasterRef,projectRef);
    }

    // this is just the sample data
    List<SampleObject> sampleObjects() {

        List<SampleObject> sampleObjects = new ArrayList<>();
        for (int x = 1; x <= lIstBlockDiagramModels.size(); x++) {
            SampleObject sampleObject = new SampleObject(rowsHeader);

            sampleObjects.add(sampleObject);
        }

        return sampleObjects;

    }

    // initalized components
    private void initComponents() {

        this.tableX = new TableLayout(this.context);

        this.tableA = new TableLayout(this.context);
        this.tableB = new TableLayout(this.context);
        this.tableC = new TableLayout(this.context);
        this.tableD = new TableLayout(this.context);

        this.horizontalScrollViewB = new MyHorizontalScrollView(this.context);
        this.horizontalScrollViewD = new MyHorizontalScrollView(this.context);

        this.scrollViewC = new MyScrollView(this.context);
        this.scrollViewD = new MyScrollView(this.context);

        this.tableA.setBackgroundColor(getResources().getColor(R.color.colorHeaderDiagramMatic));
        this.horizontalScrollViewB.setBackgroundColor(Color.LTGRAY);

    }

    // set essential component IDs
    private void setComponentsId() {
        String idTableOne = "1";
        String idTableTwo = "2";
        String idTableThree = "3";
        String idTableFour = "4";

        this.tableA.setId(Integer.parseInt(idTableOne));
        this.horizontalScrollViewB.setId(Integer.parseInt(idTableTwo));
        this.scrollViewC.setId(Integer.parseInt(idTableThree));
        this.scrollViewD.setId(Integer.parseInt(idTableFour));
    }

    // set tags for some horizontal and vertical scroll view
    private void setScrollViewAndHorizontalScrollViewTag() {

        this.horizontalScrollViewB.setTag("horizontal scroll view b");
        this.horizontalScrollViewD.setTag("horizontal scroll view d");

        this.scrollViewC.setTag("scroll view c");
        this.scrollViewD.setTag("scroll view d");
    }

    //tableX
    private void addComponentToTableX() {
       /* LayoutParams componentX_Params = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
        this.addView(tableX);*/
    }

    // we add the components here in our TableMainLayout
    private void addComponentToMainLayout() {

        // RelativeLayout params were very useful here
        // the addRule method is the key to arrange the components properly
        LayoutParams componentB_Params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        componentB_Params.addRule(RelativeLayout.RIGHT_OF, this.tableA.getId());

        LayoutParams componentC_Params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        componentC_Params.addRule(RelativeLayout.BELOW, this.tableA.getId());

        LayoutParams componentD_Params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        componentD_Params.addRule(RelativeLayout.RIGHT_OF, this.scrollViewC.getId());
        componentD_Params.addRule(RelativeLayout.BELOW, this.horizontalScrollViewB.getId());

        // 'this' is a relative layout,
        // we extend this table layout as relative layout as seen during the creation of this class
        this.addView(this.tableA);
        this.addView(this.horizontalScrollViewB, componentB_Params);
        this.addView(this.scrollViewC, componentC_Params);
        this.addView(this.scrollViewD, componentD_Params);

    }

    //tableX
    private void addTableRowToTableX() {
        this.tableX.addView(this.componentXTableRow());
    }

    private void addTableRowToTableA() {
        this.tableA.addView(this.componentATableRow());
    }

    private void addTableRowToTableB() {
        this.tableB.addView(this.componentBTableRow());
    }

    //tableX
    TableRow componentXTableRow() {
        TableRow componentXTableRow = new TableRow(this.context);

        TableRow.LayoutParams params = new TableRow.LayoutParams(500, 150);
        params.setMargins(0, 2, 0, 0);

        TextView textView = this.headerTextView("INI TABLE X");

        componentXTableRow.addView(textView, params);
        return componentXTableRow;
    }

    // generate table row of table A
    //pojok kiri atas
    TableRow componentATableRow() {

        TableRow componentATableRow = new TableRow(this.context);

        TableRow.LayoutParams params = new TableRow.LayoutParams(150, 150);
        params.setMargins(0, 2, 0, 0);
        /*TextView textView = this.headerTextView(this.headers[0]);*/
        textViewHeader = this.headerTextView("          ");
        textViewHeader.setBackground(getResources().getDrawable(R.drawable.ic_diagrammatic));
//        final String labels = this.headers.get(0);
        textViewHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getContext(),"componentATableRow -"+labels,Toast.LENGTH_SHORT).show();
            }
        });
        componentATableRow.addView(textViewHeader, params);

        return componentATableRow;
    }

    // generate table row of table B
    //display unit header
    TableRow componentBTableRow() {

        TableRow componentBTableRow = new TableRow(this.context);
        int headerFieldCount = this.headers.size();

        TableRow.LayoutParams params = new TableRow.LayoutParams(150, 150);
        params.setMargins(2, 0, 0, 0);

        for (int x = 0; x < (headerFieldCount - 1); x++) {
            TextView textView = this.headerTextView(this.headers.get(x));
            textView.setLayoutParams(params);
            textView.setBackgroundColor(getResources().getColor(R.color.colorHeaderDiagramMatic));
            textView.setTextColor(getResources().getColor(R.color.colorWhite));
            componentBTableRow.addView(textView);
        }

        return componentBTableRow;
    }

    // generate table row of table C and table D
    private void generateTableC_AndTable_B() {

        // just seeing some header cell width
        for (int x = 0; x < this.headerCellsWidth.length; x++) {
            Log.v("TableMainLayout.java", this.headerCellsWidth[x] + "");
        }

        int i = 0;
        for (SampleObject sampleObject : this.sampleObjects) {
            Log.d("forSample", "" + i);
            Log.d("forSamplet", "" + unitMapingRows.get(i));
            String blockUnitMaping = unitMapingRows.get(i);
            String productRef = productRefList.get(i);
            String unitStatus = unitStatusList.get(i);
            String unitRef = unitRefList.get(i);
            String productNames = productNameList.get(i);
            //display block header
            TableRow tableRowForTableC = this.tableRowForTableC(sampleObject, i);

            TableRow taleRowForTableD = this.taleRowForTableD(sampleObject, blockUnitMaping, productRef, unitStatus, unitRef, productNames);

            tableRowForTableC.setBackgroundColor(Color.LTGRAY);
            taleRowForTableD.setBackgroundColor(Color.LTGRAY);

            this.tableC.addView(tableRowForTableC);
            this.tableD.addView(taleRowForTableD);

            i = i + 1;
        }
    }

    // a TableRow for table C
    TableRow tableRowForTableC(SampleObject sampleObject, int i) {

        /*TableRow.LayoutParams params = new TableRow.LayoutParams( this.headerCellsWidth[0],LayoutParams.MATCH_PARENT);*/
        TableRow.LayoutParams params = new TableRow.LayoutParams(150, 150);
        params.setMargins(0, 2, 0, 0);

        TableRow tableRowForTableC = new TableRow(this.context);
        /*TextView textView = this.bodyTextView(sampleObject.header1);*/
        if (sampleObject.hdrStr.length > 0) {
            /*String[] dataUnitMaping = blockUnitMaping.split(",");*/
            //display block header
            TextView textView = this.bodyTextView(sampleObject.hdrStr[i]);
            textView.setBackgroundColor(getResources().getColor(R.color.colorBlockDiagramMatic));
            textView.setTextColor(getResources().getColor(R.color.colorWhite));
            final String labels = sampleObject.hdrStr[0];
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(getContext(),"sampleObject -"+labels,Toast.LENGTH_SHORT).show();
                }
            });
            tableRowForTableC.addView(textView, params);
        }

        return tableRowForTableC;
    }

    //display unitmaping
    TableRow taleRowForTableD(SampleObject sampleObject, String blockUnitMaping, String productRef, String unitStatus, String unitRef, String productNames) {

        TableRow taleRowForTableD = new TableRow(this.context);

        int loopCount = ((TableRow) this.tableB.getChildAt(0)).getChildCount();

        Object info[] = headers.toArray();

        final String[] dataUnitMaping = blockUnitMaping.split(",");
        final String[] dataProductRef = productRef.split(",");
        final String[] dataUnitStatus = unitStatus.split(",");
        final String[] dataUnitRef = unitRef.split(",");
        final String[] dataProductName = productNames.split(",");
        for (int x = 0; x < loopCount; x++) {
            TableRow.LayoutParams params = new TableRow.LayoutParams(150, 150);
            params.setMargins(2, 2, 0, 0);

            TextView textViewB = this.bodyTextView(dataUnitMaping[x] + "\n" + dataProductName[x]);
            textViewB.setTextSize(12);
            final int y = x;
            textViewB.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(getContext(),dataProductRef[y]+"-"+dataUnitStatus[y],Toast.LENGTH_SHORT).show();
                    if (dataUnitStatus[y].equals("A")) {
                        Intent intent = new Intent(getContext(), IlustrationPaymentTermActivity.class);
                        intent.putExtra(DBMASTER_REF, dbMasterRef);
                        intent.putExtra(PROJECT_REF, projectRef);
                        intent.putExtra(CLUSTER_REF, clusterRef);
                        intent.putExtra(CATEGORY_REF, categoryRef);
                        intent.putExtra(PROJECT_NAME, projectName);
                        intent.putExtra(PRODUCT_REF, dataProductRef[y]);
                        intent.putExtra(UNIT_REF, dataUnitRef[y]);

                        intent.putExtra(IS_BOOKING, isBooking);
                        context.startActivity(intent);
                    } else {
                        if (!dataUnitMaping[y].equals(" ")) {
                            Toast.makeText(getContext(), "Unit " + dataUnitMaping[y] + " Sudah Terjual", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });

            //isShowUnitAvalible
            /*if (isShowAvailableUnit.equals("1")) {
                if (dataUnitStatus[y].equals("A")) {
                    textViewB.setBackgroundColor(getResources().getColor(R.color.colorPrimarySecond));
                } else {
                    textViewB.setBackgroundColor(getResources().getColor(R.color.colorRed));
                }
            } else {
                textViewB.setBackground(getResources().getDrawable(R.drawable.selector_diagrammatic));
            }

            //productdetail
            if (!productRefIntent.equals("")) {
                Log.d("if1", "" + productRefIntent);
                if (dataProductRef[y].equals(productRefIntent)) {
                    Log.d("if2", "" + productRefIntent);
                    textViewB.setBackgroundColor(getResources().getColor(R.color.colorGray));
                }else {
                    textViewB.setBackgroundColor(getResources().getColor(R.color.colorGraySecond));
                }

            }*/


            if (!color1.equals("") && !color2.equals("")) {
                if (dataUnitStatus[y].equals("A")) {
                    textViewB.setBackgroundColor(Color.parseColor(color1));
                } else {
                    textViewB.setBackgroundColor(Color.parseColor(color2));
                }
            } else {
                if (!productRefIntent.equals("")) {
                    if (isShowAvailableUnit.equals("1")) {
                        if (dataProductRef[y].equals(productRefIntent)) {
                            if (dataUnitStatus[y].equals("A")) {
                                textViewB.setBackgroundColor(getResources().getColor(R.color.colorPrimarySecond));
                            } else if (!dataUnitStatus[y].equals("A")) {
                                textViewB.setBackgroundColor(getResources().getColor(R.color.colorRed));
                            } else {
                                textViewB.setBackgroundColor(getResources().getColor(R.color.colorGray));
                            }
                        } else {
                            textViewB.setBackgroundColor(getResources().getColor(R.color.colorGray));
                        }

                    } else {
                        if (dataProductRef[y].equals(productRefIntent)) {
                            Log.d("if2", "" + productRefIntent);
                            textViewB.setBackgroundColor(getResources().getColor(R.color.colorGray));
                        } else {
                            textViewB.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                        }

                    }

                } else {
                    if (isShowAvailableUnit.equals("1")) {
                        if (dataUnitStatus[y].equals("A")) {
                            textViewB.setBackgroundColor(getResources().getColor(R.color.colorPrimarySecond));
                        } else {
                            textViewB.setBackgroundColor(getResources().getColor(R.color.colorRed));
                        }
                    } else {
                        textViewB.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    }
                }

            }


            if (dataUnitMaping[x].equals(" ")) {
                textViewB.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                //textViewB.setBackgroundColor(Color.parseColor("#f54a61"));
                Log.d("unitMapingWhite", "++++++");
            }

            taleRowForTableD.addView(textViewB, params);
        }

/*        for(int x=0 ; x<loopCount; x++){
            TableRow.LayoutParams params = new TableRow.LayoutParams( headerCellsWidth[x+1],LayoutParams.MATCH_PARENT);
            params.setMargins(2, 2, 0, 0);

            TextView textViewB = this.bodyTextView(info[x]);
            taleRowForTableD.addView(textViewB,params);
        }*/

        return taleRowForTableD;

    }

    // table cell standard TextView
    TextView bodyTextView(String label) {

        TextView bodyTextView = new TextView(this.context);
        bodyTextView.setBackgroundColor(Color.WHITE);
        bodyTextView.setText(label);
        bodyTextView.setGravity(Gravity.CENTER);
        bodyTextView.setPadding(5, 5, 5, 5);

        return bodyTextView;
    }

    // header standard TextView
    TextView headerTextView(String label) {

        TextView headerTextView = new TextView(this.context);
        headerTextView.setBackgroundColor(Color.WHITE);
        headerTextView.setText(label);
        headerTextView.setGravity(Gravity.CENTER);
        headerTextView.setPadding(5, 5, 5, 5);

        return headerTextView;
    }

    // resizing TableRow height starts here
    void resizeHeaderHeight() {

        TableRow productNameHeaderTableRow = (TableRow) this.tableA.getChildAt(0);
        TableRow productInfoTableRow = (TableRow) this.tableB.getChildAt(0);

        int rowAHeight = this.viewHeight(productNameHeaderTableRow);
        int rowBHeight = this.viewHeight(productInfoTableRow);

        TableRow tableRow = rowAHeight < rowBHeight ? productNameHeaderTableRow : productInfoTableRow;
        int finalHeight = rowAHeight > rowBHeight ? rowAHeight : rowBHeight;

        this.matchLayoutHeight(tableRow, finalHeight);
    }

    void getTableRowHeaderCellWidth() {

        int tableAChildCount = ((TableRow) this.tableA.getChildAt(0)).getChildCount();
        int tableBChildCount = ((TableRow) this.tableB.getChildAt(0)).getChildCount();

        for (int x = 0; x < (tableAChildCount + tableBChildCount); x++) {
            //Log.v("Length", String.valueOf(this.headerCellsWidth.length) + "");

            if (this.headerCellsWidth.length != 0 && headerCellsWidth != null) {
                if (x == 0) {
                    this.headerCellsWidth[x] = this.viewWidth(((TableRow) this.tableA.getChildAt(0)).getChildAt(x));
                } else {
                    this.headerCellsWidth[x] = this.viewWidth(((TableRow) this.tableB.getChildAt(0)).getChildAt(x - 1));
                }
            } else {
                //Toast.makeText(getContext(),"No Data Avalilable",Toast.LENGTH_LONG).show();
                textViewHeader.setVisibility(GONE);
            }

        }
    }

    // resize body table row height
    void resizeBodyTableRowHeight() {

        int tableC_ChildCount = this.tableC.getChildCount();

        for (int x = 0; x < tableC_ChildCount; x++) {

            TableRow productNameHeaderTableRow = (TableRow) this.tableC.getChildAt(x);
            TableRow productInfoTableRow = (TableRow) this.tableD.getChildAt(x);

            int rowAHeight = this.viewHeight(productNameHeaderTableRow);
            int rowBHeight = this.viewHeight(productInfoTableRow);

            TableRow tableRow = rowAHeight < rowBHeight ? productNameHeaderTableRow : productInfoTableRow;
            int finalHeight = rowAHeight > rowBHeight ? rowAHeight : rowBHeight;

            this.matchLayoutHeight(tableRow, finalHeight);
        }

    }

    // match all height in a table row
    // to make a standard TableRow height
    private void matchLayoutHeight(TableRow tableRow, int height) {

        int tableRowChildCount = tableRow.getChildCount();

        // if a TableRow has only 1 child
        if (tableRow.getChildCount() == 1) {

            View view = tableRow.getChildAt(0);
            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
            params.height = height - (params.bottomMargin + params.topMargin);

            return;
        }

        // if a TableRow has more than 1 child
        for (int x = 0; x < tableRowChildCount; x++) {

            View view = tableRow.getChildAt(x);

            TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();

            if (!isTheHeighestLayout(tableRow, x)) {
                params.height = height - (params.bottomMargin + params.topMargin);
                return;
            }
        }

    }

    // check if the view has the highest height in a TableRow
    private boolean isTheHeighestLayout(TableRow tableRow, int layoutPosition) {

        int tableRowChildCount = tableRow.getChildCount();
        int heighestViewPosition = -1;
        int viewHeight = 0;

        for (int x = 0; x < tableRowChildCount; x++) {
            View view = tableRow.getChildAt(x);
            int height = this.viewHeight(view);

            if (viewHeight < height) {
                heighestViewPosition = x;
                viewHeight = height;
            }
        }

        return heighestViewPosition == layoutPosition;
    }

    // read a view's height
    private int viewHeight(View view) {
        view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        return view.getMeasuredHeight();
    }

    // read a view's width
    private int viewWidth(View view) {
        view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        return view.getMeasuredWidth();
    }

    public void showBlockMappingResults(retrofit2.Response<List<LIstBlockDiagramModel>> response) {
        progressDialog.dismiss();
        lIstBlockDiagramModels = response.body();

        initRows();
        initHeader();
        sampleObjects = sampleObjects();

        initComponents();
        setComponentsId();
        setScrollViewAndHorizontalScrollViewTag();
        // no need to assemble component A, since it is just a table
        horizontalScrollViewB.addView(tableB);
        scrollViewC.addView(tableC);
        scrollViewD.addView(horizontalScrollViewD);
        horizontalScrollViewD.addView(tableD);
        // add the components to be part of the main layout
        addTableRowToTableX();
        addComponentToMainLayout();
        setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        addComponentToTableX();
        addTableRowToTableA();
        // add some table rows
        addTableRowToTableB();

        resizeHeaderHeight();
        getTableRowHeaderCellWidth();

        generateTableC_AndTable_B();
        resizeBodyTableRowHeight();
    }

    public void showBlockMappingFailure(Throwable t) {
        progressDialog.dismiss();
    }

    public void showDiagramColorResults(retrofit2.Response<DiagramColor> response) {
        int status = response.body().getStatus();
        color1 = response.body().getColor1();
        color2 = response.body().getColor2();

        if (status == 200) {
            requestBlockMapping();
            requestUnitMapping();
        }
    }

    public void showDiagramColorFailure(Throwable t) {
        Toast.makeText(context, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
    }

    // horizontal scroll view custom class
    class MyHorizontalScrollView extends HorizontalScrollView {

        public MyHorizontalScrollView(Context context) {
            super(context);
        }

        @Override
        protected void onScrollChanged(int l, int t, int oldl, int oldt) {
            String tag = (String) this.getTag();

            if (tag.equalsIgnoreCase("horizontal scroll view b")) {
                horizontalScrollViewD.scrollTo(l, 0);
            } else {
                horizontalScrollViewB.scrollTo(l, 0);
            }
        }

    }

    // scroll view custom class
    class MyScrollView extends ScrollView {

        public MyScrollView(Context context) {
            super(context);
        }

        @Override
        protected void onScrollChanged(int l, int t, int oldl, int oldt) {

            String tag = (String) this.getTag();

            if (tag.equalsIgnoreCase("scroll view c")) {
                scrollViewD.scrollTo(0, t);
            } else {
                scrollViewC.scrollTo(0, t);
            }
        }
    }
}
