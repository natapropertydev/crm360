package com.nataproperti.crm360.view.ilustration.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyGridView;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.ilustration.adapter.NewUnitMappingAdapter;
import com.nataproperti.crm360.view.ilustration.model.DiagramColor;
import com.nataproperti.crm360.view.ilustration.model.MapingModelNew;
import com.nataproperti.crm360.view.ilustration.model.UnitMappingModel;
import com.nataproperti.crm360.view.ilustration.presenter.UnitMappingPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 5/30/2016.
 */
public class UnitMappingActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CATEGORY_REF = "categoryRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String BLOCK_NAME = "blockName";
    public static final String UNIT_REF = "unitRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String IS_BOOKING = "isBooking";
    public static final String IS_SHOW_AVAILABLE_UNIT = "isShowAvailableUnit";
    private List<MapingModelNew> listUnitMapping = new ArrayList<>();
    private List<UnitMappingModel> listUnitMapping2 = new ArrayList<>();
    private NewUnitMappingAdapter adapterUnit;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private UnitMappingPresenter presenter;
    ProgressDialog progressDialog;
    ImageView imgLogo;
    TextView txtProjectName;
    MyGridView listUnit, listView;
    String projectRef, dbMasterRef, categoryRef, clusterRef, projectName, blockName, unitRef, productRef, unitStatus, isBooking, isShowAvailableUnit;
    String memberRef, color1, color2;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    SharedPreferences sharedPreferences;
    RelativeLayout rPage;
    Point size;
    Display display;
    Integer width;
    Double result;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_mapping);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new UnitMappingPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        categoryRef = intent.getStringExtra(CATEGORY_REF);
        clusterRef = intent.getStringExtra(CLUSTER_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        blockName = intent.getStringExtra(BLOCK_NAME);
        isBooking = intent.getStringExtra(IS_BOOKING);
        productRef = intent.getStringExtra(PRODUCT_REF);
        isShowAvailableUnit = intent.getStringExtra(IS_SHOW_AVAILABLE_UNIT);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Log.d("cek unit", dbMasterRef + " " + projectRef + " " + categoryRef + " " + clusterRef + " " + blockName);
        txtProjectName.setText(projectName);
        Glide.with(this)
                .load(WebService.getProjectImage() + dbMasterRef +
                        "&pr=" + projectRef).into(imgLogo);
        requestDiagramColor();
        initAdapter();
    }

    private void initAdapter() {
        adapterUnit = new NewUnitMappingAdapter(this, listUnitMapping2);
        listView.setAdapter(adapterUnit);
        listView.setExpanded(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                unitRef = listUnitMapping.get(position).getUnitRef();
                productRef = listUnitMapping.get(position).getProductRef();
                unitStatus = listUnitMapping.get(position).getUnitStatus();

                if (unitStatus.equals("A")) {
                    Intent intent = new Intent(UnitMappingActivity.this, IlustrationPaymentTermActivity.class);
                    intent.putExtra(DBMASTER_REF, dbMasterRef);
                    intent.putExtra(PROJECT_REF, projectRef);
                    intent.putExtra(CLUSTER_REF, clusterRef);
                    intent.putExtra(CATEGORY_REF, categoryRef);
                    intent.putExtra(PROJECT_NAME, projectName);
                    intent.putExtra(PRODUCT_REF, productRef);
                    intent.putExtra(UNIT_REF, unitRef);

                    intent.putExtra(IS_BOOKING, isBooking);
                    startActivity(intent);

                } else {
                    Toast.makeText(UnitMappingActivity.this, "Unit " + listUnitMapping.get(position).getUnitName() +
                            " Not Available", Toast.LENGTH_LONG).show();
                }


            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title.setText(getResources().getString(R.string.title_illustrastion_unit_mapping));
        imgLogo = (ImageView) findViewById(R.id.img_logo_project);
        txtProjectName = (TextView) findViewById(R.id.txt_project_name);
        rPage = (RelativeLayout) findViewById(R.id.rPage);
        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
        listView = (MyGridView) findViewById(R.id.list_unit_mapping);
    }

    public void requestDiagramColor() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getDiagramColor(memberRef,dbMasterRef,projectRef);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    public void requestUnitMapping() {
        presenter.getUnitMapping(dbMasterRef, projectRef, categoryRef, clusterRef, blockName);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public void showDiagramColorResults(retrofit2.Response<DiagramColor> response) {
        progressDialog.dismiss();
        int status = response.body().getStatus();
        color1 = response.body().getColor1();
        color2 = response.body().getColor2();

        if (status == 200) {
            requestUnitMapping();
        }
    }


    public void showDiagramColorFailure(Throwable t) {
        progressDialog.dismiss();
    }

    public void showUnitMappingResults(retrofit2.Response<List<MapingModelNew>> response) {
        listUnitMapping = response.body();
        for (int i = 0; i < listUnitMapping.size(); i++) {
            UnitMappingModel unitMappingModel = new UnitMappingModel();
            unitMappingModel.setUnitName(listUnitMapping.get(i).getUnitName());
            unitMappingModel.setUnitRef(listUnitMapping.get(i).getUnitRef());
            unitMappingModel.setProductRef(listUnitMapping.get(i).getProductRef());
            unitMappingModel.setUnitStatus(listUnitMapping.get(i).getUnitStatus());
            unitMappingModel.setProductName(listUnitMapping.get(i).getProductName());
            unitMappingModel.setProductNameInt(productRef);
            unitMappingModel.setIsShowAvailableUnit(isShowAvailableUnit);
            unitMappingModel.setColor1(color1);
            unitMappingModel.setColor2(color2);
            listUnitMapping2.add(unitMappingModel);
        }
        initAdapter();
    }

    public void showUnitMappingFailure(Throwable t) {

    }

}
