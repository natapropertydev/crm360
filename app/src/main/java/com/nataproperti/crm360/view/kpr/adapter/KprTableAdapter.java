package com.nataproperti.crm360.view.kpr.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.kpr.model.DataKPR;

import java.util.List;

/**
 * Created by User on 5/14/2016.
 */
public class KprTableAdapter extends BaseAdapter {
    private Context context;
    private List<DataKPR> list;
    private ListKprTableHolder holder;
    private Typeface font;

    public KprTableAdapter(Context context, List<DataKPR> list, Typeface font) {
        this.context = context;
        this.list = list;
        this.font = font;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_kpr_table,null);
            holder = new ListKprTableHolder();
            holder.insPeriode = (TextView) convertView.findViewById(R.id.txt_insPeriode);
            holder.instMonth = (TextView) convertView.findViewById(R.id.txt_instMonth);
            holder.pct = (TextView) convertView.findViewById(R.id.txt_pct);
            holder.total = (TextView) convertView.findViewById(R.id.txt_total);

            convertView.setTag(holder);
        }else{
            holder = (ListKprTableHolder) convertView.getTag();
        }

        DataKPR kprTableModel = list.get(position);
        holder.insPeriode.setText(kprTableModel.getInsPeriode());
        holder.pct.setText(kprTableModel.getPct());
        holder.instMonth.setText(kprTableModel.getInstMonth());
        holder.total.setText(kprTableModel.getTotal());
        holder.insPeriode.setTypeface(font);
        holder.pct.setTypeface(font);
        holder.instMonth.setTypeface(font);
        holder.total.setTypeface(font);
        return convertView;
    }

    private class ListKprTableHolder {
        TextView insPeriode,instMonth,total,pct;
    }
}
