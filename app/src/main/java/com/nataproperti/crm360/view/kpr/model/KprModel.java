package com.nataproperti.crm360.view.kpr.model;

/**
 * Created by User on 7/12/2016.
 */
public class KprModel {
    String tahunKpr,bunga;

    public String getTahunKpr() {
        return tahunKpr;
    }

    public void setTahunKpr(String tahunKpr) {
        this.tahunKpr = tahunKpr;
    }

    public String getBunga() {
        return bunga;
    }

    public void setBunga(String bunga) {
        this.bunga = bunga;
    }
}
