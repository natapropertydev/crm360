package com.nataproperti.crm360.view.listing.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.listing.model.ListingCategoryTypeModel;

import java.util.List;

/**
 * Created by User on 5/15/2016.
 */
public class ListingCategoryTypeAdapter extends BaseAdapter {
    private Context context;
    private List<ListingCategoryTypeModel> list;
    private ListHolder holder;

    public ListingCategoryTypeAdapter(Context context, List<ListingCategoryTypeModel> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_spiner_listing_type,null);
            holder = new ListHolder();
            holder.listingType = (TextView) convertView.findViewById(R.id.txt_listing_type);

            convertView.setTag(holder);
        }else{
            holder = (ListHolder) convertView.getTag();
        }

        ListingCategoryTypeModel listingCategoryTypeModel = list.get(position);
        holder.listingType.setText(listingCategoryTypeModel.getCategoryTypeName());

        return convertView;
    }

    private class ListHolder {
        TextView listingType;
    }
}
