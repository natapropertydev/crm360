package com.nataproperti.crm360.view.listing.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.view.listing.model.ListingMemberInfoModel;

import java.util.ArrayList;

/**
 * Created by User on 4/19/2016.
 */
public class ListingMemberInfoAdapter extends BaseAdapter {

    public static final String PROPERTY_REF = "propertyRef" ;
    public static final String LINK_DETAIL = "linkDetail" ;
    public static final String PROPERTY_NAME = "propertyName" ;

    public static final String PREF_NAME = "pref" ;

    private Context context;
    private ArrayList<ListingMemberInfoModel> list;

    private Display display;
    private ListPropertyHolder holder;

    public ListingMemberInfoAdapter(Context context, ArrayList<ListingMemberInfoModel> list, Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
    }

    private SharedPreferences sharedPreferences;
    private boolean state;
    private String memberType;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        state =  sharedPreferences.getBoolean("isLogin", false);
        memberType = sharedPreferences.getString("isMemberType", null);

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_listing_member_info,null);
            holder = new ListPropertyHolder();
            holder.name = (TextView) convertView.findViewById(R.id.txt_name);
            holder.email = (TextView) convertView.findViewById(R.id.txt_email);
            holder.hp = (TextView) convertView.findViewById(R.id.txt_mobile);
            holder.position = (TextView) convertView.findViewById(R.id.txt_position);
            holder.imageProfile = (ImageView) convertView.findViewById(R.id.img_profile);

            convertView.setTag(holder);
        }else{
            holder = (ListPropertyHolder) convertView.getTag();
        }

        ListingMemberInfoModel propertyModel = list.get(position);

         /*Independent*/
        if (memberType.startsWith("Independent")) {
            holder.name.setText(propertyModel.getName().toUpperCase());
        } else {
            holder.name.setText(propertyModel.getName().toUpperCase());
            holder.email.setText(propertyModel.getEmail().toLowerCase());
            holder.hp.setText(propertyModel.getHp1());
            String cekPosition = propertyModel.getPosision();
            if (cekPosition!=null && cekPosition.startsWith("Unknown")){
                holder.position.setVisibility(View.GONE);
            }
            holder.position.setText(propertyModel.getPosision());
        }

        Glide.with(context).load(WebService.getProfile()+propertyModel.getListingMemberRef())
                .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.imageProfile);

        return convertView;
    }

    private class ListPropertyHolder {
        TextView name,email,hp,position;
        ImageView imageProfile;
    }

}
