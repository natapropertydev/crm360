package com.nataproperti.crm360.view.listing.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.view.listing.model.ListingPropertyGalleryModel;
import com.nataproperti.crm360.view.listing.ui.ListingPropertyDetailImageActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 4/19/2016.
 */
public class ListingPropertyGalleryAdapter extends BaseAdapter {
    public static final String TAG = "NewsGalleryAdapter" ;

    public static final String PRODUCT_REF = "productRef";
    public static final String CONTENT_REF = "contentRef";
    public static final String IMAGE_REF = "imageRef";
    public static final String POSISION = "position";
    public static final String PROPERTY_REF = "propertyRef";

    private Context context;
    private List<ListingPropertyGalleryModel> list;
    private ListGalleryHolder holder;
    ListingPropertyGalleryAdapter adapter;
    private String  listingRef,imageRef;

    private Display display;

    public ListingPropertyGalleryAdapter(Context context, List<ListingPropertyGalleryModel> list, Display display,String listingRef) {
        this.context = context;
        this.list = list;
        this.display = display;
        this.listingRef = listingRef;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_listing_property_gallery,null);
            holder = new ListGalleryHolder();
            holder.imgNewsGallery = (ImageView) convertView.findViewById(R.id.img_item_list_property_gallery);
            holder.txtCaption = (TextView) convertView.findViewById(R.id.txt_caption);

            convertView.setTag(holder);
        }else{
            holder = (ListGalleryHolder) convertView.getTag();
        }

        ListingPropertyGalleryModel propertyGalleryModel = list.get(position);

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x/2;
        Double result = width/1.233333333333333;

        ViewGroup.LayoutParams params = holder.imgNewsGallery.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        holder.imgNewsGallery.setLayoutParams(params);
        holder.imgNewsGallery.requestLayout();

        Glide.with(context).load(propertyGalleryModel.getImageFile()).skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(holder.imgNewsGallery);
        //Log.d(TAG,""+propertyGalleryModel.getImgLink());
        holder.txtCaption.setText(propertyGalleryModel.getImageTitle());

        holder.imgNewsGallery.setOnClickListener(new OnImageClickListener(position));
       // holder.imgNewsGallery.setOnLongClickListener(new OnLongClickListener(position));

        return convertView;
    }

    class OnImageClickListener implements View.OnClickListener {

        int position;

        // constructor
        public OnImageClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            // on selecting grid view image
            // launch full screen activity
            Intent i = new Intent(context, ListingPropertyDetailImageActivity.class);
            i.putExtra(CONTENT_REF,list.get(position).getImageRef());
            i.putExtra(POSISION, position);
            i.putExtra(PROPERTY_REF, listingRef);
            context.startActivity(i);
        }

    }

    class OnLongClickListener implements View.OnLongClickListener {

        int position;

        // constructor
        public OnLongClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onLongClick(View v) {
            imageRef = list.get(position).getImageRef();

            AlertDialog.Builder alertDialogBuilder =
                    new AlertDialog.Builder(context);
            alertDialogBuilder.setMessage("Delete image");
            alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    deleteImage(listingRef,imageRef);
                    list.remove(position);
                    notifyDataSetChanged();
                    notifyDataSetInvalidated();
                }
            });
            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            return false;
        }
    }

    public void deleteImage(final String listingRef,final String imageRef) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.deleteImageListingProperty(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        //list.notify();
                       /* Intent intent = new Intent(context, ListingAddGalleryActivity.class);
                        ((Activity) context).finish();
                        adapter.notifyDataSetChanged();
                        context.startActivity(intent);*/
                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            //Toast.makeText(ListingAddGalleryActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", "5");
                params.put("imageRef", imageRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "deleteImage");

    }

    private class ListGalleryHolder {
        ImageView imgNewsGallery;
        TextView txtCaption;
    }
}
