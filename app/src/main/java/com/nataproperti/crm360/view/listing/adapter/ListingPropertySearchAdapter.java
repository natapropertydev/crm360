package com.nataproperti.crm360.view.listing.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.listing.model.ListingPropertyModel;

import java.util.ArrayList;

/**
 * Created by User on 4/19/2016.
 */
public class ListingPropertySearchAdapter extends BaseAdapter implements Filterable {

    public static final String PROPERTY_REF = "propertyRef" ;
    public static final String LINK_DETAIL = "linkDetail" ;
    public static final String PROPERTY_NAME = "propertyName" ;

    public static final String PREF_NAME = "pref" ;

    private int mCount = 3;
    private Context context;
    private ArrayList<ListingPropertyModel> list;
    public ArrayList<ListingPropertyModel> orig;

    private Display display;
    private ListPropertyHolder holder;

    String propertyRef,propertyName,subPropertyName,propertyDesc,linkImage;

    public ListingPropertySearchAdapter(Context context, ArrayList<ListingPropertyModel> list, Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
    }

    SharedPreferences sharedPreferences;
    private boolean state;

   /* public void addMoreItems(int count) {
        mCount += count;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mCount;
    }*/

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        state =  sharedPreferences.getBoolean("isLogin", false);

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_listing_property,null);
            holder = new ListPropertyHolder();
            holder.propertyName = (TextView) convertView.findViewById(R.id.txt_property_name);
//            holder.subPropertyName = (TextView) convertView.findViewById(R.id.txt_sub_property_name);
            //holder.propertyDesc = (TextView) convertView.findViewById(R.id.txt_property_desc);
            holder.imageHeader = (ImageView) convertView.findViewById(R.id.img_header);

            convertView.setTag(holder);
        }else{
            holder = (ListPropertyHolder) convertView.getTag();
        }

        ListingPropertyModel propertyModel = list.get(position);
        holder.propertyName.setText(propertyModel.getListingTitle());
        holder.subPropertyName.setText(propertyModel.getSubLocationName());
        //holder.propertyDesc.setText(propertyModel.getPropertyDesc());

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width/2.063037249283668;
        Log.d("screen width", result.toString()+"--"+Math.round(result));

        ViewGroup.LayoutParams params = holder.imageHeader.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        holder.imageHeader.setLayoutParams(params);
        holder.imageHeader.requestLayout();

        Glide.with(context)
                .load(propertyModel.getImageCover()).into(holder.imageHeader);

       /* holder.imageHeader.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                propertyRef = list.get(position).getPropertyRef();
                propertyName = list.get(position).getPropertyName();
                subPropertyName = list.get(position).getSubPropertyName();
                linkImage = list.get(position).getLinkImage();

                Intent intent = new Intent(context, ListingPropertyTabsActivity.class);
                intent.putExtra(LINK_DETAIL,linkImage);
                intent.putExtra(PROPERTY_REF,propertyRef);
                intent.putExtra(PROPERTY_NAME,propertyName);
                context.startActivity(intent);
            }
        });*/

        return convertView;
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<ListingPropertyModel> results = new ArrayList<ListingPropertyModel>();
                if (orig == null)
                    orig = list;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final ListingPropertyModel PropertyModel : orig) {
                            if (PropertyModel.getListingTitle().toLowerCase().contains(constraint.toString()))
                                results.add(PropertyModel);
                        }
                    }
                    oReturn.values = results;
                    oReturn.count = results.size();
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list = (ArrayList<ListingPropertyModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    private class ListPropertyHolder {
        TextView propertyName,subPropertyName,propertyDesc;
        ImageView imageHeader;
    }

}
