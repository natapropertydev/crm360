package com.nataproperti.crm360.view.listing.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.project.model.ChatRoomModel;
import com.rockerhieu.emojicon.EmojiconTextView;

import java.util.List;

/**
 * Created by User on 10/28/2016.
 */
public class RVChatRoomAdapter extends RecyclerView.Adapter<RVChatRoomAdapter.ProjectRequestHolder> {
    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";
    private String userId;
    private int SELF = 100;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private static String today;
    String memberRef;

    List<ChatRoomModel> chatRoomModels;
    private Context context;
    //    private Display display;


    Integer position = 0; //Need to declare because onbind is executed after oncreate, and idk how to get position on oncreate

    public RVChatRoomAdapter(Context context, List<ChatRoomModel> chatRoomModels) {
        this.context = context;
//        this.display = display;
        this.chatRoomModels = chatRoomModels;
//        this.chatRoomModels = chatRoomModels;
//        Calendar calendar = Calendar.getInstance();
//        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static class ProjectRequestHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView timestamp;
        CardView cv;
        TextView chatName, chatValue, chatDate;
        ImageView statusImg;
        TextView chatName2, chatValue2, chatDate2;
        ImageView statusImg2;
        Context context;
        EmojiconTextView message;

        SharedPreferences sharedPreferences;
        RelativeLayout relativeLayout1, relativeLayout2;


        ProjectRequestHolder(View itemView) {
            super(itemView);


            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            context = itemView.getContext();
//            cv = (CardView) itemView.findViewById(R.id.cv);
//            chatName = (TextView) itemView.findViewById(R.id.txt_name);
//            chatValue = (TextView) itemView.findViewById(R.id.txt_chatting);
//            chatDate = (TextView) itemView.findViewById(R.id.txt_date);
            statusImg = (ImageView) itemView.findViewById(R.id.status);
//            relativeLayout1 = (RelativeLayout)itemView.findViewById(R.id.relativeChat);
            message = (EmojiconTextView) itemView.findViewById(R.id.message);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);


        }

        @Override
        public void onClick(final View v) {

        }

    }

    @Override
    public ProjectRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (viewType == SELF) {
            // self message
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_other, parent, false);
        } else {
            // others message
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_self, parent, false);//chat_item_other
        }
        final ProjectRequestHolder holder = new ProjectRequestHolder(v);
        ProjectRequestHolder crh = new ProjectRequestHolder(v);

        return crh;
    }

    @Override
    public void onBindViewHolder(final ProjectRequestHolder holder, final int position) {
        this.position = position + 1;
        holder.sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        memberRef = holder.sharedPreferences.getString("isMemberRef", null);
        String memberNames = holder.sharedPreferences.getString("isName", null);

        final ChatRoomModel roomModel = chatRoomModels.get(position);
        String idStatus = roomModel.getStatus();
        holder.message.setText(roomModel.getChatMessage());
        String names = roomModel.getMemberRefSender();
        String status = roomModel.getReadMessage();
        if (status.equals("false")) {
            if (names.equals(memberNames)) {
                holder.statusImg.setVisibility(View.VISIBLE);
                holder.statusImg.setImageResource(R.drawable.ic_pending);
            } else {
                holder.statusImg.setVisibility(View.GONE);
            }
        } else {
            if (names.equals(memberNames)) {
                holder.statusImg.setVisibility(View.VISIBLE);
                holder.statusImg.setImageResource(R.drawable.ic_read);
            } else {
                holder.statusImg.setVisibility(View.GONE);
            }

        }

        if (names.equals(memberNames)) {
            holder.timestamp.setText("You " + roomModel.getSendTime());
        } else {
            if (names.equals(memberNames)) {
                holder.timestamp.setText(roomModel.getMemberRefReceiver() + " " + roomModel.getSendTime());
            } else {
                holder.timestamp.setText(roomModel.getMemberRefSender() + " " + roomModel.getSendTime());
            }

        }

    }

    @Override
    public int getItemViewType(int position) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        String memberRefId = sharedPreferences.getString("isName", null);
        ChatRoomModel message = chatRoomModels.get(position);
        String nameMember = message.getMemberRefSender();
        if (nameMember.equals(memberRefId)) {
            return position;
        }

        return SELF;
    }


    @Override
    public int getItemCount() {
        return chatRoomModels.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}
