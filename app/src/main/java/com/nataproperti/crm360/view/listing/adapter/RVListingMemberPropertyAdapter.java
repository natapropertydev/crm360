package com.nataproperti.crm360.view.listing.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.model.ListingPropertyModel;
import com.nataproperti.crm360.view.listing.model.UpdatePromote;
import com.nataproperti.crm360.config.NataApi;
import com.nataproperti.crm360.config.NataService;
import com.nataproperti.crm360.helper.LoadingBar;
import com.nataproperti.crm360.view.listing.ui.ChattingRoomActivity;
import com.nataproperti.crm360.view.listing.ui.ListingAddPropertyActivity;
import com.nataproperti.crm360.view.listing.ui.ListingPropertyMemberActivity;
import com.nataproperti.crm360.view.listing.ui.ListingPropertyTabsActivity;
import com.nataproperti.crm360.view.project.ui.MyListingActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by User on 10/28/2016.
 */
public class RVListingMemberPropertyAdapter extends RecyclerView.Adapter<RVListingMemberPropertyAdapter.ProjectRequestHolder> {
    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";
    String memberRefReceiver, name, favorite;
    String statusBookmark;
    int listingStatus;

    String listingRef, imageCover, listingTitle, agencyCompanyRef, memberRefModel, closingPrice = "", listingMemberRef,isFav;

    private int mCount = 3;
    private List<ListingPropertyModel> list;
    public List<ListingPropertyModel> orig;
    private Context context;
    private Display display;
    String memberRef, memberName, maidBed, bathMaid,memberTypeCode;
    boolean realFav;


    Integer position = 0; //Need to declare because onbind is executed after oncreate, and idk how to get position on oncreate

    public RVListingMemberPropertyAdapter(Context context, List<ListingPropertyModel> list, Display display,String memberTypeCode,
                                          String agencyCompanyRef) {
        this.context = context;
        this.display = display;
        this.list = list;
        this.agencyCompanyRef = agencyCompanyRef;
        this.memberTypeCode = memberTypeCode;
    }

    public static class ProjectRequestHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView address, facility, luas, companyName,txtCategoryType;
        MyTextViewLatoReguler landArea, price, memberName, car, bed, bath, type, property, title;
        ImageView imageHeader, btnFavorit, btnMessage, btnCall, btnPromote, btnMenu;
        CardView linearItemList;
        Context context;
        SharedPreferences sharedPreferences;
        private boolean state;

        private String memberRef, dbMasterRef, projectRef;

        LinearLayout linearLayoutItemClick;

        ProjectRequestHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            address = (TextView) itemView.findViewById(R.id.text_dec);
            landArea = (MyTextViewLatoReguler) itemView.findViewById(R.id.txt_land_area);//
            luas = (TextView) itemView.findViewById(R.id.text_demension);//
            price = (MyTextViewLatoReguler) itemView.findViewById(R.id.text_price);//
            imageHeader = (ImageView) itemView.findViewById(R.id.img_project);//
            memberName = (MyTextViewLatoReguler) itemView.findViewById(R.id.text_name);//
            car = (MyTextViewLatoReguler) itemView.findViewById(R.id.text_car);
            bed = (MyTextViewLatoReguler) itemView.findViewById(R.id.text_bed);
            bath = (MyTextViewLatoReguler) itemView.findViewById(R.id.text_bath);
            type = (MyTextViewLatoReguler) itemView.findViewById(R.id.text_type);//
            property = (MyTextViewLatoReguler) itemView.findViewById(R.id.text_property);//
            facility = (TextView) itemView.findViewById(R.id.text_facility);
            companyName = (TextView) itemView.findViewById(R.id.text_company_name);
            btnFavorit = (ImageView) itemView.findViewById(R.id.btn_favorite);
            btnMessage = (ImageView) itemView.findViewById(R.id.btn_chat);
            btnCall = (ImageView) itemView.findViewById(R.id.btn_call);
//            title = (MyTextViewLatoReguler)itemView.findViewById(R.id.text_title);
            btnPromote = (ImageView) itemView.findViewById(R.id.btn_promote);
            linearItemList = (CardView) itemView.findViewById(R.id.linear_item_list_listing);
            btnMenu = (ImageView) itemView.findViewById(R.id.ic_menu_item);
            txtCategoryType = (TextView) itemView.findViewById(R.id.text_category_type);
//            itemView.setOnClickListener(this);

            linearLayoutItemClick = (LinearLayout) itemView.findViewById(R.id.item_click);
        }

        @Override
        public void onClick(final View v) {

        }

    }

    @Override
    public ProjectRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_listing_property_fix, parent, false);
        final ProjectRequestHolder holder = new ProjectRequestHolder(v);
/*        final Context context = holder.header.getContext();*/
        ProjectRequestHolder crh = new ProjectRequestHolder(v);

        return crh;
    }

    @Override
    public void onBindViewHolder(final ProjectRequestHolder holder, final int position) {
        this.position = position + 1;
        holder.sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        holder.state = holder.sharedPreferences.getBoolean("isLogin", false);
        memberRef = holder.sharedPreferences.getString("isMemberRef", null);
        memberName = holder.sharedPreferences.getString("isName", null);
        final ListingPropertyModel propertyModel = list.get(position);
        holder.type.setText("[" + propertyModel.getListingTypeName() + "] " +propertyModel.getListingTitle());
        //holder.type.setText(propertyModel.getListingTypeName());
        //holder.property.setText(propertyModel.getListingTitle());
        listingRef = propertyModel.getListingRef();
        memberRefReceiver = propertyModel.getMemberRef();
        name = propertyModel.getMemberName();
//        holder.title.setText(propertyModel.getListingTitle());
        holder.car.setText(propertyModel.getGarageTypeName());
        holder.txtCategoryType.setText(propertyModel.getCategoryTypeName());

        /*holder.property.setTextSize(context.getResources().getDimension(R.dimen.textsize));
        holder.type.setTextSize(context.getResources().getDimension(R.dimen.textsize));*/

        maidBed = propertyModel.getMaidBRTypeName();
        if (maidBed.equals("0")) {
            holder.bed.setText(propertyModel.getBrTypeName());
        } else {
            holder.bed.setText(propertyModel.getBrTypeName() + " +" + propertyModel.getMaidBRTypeName());
        }
        bathMaid = propertyModel.getMaidBathRTypeName();
        if (bathMaid.equals("0")) {
            holder.bath.setText(propertyModel.getBathRTypeName());
        } else {
            holder.bath.setText(propertyModel.getBathRTypeName() + " +" + propertyModel.getMaidBathRTypeName());
        }

        //holder.facility.setText(propertyModel.getFacility());
        holder.facility.setText(propertyModel.getDesc());

        if (propertyModel.isFavorite()) {
            holder.btnFavorit.setVisibility(View.VISIBLE);
            holder.btnFavorit.setImageResource(R.drawable.ic_favorite_pink_500_24dp);
        } else {
            holder.btnFavorit.setVisibility(View.VISIBLE);
            holder.btnFavorit.setImageResource(R.drawable.ic_favorite_border_pink_500_24dp);
        }

        memberRefModel = propertyModel.getMemberRef();
        if (memberRef.equals(memberRefModel)) {
            holder.btnMenu.setVisibility(View.VISIBLE);
            holder.btnPromote.setVisibility(View.VISIBLE);
            holder.btnFavorit.setVisibility(View.GONE);
            holder.btnMessage.setVisibility(View.GONE);
            holder.btnCall.setVisibility(View.GONE);
        } else {
            holder.btnMenu.setVisibility(View.GONE);
            holder.btnPromote.setVisibility(View.GONE);
            holder.btnFavorit.setVisibility(View.VISIBLE);
            holder.btnMessage.setVisibility(View.VISIBLE);
            holder.btnCall.setVisibility(View.VISIBLE);
        }
        /*if (memberName.equals(name)) {
            holder.btnPromote.setVisibility(View.VISIBLE);
            holder.btnFavorit.setVisibility(View.GONE);
            holder.btnMessage.setVisibility(View.GONE);
            holder.btnCall.setVisibility(View.GONE);
        } else {
            holder.btnPromote.setVisibility(View.GONE);
            holder.btnFavorit.setVisibility(View.VISIBLE);
            holder.btnMessage.setVisibility(View.VISIBLE);
            holder.btnCall.setVisibility(View.VISIBLE);
        }*/

        holder.address.setText(propertyModel.getSubLocationName() + ", " + propertyModel.getCityName() + ", "
                + propertyModel.getProvinceName());
        holder.price.setText("Rp. " + propertyModel.getPrice() + " " + propertyModel.getPriceTypeName());
        holder.luas.setText(Html.fromHtml(String.valueOf(propertyModel.getBuildArea()) + " / " + Html.fromHtml(String.valueOf(propertyModel.getLandArea())) + " m<sup><small>2</small></sup>"));
        holder.memberName.setText(propertyModel.getMemberName());
        //hide my listing
        holder.memberName.setVisibility(View.GONE);
        holder.companyName.setVisibility(View.GONE);

        Glide.with(context)
                .load(propertyModel.getImageCover()).skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.imageHeader);
        holder.btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //phone call
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + propertyModel.getHp()));
                context.startActivity(intent);
            }
        });
        holder.btnMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChattingRoomActivity.class);
                intent.putExtra("memberRefSender", memberRef);
                intent.putExtra("memberRefReceiver", memberRefReceiver);
                intent.putExtra("name", name);
                context.startActivity(intent);
            }
        });

        /*holder.btnFavorit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (propertyModel.isFavorite()) {
                    holder.btnFavorit.setImageResource(R.drawable.ic_favorite_border_pink_500_24dp);
                    unFavorite();
                } else {
                    holder.btnFavorit.setImageResource(R.drawable.ic_favorite_pink_500_24dp);
                    favoriteStat();
                }

            }
        });*/
        isFav = "";
        realFav = false;
        holder.btnFavorit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFav != propertyModel.getListingRef()) {
                    realFav = propertyModel.isFavorite();
                }
                if (realFav) {
                    holder.btnFavorit.setImageResource(R.drawable.ic_favorite_border_pink_500_24dp);
                    unFavorite(propertyModel.getListingRef());
                    realFav = false;
                    isFav = propertyModel.getListingRef();
                    Log.d("listingRef",propertyModel.getListingRef());
                } else {
                    holder.btnFavorit.setImageResource(R.drawable.ic_favorite_pink_500_24dp);
                    favoriteStat(propertyModel.getListingRef());
                    realFav = true;
                    isFav = propertyModel.getListingRef();
                    Log.d("listingRef",propertyModel.getListingRef());
                }

            }
        });

        holder.btnPromote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Apakah anda ingin promote listing ini?");
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        holder.btnPromote.setVisibility(View.GONE);
                        NataApi service = NataService.getClient().create(NataApi.class);
                        final Call<UpdatePromote> call = service.updatePromote(propertyModel.getListingRef());
                        call.enqueue(new Callback<UpdatePromote>() {
                            @Override
                            public void onResponse(Call<UpdatePromote> call, retrofit2.Response<UpdatePromote> response) {
                                int status = response.body().getStatus();
                                String message = response.body().getMessage();
                                if (status == 200) {
                                    Intent intent = new Intent(context, ListingPropertyMemberActivity.class);
                                    intent.putExtra("listingRef", listingRef);
                                    intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                                    intent.putExtra("psRef", "");
                                    intent.putExtra("memberTypeCode", memberTypeCode);
                                    ((Activity) context).finish();
                                    context.startActivity(intent);
                                    Toast.makeText(context, "Promote has been successfull", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, "Sory you can't promote again. Please try after 3 hour.", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(Call<UpdatePromote> call, Throwable t) {

                            }
                        });

                    }
                });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });

        holder.linearLayoutItemClick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                listingRef = list.get(position).getListingRef();
                imageCover = list.get(position).getImageCover();
                listingTitle = propertyModel.getListingTitle() + " (" + propertyModel.getListingTypeName() + ")";
                listingMemberRef = list.get(position).getMemberRef();

                Intent intent = new Intent(context, ListingPropertyTabsActivity.class);
                intent.putExtra("listingTitle", listingTitle);
                intent.putExtra("listingRef", listingRef);
                intent.putExtra("imageCover", imageCover);
                intent.putExtra("listingMemberRef", listingMemberRef);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("memberTypeCode", memberTypeCode);
                intent.putExtra("addFrom", 1);
                intent.putExtra("memberRefOwner", memberRefModel);
                context.startActivity(intent);
            }
        });

        holder.btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popup = new PopupMenu(context, v);
                popup.getMenuInflater().inflate(R.menu.menu_item_listview, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.btn_delete:
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setMessage("Apakah anda ingin menghapus listing property?");
                                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        listingRef = list.get(position).getListingRef();
                                        deleteListing(listingRef, position);

                                    }
                                });
                                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                                return true;
                            //addFrom
                            //1 dari my listing
                            //2 dari agency listing
                            //3 dari cobroking
                            case R.id.btn_edit:

                                listingRef = list.get(position).getListingRef();
                                //agencyCompanyRef = list.get(position).getAgencyCompanyRef();
                                //agencyCompanyRef = list.get(position).getAgencyCompanyRef();
                                Intent intent = new Intent(context, ListingAddPropertyActivity.class);
                                intent.putExtra("listingRef", listingRef);
                                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                                intent.putExtra("memberTypeCode", memberTypeCode);
                                intent.putExtra("status", "edit");
                                intent.putExtra("addFrom", 1);
                                context.startActivity(intent);

                                return true;

                            case R.id.btn_mark_as:
                                listingRef = list.get(position).getListingRef();

                                popupListMarkAs(position);

                                return true;

                        }
                        return false;
                    }
                });
                popup.show();
            }
        });


    }

    private void favoriteStat(String listingRef) {
        NataApi service = NataService.getClient().create(NataApi.class);
        final Call<UpdatePromote> call = service.updateFavorite(listingRef, memberRef);
        call.enqueue(new Callback<UpdatePromote>() {
            @Override
            public void onResponse(Call<UpdatePromote> call, retrofit2.Response<UpdatePromote> response) {
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200) {
                    Toast.makeText(context, "Tambah ke favorite", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Failed. Please check your network", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<UpdatePromote> call, Throwable t) {

            }
        });
    }

    private void unFavorite(String listingRef) {
        NataApi service = NataService.getClient().create(NataApi.class);
        final Call<UpdatePromote> call = service.updateFavorite(listingRef, memberRef);
        call.enqueue(new Callback<UpdatePromote>() {
            @Override
            public void onResponse(Call<UpdatePromote> call, retrofit2.Response<UpdatePromote> response) {
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200) {
                    Toast.makeText(context, "Hapus dari favorite", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Failed. Please check your network", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<UpdatePromote> call, Throwable t) {

            }
        });
    }

    private void popupListMarkAs(final int position) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context,
                android.R.layout.select_dialog_item);
        arrayAdapter.add("Not Active");
        arrayAdapter.add("Sold / Rented");
        arrayAdapter.add("Closed");

        builderSingle.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //listingStatus
                        listingStatus = which + 2;
                        String strName = arrayAdapter.getItem(which);
                        if (strName.equals("Not Active")) {
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                            builderInner.setMessage("Not Active adalah status sementara, dan property ini tidak akan muncul di listing Aplikasi.\n" +
                                    "Silakan mengubah status menjadi Active kembali via Web Dashboard anda.");
                            builderInner.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            updateListingStatus(listingStatus, position);
                                            dialog.dismiss();
                                        }
                                    });
                            builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderInner.show();

                        } else if (strName.equals("Sold / Rented")) {
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                            builderInner.setMessage("Apakah anda ingin mengubah ke status sold / rented ?");
                            builderInner.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            updateListingStatus(listingStatus, position);
                                            dialog.dismiss();
                                        }
                                    });
                            builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderInner.show();

                        } else if (strName.equals("Closed")) {
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(context);
                            builderInner.setMessage("Dengan mengubah status mennjadi closed, " +
                                    "maka listing ini tidak bisa diaktifkan kembali.");
                            builderInner.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            updateListingStatus(listingStatus, position);
                                            dialog.dismiss();
                                        }
                                    });
                            builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderInner.show();
                        } else {

                        }


                    }
                });
        builderSingle.show();
    }

    public void updateListingStatus(final int listingStatus, final int position) {
//        BaseApplication.getInstance().startLoader(context);
        LoadingBar.startLoader(context);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.updateStatusListingProperty(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    BaseApplication.getInstance().stopLoader();
                    LoadingBar.stopLoader();
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        list.remove(position);
                        notifyDataSetChanged();
//                        notifyDataSetInvalidated();
                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context, context.getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);
                params.put("listingStatus", String.valueOf(listingStatus));
                params.put("closingPrice", closingPrice);
                params.put("memberRef", memberRef);

                Log.d("param", listingRef + " - " + listingStatus + " - " + closingPrice + " - " + memberRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "insertFacility");

    }

    public void deleteListing(final String listingRef, final int position) {
//        BaseApplication.getInstance().startLoader(context);
        LoadingBar.startLoader(context);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.deleteListingProperty(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    BaseApplication.getInstance().stopLoader();
                    LoadingBar.stopLoader();
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        list.remove(position);
                        notifyDataSetChanged();

                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            //Toast.makeText(ListingAddGalleryActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "deleteListing");

    }

    private void unSubscribe(final String memberRef, final String dbMasterRef, final String projectRef) {

        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.unSubscribeProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getListAutoComplite", "" + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result detail project", response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        //list.notify();
                        Intent intent = new Intent(context, MyListingActivity.class);
                        ((Activity) context).finish();
                        context.startActivity(intent);
                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(context, context.getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(context, context.getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("dbMasterRef", dbMasterRef.toString());
                params.put("projectRef", projectRef.toString());

                Log.d("MyProject", memberRef + " " + dbMasterRef + " " + projectRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "project");


    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


}
