package com.nataproperti.crm360.view.listing.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.view.listing.model.ChatHistoryDeleteResponeModel;
import com.nataproperti.crm360.view.listing.model.ChatHistoryModel;
import com.nataproperti.crm360.config.NataApi;
import com.nataproperti.crm360.config.NataService;
import com.nataproperti.crm360.view.listing.ui.ChattingRoomActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/10/2016.
 */

public class RvChatHistoryAdapter extends RecyclerView.Adapter<RvChatHistoryAdapter.ViewHolder> {
    public static final String TAG = "RvMyTicketAdapter";
    public static final String PREF_NAME = "pref";

    private Context context;
    private List<ChatHistoryModel> list;
    private Display display;

    public RvChatHistoryAdapter(Context context, List<ChatHistoryModel> list,Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout;
        ImageView imageProfile;
        TextView txtName, txtLastChat, txtLatTime,txtCount;
        Context context;
        LinearLayout linearLayoutCount;
        SharedPreferences sharedPreferences;

        ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();

            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout);
            imageProfile= (ImageView) itemView.findViewById(R.id.image_profile);
            txtName = (TextView) itemView.findViewById(R.id.txt_name);
            txtLastChat = (TextView) itemView.findViewById(R.id.txt_last_chat);
            txtLatTime = (TextView) itemView.findViewById(R.id.txt_last_time);
            txtCount = (TextView) itemView.findViewById(R.id.txt_count);
            linearLayoutCount = (LinearLayout) itemView.findViewById(R.id.linear_count);

        }
    }

    @Override
    public RvChatHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_history_chat, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return (null != list ? list.size() : 0);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String chatRef,name, lastChat, lastTime, memberRef,count,memberRefReceiver;
        holder.sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        final String memberRefSender = holder.sharedPreferences.getString("isMemberRef", null);

        final ChatHistoryModel chatHistoryModel = list.get(position);
        chatRef = chatHistoryModel.getChatRef();
        name = chatHistoryModel.getMemberNameReceiver();
        lastChat = chatHistoryModel.getLastChat();
        lastTime = chatHistoryModel.getLastTime();
        memberRef = chatHistoryModel.getMemberRefReceiver();
        count = chatHistoryModel.getNewMessageCount();
        memberRefReceiver = chatHistoryModel.getMemberRefReceiver();

        holder.txtName.setText(name);
        holder.txtLastChat.setText(lastChat);
        holder.txtLatTime.setText(lastTime);

        if (count.equals("0")){
            holder.linearLayoutCount.setVisibility(View.INVISIBLE);
        } else {
            holder.txtCount.setText(count);
        }

        Glide.with(context).load(WebService.getProfile() + memberRef).asBitmap()
                .centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(new BitmapImageViewTarget(holder.imageProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                        .create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                holder.imageProfile.setImageDrawable(circularBitmapDrawable);
            }
        });
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChattingRoomActivity.class);
                intent.putExtra("memberRefSender", memberRefSender);
                intent.putExtra("memberRefReceiver", memberRefReceiver);
                intent.putExtra("name", name);
                context.startActivity(intent);
            }
        });

        holder.relativeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder alertDialogBuilder =
                        new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Apakah anda ingin hapus history chat?");
                alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NataApi service = NataService.getClient().create(NataApi.class);
                        final Call<ChatHistoryDeleteResponeModel> call = service.deleteMyAgencyChatHistory(chatRef);
                        call.enqueue(new Callback<ChatHistoryDeleteResponeModel>() {
                            @Override
                            public void onResponse(Call<ChatHistoryDeleteResponeModel> call, Response<ChatHistoryDeleteResponeModel> response) {
                                int status = response.body().getStatus();
                                String message = response.body().getMessage();

                                if (status == 200) {
                                    //Toast.makeText(context,position, Toast.LENGTH_SHORT).show();
                                    list.remove(position);
                                    notifyDataSetChanged();
                                } else {
                                    Toast.makeText(context,message, Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(Call<ChatHistoryDeleteResponeModel> call, Throwable t) {

                            }
                        });

                    }
                });
                alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                return false;
            }
        });

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;

        ViewGroup.LayoutParams params = holder.relativeLayout.getLayoutParams();
        params.width = width;
        //params.height = width.intValue() ;
        holder.relativeLayout.setLayoutParams(params);
        holder.relativeLayout.requestLayout();

    }
}
