package com.nataproperti.crm360.view.listing.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.view.listing.model.ListingMemberInfoModel;
import com.nataproperti.crm360.view.listing.ui.ChattingRoomActivity;

import java.util.List;

/**
 * Created by nata on 11/10/2016.
 */

public class RvContactMessageAdapter extends RecyclerView.Adapter<RvContactMessageAdapter.ViewHolder> {
    public static final String TAG = "RvMyTicketAdapter";
    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";

    private Context context;
    private List<ListingMemberInfoModel> list;
    private Display display;
    private String memberTypeCode;

    public RvContactMessageAdapter(Context context, List<ListingMemberInfoModel> list, Display display,String memberTypeCode) {
        this.context = context;
        this.list = list;
        this.display = display;
        this.memberTypeCode = memberTypeCode;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout relativeLayout;
        ImageView imageProfile;
        TextView txtName, txtEmail, txtPhone;
        Context context;
        SharedPreferences sharedPreferences;


        ViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();

            relativeLayout = (LinearLayout) itemView.findViewById(R.id.relativeLayout);
            imageProfile = (ImageView) itemView.findViewById(R.id.image_profile);
            txtName = (TextView) itemView.findViewById(R.id.txt_name);
            txtEmail = (TextView) itemView.findViewById(R.id.txt_email);
            txtPhone = (TextView) itemView.findViewById(R.id.txt_phone);

        }
    }

    @Override
    public RvContactMessageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_contact_chat, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return (null != list ? list.size() : 0);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String memberRefReceiver, name, phone;
        holder.sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        final String memberRefSender = holder.sharedPreferences.getString("isMemberRef", null);

        final ListingMemberInfoModel listingMemberInfoModel = list.get(position);
        holder.txtName.setText(listingMemberInfoModel.getName());

        if (memberTypeCode.equals("2")){
            holder.txtEmail.setText(listingMemberInfoModel.getEmail());
        } else {
            holder.txtEmail.setVisibility(View.GONE);
            holder.txtPhone.setVisibility(View.GONE);
        }

        holder.txtPhone.setText(listingMemberInfoModel.getHp1());
        memberRefReceiver = listingMemberInfoModel.getMemberRef();
        name = listingMemberInfoModel.getName();
//        name = listingMemberInfoModel.getName();
//        email = listingMemberInfoModel.getLastChat();
//        phone = listingMemberInfoModel.getLastTime();
//        final String memberRefReceiver = chatHistoryModel.getMemberRefReceiver();
//
//        holder.txtName.setText(name);
//        holder.txtLastChat.setText(lastChat);
//        holder.txtLatTime.setText(lastTime);
//
        Glide.with(context).load(WebService.getProfile() + memberRefReceiver).asBitmap()
                .centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(new BitmapImageViewTarget(holder.imageProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                        .create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                holder.imageProfile.setImageDrawable(circularBitmapDrawable);
            }
        });
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChattingRoomActivity.class);
                intent.putExtra("memberRefSender", memberRefSender);
                intent.putExtra("memberRefReceiver", memberRefReceiver);
                intent.putExtra("name", name);
                context.startActivity(intent);
            }
        });

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;

        ViewGroup.LayoutParams params = holder.relativeLayout.getLayoutParams();
        params.width = width;
        //params.height = width.intValue() ;
        holder.relativeLayout.setLayoutParams(params);
        holder.relativeLayout.requestLayout();

    }
}
