package com.nataproperti.crm360.view.listing.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterAddGaleryInteractor {
    void postIsCobroke(String listingRef,String memberRef,String isCobroke);
    void rxUnSubscribe();

}
