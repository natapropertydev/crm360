package com.nataproperti.crm360.view.listing.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterAddMapsInteractor {
    void saveMaps(String listingRef,String latitude,String longitude);
    void getInfoMap(String listingRef);
    void rxUnSubscribe();

}
