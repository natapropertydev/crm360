package com.nataproperti.crm360.view.listing.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterChattingRoomInteractor {
    void getSendMsg(String memberRefSender, String memberRefReceiver, String messaging,int pageNo);
    void getMsgFromServer(String memberRefSender, String memberRefReceiver);
    void postSendMsg(String memberRefSender, String memberRefReceiver, String messaging);
    void rxUnSubscribe();

}
