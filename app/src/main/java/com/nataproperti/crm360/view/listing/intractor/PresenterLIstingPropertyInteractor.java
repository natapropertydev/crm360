package com.nataproperti.crm360.view.listing.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterLIstingPropertyInteractor {
    void getSearchListing(String agencyCompanyRef,String listingStatus,String pageNo
            ,String provinceCode,String cityCode,String locationRef,String listingTypeRef,String categoryType,String memberRef);
    void getLocation(String memberRef);
    void getLookupListingPropertySvc();
    void getLocationListing(String agencyCompanyRef);
    void getListProperty(String agencyCompanyRef,String psRef,String listingStatus,String pageNo,String locationRef,String keyword);
    void rxUnSubscribe();

}
