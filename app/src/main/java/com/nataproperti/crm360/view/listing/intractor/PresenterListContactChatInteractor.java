package com.nataproperti.crm360.view.listing.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterListContactChatInteractor {
    void getContactChat(String agencyCompanyRef);
    void getContactChatPage(String agencyCompanyRef,int pageNo);
    void rxUnSubscribe();

}
