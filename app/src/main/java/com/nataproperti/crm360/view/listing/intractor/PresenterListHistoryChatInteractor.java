package com.nataproperti.crm360.view.listing.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterListHistoryChatInteractor {
    void getHistoryChat(String memberRef);
    void rxUnSubscribe();

}
