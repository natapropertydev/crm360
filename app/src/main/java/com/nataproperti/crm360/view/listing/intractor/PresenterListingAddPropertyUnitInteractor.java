package com.nataproperti.crm360.view.listing.intractor;

import java.util.Map;

/**
 * Created by Nata on 12/6/2016.
 */

public interface PresenterListingAddPropertyUnitInteractor {
    void getListingAddPropertyUnitSvc();
    void getListingPropertyUnitInfoSvc(String listingRef);
    void saveListingInformasiUnit(Map<String, String> fields,int btnStatus);
    void rxUnSubscribe();
}
