package com.nataproperti.crm360.view.listing.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterPropertyMapsInteractor {
    void getInfoMap(String listingRef);
    void rxUnSubscribe();

}
