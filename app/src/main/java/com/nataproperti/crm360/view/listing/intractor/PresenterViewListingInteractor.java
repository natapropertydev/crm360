package com.nataproperti.crm360.view.listing.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterViewListingInteractor {
    void getListingMember(String agencyCompanyRef, String memberRef, String pageNo, String locationRef);
    void rxUnSubscribe();

}
