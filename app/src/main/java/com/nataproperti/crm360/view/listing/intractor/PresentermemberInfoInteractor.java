package com.nataproperti.crm360.view.listing.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresentermemberInfoInteractor {
    void getProvinsiListing(String memberRef);
    void getCityListing ( String countryCode,String provinceCode);
    void rxUnSubscribe();

}
