package com.nataproperti.crm360.view.listing.model;

import java.util.List;

/**
 * Created by nata on 12/19/2016.
 */
public class AddressComponent {
    private String longName;
    private String shortName;
    private List<String> types = null;

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }
}
