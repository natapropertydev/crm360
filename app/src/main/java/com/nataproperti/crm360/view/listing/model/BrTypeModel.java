package com.nataproperti.crm360.view.listing.model;

/**
 * Created by Nata on 12/6/2016.
 */

public class BrTypeModel {
    String brType;
    String brTypeName;

    public String getBrTypeName() {
        return brTypeName;
    }

    public void setBrTypeName(String brTypeName) {
        this.brTypeName = brTypeName;
    }

    public String getBrType() {
        return brType;
    }

    public void setBrType(String brType) {
        this.brType = brType;
    }
}
