package com.nataproperti.crm360.view.listing.model;

/**
 * Created by Nata on 12/8/2016.
 */

public class CategoryTypeModel {
    String categoryType,categoryTypeName;

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCategoryTypeName() {
        return categoryTypeName;
    }

    public void setCategoryTypeName(String categoryTypeName) {
        this.categoryTypeName = categoryTypeName;
    }
}
