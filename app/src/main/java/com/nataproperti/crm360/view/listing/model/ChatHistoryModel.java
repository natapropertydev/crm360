package com.nataproperti.crm360.view.listing.model;

/**
 * Created by nata on 11/17/2016.
 */
public class ChatHistoryModel {
    long idChatHistory;
    String chatRef;
    String memberRefReceiver;
    String memberNameReceiver;
    String lastChat;
    String lastTime;
    String newMessageCount;

    public String getNewMessageCount() {
        return newMessageCount;
    }

    public void setNewMessageCount(String newMessageCount) {
        this.newMessageCount = newMessageCount;
    }

    public long getIdChatHistory() {
        return idChatHistory;
    }

    public void setIdChatHistory(long idChatHistory) {
        this.idChatHistory = idChatHistory;
    }

    public String getChatRef() {
        return chatRef;
    }

    public void setChatRef(String chatRef) {
        this.chatRef = chatRef;
    }

    public String getMemberRefReceiver() {
        return memberRefReceiver;
    }

    public void setMemberRefReceiver(String memberRefReceiver) {
        this.memberRefReceiver = memberRefReceiver;
    }

    public String getMemberNameReceiver() {
        return memberNameReceiver;
    }

    public void setMemberNameReceiver(String memberNameReceiver) {
        this.memberNameReceiver = memberNameReceiver;
    }

    public String getLastChat() {
        return lastChat;
    }

    public void setLastChat(String lastChat) {
        this.lastChat = lastChat;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }
}
