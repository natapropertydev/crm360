package com.nataproperti.crm360.view.listing.model;

import com.nataproperti.crm360.view.project.model.ChatRoomModel;

/**
 * Created by nata on 12/22/2016.
 */
public class ChatSingleModelStatus {
    private String status;
    private String message;
    private ChatRoomModel data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ChatRoomModel getData() {
        return data;
    }

    public void setData(ChatRoomModel data) {
        this.data = data;
    }
}
