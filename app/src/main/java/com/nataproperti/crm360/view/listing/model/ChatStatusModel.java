package com.nataproperti.crm360.view.listing.model;

import com.nataproperti.crm360.view.project.model.ChatRoomModel;

import java.util.List;

/**
 * Created by nata on 12/22/2016.
 */
public class ChatStatusModel {
    private String status;
    private String message;
    private String count;
    private String totalPage;
    private List<ChatRoomModel> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<ChatRoomModel> getData() {
        return data;
    }

    public void setData(List<ChatRoomModel> data) {
        this.data = data;
    }
}
