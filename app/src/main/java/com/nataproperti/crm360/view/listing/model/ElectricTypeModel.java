package com.nataproperti.crm360.view.listing.model;

/**
 * Created by Nata on 12/6/2016.
 */

public class ElectricTypeModel {
    String electricType;
    String electricTypeName;

    public String getElectricTypeName() {
        return electricTypeName;
    }

    public void setElectricTypeName(String electricTypeName) {
        this.electricTypeName = electricTypeName;
    }

    public String getElectricType() {
        return electricType;
    }

    public void setElectricType(String electricType) {
        this.electricType = electricType;
    }
}
