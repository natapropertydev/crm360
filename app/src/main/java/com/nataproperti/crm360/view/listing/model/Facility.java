package com.nataproperti.crm360.view.listing.model;

/**
 * Created by nata on 12/19/2016.
 */
public class Facility {
    private String facility;
    private String facilityName;

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }
}
