package com.nataproperti.crm360.view.listing.model;

/**
 * Created by Nata on 12/6/2016.
 */

public class FacingTypeModel {
    String facingType;
    String facingName;

    public String getFacingType() {
        return facingType;
    }

    public void setFacingType(String facingType) {
        this.facingType = facingType;
    }

    public String getFacingName() {
        return facingName;
    }

    public void setFacingName(String facingName) {
        this.facingName = facingName;
    }
}
