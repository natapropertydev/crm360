package com.nataproperti.crm360.view.listing.model;

import java.util.List;

/**
 * Created by nata on 12/19/2016.
 */

public class FindLocation {
    private List<Object> htmlAttributions = null;
    private Result result;
    private String status;

    public List<Object> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<Object> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
