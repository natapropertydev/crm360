package com.nataproperti.crm360.view.listing.model;

/**
 * Created by nata on 12/19/2016.
 */
public class Geometry {

    private Locations location;
    private Viewport viewport;

    public Locations getLocation() {
        return location;
    }

    public void setLocation(Locations location) {
        this.location = location;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void setViewport(Viewport viewport) {
        this.viewport = viewport;
    }
}
