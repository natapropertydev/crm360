package com.nataproperti.crm360.view.listing.model;


import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nata on 12/16/2016.
 */

public class GooglePlaceResult {
    @Expose
    private List<Predictions> predictions = new ArrayList<>();
    @Expose
    private String status;

    public List<Predictions> getPredictions() {
        return predictions;
    }

    public void setPredictions(List<Predictions> predictions) {
        this.predictions = predictions;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
