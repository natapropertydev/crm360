package com.nataproperti.crm360.view.listing.model;

import com.nataproperti.crm360.view.project.model.IlustrationProductModel;

/**
 * Created by nata on 11/29/2016.
 */

public class IlustrationStatusModel {
    int status;
    String message;
    IlustrationProductModel data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IlustrationProductModel getData() {
        return data;
    }

    public void setData(IlustrationProductModel data) {
        this.data = data;
    }
}
