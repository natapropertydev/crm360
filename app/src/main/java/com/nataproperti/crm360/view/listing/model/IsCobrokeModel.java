package com.nataproperti.crm360.view.listing.model;

/**
 * Created by nata on 12/14/2016.
 */
public class IsCobrokeModel {
    String status;
    String message;
    String listingRef;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getListingRef() {
        return listingRef;
    }

    public void setListingRef(String listingRef) {
        this.listingRef = listingRef;
    }
}
