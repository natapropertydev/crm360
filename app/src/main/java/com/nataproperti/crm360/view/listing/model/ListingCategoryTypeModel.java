package com.nataproperti.crm360.view.listing.model;

/**
 * Created by User on 10/6/2016.
 */
public class ListingCategoryTypeModel {
    long id;
    String categoryType, categoryTypeGroup, categoryTypeName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCategoryTypeGroup() {
        return categoryTypeGroup;
    }

    public void setCategoryTypeGroup(String categoryTypeGroup) {
        this.categoryTypeGroup = categoryTypeGroup;
    }

    public String getCategoryTypeName() {
        return categoryTypeName;
    }

    public void setCategoryTypeName(String categoryTypeName) {
        this.categoryTypeName = categoryTypeName;
    }
}
