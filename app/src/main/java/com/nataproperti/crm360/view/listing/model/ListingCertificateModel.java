package com.nataproperti.crm360.view.listing.model;

/**
 * Created by User on 10/6/2016.
 */
public class ListingCertificateModel {
    long id;
    String certRef, certName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCertRef() {
        return certRef;
    }

    public void setCertRef(String certRef) {
        this.certRef = certRef;
    }

    public String getCertName() {
        return certName;
    }

    public void setCertName(String certName) {
        this.certName = certName;
    }
}
