package com.nataproperti.crm360.view.listing.model;

/**
 * Created by User on 10/6/2016.
 */
public class ListingListingTypeModel {
    long id;
    String listingTypeRef, listingTypeName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getListingTypeRef() {
        return listingTypeRef;
    }

    public void setListingTypeRef(String listingTypeRef) {
        this.listingTypeRef = listingTypeRef;
    }

    public String getListingTypeName() {
        return listingTypeName;
    }

    public void setListingTypeName(String listingTypeName) {
        this.listingTypeName = listingTypeName;
    }
}
