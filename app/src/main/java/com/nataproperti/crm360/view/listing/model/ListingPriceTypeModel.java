package com.nataproperti.crm360.view.listing.model;

/**
 * Created by User on 10/6/2016.
 */
public class ListingPriceTypeModel {
    long id;

    String priceType, priceTypeName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getPriceTypeName() {
        return priceTypeName;
    }

    public void setPriceTypeName(String priceTypeName) {
        this.priceTypeName = priceTypeName;
    }
}
