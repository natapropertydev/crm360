package com.nataproperti.crm360.view.listing.model;

/**
 * Created by User on 7/26/2016.
 */
public class ListingPropertyFeatureModel {
    String projectFeature,projectFeatureName;

    public String getProjectFeature() {
        return projectFeature;
    }

    public void setProjectFeature(String projectFeature) {
        this.projectFeature = projectFeature;
    }

    public String getProjectFeatureName() {
        return projectFeatureName;
    }

    public void setProjectFeatureName(String projectFeatureName) {
        this.projectFeatureName = projectFeatureName;
    }
}
