package com.nataproperti.crm360.view.listing.model;

/**
 * Created by User on 7/26/2016.
 */
public class ListingPropertyGalleryModel {
    String imageRef,imageTitle,imageFile,listingRef;

    public String getImageRef() {
        return imageRef;
    }

    public void setImageRef(String imageRef) {
        this.imageRef = imageRef;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public String getListingRef() {
        return listingRef;
    }

    public void setListingRef(String listingRef) {
        this.listingRef = listingRef;
    }
}
