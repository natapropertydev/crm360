package com.nataproperti.crm360.view.listing.model;

/**
 * Created by Nata on 12/8/2016.
 */

public class ListingTypeModel {
    String listingTypeRef,listingTypeName;

    public String getListingTypeRef() {
        return listingTypeRef;
    }

    public void setListingTypeRef(String listingTypeRef) {
        this.listingTypeRef = listingTypeRef;
    }

    public String getListingTypeName() {
        return listingTypeName;
    }

    public void setListingTypeName(String listingTypeName) {
        this.listingTypeName = listingTypeName;
    }
}
