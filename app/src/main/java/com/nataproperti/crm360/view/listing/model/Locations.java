package com.nataproperti.crm360.view.listing.model;

/**
 * Created by nata on 12/19/2016.
 */
public class Locations {
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
