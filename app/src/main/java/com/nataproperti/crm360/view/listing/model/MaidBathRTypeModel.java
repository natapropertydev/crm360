package com.nataproperti.crm360.view.listing.model;

/**
 * Created by Nata on 12/6/2016.
 */

public class MaidBathRTypeModel {
    String maidBathRType;
    String maidBathRTypeName;

    public String getMaidBathRType() {
        return maidBathRType;
    }

    public void setMaidBathRType(String maidBathRType) {
        this.maidBathRType = maidBathRType;
    }

    public String getMaidBathRTypeName() {
        return maidBathRTypeName;
    }

    public void setMaidBathRTypeName(String maidBathRTypeName) {
        this.maidBathRTypeName = maidBathRTypeName;
    }
}
