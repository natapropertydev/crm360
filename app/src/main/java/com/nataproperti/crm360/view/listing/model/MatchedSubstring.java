package com.nataproperti.crm360.view.listing.model;

import com.google.gson.annotations.Expose;

/**
 * Created by nata on 12/16/2016.
 */
public class MatchedSubstring {
    @Expose
    private Integer length;
    @Expose
    private Integer offset;

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}
