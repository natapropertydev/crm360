package com.nataproperti.crm360.view.listing.model;

/**
 * Created by Nata on 12/6/2016.
 */

public class PhoneLineModel {
    String phoneLine;
    String phoneLineName;

    public String getPhoneLine() {
        return phoneLine;
    }

    public void setPhoneLine(String phoneLine) {
        this.phoneLine = phoneLine;
    }

    public String getPhoneLineName() {
        return phoneLineName;
    }

    public void setPhoneLineName(String phoneLineName) {
        this.phoneLineName = phoneLineName;
    }
}
