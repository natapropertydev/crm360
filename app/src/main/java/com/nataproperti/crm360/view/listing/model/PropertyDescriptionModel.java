package com.nataproperti.crm360.view.listing.model;

import java.util.List;

/**
 * Created by nata on 12/19/2016.
 */

public class PropertyDescriptionModel {
    private String status;
    private String message;
    private String listingTypeName;
    private String categoryTypeName;
    private String propertyRef;
    private String certName;
    private String priceTypeName;
    private String listingTitle;
    private String listingDesc;
    private String countryName;
    private String provinceName;
    private String cityName;
    private String subLocationName;
    private String block;
    private String no;
    private String postCode;
    private String price;
    private String buildArea;
    private String buildDimX;
    private String buildDimY;
    private String landArea;
    private String landDimX;
    private String landDimY;
    private String brTypeName;
    private String maidBRTypeName;
    private String bathRTypeName;
    private String maidBathRTypeName;
    private String garageTypeName;
    private String carportTypeName;
    private String phoneLineName;
    private String furnishTypeName;
    private String electricTypeName;
    private String facingName;
    private String viewTypeName;
    private String youTube1;
    private String youTube2;
    private String address;
    private String latitude;
    private String longitude;
    private String countDocument;
    private String listingTypeRef;
    private String categoryType;
    private String certRef;
    private String priceType;
    private String provinceSortNo;
    private String citySortNo;
    private String countrySortNo;
    private String propertyName;
    private String countryCode;
    private String proviceCode;
    private String cityCode;
    private String subLocationRef;
    private String isCobroke;
    private String priceFormat;
    private String buildAreaFormat;
    private String landAreaFormat;
    private String memberRefOwner;
    private String memberNameOwner;
    private String agencyCompanyRefOwner;
    private Integer imageCount;
    private Integer documentCount;
    private List<Facility> facility;
    private boolean favorite;
    private String hp;

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getListingTypeName() {
        return listingTypeName;
    }

    public void setListingTypeName(String listingTypeName) {
        this.listingTypeName = listingTypeName;
    }

    public String getCategoryTypeName() {
        return categoryTypeName;
    }

    public void setCategoryTypeName(String categoryTypeName) {
        this.categoryTypeName = categoryTypeName;
    }

    public String getPropertyRef() {
        return propertyRef;
    }

    public void setPropertyRef(String propertyRef) {
        this.propertyRef = propertyRef;
    }

    public String getCertName() {
        return certName;
    }

    public void setCertName(String certName) {
        this.certName = certName;
    }

    public String getPriceTypeName() {
        return priceTypeName;
    }

    public void setPriceTypeName(String priceTypeName) {
        this.priceTypeName = priceTypeName;
    }

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }

    public String getListingDesc() {
        return listingDesc;
    }

    public void setListingDesc(String listingDesc) {
        this.listingDesc = listingDesc;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getSubLocationName() {
        return subLocationName;
    }

    public void setSubLocationName(String subLocationName) {
        this.subLocationName = subLocationName;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBuildArea() {
        return buildArea;
    }

    public void setBuildArea(String buildArea) {
        this.buildArea = buildArea;
    }

    public String getBuildDimX() {
        return buildDimX;
    }

    public void setBuildDimX(String buildDimX) {
        this.buildDimX = buildDimX;
    }

    public String getBuildDimY() {
        return buildDimY;
    }

    public void setBuildDimY(String buildDimY) {
        this.buildDimY = buildDimY;
    }

    public String getLandArea() {
        return landArea;
    }

    public void setLandArea(String landArea) {
        this.landArea = landArea;
    }

    public String getLandDimX() {
        return landDimX;
    }

    public void setLandDimX(String landDimX) {
        this.landDimX = landDimX;
    }

    public String getLandDimY() {
        return landDimY;
    }

    public void setLandDimY(String landDimY) {
        this.landDimY = landDimY;
    }

    public String getBrTypeName() {
        return brTypeName;
    }

    public void setBrTypeName(String brTypeName) {
        this.brTypeName = brTypeName;
    }

    public String getMaidBRTypeName() {
        return maidBRTypeName;
    }

    public void setMaidBRTypeName(String maidBRTypeName) {
        this.maidBRTypeName = maidBRTypeName;
    }

    public String getBathRTypeName() {
        return bathRTypeName;
    }

    public void setBathRTypeName(String bathRTypeName) {
        this.bathRTypeName = bathRTypeName;
    }

    public String getMaidBathRTypeName() {
        return maidBathRTypeName;
    }

    public void setMaidBathRTypeName(String maidBathRTypeName) {
        this.maidBathRTypeName = maidBathRTypeName;
    }

    public String getGarageTypeName() {
        return garageTypeName;
    }

    public void setGarageTypeName(String garageTypeName) {
        this.garageTypeName = garageTypeName;
    }

    public String getCarportTypeName() {
        return carportTypeName;
    }

    public void setCarportTypeName(String carportTypeName) {
        this.carportTypeName = carportTypeName;
    }

    public String getPhoneLineName() {
        return phoneLineName;
    }

    public void setPhoneLineName(String phoneLineName) {
        this.phoneLineName = phoneLineName;
    }

    public String getFurnishTypeName() {
        return furnishTypeName;
    }

    public void setFurnishTypeName(String furnishTypeName) {
        this.furnishTypeName = furnishTypeName;
    }

    public String getElectricTypeName() {
        return electricTypeName;
    }

    public void setElectricTypeName(String electricTypeName) {
        this.electricTypeName = electricTypeName;
    }

    public String getFacingName() {
        return facingName;
    }

    public void setFacingName(String facingName) {
        this.facingName = facingName;
    }

    public String getViewTypeName() {
        return viewTypeName;
    }

    public void setViewTypeName(String viewTypeName) {
        this.viewTypeName = viewTypeName;
    }

    public String getYouTube1() {
        return youTube1;
    }

    public void setYouTube1(String youTube1) {
        this.youTube1 = youTube1;
    }

    public String getYouTube2() {
        return youTube2;
    }

    public void setYouTube2(String youTube2) {
        this.youTube2 = youTube2;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCountDocument() {
        return countDocument;
    }

    public void setCountDocument(String countDocument) {
        this.countDocument = countDocument;
    }

    public String getListingTypeRef() {
        return listingTypeRef;
    }

    public void setListingTypeRef(String listingTypeRef) {
        this.listingTypeRef = listingTypeRef;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCertRef() {
        return certRef;
    }

    public void setCertRef(String certRef) {
        this.certRef = certRef;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getProvinceSortNo() {
        return provinceSortNo;
    }

    public void setProvinceSortNo(String provinceSortNo) {
        this.provinceSortNo = provinceSortNo;
    }

    public String getCitySortNo() {
        return citySortNo;
    }

    public void setCitySortNo(String citySortNo) {
        this.citySortNo = citySortNo;
    }

    public String getCountrySortNo() {
        return countrySortNo;
    }

    public void setCountrySortNo(String countrySortNo) {
        this.countrySortNo = countrySortNo;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getProviceCode() {
        return proviceCode;
    }

    public void setProviceCode(String proviceCode) {
        this.proviceCode = proviceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getSubLocationRef() {
        return subLocationRef;
    }

    public void setSubLocationRef(String subLocationRef) {
        this.subLocationRef = subLocationRef;
    }

    public String getIsCobroke() {
        return isCobroke;
    }

    public void setIsCobroke(String isCobroke) {
        this.isCobroke = isCobroke;
    }

    public String getPriceFormat() {
        return priceFormat;
    }

    public void setPriceFormat(String priceFormat) {
        this.priceFormat = priceFormat;
    }

    public String getBuildAreaFormat() {
        return buildAreaFormat;
    }

    public void setBuildAreaFormat(String buildAreaFormat) {
        this.buildAreaFormat = buildAreaFormat;
    }

    public String getLandAreaFormat() {
        return landAreaFormat;
    }

    public void setLandAreaFormat(String landAreaFormat) {
        this.landAreaFormat = landAreaFormat;
    }

    public String getMemberRefOwner() {
        return memberRefOwner;
    }

    public void setMemberRefOwner(String memberRefOwner) {
        this.memberRefOwner = memberRefOwner;
    }

    public String getAgencyCompanyRefOwner() {
        return agencyCompanyRefOwner;
    }

    public void setAgencyCompanyRefOwner(String agencyCompanyRefOwner) {
        this.agencyCompanyRefOwner = agencyCompanyRefOwner;
    }

    public Integer getImageCount() {
        return imageCount;
    }

    public void setImageCount(Integer imageCount) {
        this.imageCount = imageCount;
    }

    public Integer getDocumentCount() {
        return documentCount;
    }

    public void setDocumentCount(Integer documentCount) {
        this.documentCount = documentCount;
    }

    public List<Facility> getFacility() {
        return facility;
    }

    public void setFacility(List<Facility> facility) {
        this.facility = facility;
    }

    public String getMemberNameOwner() {
        return memberNameOwner;
    }

    public void setMemberNameOwner(String memberNameOwner) {
        this.memberNameOwner = memberNameOwner;
    }
}
