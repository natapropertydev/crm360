package com.nataproperti.crm360.view.listing.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Nata on 12/6/2016.
 */

public class ResponeLookupListingPropertyModel {
    int status ;
    String message;
    @SerializedName("listingType")
    ArrayList<ListingListingTypeModel>  listListingType;
    @SerializedName("categoryType")
    ArrayList<ListingCategoryTypeModel> listCategoryType;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ListingListingTypeModel> getListListingType() {
        return listListingType;
    }

    public void setListListingType(ArrayList<ListingListingTypeModel> listListingType) {
        this.listListingType = listListingType;
    }

    public ArrayList<ListingCategoryTypeModel> getListCategoryType() {
        return listCategoryType;
    }

    public void setListCategoryType(ArrayList<ListingCategoryTypeModel> listCategoryType) {
        this.listCategoryType = listCategoryType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
