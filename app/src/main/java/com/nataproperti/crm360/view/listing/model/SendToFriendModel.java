package com.nataproperti.crm360.view.listing.model;

/**
 * Created by nata on 12/19/2016.
 */

public class SendToFriendModel {
    private String status;
    private String message;
    private String psRef;
    private String agencyCompanyRef;
    private String memberType;
    private String memberTypeCode;
    private String companyName;
    private String imageLogo;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getAgencyCompanyRef() {
        return agencyCompanyRef;
    }

    public void setAgencyCompanyRef(String agencyCompanyRef) {
        this.agencyCompanyRef = agencyCompanyRef;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getMemberTypeCode() {
        return memberTypeCode;
    }

    public void setMemberTypeCode(String memberTypeCode) {
        this.memberTypeCode = memberTypeCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getImageLogo() {
        return imageLogo;
    }

    public void setImageLogo(String imageLogo) {
        this.imageLogo = imageLogo;
    }
}
