package com.nataproperti.crm360.view.listing.model;

import com.google.gson.annotations.Expose;

/**
 * Created by nata on 12/16/2016.
 */
public class Term {

    @Expose
    private Integer offset;
    @Expose
    private String value;

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
