package com.nataproperti.crm360.view.listing.model;

/**
 * Created by nata on 12/8/2016.
 */
public class UpdatePromote {

    int status;
    String message;
    String listingRef;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getListingRef() {
        return listingRef;
    }

    public void setListingRef(String listingRef) {
        this.listingRef = listingRef;
    }
}
