package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterChattingRoomInteractor;
import com.nataproperti.crm360.view.project.model.ChatRoomModel;
import com.nataproperti.crm360.view.listing.model.ChatSingleModelStatus;
import com.nataproperti.crm360.view.listing.model.ChatStatusModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ChattingRoomActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ChattingRoomPresenter implements PresenterChattingRoomInteractor {
    private ChattingRoomActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ChattingRoomPresenter(ChattingRoomActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }






    @Override
    public void getSendMsg(String memberRefSender, String memberRefReceiver, String messaging,int pageNo) {
        Call<ChatStatusModel> call = service.getAPI().postMessaging(memberRefSender,memberRefReceiver,messaging,pageNo);
        call.enqueue(new Callback<ChatStatusModel>() {
            @Override
            public void onResponse(Call<ChatStatusModel> call, Response<ChatStatusModel> response) {
                view.showChatResults(response);
            }

            @Override
            public void onFailure(Call<ChatStatusModel> call, Throwable t) {
                view.showChatFailure(t);

            }


        });

    }

    @Override
    public void getMsgFromServer(String memberRefSender, String memberRefReceiver) {
        Call<ChatSingleModelStatus> call = service.getAPI().getMessagingSingle(memberRefSender,memberRefReceiver);
        call.enqueue(new Callback<ChatSingleModelStatus>() {
            @Override
            public void onResponse(Call<ChatSingleModelStatus> call, Response<ChatSingleModelStatus> response) {
                view.showChatSingleResults(response);
            }

            @Override
            public void onFailure(Call<ChatSingleModelStatus> call, Throwable t) {
                view.showChatSingleFailure(t);

            }


        });
    }

    @Override
    public void postSendMsg(String memberRefSender, String memberRefReceiver, String messaging) {
        Call<List<ChatRoomModel>> call = service.getAPI().getMessaging(memberRefSender,memberRefReceiver,messaging);
        call.enqueue(new Callback<List<ChatRoomModel>>() {
            @Override
            public void onResponse(Call<List<ChatRoomModel>> call, Response<List<ChatRoomModel>> response) {
                view.showPostChatResults(response);
            }

            @Override
            public void onFailure(Call<List<ChatRoomModel>> call, Throwable t) {
                view.showPostChatFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
