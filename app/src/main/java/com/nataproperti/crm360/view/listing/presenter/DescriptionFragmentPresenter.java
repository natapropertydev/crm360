package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterDescriptionFragmentInteractor;
import com.nataproperti.crm360.view.listing.model.PropertyDescriptionModel;
import com.nataproperti.crm360.view.listing.model.SendToFriendModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ListingPropertyDescriptionFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class DescriptionFragmentPresenter implements PresenterDescriptionFragmentInteractor {
    private ListingPropertyDescriptionFragment view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public DescriptionFragmentPresenter(ListingPropertyDescriptionFragment view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListingDetail(String listingRef,String memberRef) {
        Call<PropertyDescriptionModel> call = service.getAPI().getPropertyDetail(listingRef,memberRef);
        call.enqueue(new Callback<PropertyDescriptionModel>() {
            @Override
            public void onResponse(Call<PropertyDescriptionModel> call, Response<PropertyDescriptionModel> response) {
                view.showListingDetailResults(response);
            }

            @Override
            public void onFailure(Call<PropertyDescriptionModel> call, Throwable t) {
                view.showListingDetailFailure(t);

            }


        });
    }

    @Override
    public void sendToFriend(String email, String memberRef, String listingRef) {
        Call<SendToFriendModel> call = service.getAPI().sendToFriend(email,memberRef,listingRef);
        call.enqueue(new Callback<SendToFriendModel>() {
            @Override
            public void onResponse(Call<SendToFriendModel> call, Response<SendToFriendModel> response) {
                view.showSendToFriendResults(response);
            }

            @Override
            public void onFailure(Call<SendToFriendModel> call, Throwable t) {
                view.showSendToFriendFailure(t);

            }


        });
    }

    @Override
    public void sendNotif(String agencyCompanyRef, String listingRef, String memberRef) {
        Call<SendToFriendModel> call = service.getAPI().sendNotif(agencyCompanyRef,listingRef,memberRef);
        call.enqueue(new Callback<SendToFriendModel>() {
            @Override
            public void onResponse(Call<SendToFriendModel> call, Response<SendToFriendModel> response) {
                view.showSendNotifResults(response);
            }

            @Override
            public void onFailure(Call<SendToFriendModel> call, Throwable t) {
                view.showSendNotifFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
