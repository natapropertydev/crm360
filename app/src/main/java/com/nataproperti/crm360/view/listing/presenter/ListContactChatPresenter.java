package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterListContactChatInteractor;
import com.nataproperti.crm360.view.profile.model.BaseData;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ListingContactChatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ListContactChatPresenter implements PresenterListContactChatInteractor {
    private ListingContactChatActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListContactChatPresenter(ListingContactChatActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getContactChat(String agencyCompanyRef) {
        Call<BaseData> call = service.getAPI().getContactMessageAll(agencyCompanyRef);
        call.enqueue(new Callback<BaseData>() {
            @Override
            public void onResponse(Call<BaseData> call, Response<BaseData> response) {
                view.showContactChatResults(response);
            }

            @Override
            public void onFailure(Call<BaseData> call, Throwable t) {
                view.showContactChatFailure(t);

            }


        });

    }

    @Override
    public void getContactChatPage(String agencyCompanyRef,final int pageNo) {
        Call<BaseData> call = service.getAPI().getContactMessagePage(agencyCompanyRef,String.valueOf(pageNo));
        call.enqueue(new Callback<BaseData>() {
            @Override
            public void onResponse(Call<BaseData> call, Response<BaseData> response) {
                view.showContactChatPageResults(response,pageNo);
            }

            @Override
            public void onFailure(Call<BaseData> call, Throwable t) {
                view.showContactChatPageFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
