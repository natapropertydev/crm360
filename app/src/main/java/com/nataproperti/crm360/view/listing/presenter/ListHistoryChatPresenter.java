package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterListHistoryChatInteractor;
import com.nataproperti.crm360.view.listing.model.ChatHistoryModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ListingHistoryChatActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ListHistoryChatPresenter implements PresenterListHistoryChatInteractor {
    private ListingHistoryChatActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListHistoryChatPresenter(ListingHistoryChatActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }




    @Override
    public void getHistoryChat(String memberRef) {
        Call<List<ChatHistoryModel>> call = service.getAPI().getHistoryMessage(memberRef);
        call.enqueue(new Callback<List<ChatHistoryModel>>() {
            @Override
            public void onResponse(Call<List<ChatHistoryModel>> call, Response<List<ChatHistoryModel>> response) {
                view.showHistoryChatResults(response);
            }

            @Override
            public void onFailure(Call<List<ChatHistoryModel>> call, Throwable t) {
                view.showHistoryChatFailure(t);

            }


        });

    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
