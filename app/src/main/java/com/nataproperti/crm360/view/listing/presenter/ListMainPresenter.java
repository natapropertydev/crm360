package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterListMainInteractor;
import com.nataproperti.crm360.view.listing.model.ListCountChatModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ListingMainActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ListMainPresenter implements PresenterListMainInteractor {
    private ListingMainActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListMainPresenter(ListingMainActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getCountChat(String memberRef) {
        Call<ListCountChatModel> call = service.getAPI().getCountChat(memberRef);
        call.enqueue(new Callback<ListCountChatModel>() {
            @Override
            public void onResponse(Call<ListCountChatModel> call, Response<ListCountChatModel> response) {
                view.showListCountResults(response);
            }

            @Override
            public void onFailure(Call<ListCountChatModel> call, Throwable t) {
                view.showListCountFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
