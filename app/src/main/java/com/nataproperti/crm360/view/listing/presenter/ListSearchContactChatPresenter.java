package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterListSearchContactChatInteractor;
import com.nataproperti.crm360.view.profile.model.BaseData;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ListingSearchContactChatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ListSearchContactChatPresenter implements PresenterListSearchContactChatInteractor {
    private ListingSearchContactChatActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListSearchContactChatPresenter(ListingSearchContactChatActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getContactChatKeyword(String agencyCompanyRef,final int pageNo, final String keyword) {
        Call<BaseData> call = service.getAPI().getContactMessage(agencyCompanyRef,keyword,String.valueOf(pageNo));
        call.enqueue(new Callback<BaseData>() {
            @Override
            public void onResponse(Call<BaseData> call, Response<BaseData> response) {
                view.showContactChatKeywordResults(response,pageNo);
            }

            @Override
            public void onFailure(Call<BaseData> call, Throwable t) {
                view.showContactChatKeywordFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
