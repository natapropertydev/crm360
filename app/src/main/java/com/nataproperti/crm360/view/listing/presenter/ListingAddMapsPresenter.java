package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterAddMapsInteractor;
import com.nataproperti.crm360.view.listing.model.InfoMapModel;
import com.nataproperti.crm360.view.listing.model.SaveMapsModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ListingAddMapsActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ListingAddMapsPresenter implements PresenterAddMapsInteractor {
    private ListingAddMapsActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListingAddMapsPresenter(ListingAddMapsActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void saveMaps(String listingRef, String latitude, String longitude) {
        Call<SaveMapsModel> call = service.getAPI().postMaps(listingRef,latitude,longitude);
        call.enqueue(new Callback<SaveMapsModel>() {
            @Override
            public void onResponse(Call<SaveMapsModel> call, Response<SaveMapsModel> response) {
                view.showAddMapResults(response);
            }

            @Override
            public void onFailure(Call<SaveMapsModel> call, Throwable t) {
                view.showAddMapFailure(t);

            }


        });
    }

    @Override
    public void getInfoMap(String listingRef) {
        Call<InfoMapModel> call = service.getAPI().getMaps(listingRef);
        call.enqueue(new Callback<InfoMapModel>() {
            @Override
            public void onResponse(Call<InfoMapModel> call, Response<InfoMapModel> response) {
                view.showInfoMapResults(response);
            }

            @Override
            public void onFailure(Call<InfoMapModel> call, Throwable t) {
                view.showInfoMapFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
