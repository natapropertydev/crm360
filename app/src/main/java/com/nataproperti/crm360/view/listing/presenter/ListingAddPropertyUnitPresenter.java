package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterListingAddPropertyUnitInteractor;
import com.nataproperti.crm360.view.listing.model.ResponeListingAddPropertyUnitModel;
import com.nataproperti.crm360.view.listing.model.ResponeListingPropertyModel;
import com.nataproperti.crm360.view.listing.model.ResponeListingPropertyUnitModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ListingAddPropertyUnitActivity;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by Nata on 12/6/2016.
 */

public class ListingAddPropertyUnitPresenter implements PresenterListingAddPropertyUnitInteractor {
    private ListingAddPropertyUnitActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListingAddPropertyUnitPresenter(ListingAddPropertyUnitActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListingAddPropertyUnitSvc() {
        Call<ResponeListingAddPropertyUnitModel> call = service.getAPI().getListingPropertyUnit();
        call.enqueue(new Callback<ResponeListingAddPropertyUnitModel>() {
            @Override
            public void onResponse(Call<ResponeListingAddPropertyUnitModel> call, Response<ResponeListingAddPropertyUnitModel> response) {
                view.showListingPropertyUnitResults(response);
            }

            @Override
            public void onFailure(Call<ResponeListingAddPropertyUnitModel> call, Throwable t) {
                view.showListingPropertyUnitFailure(t);
            }
        });

    }

    @Override
    public void getListingPropertyUnitInfoSvc(String listingRef) {
        Call<ResponeListingPropertyUnitModel> call = service.getAPI().getListingPropertyUnitInfo(listingRef);
        call.enqueue(new Callback<ResponeListingPropertyUnitModel>() {
            @Override
            public void onResponse(Call<ResponeListingPropertyUnitModel> call, Response<ResponeListingPropertyUnitModel> response) {
                view.showListingPropertyUnitInfoResults(response);
            }

            @Override
            public void onFailure(Call<ResponeListingPropertyUnitModel> call, Throwable t) {
                view.showListingPropertyUnitInfoFailure(t);
            }
        });
    }


    @Override
    public void saveListingInformasiUnit(Map<String, String> fields,final int btnStatus) {
        Call<ResponeListingPropertyModel> call = service.getAPI().saveInformasiUnit(fields);
        call.enqueue(new Callback<ResponeListingPropertyModel>() {
            @Override
            public void onResponse(Call<ResponeListingPropertyModel> call, Response<ResponeListingPropertyModel> response) {
                view.showResultInformation(response,btnStatus);

            }

            @Override
            public void onFailure(Call<ResponeListingPropertyModel> call, Throwable t) {
                view.showResultInformationFailure(t);

            }
        });
    }

    @Override
    public void rxUnSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();
    }
}
