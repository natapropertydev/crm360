package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterListingMemberInteractor;
import com.nataproperti.crm360.view.listing.model.ListingPropertyStatus;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ListingPropertyMemberActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ListingMemberPresenter implements PresenterListingMemberInteractor {
    private ListingPropertyMemberActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListingMemberPresenter(ListingPropertyMemberActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListingMember(String agencyCompanyRef, String memberRef, final String pageNo, String locationRef) {
        Call<ListingPropertyStatus> call = service.getAPI().getListingMember(agencyCompanyRef,memberRef,pageNo,locationRef);
        call.enqueue(new Callback<ListingPropertyStatus>() {
            @Override
            public void onResponse(Call<ListingPropertyStatus> call, Response<ListingPropertyStatus> response) {
                view.showListingMemberResults(response,pageNo);
            }

            @Override
            public void onFailure(Call<ListingPropertyStatus> call, Throwable t) {
                view.showListingMemberFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
