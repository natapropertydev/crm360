package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterListingsearchPropertyInteractor;
import com.nataproperti.crm360.view.listing.model.ListingPropertyStatus;
import com.nataproperti.crm360.view.listing.model.ResponeLookupListingPropertyModel;
import com.nataproperti.crm360.view.profile.model.CityModel;
import com.nataproperti.crm360.view.profile.model.ProvinceModel;
import com.nataproperti.crm360.view.profile.model.SubLocationModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ListingSearchPropertyActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ListingSearchPropertyPresenter implements PresenterListingsearchPropertyInteractor {
    private ListingSearchPropertyActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListingSearchPropertyPresenter(ListingSearchPropertyActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getSubLocListing(String countryCode, String provinceCode, String cityCode) {
        Call<List<SubLocationModel>> call = service.getAPI().getSubLocListListing(countryCode,provinceCode,cityCode);
        call.enqueue(new Callback<List<SubLocationModel>>() {
            @Override
            public void onResponse(Call<List<SubLocationModel>> call, Response<List<SubLocationModel>> response) {
                view.showSubLocResults(response);
            }

            @Override
            public void onFailure(Call<List<SubLocationModel>> call, Throwable t) {
                view.showSubLocFailure(t);

            }


        });
    }

    @Override
    public void getCityListing(String countryCode, String provinceCode) {
        Call<List<CityModel>> call = service.getAPI().getListCityListListing(countryCode,provinceCode);
        call.enqueue(new Callback<List<CityModel>>() {
            @Override
            public void onResponse(Call<List<CityModel>> call, Response<List<CityModel>> response) {
                view.showCityResults(response);
            }

            @Override
            public void onFailure(Call<List<CityModel>> call, Throwable t) {
                view.showCityFailure(t);

            }


        });
    }

    @Override
    public void getProvinsiListing(String countryCode) {
        Call<List<ProvinceModel>> call = service.getAPI().getListProvinsiListListing(countryCode);
        call.enqueue(new Callback<List<ProvinceModel>>() {
            @Override
            public void onResponse(Call<List<ProvinceModel>> call, Response<List<ProvinceModel>> response) {
                view.showProvinsiResults(response);
            }

            @Override
            public void onFailure(Call<List<ProvinceModel>> call, Throwable t) {
                view.showProvinsiFailure(t);

            }


        });
    }

    @Override
    public void getLookupListingProperty() {
        Call<ResponeLookupListingPropertyModel> call= service.getAPI().getLookupListingProperty();
        call.enqueue(new Callback<ResponeLookupListingPropertyModel>() {
            @Override
            public void onResponse(Call<ResponeLookupListingPropertyModel> call, Response<ResponeLookupListingPropertyModel> response) {
                view.showLookupListingPropertyResults(response);
            }

            @Override
            public void onFailure(Call<ResponeLookupListingPropertyModel> call, Throwable t) {
                view.showLookupListingPropertyFailure(t);
            }
        });
    }

    @Override
    public void getListProperty(String agencyCompanyRef, String psRef, String listingStatus, String pageNo, String locationRef, String keyword) {
        Call<ListingPropertyStatus> call = service.getAPI().getListingPropertyKey(agencyCompanyRef, psRef, listingStatus, pageNo, locationRef, keyword);
        call.enqueue(new Callback<ListingPropertyStatus>() {
            @Override
            public void onResponse(Call<ListingPropertyStatus> call, Response<ListingPropertyStatus> response) {
                view.showListPropertyResults(response);
            }

            @Override
            public void onFailure(Call<ListingPropertyStatus> call, Throwable t) {
                view.showListPropertyFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
