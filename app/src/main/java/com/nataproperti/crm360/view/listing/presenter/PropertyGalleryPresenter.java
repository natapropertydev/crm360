package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterPropertyGalleryInteractor;
import com.nataproperti.crm360.view.listing.model.ListingPropertyGalleryModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ListingPropertyGalleryFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class PropertyGalleryPresenter implements PresenterPropertyGalleryInteractor {
    private ListingPropertyGalleryFragment view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public PropertyGalleryPresenter(ListingPropertyGalleryFragment view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }



    @Override
    public void getGallery(String listingRef) {
        Call<List<ListingPropertyGalleryModel>> call = service.getAPI().getGallery(listingRef);
        call.enqueue(new Callback<List<ListingPropertyGalleryModel>>() {
            @Override
            public void onResponse(Call<List<ListingPropertyGalleryModel>> call, Response<List<ListingPropertyGalleryModel>> response) {
                view.showGalleryResults(response);
            }

            @Override
            public void onFailure(Call<List<ListingPropertyGalleryModel>> call, Throwable t) {
                view.showGalleryFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
