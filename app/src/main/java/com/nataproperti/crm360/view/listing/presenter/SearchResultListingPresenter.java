package com.nataproperti.crm360.view.listing.presenter;

import com.nataproperti.crm360.view.listing.intractor.PresenterSearchListingInteractor;
import com.nataproperti.crm360.view.listing.model.ListingPropertyStatus;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.ui.ListingSearchResultActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class SearchResultListingPresenter implements PresenterSearchListingInteractor {
    private ListingSearchResultActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public SearchResultListingPresenter(ListingSearchResultActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getSearchListing(String agencyCompanyRef, String listingStatus, String pageNo, String provinceCode, String cityCode, String locationRef, String listingTypeRef, String categoryType, String memberRef) {
        Call<ListingPropertyStatus> call = service.getAPI().getListingSearch(agencyCompanyRef,listingStatus,pageNo,provinceCode,cityCode,locationRef,listingTypeRef,categoryType,memberRef);
        call.enqueue(new Callback<ListingPropertyStatus>() {
            @Override
            public void onResponse(Call<ListingPropertyStatus> call, Response<ListingPropertyStatus> response) {
                view.showSearchResults(response);
            }

            @Override
            public void onFailure(Call<ListingPropertyStatus> call, Throwable t) {
                view.showSearchFailure(t);

            }


        });
    }

    @Override
    public void getListingCobroking(String agencyCompanyRef, String listingStatus, String pageNo, String provinceCode, String cityCode, String locationRef, String listingTypeRef, String categoryType, String memberRef) {
        Call<ListingPropertyStatus> call = service.getAPI().getListingAllCoBroke(agencyCompanyRef,listingStatus,pageNo,provinceCode,cityCode,
                locationRef,listingTypeRef,categoryType,memberRef);
        call.enqueue(new Callback<ListingPropertyStatus>() {
            @Override
            public void onResponse(Call<ListingPropertyStatus> call, Response<ListingPropertyStatus> response) {
                view.showListingMemberResults(response);
            }

            @Override
            public void onFailure(Call<ListingPropertyStatus> call, Throwable t) {
                view.showListingMemberFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
