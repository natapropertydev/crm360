package com.nataproperti.crm360.view.listing.ui;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.github.ybq.endless.Endless;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.gcm.AlarmManagerBroadcastReceiver;
import com.nataproperti.crm360.gcm.ConfigGCM;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.adapter.RVChatRoomAdapter;
import com.nataproperti.crm360.view.listing.model.ChatSingleModelStatus;
import com.nataproperti.crm360.view.listing.model.ChatStatusModel;
import com.nataproperti.crm360.view.listing.presenter.ChattingRoomPresenter;
import com.nataproperti.crm360.view.project.model.ChatRoomModel;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * Created by nata on 11/17/2016.
 */

public class ChattingRoomActivity extends AppCompatActivity implements View.OnClickListener, EmojiconGridFragment.OnEmojiconClickedListener,
        EmojiconsFragment.OnEmojiconBackspaceClickedListener {


    public static final String PREF_NAME = "pref";
    private ChatStatusModel chatStatusModel;
    private List<ChatRoomModel> chatRoomModels = new ArrayList<>();
    private List<ChatRoomModel> chatRoomModels1 = new ArrayList<>();
    public static final String TAG = "ListingMainActivity";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    //    private List<ListCountChatModel> listCountChatModels = new ArrayList<>();
//    private List<ListCountChatModel> listCountChatModels2 = new ArrayList<>();
    private ChattingRoomPresenter presenter;
    ProgressDialog progressDialog;
    String memberRefSender, memberRefReceiver, messaging, name, memberName;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    RecyclerView rvChatRoom;
    EditText textChat;
    ImageView btnBack, btnSendChatting, photoProfile;
    //    Display display;
    Context context;
    TextView textUsername;
    SharedPreferences sharedPreferences;
    public static boolean OFF_LINE_MODE = false;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    RVChatRoomAdapter adapter;
    Handler handler;
    private static String notifStatus;
    Runnable r;
    Button btnEmoticon;
    Endless endless;
    View loadingView;
    private int page = 1;
    LinearLayoutManager llm;
    String message, sendTimes, chatNames;
    private boolean loading = true;
    Runnable timerRunnable;
    long startTime = 0;
    private AlarmManagerBroadcastReceiver alarm;
    Handler timerHandler;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ChattingRoomPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRefSender = sharedPreferences.getString("isMemberRef", null);
        memberName = sharedPreferences.getString("isName", null);
//        name = sharedPreferences.getString("isName", null);
        context = this;
        Intent intent = getIntent();
        memberRefReceiver = intent.getStringExtra("memberRefReceiver");
        name = intent.getStringExtra("name");
        messaging = "";
        initWidget();
        toolbarWidget();
//        initializeAdapter();
        chatRoomIn();



//        loadingView = View.inflate(this, R.layout.loading, null);
//        loadingView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT));
//        rvChatRoom.addOnScrollListener(new RecyclerView.OnScrollListener()
//        {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
//            {
//                if(dy > 5) //check for scroll down
//                {
//                    String a= "";
////                    visibleItemCount = llm.getChildCount();
////                    totalItemCount = llm.getItemCount();
////                    pastVisiblesItems = llm.findFirstVisibleItemPosition();
////
////                    if (loading)
////                    {
////                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
////                        {
////                            loading = false;
////                            Log.v("...", "Last Item Wow !");
////                            //Do pagination.. i.e. fetch new data
////                        }
////                    }
//                }else{
//                    String test = "";
////                    chatRoomIn2(page);
//                }
//            }
//        });

//        endless = Endless.applyTo(rvChatRoom, loadingView);
//        endless.setAdapter(adapter);
//        endless.setLoadMoreListener(new Endless.LoadMoreListener() {
//            @Override
//            public void onLoadMore(int page) {
////                requestContact(agencyCompanyRef, page);
//                chatRoomIn2(page);
//            }
//        });
        timerHandler = new Handler();
        timerRunnable = new Runnable() {

            @Override
            public void run() {
                long millis = System.currentTimeMillis() - startTime;
                int seconds = (int) (millis / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                chatRoomIn2();

//                    timerTextView.setText(String.format("%d:%02d", minutes, seconds));

                timerHandler.postDelayed(this, 5000);
            }
        };
        timerHandler.postDelayed(timerRunnable, 0);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ConfigGCM.PUSH_NOTIFICATION)) {
                    // new push message is received
//                    timerHandler.removeCallbacks(timerRunnable);
                    handlePushNotification(intent);
                }
            }
        };
//        notifStatus = "failed";
//        if(notifStatus.equals("failed")){


//        }
//        handler = new Handler();
//
//        r = new Runnable() {
//            public void run() {
//                chatRoomIn();
//                handler.postDelayed(this, 1000);
//            }
//        };
//
//        handler.postDelayed(r, 1000);
//        delayedStart(3000);


//        chatRoomIn();
//        textUsername.setText(name);
        Glide.with(context).load(WebService.getProfile() + memberRefReceiver).asBitmap().centerCrop().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
                .into(new BitmapImageViewTarget(photoProfile) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        photoProfile.setImageDrawable(circularBitmapDrawable);
                    }
                });

//        btnBack.setOnClickListener(this);
        btnSendChatting.setOnClickListener(this);
//        btnEmoticon.setOnClickListener(this);
        textChat.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    chatRoom();
                    return true;
                }
                return false;
            }
        });


    }

    private void handlePushNotification(Intent intent) {
//        Message message = (Message) intent.getSerializableExtra("message");
        message = intent.getStringExtra("message");
        sendTimes = intent.getStringExtra("sendTime");
        chatNames = intent.getStringExtra("nameChat");

        if (message != null) {
//            chatRoomIn();

            ChatRoomModel roomModel = new ChatRoomModel();
            roomModel.setChatMessage(message);
            roomModel.setMemberRefReceiver(" ");
            roomModel.setMemberRefSender(chatNames);
            roomModel.setStatus("false");
            roomModel.setSendTime(sendTimes);
            roomModel.setReadMessage("true");
//            roomModel.setIdChat(1);
//            chatRoomIn();
            chatRoomModels.add(roomModel);

        }
        adapter.notifyDataSetChanged();
        if (adapter.getItemCount() > 1) {
            rvChatRoom.getLayoutManager().smoothScrollToPosition(rvChatRoom, null, adapter.getItemCount() - 1);
        }
        timerHandler.postDelayed(timerRunnable, 0);


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // registering the receiver for new notification
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ConfigGCM.PUSH_NOTIFICATION));

//        NotificationUtils.clearNotifications();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
////        chatRoomModels.clear();
//        chatRoomIn();
//    }

    private void toolbarWidget() {

        title.setText(name);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    private void chatRoom() {
        chatRoomModels1.clear();
        presenter.postSendMsg(memberRefSender, memberRefReceiver, messaging);
    }

    private void chatRoomIn() {
        chatRoomModels1.clear();
        presenter.postSendMsg(memberRefSender, memberRefReceiver, "");
    }

    private void chatRoomIn2() {
//        timerHandler.removeCallbacks(timerRunnable);
//        alarm.CancelAlarm(context);
//        chatRoomModels1.clear();
        presenter.getMsgFromServer(memberRefSender, memberRefReceiver);
    }

    private void initializeAdapter() {
//        int count = chatRoomModels.size();


//        rvChatRoom.removeAllViews();

//        adapter.notifyDataSetChanged();


        adapter = new RVChatRoomAdapter(this, chatRoomModels);
//        rvChatRoom.scrollToPosition(count);
        rvChatRoom.setAdapter(adapter);



//        rvChatRoom.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
//            @Override
//            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
//                Log.d("bottom"," "+bottom+" "+oldBottom);
//
//                if ( bottom < oldBottom) {
//                    rvChatRoom.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (adapter.getItemCount()>0){
//                                rvChatRoom.getLayoutManager().smoothScrollToPosition(rvChatRoom, null, adapter.getItemCount() - 1);
//                            }
//                        }
//                    }, 100);
//                }
//            }
//        });

    }

    private void initWidget() {
//        btnEmoticon=(Button)findViewById(R.id.btn_emoticon);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.idtxt_user_name);
        photoProfile = (ImageView) toolbar.findViewById(R.id.image_user);
        btnBack = (ImageView) toolbar.findViewById(R.id.ic_menu_back);
//        textUsername = (TextView)findViewById(R.id.txt_user_name);
        rvChatRoom = (RecyclerView) findViewById(R.id.rv_myproject);
        textChat = (EditText) findViewById(R.id.edit_chat);
        llm = new LinearLayoutManager(this);
//        llm = new LinearLayoutManager(this);
        rvChatRoom.setLayoutManager(llm);
//        llm.setSmoothScrollbarEnabled(true);
        llm.setStackFromEnd(true);
//        llm.setReverseLayout(true);
//        mLayoutManager.setStackFromEnd(true);
        rvChatRoom.setItemAnimator(new DefaultItemAnimator());
//        rvChatRoom.setLayoutManager(llm);

//        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(mAdapter);

//        rvChatRoom.setItemAnimator(new DefaultItemAnimator());

//        btnBack = (ImageView) findViewById(R.id.ic_menu_back);
        btnSendChatting = (ImageView) findViewById(R.id.btn_send_chatting);
//        photoProfile = (ImageView) findViewById(R.id.image_user);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.btn_emoticon:
//                setEmojiconFragment(false);
//                 break;
            case R.id.ic_menu_back:
//                alarm.CancelAlarm(context);
                timerHandler.removeCallbacks(timerRunnable);
//                handler.removeCallbacksAndMessages(r);
                finish();
                break;
            case R.id.btn_send_chatting:
                if (OFF_LINE_MODE) {
                    Toast.makeText(getApplicationContext(), "Silahkan cek koneksi anda", Toast.LENGTH_LONG).show();
                } else {
                    messaging = textChat.getText().toString();
                    chatRoom();
                    textChat.getText().clear();
                }
                break;

        }

    }

    public void showChatResults(Response<ChatStatusModel> response) {
        //final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        OFF_LINE_MODE = false;
        String status = response.body().getStatus();
        int totalPage = Integer.parseInt(response.body().getTotalPage());
        chatRoomModels = response.body().getData();
        if (page > totalPage) {
            loadingView.setVisibility(View.GONE);
        } else {
            if (page == 1) {
                for (int i = 0; i < chatRoomModels.size(); i++) {
                    ChatRoomModel roomModel = new ChatRoomModel();
                    roomModel.setChatMessage(chatRoomModels.get(i).getChatMessage());
                    roomModel.setMemberRefReceiver(chatRoomModels.get(i).getMemberRefReceiver());
                    roomModel.setMemberRefSender(chatRoomModels.get(i).getMemberRefSender());
                    roomModel.setStatus(chatRoomModels.get(i).getStatus());
                    roomModel.setSendTime(chatRoomModels.get(i).getSendTime());
                    roomModel.setReadMessage(chatRoomModels.get(i).getReadMessage());
                    roomModel.setIdChat(i);
//                    ChatRoomModelDao chatRoomModelDao = daoSession.getChatRoomModelDao();
//                    chatRoomModelDao.insertOrReplace(roomModel);
//                    chatRoomModels1.add(roomModel);
                    adapter.notifyDataSetChanged();
//                    if (adapter.getItemCount() > 1) {
//                        // scrolling to bottom of the recycler view
//                        rvChatRoom.getLayoutManager().smoothScrollToPosition(rvChatRoom, null, adapter.getItemCount() - 1);
//                    }
//                    adapter.notifyDataSetChanged();
                }
            } else {
                for (int i = 0; i < chatRoomModels.size(); i++) {
                    ChatRoomModel roomModel = new ChatRoomModel();
                    roomModel.setChatMessage(chatRoomModels.get(i).getChatMessage());
                    roomModel.setMemberRefReceiver(chatRoomModels.get(i).getMemberRefReceiver());
                    roomModel.setMemberRefSender(chatRoomModels.get(i).getMemberRefSender());
                    roomModel.setStatus(chatRoomModels.get(i).getStatus());
                    roomModel.setSendTime(chatRoomModels.get(i).getSendTime());
                    roomModel.setReadMessage(chatRoomModels.get(i).getReadMessage());
                    roomModel.setIdChat(i);
//                    ChatRoomModelDao chatRoomModelDao = daoSession.getChatRoomModelDao();
//                    chatRoomModelDao.insertOrReplace(roomModel);
//                    chatRoomModels1.add(roomModel);
                    adapter.notifyDataSetChanged();
                    if (adapter.getItemCount() > 1) {
                        // scrolling to bottom of the recycler view
                        rvChatRoom.getLayoutManager().smoothScrollToPosition(rvChatRoom, null, adapter.getItemCount() - 1);
                    }
                    adapter.notifyDataSetChanged();
                    endless.loadMoreComplete();
                }
            }
        }


//        initializeAdapter();
    }

    public void showChatFailure(Throwable t) {
        OFF_LINE_MODE = true;
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
//        ChatRoomModelDao chatRoomModelDao = daoSession.getChatRoomModelDao();
//        List<ChatRoomModel> chatRoomModelss = chatRoomModelDao.queryBuilder().where(ChatRoomModelDao.Properties.MemberRefSender.eq(memberName))
//                .orderDesc(ChatRoomModelDao.Properties.IdChat)
//                .list();
//        Log.d("getProjectLocalDB", "" + chatRoomModelss.toString());
//        int numData = chatRoomModelss.size();
//        if (numData > 0) {
//            for (int i = 0; i < numData; i++) {
//                ChatRoomModel roomModel = chatRoomModelss.get(i);
//                chatRoomModels.add(roomModel);
//                adapter.notifyDataSetChanged();
//                if (adapter.getItemCount() > 1) {
//                    // scrolling to bottom of the recycler view
//                    rvChatRoom.getLayoutManager().smoothScrollToPosition(rvChatRoom, null, adapter.getItemCount() - 1);
//                }
////                initializeAdapter();
//
//
//            }
//        }

    }
    /**
     * Set the Emoticons in Fragment.
     * @param useSystemDefault
     */
//    private void setEmojiconFragment(boolean useSystemDefault) {
//
//        getSupportFragmentManager()
//                .beginTransaction()
//                .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
//                .commit();
//    }

    /**
     * It called, when click on icon of Emoticons.
     *
     * @param emojicon
     */
    @Override
    public void onEmojiconClicked(Emojicon emojicon) {

        EmojiconsFragment.input(textChat, emojicon);
    }

    /**
     * It called, when backspace button of Emoticons pressed
     *
     * @param view
     */
    @Override
    public void onEmojiconBackspaceClicked(View view) {

        EmojiconsFragment.backspace(textChat);
    }

    public void showPostChatResults(Response<List<ChatRoomModel>> response) {
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        OFF_LINE_MODE = false;
        chatRoomModels = response.body();


        for (int i = 0; i < chatRoomModels.size(); i++) {
            ChatRoomModel roomModel = new ChatRoomModel();
            roomModel.setChatMessage(response.body().get(i).getChatMessage());
            roomModel.setMemberRefReceiver(response.body().get(i).getMemberRefReceiver());
            roomModel.setMemberRefSender(response.body().get(i).getMemberRefSender());
            roomModel.setStatus(response.body().get(i).getStatus());
            roomModel.setSendTime(response.body().get(i).getSendTime());
            roomModel.setReadMessage(response.body().get(i).getReadMessage());
            roomModel.setIdChat(i);
//            ChatRoomModelDao chatRoomModelDao = daoSession.getChatRoomModelDao();
//            chatRoomModelDao.insertOrReplace(roomModel);
//            chatRoomModels1.add(roomModel);

        }
        initializeAdapter();
//        adapter.notifyDataSetChanged();
//        if (adapter.getItemCount() > 1) {
//            // scrolling to bottom of the recycler view
//            rvChatRoom.getLayoutManager().smoothScrollToPosition(rvChatRoom, null, adapter.getItemCount() - 1);
//        }
    }

    public void showPostChatFailure(Throwable t) {
        OFF_LINE_MODE = true;
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
//        ChatRoomModelDao chatRoomModelDao = daoSession.getChatRoomModelDao();
//        List<ChatRoomModel> chatRoomModelss = chatRoomModelDao.queryBuilder().where(ChatRoomModelDao.Properties.MemberRefSender.eq(memberName))
//                .orderDesc(ChatRoomModelDao.Properties.IdChat)
//                .list();
//        Log.d("getProjectLocalDB", "" + chatRoomModelss.toString());
//        int numData = chatRoomModelss.size();
//        if (numData > 0) {
//            for (int i = 0; i < numData; i++) {
//                ChatRoomModel roomModel = chatRoomModelss.get(i);
//                chatRoomModels.add(roomModel);
//
////                initializeAdapter();
//
//
//            }
//            initializeAdapter();
////            adapter.notifyDataSetChanged();
////            if (adapter.getItemCount() > 1) {
////                // scrolling to bottom of the recycler view
////                rvChatRoom.getLayoutManager().smoothScrollToPosition(rvChatRoom, null, adapter.getItemCount() - 1);
////            }
//        }

    }

    public void showChatSingleResults(Response<ChatSingleModelStatus> response) {
        String status = response.body().getStatus();
        if (status != null) {
            if (status.equals("200")) {
                ChatRoomModel roomModel = new ChatRoomModel();
                roomModel.setChatMessage(response.body().getData().getChatMessage());
                roomModel.setMemberRefReceiver(response.body().getData().getMemberRefReceiver());
                roomModel.setMemberRefSender(response.body().getData().getMemberRefSender());
                roomModel.setStatus(response.body().getData().getStatus());
                roomModel.setSendTime(response.body().getData().getSendTime());
                roomModel.setReadMessage(response.body().getData().getReadMessage());
//            roomModel.setIdChat(1);
//            chatRoomIn();
                chatRoomModels.add(roomModel);
            }
        }

    }

    public void showChatSingleFailure(Throwable t) {
        //Toast.makeText(this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();

    }

}
