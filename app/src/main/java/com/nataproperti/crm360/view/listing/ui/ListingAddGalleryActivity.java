package com.nataproperti.crm360.view.listing.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.snackbar.Snackbar;
import com.jaredrummler.android.device.DeviceName;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.listing.adapter.ListingAddPropertyGalleryAdapter;
import com.nataproperti.crm360.view.listing.model.IsCobrokeModel;
import com.nataproperti.crm360.view.listing.model.ListingPropertyGalleryModel;
import com.nataproperti.crm360.view.listing.presenter.AddGaleryPresenter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.nataproperti.crm360.view.profile.ui.EditProfileActivity.getImageContentUri;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class ListingAddGalleryActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String TAG = "ListingAddGallery";

    //    public static final String TAG = "ListingMainActivity";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private AddGaleryPresenter presenter;
    ProgressDialog progressDialog;

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    SharedPreferences sharedPreferences;
    String memberRef, memberType, companyName;

    private ArrayList<ListingPropertyGalleryModel> listGallery = new ArrayList<ListingPropertyGalleryModel>();
    private ListingAddPropertyGalleryAdapter adapter;
    private String listingRef, imageRef, agencyCompanyRef, memberTypeCode;
    private GridView listView;
    private RelativeLayout relativSwitch;

    @BindView(R.id.btn_upload)
    Button btnUpload;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.linear_no_data)
    LinearLayout linearLayoutNoData;
    @BindView(R.id.btn_finish)
    Button btnFinish;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    SwitchCompat switchCompatCobroke;
    //static String isCobroke;
    String isCobroke = "1", isCobrokePref, manufacturer, mTmpGalleryPicturePath;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    Display display;

    Uri file, uri, selectedImage;
    String pathImage = "";

    int addFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_add_gallery);
        ButterKnife.bind(this);
//        LoadingBar.stopLoader();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new AddGaleryPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        Intent intent = getIntent();
        listingRef = intent.getStringExtra("listingRef");
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        addFrom = intent.getIntExtra("addFrom", 0);
        isCobrokePref = sharedPreferences.getString("isCobroke", "1");

        Log.d(TAG, "listingRef " + listingRef);
        Log.d(TAG, "agencyCompanyRef " + agencyCompanyRef);
        Log.d(TAG, "memberTypeCode " + memberTypeCode);
        Log.d(TAG, "addFrom " + addFrom);
        Log.d(TAG, "isCobroke " + isCobrokePref);

        initWidget();

        adapter = new ListingAddPropertyGalleryAdapter(this, listGallery, display);
        listView.setAdapter(adapter);
        /*CoBrokeStatic coBrokeStatic = new CoBrokeStatic();
        isCobroke = coBrokeStatic.getStatus();*/

        isCobroke = isCobrokePref;

        if (isCobrokePref != null) {
            if (isCobrokePref.equals("1")) {
                switchCompatCobroke.setChecked(true);
            } else {
                switchCompatCobroke.setChecked(false);
            }
        } else {
            switchCompatCobroke.setChecked(true);
        }

        switchCompatCobroke.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isCobroke = "1";
                } else {
                    isCobroke = "0";
                }
            }
        });

        if (memberTypeCode.equals("2")) {
            //switchCompatCobroke.setVisibility(View.VISIBLE);
            relativSwitch.setVisibility(View.VISIBLE);
        } else {
            //switchCompatCobroke.setVisibility(View.GONE);
            relativSwitch.setVisibility(View.GONE);
            switchCompatCobroke.setChecked(true);
            isCobroke = "1";
        }

        btnUpload.setTypeface(font);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
            }
        });

        btnNext.setTypeface(font);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("param ", listingRef + " " + memberRef + " " + isCobroke);
                presenter.postIsCobroke(listingRef, memberRef, isCobroke);
                popupImageTitle(1);

            }
        });

        btnFinish.setTypeface(font);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.postIsCobroke(listingRef, memberRef, isCobroke);
                popupImageTitle(2);
            }
        });

        requestPropertyGallery(listingRef);

    }

    private void initWidget() {
        switchCompatCobroke = (SwitchCompat) findViewById(R.id.switch_compat_co_broke);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Image Gallery");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        listView = (GridView) findViewById(R.id.list_property_gallery);
        relativSwitch = (RelativeLayout) findViewById(R.id.relativ_switch);
    }

    public void requestPropertyGallery(final String listingRef) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListListingPropertyGallery(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.d("AddGallery", "" + jsonArray.length());
                    if (jsonArray.length() == 0) {
                        linearLayoutNoData.setVisibility(View.VISIBLE);
                    } else {
                        linearLayoutNoData.setVisibility(View.GONE);
                        generateGallery(jsonArray);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingAddGalleryActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "galleryProperty");

    }

    private void generateGallery(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ListingPropertyGalleryModel propertyGalleryModel = new ListingPropertyGalleryModel();
                propertyGalleryModel.setImageRef(jo.getString("imageRef"));
                propertyGalleryModel.setImageTitle(jo.getString("imageTitle"));
                propertyGalleryModel.setImageFile(jo.getString("imageFile"));
                propertyGalleryModel.setListingRef(listingRef);

                listGallery.add(propertyGalleryModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ListingAddGalleryActivity.this);
        //builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                   /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);*/
                    if (ContextCompat.checkSelfPermission(ListingAddGalleryActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(ListingAddGalleryActivity.this, "Enable camera permission", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(ListingAddGalleryActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        file = Uri.fromFile(getOutputMediaFile());
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }
                } else if (items[item].equals("Use Existing Foto")) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    startActivityForResult(intent, SELECT_FILE);
                }
            }
        });
        builder.show();
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "nataproperty");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                // onSelectFromGalleryResult(data);
                //handleGalleryResult(data);
                if (Build.VERSION.SDK_INT < 19) {
                    handleGalleryResult18(data);
                    //Toast.makeText(ListingAddGalleryActivity.this, "18", Toast.LENGTH_SHORT).show();
                } else {
                    handleGalleryResult19(data);
                    //Toast.makeText(ListingAddGalleryActivity.this, "19", Toast.LENGTH_SHORT).show();
                }
            else if (requestCode == REQUEST_CAMERA) {
                //onCaptureImageResult(data);
                try {
                    Intent i = new Intent(this, ListingUploadImageActivity.class);
                    i.putExtra("pathImage", file.getEncodedPath());
                    i.putExtra("listingRef", listingRef);
                    i.putExtra("agencyCompanyRef", agencyCompanyRef);
                    i.putExtra("memberTypeCode", memberTypeCode);
                    i.putExtra("addFrom", addFrom);
                    i.putExtra("isCobroke", isCobroke);
                    startActivity(i);
                } catch (Exception e) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Upload failed", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        }
    }

    private void handleGalleryResult19(Intent data) {
        uri = data.getData();
        //mTmpGalleryPicturePath = getPath(selectedImage);

        DeviceName.with(this).request(new DeviceName.Callback() {
            @Override
            public void onFinished(DeviceName.DeviceInfo info, Exception error) {
                manufacturer = info.manufacturer;

                if (manufacturer.contains("Xiaomi")) {
                    File myFile = new File(uri.getPath());
                    selectedImage = getImageContentUri(ListingAddGalleryActivity.this, myFile);
                    mTmpGalleryPicturePath = getRealPathFromURI(ListingAddGalleryActivity.this, selectedImage);
                } else {
                    mTmpGalleryPicturePath = getRealPathFromURI(ListingAddGalleryActivity.this, uri);
                    if (mTmpGalleryPicturePath == null) {
                        mTmpGalleryPicturePath = getPath(selectedImage);
                    }
                }

                if (!mTmpGalleryPicturePath.equals("")) {
                    if (mTmpGalleryPicturePath != null) {
                        Intent i = new Intent(ListingAddGalleryActivity.this, ListingUploadImageActivity.class);
                        i.putExtra("pathImage", mTmpGalleryPicturePath);
                        i.putExtra("listingRef", listingRef);
                        i.putExtra("agencyCompanyRef", agencyCompanyRef);
                        i.putExtra("memberTypeCode", memberTypeCode);
                        i.putExtra("addFrom", addFrom);
                        i.putExtra("isCobroke", isCobroke);
                        startActivity(i);

                    } else {
                        try {
                            InputStream is = getContentResolver().openInputStream(selectedImage);
                            //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                            mTmpGalleryPicturePath = selectedImage.getPath();
                            Intent i = new Intent(ListingAddGalleryActivity.this, ListingUploadImageActivity.class);
                            i.putExtra("pathImage", mTmpGalleryPicturePath);
                            i.putExtra("listingRef", listingRef);
                            i.putExtra("agencyCompanyRef", agencyCompanyRef);
                            i.putExtra("memberTypeCode", memberTypeCode);
                            i.putExtra("addFrom", addFrom);
                            i.putExtra("isCobroke", isCobroke);
                            startActivity(i);

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(ListingAddGalleryActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @SuppressLint("NewApi")
    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
//        if (contentUri == null) {
//            return null;
//        }
//
//        String[] projection = {MediaStore.Images.Media.DATA};
//
//        Cursor cursor;
//        if (Build.VERSION.SDK_INT > 19) {
//            // Will return "image:x*"
//            String wholeID = DocumentsContract.getDocumentId(contentUri);
//            // Split at colon, use second item in the array
//            String id = wholeID.split(":")[1];
//            // where id is equal to
//            String sel = MediaStore.Images.Media._ID + "=?";
//
//            cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                    projection, sel, new String[]{id}, null);
//        } else {
//            cursor = getContentResolver().query(contentUri, projection, null, null, null);
//        }
//        String path = null;
//        try {
//            int column_index = cursor
//                    .getColumnIndex(MediaStore.Images.Media.DATA);
//            cursor.moveToFirst();
//            path = cursor.getString(column_index).toString();
//            cursor.close();
//        } catch (NullPointerException e) {
//
//        }
//        return path;
    }

    private void handleGalleryResult18(Intent data) {
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        mTmpGalleryPicturePath = getRealPathFromURI_API11to18(this, selectedImage);

        if (!mTmpGalleryPicturePath.equals("")) {
            if (mTmpGalleryPicturePath != null) {
                Intent i = new Intent(this, ListingUploadImageActivity.class);
                i.putExtra("pathImage", mTmpGalleryPicturePath);
                i.putExtra("listingRef", listingRef);
                i.putExtra("agencyCompanyRef", agencyCompanyRef);
                i.putExtra("memberTypeCode", memberTypeCode);
                i.putExtra("addFrom", addFrom);
                i.putExtra("isCobroke", isCobroke);
                startActivity(i);

            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    Intent i = new Intent(this, ListingUploadImageActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    i.putExtra("listingRef", listingRef);
                    i.putExtra("agencyCompanyRef", agencyCompanyRef);
                    i.putExtra("memberTypeCode", memberTypeCode);
                    i.putExtra("addFrom", addFrom);
                    i.putExtra("isCobroke", isCobroke);
                    startActivity(i);

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } else {
            Toast.makeText(ListingAddGalleryActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressLint("NewApi")
    private String getPath(Uri uri) {
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.indexOf(":") > -1 ? wholeID.split(":")[1] : wholeID.indexOf(";") > -1 ? wholeID
                    .split(";")[1] : wholeID;
            String[] column = {MediaStore.Images.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column,
                    sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            filePath = "";
        }
        return filePath;

    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        File destination = new File(Environment.getExternalStorageDirectory(), "/nataproperty/" +
                System.currentTimeMillis() + ".jpg");

        /*File path = Environment.getExternalStoragePublicDirectory(
                Environment.getExternalStorageDirectory() + "/nataproperty");
        path.mkdirs();
        File destination = new File(path, "image.jpg");*/

        Log.d("pathImage", destination.toString());

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

            Intent i = new Intent(this, ListingUploadImageActivity.class);
            i.putExtra("pathImage", destination.toString());
            i.putExtra("listingRef", listingRef);
            i.putExtra("agencyCompanyRef", agencyCompanyRef);
            i.putExtra("memberTypeCode", memberTypeCode);
            i.putExtra("addFrom", addFrom);
            i.putExtra("isCobroke", isCobroke);

            //i.putExtra("Image", byteArray);
            startActivity(i);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 300;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        byte[] byteArray = bytes.toByteArray();

        try {
            Intent i = new Intent(this, ListingUploadImageActivity.class);
            //i.putExtra("Image", byteArray);
            i.putExtra("pathImage", selectedImagePath);
            i.putExtra("listingRef", listingRef);
            i.putExtra("agencyCompanyRef", agencyCompanyRef);
            i.putExtra("memberTypeCode", memberTypeCode);
            i.putExtra("addFrom", addFrom);
            i.putExtra("isCobroke", isCobroke);
            startActivity(i);
        } catch (Exception e) {
            Toast.makeText(ListingAddGalleryActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }

    }

    public void popupImageTitle(int btnStatus) {

        if (btnStatus == 1) {
            Intent intent1 = new Intent(ListingAddGalleryActivity.this, ListingAddMapsActivity.class);
            intent1.putExtra("listingRef", listingRef);
            intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
            intent1.putExtra("psRef", "");
            intent1.putExtra("memberTypeCode", memberTypeCode);
            intent1.putExtra("addFrom", addFrom);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
        } else {
            if (memberTypeCode.equals("2")) {
                Intent intent1 = new Intent(ListingAddGalleryActivity.this, ListingPropertyActivity.class);
                intent1.putExtra("listingRef", listingRef);
                intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent1.putExtra("psRef", "");
                intent1.putExtra("memberTypeCode", memberTypeCode);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            } else {
                Intent intent1 = new Intent(ListingAddGalleryActivity.this, ListingPropertyCoBrokeActivity.class);
                intent1.putExtra("listingRef", listingRef);
                intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent1.putExtra("psRef", "");
                intent1.putExtra("memberTypeCode", memberTypeCode);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            }

            if (addFrom == 1) {
                Intent intent1 = new Intent(ListingAddGalleryActivity.this, ListingPropertyMemberActivity.class);
                intent1.putExtra("listingRef", listingRef);
                intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent1.putExtra("psRef", "");
                intent1.putExtra("memberTypeCode", memberTypeCode);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            } else if (addFrom == 2) {
                Intent intent1 = new Intent(ListingAddGalleryActivity.this, ListingPropertyActivity.class);
                intent1.putExtra("listingRef", listingRef);
                intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent1.putExtra("psRef", "");
                intent1.putExtra("memberTypeCode", memberTypeCode);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            } else if (addFrom == 3) {
                Intent intent1 = new Intent(ListingAddGalleryActivity.this, ListingPropertyCoBrokeActivity.class);
                intent1.putExtra("listingRef", listingRef);
                intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent1.putExtra("psRef", "");
                intent1.putExtra("memberTypeCode", memberTypeCode);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            } else {
                Intent intent1 = new Intent(ListingAddGalleryActivity.this, ListingMainActivity.class);
                intent1.putExtra("listingRef", listingRef);
                intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent1.putExtra("psRef", "");
                intent1.putExtra("memberTypeCode", memberTypeCode);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    public void showUpdateCoBrokeResults(retrofit2.Response<IsCobrokeModel> response) {
        String status = response.body().getStatus();
        String message = response.body().getMessage();
        if (status.equals("200")) {
            //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }

    }

    public void showUpdateCoBrokeFailure(Throwable t) {

    }

}
