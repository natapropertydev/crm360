package com.nataproperti.crm360.view.listing.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.NataApi;
import com.nataproperti.crm360.config.NataService;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.gcm.GPSTracker;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.model.FindLocation;
import com.nataproperti.crm360.view.listing.model.Geometry;
import com.nataproperti.crm360.view.listing.model.GooglePlaceResult;
import com.nataproperti.crm360.view.listing.model.InfoMapModel;
import com.nataproperti.crm360.view.listing.model.Locations;
import com.nataproperti.crm360.view.listing.model.Predictions;
import com.nataproperti.crm360.view.listing.model.Result;
import com.nataproperti.crm360.view.listing.model.SaveMapsModel;
import com.nataproperti.crm360.view.listing.presenter.ListingAddMapsPresenter;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nataproperti.crm360.nataproperty.R.id.map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class ListingAddMapsActivity extends AppCompatActivity implements View.OnClickListener, ResultCallback<LocationSettingsResult>, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, OnMapReadyCallback {

    GoogleMap googleMap;
    private static final String TAG = ListingAddMapsActivity.class.getSimpleName();
    AutoCompleteTextView mAutocompleteView;
    private Marker currentLocMarker;
    private List<GooglePlaceResult> googlePlaceResults = new ArrayList<>();
    private List<Predictions> predictionses = new ArrayList<>();
    private List<Geometry> geometries = new ArrayList<>();
    private Locations locationses;
    private Result results;
    private Geometry geometry;
    double latitudeLoc, longitudeLoc, latProperty, lngProperty;
    private double latitude, longitude;
    String refrensi;
    private Timer timer;
    Button btnSave, btnClear;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    SharedPreferences sharedPreferences;
    String memberRef, memberType, companyName;
    public static final String PREF_NAME = "pref";
    String isCobroke, isCobrokePref;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private ListingAddMapsPresenter presenter;
    ProgressDialog progressDialog;
    private GoogleApiClient mGoogleApiClient;
    protected LocationSettingsRequest mLocationSettingsRequest;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 60000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private Status gpsStatus;
    protected final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    protected final static String KEY_LOCATION = "location";
    protected final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    private String listingRef, membeRefAgent, agencyCompanyRef, memberTypeCode, lat, lng;
    Typeface font;
    double latitudeCurrent, longitudeCurrent, latitudeCurrentdtl, longitudeCurrentdtl;
    String keyGoogle = "AIzaSyD-DsgWyVAqNuynLLw5AXrqf3jyFg80JvQ";
    Button btnSearch;
    int addFrom;
    protected Boolean mRequestingLocationUpdates;
    protected String mLastUpdateTime;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    GPSTracker gps;

    double latitudeMove2,longitudeMove2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_add_maps);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        isCobrokePref = sharedPreferences.getString("isCobroke", null);

        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListingAddMapsPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";
        updateValuesFromBundle(savedInstanceState);
        createLocationRequest();
        buildLocationSettingsRequest();
        buildGoogleApiClient();
        Intent intent = getIntent();
        listingRef = intent.getStringExtra("listingRef");
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        addFrom = intent.getIntExtra("addFrom", 0);
        SupportMapFragment supportMapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(map);

        // Getting a reference to the map
        supportMapFragment.getMapAsync(this);
        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getInfoMap(listingRef);
//        lat = intent.getStringExtra("lat");
//        lng = intent.getStringExtra("lng");

        isCobrokePref = sharedPreferences.getString("isCobroke", "1");
        initWidget();
        final Window mRootWindow = this.getWindow();
        View mRootView = mRootWindow.getDecorView().findViewById(android.R.id.content);
        mRootView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        Rect r = new Rect();
                        View view = mRootWindow.getDecorView();
                        view.getWindowVisibleDisplayFrame(r);
                        int dialogHeight = getWindow().getDecorView().getHeight();
                        int keyboardHeight = r.bottom - r.top;
                        int heightDifference = keyboardHeight - dialogHeight;
                        mAutocompleteView.setDropDownHeight(keyboardHeight);
                    }
                }
        );

        LatLng center2 = googleMap.getCameraPosition().target;
        latitudeMove2 = center2.latitude;
        longitudeMove2 = center2.longitude;

        btnSave.setTypeface(font);
        btnSave.setOnClickListener(this);
        btnClear.setOnClickListener(this);

        mAutocompleteView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (adapterView.getItemAtPosition(position).equals("Result Not Found")) {
                    mAutocompleteView.setText("");


                } else {
//                    String desc = (String) adapterView.getItemAtPosition(position);
                    mAutocompleteView.setText((String) adapterView.getItemAtPosition(position));
                    refrensi = predictionses.get(position).getReference();
                    searchLocation(refrensi);
                }
            }

        });

        mAutocompleteView.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // user is typing: reset already started timer (if existing)
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                // user typed: start the timer
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {

                        if (s.toString().length() > 2) {
                            NataApi service = NataService.getGoogle().create(NataApi.class);
                            final Call<GooglePlaceResult> call = service.findPlaces(keyGoogle, "country:id", "id", "geocode", s.toString());
                            call.enqueue(new Callback<GooglePlaceResult>() {
                                @Override
                                public void onResponse(Call<GooglePlaceResult> call, retrofit2.Response<GooglePlaceResult> response) {

                                    predictionses = response.body().getPredictions();
                                    String[] predictionValues = new String[predictionses.size()];
                                    if (predictionses.size() == 0) {
                                        predictionValues = new String[]{"Result Not Found"};
                                    }

                                    for (int i = 0; i < predictionses.size(); i++) {
                                        predictionValues[i] = predictionses.get(i).getDescription();
                                        Log.i(TAG, predictionValues[i]);

                                    }
                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(ListingAddMapsActivity.this, android.R.layout.simple_dropdown_item_1line, predictionValues);
                                    mAutocompleteView.setAdapter(adapter);
                                    mAutocompleteView.setThreshold(2);
                                    adapter.notifyDataSetChanged();


                                }

                                @Override
                                public void onFailure(Call<GooglePlaceResult> call, Throwable t) {
                                    Log.e(TAG, t.getMessage());

                                }
                            });

                        }
                    }
                }, 1000);
            }
        });

//        locationGoogleMap();

        // Setting a click event handler for the map
        /*googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                //BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location_on_indigo_a700_24dp);
                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions();

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title("Lokasi property");
                latProperty = latLng.latitude;
                lngProperty = latLng.longitude;

                // Clears the previously touched position
                googleMap.clear();

                // Animating to the touched position
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                // Placing a marker on the touched position

                //googleMap.addMarker(markerOptions);

                // map is a GoogleMap object
                googleMap.setMyLocationEnabled(true);


            }
        });*/

    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {
                mLastUpdateTime = savedInstanceState.getString(KEY_LAST_UPDATED_TIME_STRING);
            }
//            updateUI();
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        checkLocationSettings();
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * Handles the Start Updates button and requests start of location updates. Does nothing if
     * updates have already been requested.
     */
    public void startUpdatesButtonHandler(View view) {
        checkLocationSettings();
    }

    /**
     * Handles the Stop Updates button, and requests removal of location updates.
     */
    public void stopUpdatesButtonHandler(View view) {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        stopLocationUpdates();
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
//        LocationServices.FusedLocationApi.requestLocationUpdates(
//                mGoogleApiClient,
//                mLocationRequest,
//                this
//        ).setResultCallback(new ResultCallback<Status>() {
//            @Override
//            public void onResult(Status status) {
//                mRequestingLocationUpdates = true;
////                setButtonsEnabledState();
//            }
//        });

    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }

    public void openGpsSetting() {
        switch (gpsStatus.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    gpsStatus.startResolutionForResult(this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }


    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void searchLocation(String refrensi) {
        NataApi service = NataService.getGoogle().create(NataApi.class);
        final Call<FindLocation> call = service.getLocation(refrensi, "false", keyGoogle);
        call.enqueue(new Callback<FindLocation>() {
            @Override
            public void onResponse(Call<FindLocation> call, retrofit2.Response<FindLocation> response) {
                results = response.body().getResult();
                geometry = results.getGeometry();
                locationses = geometry.getLocation();
                latitudeLoc = locationses.getLat();
                longitudeLoc = locationses.getLng();
                locationChange();
            }

            @Override
            public void onFailure(Call<FindLocation> call, Throwable t) {
                Log.e(TAG, t.getMessage());

            }
        });
    }

    private void locationChange() {
        googleMap.clear();
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location_on_indigo_a700_48dp);
        currentLocMarker = googleMap.addMarker(new MarkerOptions().position(
                new LatLng(latitudeLoc, longitudeLoc)).icon(icon));
        LatLng coordinate = new LatLng(latitudeLoc, longitudeLoc);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15.0f);
        googleMap.moveCamera(yourLocation);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Add Property Location");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.location);
        btnClear = (Button) findViewById(R.id.btn_clear);
        btnSave = (Button) findViewById(R.id.button_save);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_save:
                saveLocation();
                break;

            case R.id.btn_clear:
                mAutocompleteView.setText("");
                break;

        }
    }

    private void saveLocation() {
//        latProperty = latLng.latitude;
//        lngProperty = latLng.longitude;
        //latitudeLoc,longitudeLoc
        //latitudeCurrent = gps.getLatitude();
//        longitudeCurrent = gps.getLongitude();
        Log.d(TAG, "latProperty " + latProperty);
        LatLng center = googleMap.getCameraPosition().target;
        double latitudeMove = center.latitude;
        double longitudeMove = center.longitude;
        String test = "";

        if (latProperty == 0.0) {
            progressDialog = ProgressDialog.show(this, "",
                    "Please Wait...", true);
            presenter.saveMaps(listingRef, String.valueOf(latitudeMove), String.valueOf(longitudeMove));
        }else{
//            progressDialog = ProgressDialog.show(this, "",
//                    "Please Wait...", true);
//            presenter.saveMaps(listingRef, String.valueOf(latitudeMove), String.valueOf(longitudeMove));
//            if (latitudeMove2!=latitudeMove){
//                progressDialog = ProgressDialog.show(this, "",
//                        "Please Wait...", true);
//                presenter.saveMaps(listingRef, String.valueOf(latitudeMove), String.valueOf(longitudeMove));
//            } else {
                progressDialog = ProgressDialog.show(this, "",
                        "Please Wait...", true);
                presenter.saveMaps(listingRef, String.valueOf(latProperty), String.valueOf(lngProperty));
//            }

        }

    }

    public void showAddMapResults(Response<SaveMapsModel> response) {
        progressDialog.dismiss();
        String message = response.body().getMessage();

        if (addFrom == 1) {
            Intent intent1 = new Intent(this, ListingPropertyMemberActivity.class);
            intent1.putExtra("listingRef", listingRef);
            intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
            intent1.putExtra("psRef", "");
            intent1.putExtra("memberTypeCode", memberTypeCode);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
        } else if (addFrom == 2) {
            Intent intent1 = new Intent(this, ListingPropertyActivity.class);
            intent1.putExtra("listingRef", listingRef);
            intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
            intent1.putExtra("psRef", "");
            intent1.putExtra("memberTypeCode", memberTypeCode);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
        } else if (addFrom == 3) {
            Intent intent1 = new Intent(this, ListingPropertyCoBrokeActivity.class);
            intent1.putExtra("listingRef", listingRef);
            intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
            intent1.putExtra("psRef", "");
            intent1.putExtra("memberTypeCode", memberTypeCode);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
        } else {
            Intent intent1 = new Intent(this, ListingMainActivity.class);
            intent1.putExtra("listingRef", listingRef);
            intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
            intent1.putExtra("psRef", "");
            intent1.putExtra("memberTypeCode", memberTypeCode);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    public void showAddMapFailure(Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();

    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");

        // If the initial location was never previously requested, we use
        // FusedLocationApi.getLastLocation() to get it. If it was previously requested, we store
        // its value in the Bundle and check for it in onCreate(). We
        // do not request it again unless the user specifically requests location updates by pressing
        // the Start Updates button.
        //
        // Because we cache the value of the initial location in the Bundle, it means that if the
        // user launches the activity,
        // moves to a new location, and then changes the device orientation, the original location
        // is displayed as the activity is re-created.
//        if (mCurrentLocation == null) {
//            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
////            updateLocationUI();
//        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

        getCurrentLoc();

        //Toast.makeText(this, "Location Found", Toast.LENGTH_LONG).show();

    }


    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        gpsStatus = locationSettingsResult.getStatus();
        switch (gpsStatus.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    gpsStatus.startResolutionForResult(this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        if (mGoogleApiClient.isConnected()) {
                            startLocationUpdates();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    public String getCurrentLoc() {
        String currentLoc = null;

        if (LocationHelper.isLocationDetected()) {
            LocationHelper.setLatitude(mCurrentLocation.getLatitude());
            LocationHelper.setLongitude(mCurrentLocation.getLongitude());
            latitudeCurrentdtl = mCurrentLocation.getLatitude();
            longitudeCurrentdtl = mCurrentLocation.getLongitude();
        }

        return currentLoc;
    }


    public void showInfoMapResults(Response<InfoMapModel> response) {
        progressDialog.dismiss();
        String status = response.body().getStatus();
        lat = response.body().getLatitude();
        lng = response.body().getLongitude();
        latitudeCurrent = Double.parseDouble(lat);
        longitudeCurrent = Double.parseDouble(lng);
        membeRefAgent = response.body().getMemberRef();

        initMap();

    }

    private void initMap() {
        if (lat.equals("0")) {
            gps = new GPSTracker(this);
            if (gps.canGetLocation()) {

                latitudeCurrent = gps.getLatitude();
                longitudeCurrent = gps.getLongitude();

                /*latitudeLoc = latitudeCurrent;
                longitudeLoc = longitudeCurrent;*/

                // \n is for new line
//                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
            gps.stopUsingGPS();

        } else {
            latitudeCurrent = Double.parseDouble(lat);
            longitudeCurrent = Double.parseDouble(lng);
        }

        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location_on_indigo_a700_48dp);
        currentLocMarker = googleMap.addMarker(new MarkerOptions().position(
                new LatLng(latitudeCurrent, longitudeCurrent)).icon(icon));
        LatLng coordinate = new LatLng(latitudeCurrent, longitudeCurrent);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15.0f);
//        mMap.addMarker(new MarkerOptions().position(coordinate)
//                .title("Your Title")
//                .snippet("Please move the marker if needed.")
//                .draggable(true));
//        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(coordinate, 10));
//        googleMap.getCameraPosition().target;
        googleMap.moveCamera(yourLocation);

//        googleMap.getCameraPosition().target;
    }

    public void showInfoMapFailure(Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude));
        googleMap.addMarker(marker);

        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(latitude, longitude)).zoom(14).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        // check if map is created successfully or not
        if (googleMap == null) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }
    }
}
