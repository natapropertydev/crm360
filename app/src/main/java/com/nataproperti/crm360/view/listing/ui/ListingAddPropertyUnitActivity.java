package com.nataproperti.crm360.view.listing.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.snackbar.Snackbar;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.listing.adapter.ListingBathRTypeAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingBrTypeAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingCarportTypeAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingElectricTypeAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingFacingTypeAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingFurnishTypeAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingGarageTypeAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingMaidBRTypeAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingMaidBathRTypeAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingPhoneLineAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingVewTypeAdapter;
import com.nataproperti.crm360.view.listing.model.BathRTypeModel;
import com.nataproperti.crm360.view.listing.model.BrTypeModel;
import com.nataproperti.crm360.view.listing.model.CarportTypeModel;
import com.nataproperti.crm360.view.listing.model.ElectricTypeModel;
import com.nataproperti.crm360.view.listing.model.FacingTypeModel;
import com.nataproperti.crm360.view.listing.model.FurnishTypeModel;
import com.nataproperti.crm360.view.listing.model.GarageTypeModel;
import com.nataproperti.crm360.view.listing.model.MaidBRTypeModel;
import com.nataproperti.crm360.view.listing.model.MaidBathRTypeModel;
import com.nataproperti.crm360.view.listing.model.ParamSaveInformasiUnitModel;
import com.nataproperti.crm360.view.listing.model.PhoneLineModel;
import com.nataproperti.crm360.view.listing.model.ResponeListingAddPropertyUnitModel;
import com.nataproperti.crm360.view.listing.model.ResponeListingPropertyModel;
import com.nataproperti.crm360.view.listing.model.ResponeListingPropertyUnitModel;
import com.nataproperti.crm360.view.listing.model.ViewTypeModel;
import com.nataproperti.crm360.view.listing.presenter.ListingAddPropertyUnitPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

//import static com.nataproperty.android.view.listing.ui.ListingAddGalleryActivity.isCobroke;

public class ListingAddPropertyUnitActivity extends AppCompatActivity {
    private static final String EXTRA_RX = "EXTRA_RX";
    public static final String TAG = "ListingAddPropertyUnit";

    @BindView(R.id.spn_br_type)
    Spinner spnBRType;
    @BindView(R.id.spn_maidBRType)
    Spinner spnMaidBRType;
    @BindView(R.id.spn_bathRType)
    Spinner spnBathRType;
    @BindView(R.id.spn_maidBathRType)
    Spinner spnMaidBathRType;
    @BindView(R.id.spn_garageType)
    Spinner spnGarageType;
    @BindView(R.id.spn_carportType)
    Spinner spnCarportType;
    @BindView(R.id.spn_phoneLine)
    Spinner spnPhoneLine;
    @BindView(R.id.spn_furnishType)
    Spinner spnFurnishType;
    @BindView(R.id.spn_electricType)
    Spinner spnElectricType;
    @BindView(R.id.spn_facingType)
    Spinner spnFacingType;
    @BindView(R.id.spn_viewType)
    Spinner spnViewType;
    @BindView(R.id.edt_youtube1)
    EditText edtYoutube1;
    @BindView(R.id.edt_youtube2)
    EditText edtYoutube2;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.btn_finish)
    Button btnFinish;

    Typeface font;
    Toolbar toolbar;
    MyTextViewLatoReguler title;

    private ServiceRetrofit service;
    private ListingAddPropertyUnitPresenter presenter;
    private boolean rxCallInWorks = false;

    int status;
    String message;
    ArrayList<BrTypeModel> listBrType = new ArrayList<>();
    ArrayList<MaidBRTypeModel> listMaidBrType = new ArrayList<>();
    ArrayList<BathRTypeModel> listBathRType = new ArrayList<>();
    ArrayList<MaidBathRTypeModel> listMaidBathRType = new ArrayList<>();
    ArrayList<GarageTypeModel> listGarageType = new ArrayList<>();
    ArrayList<CarportTypeModel> listCarportType = new ArrayList<>();
    ArrayList<PhoneLineModel> listPhoneLine = new ArrayList<>();
    ArrayList<FurnishTypeModel> listFurnishType = new ArrayList<>();
    ArrayList<ElectricTypeModel> listElectricType = new ArrayList<>();
    ArrayList<FacingTypeModel> listFacingType = new ArrayList<>();
    ArrayList<ViewTypeModel> listViewType = new ArrayList<>();
    private ListingBrTypeAdapter listingBrTypeAdapter;
    private ListingMaidBRTypeAdapter listingMaidBRTypeAdapter;
    private ListingBathRTypeAdapter listingBathRTypeAdapter;
    private ListingMaidBathRTypeAdapter listingMaidBathRTypeAdapter;
    private ListingGarageTypeAdapter listingGarageTypeAdapter;
    private ListingCarportTypeAdapter listingCarportTypeAdapter;
    private ListingPhoneLineAdapter listingPhoneLineAdapter;
    private ListingFurnishTypeAdapter listingFurnishTypeAdapter;
    private ListingElectricTypeAdapter listingElectricTypeAdapter;
    private ListingFacingTypeAdapter listingFacingTypeAdapter;
    private ListingVewTypeAdapter listingVewTypeAdapter;

    String agencyCompanyRef, listingRef, brType = "1", maidBRType = "1", bathRType = "1", maidBathRType = "1",
            garageType = "1", carportType = "1", phoneLine = "1", furnishType = "1", electricType = "0",
            facingType = "1", viewType = "1", youtube1 = "", youtube2 = "", memberTypeCode, statusAdd;

    ParamSaveInformasiUnitModel model;
    LinearLayout snackBar;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    int addFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_add_property_unit);
        ButterKnife.bind(this);
        initToolbar();
        snackBar = (LinearLayout) findViewById(R.id.main_snackBar);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListingAddPropertyUnitPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
//        LoadingBar.stopLoader();

        Intent intent = getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        listingRef = intent.getStringExtra("listingRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        statusAdd = intent.getStringExtra("status");
        addFrom = intent.getIntExtra("addFrom", 0);
//        setupSpinner();

        Log.d("TAG", " " + listingRef);
        Log.d(TAG, " " + addFrom);

        requestAddProeprtyUnit();

        btnSave.setTypeface(font);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveInvormasiUnit(1);
            }

        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        btnFinish.setTypeface(font);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveInvormasiUnit(2);
            }
        });
    }

    /**
     * onBackPress
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "resultCode " + resultCode);
        Log.d(TAG, "listingRef " + listingRef);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                listingRef = data.getStringExtra("listingRef");

            }
        }
    }


    private void saveInvormasiUnit(int btnStatus) {
        youtube1 = edtYoutube1.getText().toString();
        youtube2 = edtYoutube2.getText().toString();
        Map<String, String> fields = new HashMap<>();
        fields.put("listingRef", listingRef);
        fields.put("BRType", brType);
        fields.put("maidBRType", maidBRType);
        fields.put("bathRType", bathRType);
        fields.put("maidBathRType", maidBathRType);
        fields.put("garageType", garageType);
        fields.put("carportType", carportType);
        fields.put("phoneLine", phoneLine);
        fields.put("furnishType", furnishType);
        fields.put("electricType", electricType);
        fields.put("facingType", facingType);
        fields.put("viewType", viewType);
        fields.put("youTube1", youtube1);
        fields.put("youTube2", youtube2);
//        LoadingBar.startLoader(this);
        presenter.saveListingInformasiUnit(fields, btnStatus);

    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Listing Property");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void requestAddProeprtyUnit() {
        presenter.getListingAddPropertyUnitSvc();
    }

    private void requestProeprtyUnit() {
        presenter.getListingPropertyUnitInfoSvc(listingRef);
    }

    public void showListingPropertyUnitResults(Response<ResponeListingAddPropertyUnitModel> response) {
        status = response.body().getStatus();
        message = response.body().getMessage();
        if (status == 200) {
            listBrType = response.body().getListBrType();
            listMaidBrType = response.body().getListMaidBrType();
            listBathRType = response.body().getListBathRType();
            listMaidBathRType = response.body().getListMaidBathRType();
            listGarageType = response.body().getListGarageType();
            listCarportType = response.body().getListCarportType();
            listPhoneLine = response.body().getListPhoneLine();
            listFurnishType = response.body().getListFurnishType();
            listElectricType = response.body().getListElectricType();
            listFacingType = response.body().getListFacingType();
            listViewType = response.body().getListViewType();
        }
//        setupSpinner();
//        requestProeprtyUnit();
//        setupSpinner();
        if(statusAdd.equals("edit")) {
//            setupSpinner();
            requestProeprtyUnit();
        }
        else{
            setupSpinner();
        }
    }

    public void showListingPropertyUnitFailure(Throwable t) {

    }

    public void showListingPropertyUnitInfoResults(Response<ResponeListingPropertyUnitModel> response) {

        if (response.body().getBRType() != null) {
            status = response.body().getStatus();
            message = response.body().getMessage();
            brType = response.body().getBRType();
            maidBRType = response.body().getMaidBRType();
            bathRType = response.body().getBathRType();
            maidBathRType = response.body().getMaidBathRType();
            garageType = response.body().getGarageType();
            carportType = response.body().getCarportType();
            phoneLine = response.body().getPhoneLine();
            furnishType = response.body().getFurnishType();
            electricType = response.body().getElectricType();
            facingType = response.body().getFacingType();
            viewType = response.body().getViewType();
            youtube2 = response.body().getYoutube2();
            youtube1 = response.body().getYoutube1();
        } else {
            Toast.makeText(ListingAddPropertyUnitActivity.this, "get info failed", Toast.LENGTH_LONG).show();
        }

//        spnBRType.setSelection(Integer.parseInt(brType));
//        spnMaidBRType.setSelection(Integer.parseInt(maidBRType));
//        spnBathRType.setSelection(Integer.parseInt(bathRType));
//        spnMaidBathRType.setSelection(Integer.parseInt(maidBathRType));
//        spnGarageType.setSelection(Integer.parseInt(garageType));
//        spnCarportType.setSelection(Integer.parseInt(carportType));
//        spnFurnishType.setSelection(Integer.parseInt(furnishType));
//        spnElectricType.setSelection(Integer.parseInt(electricType));
//        spnFacingType.setSelection(Integer.parseInt(facingType));
//        spnViewType.setSelection(Integer.parseInt(viewType));
        edtYoutube1.setText(youtube1);
        edtYoutube2.setText(youtube2);

        Log.d("TAG", brType + " " + maidBRType + " " + bathRType + " " + maidBathRType + " " + garageType + " " + carportType + " " + phoneLine + " " +
                furnishType + " " + electricType + " " + facingType + " " + viewType);
        setupSpinner2();
    }

    public void showListingPropertyUnitInfoFailure(Throwable t) {
        Toast.makeText(ListingAddPropertyUnitActivity.this, "Terjadi Kesalahan", Toast.LENGTH_LONG).show();
    }

    private void setupSpinner() {
        listingBrTypeAdapter = new ListingBrTypeAdapter(this, listBrType);
        spnBRType.setAdapter(listingBrTypeAdapter);
        spnBRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                brType = listBrType.get(position).getBrType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingMaidBRTypeAdapter = new ListingMaidBRTypeAdapter(this, listMaidBrType);
        spnMaidBRType.setAdapter(listingMaidBRTypeAdapter);
        spnMaidBRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maidBRType = listMaidBrType.get(position).getMaidBRType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingBathRTypeAdapter = new ListingBathRTypeAdapter(this, listBathRType);
        spnBathRType.setAdapter(listingBathRTypeAdapter);
        spnBathRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bathRType = listBathRType.get(position).getBathRType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingMaidBathRTypeAdapter = new ListingMaidBathRTypeAdapter(this, listMaidBathRType);
        spnMaidBathRType.setAdapter(listingMaidBathRTypeAdapter);
        spnMaidBathRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maidBathRType = listMaidBathRType.get(position).getMaidBathRType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingGarageTypeAdapter = new ListingGarageTypeAdapter(this, listGarageType);
        spnGarageType.setAdapter(listingGarageTypeAdapter);
        spnGarageType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                garageType = listGarageType.get(position).getGarageType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingCarportTypeAdapter = new ListingCarportTypeAdapter(this, listCarportType);
        spnCarportType.setAdapter(listingCarportTypeAdapter);
        spnCarportType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                carportType = listCarportType.get(position).getCarportType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingPhoneLineAdapter = new ListingPhoneLineAdapter(this, listPhoneLine);
        spnPhoneLine.setAdapter(listingPhoneLineAdapter);
        spnPhoneLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                phoneLine = listPhoneLine.get(position).getPhoneLine();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingFurnishTypeAdapter = new ListingFurnishTypeAdapter(this, listFurnishType);
        spnFurnishType.setAdapter(listingFurnishTypeAdapter);
        spnFurnishType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                furnishType = listFurnishType.get(position).getFurnishType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingElectricTypeAdapter = new ListingElectricTypeAdapter(this, listElectricType);
        spnElectricType.setAdapter(listingElectricTypeAdapter);
        spnElectricType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                electricType = listElectricType.get(position).getElectricType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingFacingTypeAdapter = new ListingFacingTypeAdapter(this, listFacingType);
        spnFacingType.setAdapter(listingFacingTypeAdapter);
        spnFacingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                facingType = listFacingType.get(position).getFacingType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingVewTypeAdapter = new ListingVewTypeAdapter(this, listViewType);
        spnViewType.setAdapter(listingVewTypeAdapter);
        spnViewType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewType = listViewType.get(position).getViewType();
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setupSpinner2() {
        listingBrTypeAdapter = new ListingBrTypeAdapter(this, listBrType);
        spnBRType.setAdapter(listingBrTypeAdapter);
        spnBRType.setSelection(Integer.parseInt(brType) - 1);
        spnBRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                brType = listBrType.get(position).getBrType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingMaidBRTypeAdapter = new ListingMaidBRTypeAdapter(this, listMaidBrType);
        spnMaidBRType.setAdapter(listingMaidBRTypeAdapter);
        spnMaidBRType.setSelection(Integer.parseInt(maidBRType) - 1);
        spnMaidBRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maidBRType = listMaidBrType.get(position).getMaidBRType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingBathRTypeAdapter = new ListingBathRTypeAdapter(this, listBathRType);
        spnBathRType.setAdapter(listingBathRTypeAdapter);
        spnBathRType.setSelection(Integer.parseInt(bathRType) - 1);
        spnBathRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bathRType = listBathRType.get(position).getBathRType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingMaidBathRTypeAdapter = new ListingMaidBathRTypeAdapter(this, listMaidBathRType);
        spnMaidBathRType.setAdapter(listingMaidBathRTypeAdapter);
        spnMaidBathRType.setSelection(Integer.parseInt(maidBathRType) - 1);
        spnMaidBathRType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maidBathRType = listMaidBathRType.get(position).getMaidBathRType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingGarageTypeAdapter = new ListingGarageTypeAdapter(this, listGarageType);
        spnGarageType.setAdapter(listingGarageTypeAdapter);
        spnGarageType.setSelection(Integer.parseInt(garageType) - 1);
        spnGarageType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                garageType = listGarageType.get(position).getGarageType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingCarportTypeAdapter = new ListingCarportTypeAdapter(this, listCarportType);
        spnCarportType.setAdapter(listingCarportTypeAdapter);
        spnCarportType.setSelection(Integer.parseInt(carportType) - 1);
        spnCarportType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                carportType = listCarportType.get(position).getCarportType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingPhoneLineAdapter = new ListingPhoneLineAdapter(this, listPhoneLine);
        spnPhoneLine.setAdapter(listingPhoneLineAdapter);
        spnPhoneLine.setSelection(Integer.parseInt(phoneLine) - 1);
        spnPhoneLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                phoneLine = listPhoneLine.get(position).getPhoneLine();//phoneLine


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingFurnishTypeAdapter = new ListingFurnishTypeAdapter(this, listFurnishType);
        spnFurnishType.setAdapter(listingFurnishTypeAdapter);
        spnFurnishType.setSelection(Integer.parseInt(furnishType));
        spnFurnishType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                furnishType = listFurnishType.get(position).getFurnishType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingElectricTypeAdapter = new ListingElectricTypeAdapter(this, listElectricType);
        spnElectricType.setAdapter(listingElectricTypeAdapter);
        spnElectricType.setSelection(Integer.parseInt(electricType));
        spnElectricType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                electricType = listElectricType.get(position).getElectricType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingFacingTypeAdapter = new ListingFacingTypeAdapter(this, listFacingType);
        spnFacingType.setAdapter(listingFacingTypeAdapter);
        spnFacingType.setSelection(Integer.parseInt(facingType));
        spnFacingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                facingType = listFacingType.get(position).getFacingType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listingVewTypeAdapter = new ListingVewTypeAdapter(this, listViewType);
        spnViewType.setAdapter(listingVewTypeAdapter);
        spnViewType.setSelection(Integer.parseInt(viewType));
        spnViewType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                viewType = listViewType.get(position).getViewType();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void showResultInformation(Response<ResponeListingPropertyModel> response, int btnStatus) {
        int status = response.body().getStatus();
        String message = response.body().getMessage();
//        Log.d("result",String.valueOf(test));
        //insert succeed
        if (btnStatus == 1) {
            if (status == 200) {
                Intent intent = new Intent(this, ListingAddFacilityActivity.class);
                intent.putExtra("listingRef", listingRef);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("memberTypeCode", memberTypeCode);
                intent.putExtra("addFrom", addFrom);
                startActivityForResult(intent, 1);
            } else {
                Snackbar snackbar = Snackbar.make(snackBar, "Save Failed.. Please Check Your Network", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        } else {
            if (addFrom == 1) {
                Intent intent1 = new Intent(this, ListingPropertyMemberActivity.class);
                intent1.putExtra("listingRef", listingRef);
                intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent1.putExtra("psRef", "");
                intent1.putExtra("memberTypeCode", memberTypeCode);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            } else if (addFrom == 2) {
                Intent intent1 = new Intent(this, ListingPropertyActivity.class);
                intent1.putExtra("listingRef", listingRef);
                intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent1.putExtra("psRef", "");
                intent1.putExtra("memberTypeCode", memberTypeCode);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            } else if (addFrom == 3) {
                Intent intent1 = new Intent(this, ListingPropertyCoBrokeActivity.class);
                intent1.putExtra("listingRef", listingRef);
                intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent1.putExtra("psRef", "");
                intent1.putExtra("memberTypeCode", memberTypeCode);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            } else {
                Intent intent1 = new Intent(this, ListingMainActivity.class);
                intent1.putExtra("listingRef", listingRef);
                intent1.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent1.putExtra("psRef", "");
                intent1.putExtra("memberTypeCode", memberTypeCode);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
            }
        }
    }

    public void showResultInformationFailure(Throwable t) {
        Snackbar snackbar = Snackbar
                .make(snackBar, "Save Failed.. Please Check Your Network", Snackbar.LENGTH_LONG);

        snackbar.show();

    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("ListingAddPropertyUnit Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("listingRef", listingRef);
        setResult(RESULT_OK, intent);

        super.onBackPressed();
    }

}
