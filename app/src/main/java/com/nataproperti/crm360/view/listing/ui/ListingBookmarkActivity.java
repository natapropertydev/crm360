package com.nataproperti.crm360.view.listing.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.ybq.endless.Endless;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.model.ListingPropertyModel;
import com.nataproperti.crm360.view.listing.model.ListingPropertyStatus;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.presenter.ListingBookmarkPresenter;
import com.nataproperti.crm360.view.listing.adapter.RVListingBookmarkAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListingBookmarkActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    public static final String TAG = "ListingSearchProperty";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private ListingBookmarkPresenter
            presenter;
    ProgressDialog progressDialog;
    int setCity = 0;
    private List<ListingPropertyModel> listProperty = new ArrayList<>();
    private List<ListingPropertyModel> listProperty2 = new ArrayList<>();


    RecyclerView rvSearchProperty;
    private SharedPreferences sharedPreferences;

    int countTotal;
    Toolbar toolbar;
    Typeface font;
    Display display;
    RVListingBookmarkAdapter adapter;
    Button nextBtn;
    View loadingView;
    private int page = 1;
    Endless endless;
    MyTextViewLatoReguler title;
    private String provinceCode, agencyCompanyRef, cityCode, subLocation, listingTypeRef, categoryType, aboutMe, quotes, memberType,
            imageCover,listingRefpsRef,memberRef,psRef,memberTypeCode;

    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_member);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListingBookmarkPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        Intent intent = getIntent();
        psRef = intent.getStringExtra("psRef");
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        imageCover = intent.getStringExtra("imageCover");
        memberTypeCode = intent.getStringExtra("memberTypeCode");

        Log.d(TAG, "agencyCompanyRef " + agencyCompanyRef);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        initWidget();
        listingMember();
        adapter = new RVListingBookmarkAdapter(this,listProperty2,display,memberTypeCode,agencyCompanyRef);
        rvSearchProperty.setAdapter(adapter);
        //adapter.notifyDataSetChanged();

        loadingView = View.inflate(this, R.layout.loading, null);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        endless = Endless.applyTo(rvSearchProperty,
                loadingView
        );
        endless.setAdapter(adapter);
        endless.setLoadMoreListener(new Endless.LoadMoreListener() {
            @Override
            public void onLoadMore(int page) {
                presenter.getListingMember(agencyCompanyRef,"1",String.valueOf(page),memberRef);
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.GONE);

    }

    private void listingMember() {
        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getListingMember(agencyCompanyRef,"1",String.valueOf(page),memberRef);
    }



    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        setSupportActionBar(toolbar);
        title.setText("Favorite");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        rvSearchProperty = (RecyclerView)findViewById(R.id.rv_list_search_project);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvSearchProperty.setLayoutManager(llm);

    }

    @Override
    public void onClick(View v) {

    }

    private void initAdapter() {
//        progressDialog.dismiss();
        adapter = new RVListingBookmarkAdapter(this,listProperty,display,memberTypeCode,agencyCompanyRef);
        rvSearchProperty.setAdapter(adapter);
//        adapter.notifyDataSetChanged();

    }


    public void showListingMemberResults(Response<ListingPropertyStatus> response,String page) {
        progressDialog.dismiss();
        int totalPage = response.body().getTotalPage();
        agencyCompanyRef = response.body().getAgencyCompanyRef();
        int count = Integer.parseInt(response.body().getCount());
        listProperty = response.body().getData();
        Log.d(TAG, "bodySize " + listProperty.size());
        Log.d(TAG, "responsePage " + page);

        if (count > 0){
            if (Integer.parseInt(page) > totalPage) {
                //Toast.makeText(ListingPropertyActivity.this, "No data available", Toast.LENGTH_LONG).show();
                loadingView.setVisibility(View.GONE);
            } else {
                if (Integer.parseInt(page) == 1) {
                    for (int i = 0; i < listProperty.size(); i++) {
                        ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                        listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                        listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                        listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                        listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                        listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                        listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                        listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                        listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                        listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                        listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                        listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                        listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                        listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                        listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                        listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                        listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                        listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                        listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                        listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                        listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                        listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                        listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                        listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                        listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                        listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                        listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                        listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                        listingFavoritModel.setHp(listProperty.get(i).getHp());
                        listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                        listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                        listingFavoritModel.setDesc(listProperty.get(i).getDesc());
                        listProperty2.add(listingFavoritModel);
                    }
                    adapter.notifyDataSetChanged();
                }   else {
                    for (int i = 0; i < listProperty.size(); i++) {
                        ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                        listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                        listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                        listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                        listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                        listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                        listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                        listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                        listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                        listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                        listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                        listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                        listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                        listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                        listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                        listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                        listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                        listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                        listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                        listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                        listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                        listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                        listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                        listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                        listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                        listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                        listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                        listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                        listingFavoritModel.setHp(listProperty.get(i).getHp());
                        listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                        listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                        listingFavoritModel.setDesc(listProperty.get(i).getDesc());
                        listProperty2.add(listingFavoritModel);
                    }
                    adapter.notifyDataSetChanged();
                    endless.loadMoreComplete();
                }
            }
        } else {
            if (memberTypeCode.equals("2")){
                AlertDialog.Builder builderInner = new AlertDialog.Builder(this);
                builderInner.setMessage("Anda tidak mempunyai listing favorite, untuk menambahkan favorite klik tombol love " +
                        "di menu Co-Broking atau Agency Listing");
                builderInner.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                builderInner.show();
            } else {
                AlertDialog.Builder builderInner = new AlertDialog.Builder(this);
                builderInner.setMessage("Anda tidak mempunyai listing favorite, untuk menambahkan favorite klik tombol love " +
                        "di menu Co-Broking");
                builderInner.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                builderInner.show();
            }

        }


        /*countTotal = response.body().getTotalPage();

        if (countTotal != 0) {
            if (page <= countTotal) {
                listProperty = response.body().getData();
//                listProperty2.clear();
                for (int i = 0; i < listProperty.size(); i++) {
                    ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                    listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                    listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                    listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                    listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                    listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                    listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                    listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                    listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                    listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                    listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                    listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                    listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                    listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                    listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                    listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                    listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                    listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                    listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                    listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                    listingFavoritModel.setHp(listProperty.get(i).getHp());
                    listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                    listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                    listProperty2.add(listingFavoritModel);
                }
                initAdapter();
            }
        }*/

    }

    public void showListingMemberFailure(Throwable t) {
        progressDialog.dismiss();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);

    }
}

