package com.nataproperti.crm360.view.listing.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.LoadingBar;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.nataproperty.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListingCompanyInfoActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref" ;

    SharedPreferences sharedPreferences;

    String memberRef,agencyCompanyRef,agencyRef,companyCode,companyName,companyPhone,companyHp,companyEmail,companyCountryName,companyProvinceName,
            companyCityName,companyPostCode,companyAddress,principleName,principleHP1,principleHP2,principleEmail1,principleEmail2,npwp,siup,tdp,
            domisili,sppkp,ptName,bankRef,bankName,bankAccNo,bankBranch,bankAccName,imageLogo, memberTypeCode;

    @BindView(R.id.txt_agency_code)
    TextView txtAgencyCode;
    @BindView(R.id.txt_agency_name)
    TextView txtAgencyName;
    @BindView(R.id.txt_agency_email)
    TextView txtAgencyEmail;
    @BindView(R.id.txt_agency_mobile)
    TextView txtAgencyMobile;
    @BindView(R.id.txt_agency_phone)
    TextView txtAgencyPhone;

    @BindView(R.id.txt_country)
    TextView txtCompanyCountry;
    @BindView(R.id.txt_province)
    TextView txtCompanyProvince;
    @BindView(R.id.txt_city)
    TextView txtCompanyCity;
    @BindView(R.id.txt_post_code)
    TextView txtCompanyPostCode;
    @BindView(R.id.txt_address)
    TextView txtCompanyAddress;

    @BindView(R.id.txt_principle_name)
    TextView txtPrincipleName;
    @BindView(R.id.txt_principle_email1)
    TextView txtPrincipleEmail1;
    @BindView(R.id.txt_principle_email2)
    TextView txtPrincipleEmail2;
    @BindView(R.id.txt_principle_hp1)
    TextView txtPrincipleHp1;
    @BindView(R.id.txt_principle_hp2)
    TextView txtPrincipleHp2;

    @BindView(R.id.txt_pt_name)
    TextView txtPtName;
    @BindView(R.id.txt_company_npwp)
    TextView txtNPWP;
    @BindView(R.id.txt_company_sppkp)
    TextView txtSPPKP;
    @BindView(R.id.txt_company_tdp)
    TextView txtTDP;
    @BindView(R.id.txt_company_siup)
    TextView txtSIUP;
    @BindView(R.id.txt_company_sk_domisili)
    TextView txtSkDomisili;

    @BindView(R.id.txt_account_bank)
    TextView txtAccountBank;
    @BindView(R.id.txt_account_name)
    TextView txtAccountName;
    @BindView(R.id.txt_account_nomor)
    TextView txtAccountNomor;
    @BindView(R.id.txt_bank_branch)
    TextView txtBankBranch;
    @BindView(R.id.btn_change_agency_code)
    Button btnChanegAgencyCode;

    ImageView imageView;
    Toolbar toolbar;
    TextView title;
    Typeface font;
    RelativeLayout rPage;
    EditText edtAgencyCode;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_company_info);
        ButterKnife.bind(this);
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef",null );

        agencyCompanyRef = getIntent().getStringExtra("agencyCompanyRef");
        imageLogo = getIntent().getStringExtra("imageLogo");
        memberTypeCode  = getIntent().getStringExtra("memberTypeCode");

        Glide.with(this).load(imageLogo).centerCrop().into(imageView);

        requestCompanyInfo();

        if (memberTypeCode.equals(WebService.propertyAgency)){
            btnChanegAgencyCode.setVisibility(View.VISIBLE);
        } else {
            btnChanegAgencyCode.setVisibility(View.GONE);
        }

        btnChanegAgencyCode.setTypeface(font);
        btnChanegAgencyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ListingCompanyInfoActivity.this);
                LayoutInflater inflater = ListingCompanyInfoActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_change_agency_code, null);
                dialogBuilder.setView(dialogView);
                edtAgencyCode = (EditText) dialogView.findViewById(R.id.edt_agency_code);

                dialogBuilder.setMessage("Masukkan Company Code");

                dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //subscribeProject();
                    }
                });
                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                alertDialog = dialogBuilder.create();
                alertDialog.setCancelable(false);
                alertDialog.show();
                Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edtAgencyCode.getText().toString().equals("")) {
                            edtAgencyCode.setError("Harus diisi.");
                        } else {
                            changeAgencyCode();
                        }
                    }
                });
            }
        });
    }

    private void initWidget(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Informasi Perusahaan");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imageView = (ImageView) findViewById(R.id.image_projek);
        rPage = (RelativeLayout) findViewById(R.id.rPage);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));
        Double widthButton = size.x / 3.0;
        Double heightButton = widthButton / 1.0;

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
    }

    public void requestCompanyInfo() {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListingCompanyInfo(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");

                    if (status==200){
                        /*Agency*/
                        companyCode = jsonObject.getJSONObject("data").getString("companyCode");
                        companyName = jsonObject.getJSONObject("data").getString("companyName");
                        companyEmail = jsonObject.getJSONObject("data").getString("companyEmail");
                        companyHp = jsonObject.getJSONObject("data").getString("companyHp");
                        companyPhone = jsonObject.getJSONObject("data").getString("companyPhone");

                        txtAgencyCode.setText(companyCode);

                        if (companyName.equals("")) {
                            txtAgencyName.setText("-");
                        } else {
                            txtAgencyName.setText(companyName);
                        }

                        if (companyEmail.equals("")) {
                            txtAgencyEmail.setText("-");
                        } else {
                            txtAgencyEmail.setText(companyEmail);
                        }

                        if (companyHp.equals("")) {
                            txtAgencyMobile.setText("-");
                        } else {
                            txtAgencyMobile.setText(companyHp);
                        }

                        if (companyPhone.equals("")) {
                            txtAgencyPhone.setText("-");
                        } else {
                            txtAgencyPhone.setText(companyPhone);
                        }

                        /*Address*/
                        companyCountryName = jsonObject.getJSONObject("data").getString("companyCountryName");
                        companyProvinceName = jsonObject.getJSONObject("data").getString("companyProvinceName");
                        companyCityName = jsonObject.getJSONObject("data").getString("companyCityName");
                        companyPostCode = jsonObject.getJSONObject("data").getString("companyPostCode");
                        companyAddress = jsonObject.getJSONObject("data").getString("companyAddress");

                        txtCompanyCountry.setText(companyCountryName);
                        txtCompanyProvince.setText(companyProvinceName);
                        txtCompanyCity.setText(companyCityName);

                        if (companyPostCode.equals("")) {
                            txtCompanyPostCode.setText("-");
                        } else {
                            txtCompanyPostCode.setText(companyPostCode);
                        }

                        if (companyAddress.equals("")) {
                            txtCompanyAddress.setText("-");
                        } else {
                            txtCompanyAddress.setText(companyAddress);
                        }

                        /*Principle*/
                        principleName = jsonObject.getJSONObject("data").getString("principleName");
                        principleHP1 = jsonObject.getJSONObject("data").getString("principleHP1");
                        principleHP2 = jsonObject.getJSONObject("data").getString("principleHP2");
                        principleEmail1 = jsonObject.getJSONObject("data").getString("principleEmail1");
                        principleEmail2 = jsonObject.getJSONObject("data").getString("principleEmail2");

                        if (principleName.equals("")) {
                            txtPrincipleName.setText("-");
                        } else {
                            txtPrincipleName.setText(principleName);
                        }

                        if (principleHP1.equals("")) {
                            txtPrincipleHp1.setText("-");
                        } else {
                            txtPrincipleHp1.setText(principleHP1);
                        }

                        if (principleHP2.equals("")) {
                            txtPrincipleHp2.setText("-");
                        } else {
                            txtPrincipleHp2.setText(principleHP2);
                        }

                        if (principleEmail1.equals("")) {
                            txtPrincipleEmail1.setText("-");
                        } else {
                            txtPrincipleEmail1.setText(principleEmail1);
                        }

                        if (principleEmail2.equals("")) {
                            txtPrincipleEmail2.setText("-");
                        } else {
                            txtPrincipleEmail2.setText(principleEmail2);
                        }

                        /*AgencyInfo*/
                        ptName = jsonObject.getJSONObject("data").getString("ptName");
                        npwp = jsonObject.getJSONObject("data").getString("npwp");
                        siup = jsonObject.getJSONObject("data").getString("siup");
                        sppkp = jsonObject.getJSONObject("data").getString("sppkp");
                        tdp = jsonObject.getJSONObject("data").getString("tdp");
                        domisili = jsonObject.getJSONObject("data").getString("domisili");

                        if (ptName.equals("")) {
                            txtPtName.setText("-");
                        } else {
                            txtPtName.setText(ptName);
                        }

                        if (npwp.equals("")) {
                            txtNPWP.setText("-");
                        } else {
                            txtNPWP.setText(npwp);
                        }

                        if (siup.equals("")) {
                            txtSIUP.setText("-");
                        } else {
                            txtSIUP.setText(siup);
                        }

                        if (sppkp.equals("")) {
                            txtSPPKP.setText("-");
                        } else {
                            txtSPPKP.setText(sppkp);
                        }

                        if (tdp.equals("")) {
                            txtTDP.setText("-");
                        } else {
                            txtTDP.setText(tdp);
                        }

                        if (domisili.equals("")) {
                            txtSkDomisili.setText("-");
                        } else {
                            txtSkDomisili.setText(domisili);
                        }

                        /*Bank*/
                        bankRef = jsonObject.getJSONObject("data").getString("bankRef");
                        bankName = jsonObject.getJSONObject("data").getString("bankName");
                        bankAccNo = jsonObject.getJSONObject("data").getString("bankAccNo");
                        bankAccName = jsonObject.getJSONObject("data").getString("bankAccName");
                        bankBranch = jsonObject.getJSONObject("data").getString("bankBranch");

                        if (bankRef.equals("0")){
                            txtAccountBank.setText("-");
                        } else {
                            txtAccountBank.setText(bankName);
                        }

                        if (bankAccName.equals("")){
                            txtAccountName.setText("-");
                        } else {
                            txtAccountName.setText(bankAccName);
                        }

                        if (bankAccNo.equals("")){
                            txtAccountNomor.setText("-");
                        }else {
                            txtAccountNomor.setText(bankAccNo);
                        }

                        if (bankBranch.equals("")){
                            txtBankBranch.setText("-");
                        }else {
                            txtBankBranch.setText(bankBranch);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ListingCompanyInfoActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("agencyCompanyRef", agencyCompanyRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "companyInfo");

    }

    private void changeAgencyCode() {
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.ChangeAgencyCodeSvc(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                LoadingBar.stopLoader();
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    String agencyCompanyRef = jo.getString("agencyCompanyRef");
                    String imageLogo = jo.getString("imageLogo");
                    String memberTypeCode = jo.getString("memberType");
                    String companyName = jo.getString("companyName");

                    if (status == 200) {
                        Toast.makeText(ListingCompanyInfoActivity.this, message, Toast.LENGTH_LONG).show();
                        alertDialog.dismiss();
                        //Intent intent = new Intent(ListingCompanyInfoActivity.this, MainMenuActivity.class);
                        Intent intent = new Intent(ListingCompanyInfoActivity.this, ListingMainActivity.class);
                        intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                        intent.putExtra("imageLogo", imageLogo);
                        intent.putExtra("memberTypeCode", memberTypeCode);
                        intent.putExtra("companyName", companyName);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else if (status == 201) {
                        edtAgencyCode.setError(message);
                    } else if (status == 202) {
                        edtAgencyCode.setError(message);
                    } else {
                        Toast.makeText(ListingCompanyInfoActivity.this, message, Toast.LENGTH_LONG).show();
                        alertDialog.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(ListingCompanyInfoActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingCompanyInfoActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("companyCode", edtAgencyCode.getText().toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "changeAgencyCode");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    protected void onStop() {
        super.onStop();
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

    }
}
