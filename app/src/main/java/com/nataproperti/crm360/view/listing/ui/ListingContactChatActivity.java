package com.nataproperti.crm360.view.listing.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.ybq.endless.Endless;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.helper.MyEditTextLatoReguler;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.adapter.RvContactMessageAdapter;
import com.nataproperti.crm360.view.listing.model.ListingMemberInfoModel;
import com.nataproperti.crm360.view.listing.presenter.ListContactChatPresenter;
import com.nataproperti.crm360.view.profile.model.BaseData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

//import com.arlib.floatingsearchview.FloatingSearchView;

/**
 * Created by nata on 11/17/2016.
 */

public class ListingContactChatActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    private int page = 1;

    private String memberRef, agencyCompanyRef,memberTypeCode;
    private SharedPreferences sharedPreferences;
    private Typeface font;

    private RecyclerView recyclerView;
    private BaseData baseData;
    private List<ListingMemberInfoModel> listingMemberInfoModels = new ArrayList<>();
    private List<ListingMemberInfoModel> listingMemberInfoModelss = new ArrayList<>();
    private Display display;

    public static final String TAG = "ListingMainActivity";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    //    private List<ListCountChatModel> listCountChatModels = new ArrayList<>();
//    private List<ListCountChatModel> listCountChatModels2 = new ArrayList<>();
    private ListContactChatPresenter presenter;
    ProgressDialog progressDialog;

    View loadingView;
    RvContactMessageAdapter adapter;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    MyEditTextLatoReguler search;
    ImageView btnSearch, btnGo;
    Endless endless;
    String searchQuery = "";
    String newQuery;

    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_contact_chat);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListContactChatPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        requestContact(agencyCompanyRef, page);
        initWidget();
        initAdapter();

        loadingView = View.inflate(this, R.layout.loading, null);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        endless = Endless.applyTo(recyclerView, loadingView);
        endless.setAdapter(adapter);
        endless.setLoadMoreListener(new Endless.LoadMoreListener() {
            @Override
            public void onLoadMore(int page) {
                requestContact(agencyCompanyRef, page);
            }
        });

        /*searchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {
                searchQuery = newQuery;
                searchContact();
            }
        });*/
//        adapter = new RvContactMessageAdapter(ListingContactChatActivity.this,listingMemberInfoModelss,display);
//        recyclerView.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
//        loadingView = View.inflate(this, R.layout.loading, null);
//        loadingView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT));
//        endless = Endless.applyTo(recyclerView,
//                loadingView
//        );
//        endless.setAdapter(adapter);
//        endless.setLoadMoreListener(new Endless.LoadMoreListener() {
//            @Override
//            public void onLoadMore(int page) {
//                requestContact(agencyCompanyRef, page);
//            }
//        });
    }

    /*private void contactRequest( ) {
        requestContact(agencyCompanyRef);
    }*/

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Member Kontak");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        recyclerView = (RecyclerView) findViewById(R.id.list_contact);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager
                (ListingContactChatActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        //btnSearch.setOnClickListener(this);

    }

    private void requestContact(String agencyCompanyRef, int page) {
//        progressDialog = ProgressDialog.show(this, "",
//                "Please Wait...", true);
        presenter.getContactChatPage(agencyCompanyRef, page);
    }

    private void initAdapter() {

        adapter = new RvContactMessageAdapter(ListingContactChatActivity.this, listingMemberInfoModelss, display,memberTypeCode);
        recyclerView.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
//        loadingView = View.inflate(this, R.layout.loading, null);
//        loadingView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT));
//        endless = Endless.applyTo(recyclerView,
//                loadingView
//        );
//        endless.setAdapter(adapter);
//        endless.setLoadMoreListener(new Endless.LoadMoreListener() {
//            @Override
//            public void onLoadMore(int page) {
//                requestContact(agencyCompanyRef, page);
//            }
//        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_search:
                title.setVisibility(View.GONE);
                btnSearch.setVisibility(View.GONE);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);

                break;

        }
    }

    public void showContactChatResults(Response<BaseData> response) {
//        progressDialog.dismiss();
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        baseData = response.body();
        listingMemberInfoModels = response.body().getData();
        int totalPage = Integer.parseInt(response.body().getTotalPage());
        /*if (page > totalPage) {
            //Toast.makeText(ListingContactChatActivity.this, "No data available", Toast.LENGTH_LONG).show();
            loadingView.setVisibility(View.GONE);
        } else {
            Log.d("contact", "" + listingMemberInfoModels.size());
            for (int i = 0; i < listingMemberInfoModels.size(); i++) {
                ListingMemberInfoModel listingMemberInfoModel = new ListingMemberInfoModel();
                listingMemberInfoModel.setIdAgent(listingMemberInfoModels.get(i).getIdAgent());
                listingMemberInfoModel.setMemberRef(listingMemberInfoModels.get(i).getMemberRef());
                listingMemberInfoModel.setAboutMe(listingMemberInfoModels.get(i).getAboutMe());
                listingMemberInfoModel.setAddress(listingMemberInfoModels.get(i).getAddress());
                listingMemberInfoModel.setEmail(listingMemberInfoModels.get(i).getEmail());
                listingMemberInfoModel.setHp1(listingMemberInfoModels.get(i).getHp1());
                listingMemberInfoModel.setListingMemberRef(listingMemberInfoModels.get(i).getListingMemberRef());
                listingMemberInfoModel.setName(listingMemberInfoModels.get(i).getName());
                listingMemberInfoModel.setPosision(listingMemberInfoModels.get(i).getPosision());
                listingMemberInfoModel.setPsRef(listingMemberInfoModels.get(i).getPsRef());
                listingMemberInfoModel.setQuotes(listingMemberInfoModels.get(i).getQuotes());
                ListingMemberInfoModelDao listingMemberInfoModelDao = daoSession.getListingMemberInfoModelDao();
                listingMemberInfoModelDao.insertOrReplace(listingMemberInfoModel);
                listingMemberInfoModelss.add(listingMemberInfoModel);
            }
//                    endless.loadMoreComplete();
        }*/
//        for (int i = 0; i < listingMemberInfoModels.size(); i++) {
//            ListingMemberInfoModel listingMemberInfoModel = new ListingMemberInfoModel();
//            listingMemberInfoModel.setIdAgent(listingMemberInfoModels.get(i).getIdAgent());
//            listingMemberInfoModel.setMemberRef(listingMemberInfoModels.get(i).getMemberRef());
//            listingMemberInfoModel.setAboutMe(listingMemberInfoModels.get(i).getAboutMe());
//            listingMemberInfoModel.setAddress(listingMemberInfoModels.get(i).getAddress());
//            listingMemberInfoModel.setEmail(listingMemberInfoModels.get(i).getEmail());
//            listingMemberInfoModel.setHp1(listingMemberInfoModels.get(i).getHp1());
//            listingMemberInfoModel.setListingMemberRef(listingMemberInfoModels.get(i).getListingMemberRef());
//            listingMemberInfoModel.setName(listingMemberInfoModels.get(i).getName());
//            listingMemberInfoModel.setPosision(listingMemberInfoModels.get(i).getPosision());
//            listingMemberInfoModel.setPsRef(listingMemberInfoModels.get(i).getPsRef());
//            listingMemberInfoModel.setQuotes(listingMemberInfoModels.get(i).getQuotes());
//            ListingMemberInfoModelDao listingMemberInfoModelDao = daoSession.getListingMemberInfoModelDao();
//            listingMemberInfoModelDao.insertOrReplace(listingMemberInfoModel);
//            listingMemberInfoModelss.add(listingMemberInfoModel);
//        }
        getListShowData();


    }

    public void showContactChatFailure(Throwable t) {
//        progressDialog.dismiss();
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
//        ListingMemberInfoModelDao listingMemberInfoModelDao = daoSession.getListingMemberInfoModelDao();
//        List<ListingMemberInfoModel> listingMemberInfoModels = listingMemberInfoModelDao.queryBuilder()
//                .list();
//        Log.d("getProjectLocalDB", "" + listingMemberInfoModels.toString());
//        int numData = listingMemberInfoModels.size();
//        if (numData > 0) {
//            for (int i = 0; i < numData; i++) {
//                ListingMemberInfoModel listingMemberInfoModel = listingMemberInfoModels.get(i);
//                listingMemberInfoModelss.add(listingMemberInfoModel);
//            }
//            getListShowData();
//        }
    }

    public void showContactChatPageResults(Response<BaseData> response, int page) {
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        int totalPage = Integer.parseInt(response.body().getTotalPage());
        listingMemberInfoModels = response.body().getData();
        if (page > totalPage) {
            //Toast.makeText(ListingPropertyActivity.this, "No data available", Toast.LENGTH_LONG).show();
            loadingView.setVisibility(View.GONE);
        } else {
            if (page == 1) {
                for (int i = 0; i < listingMemberInfoModels.size(); i++) {
                    ListingMemberInfoModel listingMemberInfoModel = new ListingMemberInfoModel();
                    listingMemberInfoModel.setIdAgent(listingMemberInfoModels.get(i).getIdAgent());
                    listingMemberInfoModel.setMemberRef(listingMemberInfoModels.get(i).getMemberRef());
                    listingMemberInfoModel.setAboutMe(listingMemberInfoModels.get(i).getAboutMe());
                    listingMemberInfoModel.setAddress(listingMemberInfoModels.get(i).getAddress());
                    listingMemberInfoModel.setEmail(listingMemberInfoModels.get(i).getEmail());
                    listingMemberInfoModel.setHp1(listingMemberInfoModels.get(i).getHp1());
                    listingMemberInfoModel.setListingMemberRef(listingMemberInfoModels.get(i).getListingMemberRef());
                    listingMemberInfoModel.setName(listingMemberInfoModels.get(i).getName());
                    listingMemberInfoModel.setPosision(listingMemberInfoModels.get(i).getPosision());
                    listingMemberInfoModel.setPsRef(listingMemberInfoModels.get(i).getPsRef());
                    listingMemberInfoModel.setQuotes(listingMemberInfoModels.get(i).getQuotes());
//                    ListingMemberInfoModelDao listingMemberInfoModelDao = daoSession.getListingMemberInfoModelDao();
//                    listingMemberInfoModelDao.insertOrReplace(listingMemberInfoModel);
//                    listingMemberInfoModelss.add(listingMemberInfoModel);
                }
                adapter.notifyDataSetChanged();
            } else {
                for (int i = 0; i < listingMemberInfoModels.size(); i++) {
                    ListingMemberInfoModel listingMemberInfoModel = new ListingMemberInfoModel();
                    listingMemberInfoModel.setIdAgent(listingMemberInfoModels.get(i).getIdAgent());
                    listingMemberInfoModel.setMemberRef(listingMemberInfoModels.get(i).getMemberRef());
                    listingMemberInfoModel.setAboutMe(listingMemberInfoModels.get(i).getAboutMe());
                    listingMemberInfoModel.setAddress(listingMemberInfoModels.get(i).getAddress());
                    listingMemberInfoModel.setEmail(listingMemberInfoModels.get(i).getEmail());
                    listingMemberInfoModel.setHp1(listingMemberInfoModels.get(i).getHp1());
                    listingMemberInfoModel.setListingMemberRef(listingMemberInfoModels.get(i).getListingMemberRef());
                    listingMemberInfoModel.setName(listingMemberInfoModels.get(i).getName());
                    listingMemberInfoModel.setPosision(listingMemberInfoModels.get(i).getPosision());
                    listingMemberInfoModel.setPsRef(listingMemberInfoModels.get(i).getPsRef());
                    listingMemberInfoModel.setQuotes(listingMemberInfoModels.get(i).getQuotes());
//                    ListingMemberInfoModelDao listingMemberInfoModelDao = daoSession.getListingMemberInfoModelDao();
//                    listingMemberInfoModelDao.insertOrReplace(listingMemberInfoModel);
//                    listingMemberInfoModelss.add(listingMemberInfoModel);
                }
                adapter.notifyDataSetChanged();
                endless.loadMoreComplete();
            }
        }

    }

    public void showContactChatPageFailure(Throwable t) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_icon, menu);

        /*final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchQuery = newText;
                searchContact();
                return true;
            }
        });*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_search:
                Intent intent = new Intent(ListingContactChatActivity.this,ListingSearchContactChatActivity.class);
                intent.putExtra("agencyCompanyRef",agencyCompanyRef);
                intent.putExtra("memberTypeCode",memberTypeCode);
                startActivity(intent);
                return true;


        }

        return super.onOptionsItemSelected(item);

    }

    private void searchContact() {
        recyclerView.removeAllViews();
        getListShowData();
    }

    private void getListShowData() {
        int numData = listingMemberInfoModels.size();
        listingMemberInfoModelss.clear();
        for (int i = 0; i < numData; i++) {
            ListingMemberInfoModel listingMemberInfoModel = listingMemberInfoModels.get(i);
            boolean addData = false;
            if (!searchQuery.trim().equals("")) {
                if ((listingMemberInfoModel.getName().toLowerCase().contains(searchQuery.toLowerCase())
                        || (listingMemberInfoModel.getMemberRef() + "".toLowerCase()).contains(searchQuery.toLowerCase()))) {
                    addData = true;

                }
            } else {
                addData = true;
            }
            if (addData) {
                listingMemberInfoModelss.add(listingMemberInfoModel);//listAllDataCafeInfo
            } else {
            }
            initAdapter();
        }
    }

}
