package com.nataproperti.crm360.view.listing.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.gcm.ConfigGCM;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.adapter.RvChatHistoryAdapter;
import com.nataproperti.crm360.view.listing.model.ChatHistoryModel;
import com.nataproperti.crm360.view.listing.presenter.ListHistoryChatPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListingHistoryChatActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";

    private String memberRef, agencyCompanyRef, memberTypeCode;
    private SharedPreferences sharedPreferences;
    public static final String TAG = "ListingMainActivity";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private ListHistoryChatPresenter presenter;
    ProgressDialog progressDialog;
    private Typeface font;
    String memberName, message;
    private RecyclerView recyclerView;
    private List<ChatHistoryModel> historyModelList = new ArrayList<>();
    private Display display;
    Intent intent;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    LinearLayoutManager linearLayoutManager;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private FloatingActionButton floatingActionButton;

    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_history_chat);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListHistoryChatPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();
        intent = getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberName = sharedPreferences.getString("isName", null);
        //requestHistory();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListingHistoryChatActivity.this, ListingContactChatActivity.class);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("memberTypeCode", memberTypeCode);
                startActivity(intent);
            }
        });
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ConfigGCM.PUSH_NOTIFICATION)) {
                    // new push message is received
//                    timerHandler.removeCallbacks(timerRunnable);
                    handlePushNotification(intent);
                }
            }
        };

    }

    private void handlePushNotification(Intent intent) {
        message = intent.getStringExtra("message");

        if (message != null) {
            historyModelList.clear();
            requestHistory();

        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Pesan");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        recyclerView = (RecyclerView) findViewById(R.id.list_history);
        linearLayoutManager = new LinearLayoutManager(ListingHistoryChatActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(ConfigGCM.PUSH_NOTIFICATION));
        historyModelList.clear();
        requestHistory();
    }

    private void requestHistory() {
//        progressDialog = ProgressDialog.show(this, "",
//                "Please Wait...", true);
        presenter.getHistoryChat(memberRef);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    public void showHistoryChatResults(Response<List<ChatHistoryModel>> response) {
//        progressDialog.dismiss();
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        historyModelList = response.body();

        if (historyModelList.size() > 0) {
//            for (int i = 0; i < historyModelList.size(); i++) {
//                ChatHistoryModel chatHistoryModel = new ChatHistoryModel();
//                chatHistoryModel.setChatRef(response.body().get(i).getChatRef());
//                chatHistoryModel.setMemberRefReceiver(response.body().get(i).getMemberRefReceiver());
//                chatHistoryModel.setMemberNameReceiver(response.body().get(i).getMemberNameReceiver());
//                chatHistoryModel.setLastChat(response.body().get(i).getLastChat());
//                chatHistoryModel.setLastTime(response.body().get(i).getLastTime());
//                chatHistoryModel.setNewMessageCount(response.body().get(i).getNewMessageCount());
//                chatHistoryModel.setIdChatHistory(i);
//                ChatHistoryModelDao chatHistoryModelDao = daoSession.getChatHistoryModelDao();
//                chatHistoryModelDao.insertOrReplace(chatHistoryModel);
//            }
            initAdapter();
        } else {
            AlertDialog.Builder builderInner = new AlertDialog.Builder(this);
            builderInner.setMessage("Kotak pesan kosong, untuk memulai chat silakan klik tombol +. ");
            builderInner.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
            alertDialog = builderInner.create();
            alertDialog.show();
        }


    }

    private void initAdapter() {
        RvChatHistoryAdapter adapter = new RvChatHistoryAdapter(ListingHistoryChatActivity.this, historyModelList, display);
        recyclerView.setAdapter(adapter);
    }

    public void showHistoryChatFailure(Throwable t) {
//        progressDialog.dismiss();
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
//        ChatHistoryModelDao chatHistoryModelDao = daoSession.getChatHistoryModelDao();
//        List<ChatHistoryModel> chatHistoryModels = chatHistoryModelDao.queryBuilder()
//                .list();
//        Log.d("getProjectLocalDB", "" + chatHistoryModels.toString());
//        int numData = chatHistoryModels.size();
//        if (numData > 0) {
//            for (int i = 0; i < numData; i++) {
//                ChatHistoryModel chatHistoryModel = chatHistoryModels.get(i);
//                historyModelList.add(chatHistoryModel);
//                initAdapter();
//
//            }
//        }
    }

    protected void onStop() {
        super.onStop();

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }
}
