package com.nataproperti.crm360.view.listing.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubePlayer;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.NataApi;
import com.nataproperti.crm360.config.NataService;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.helper.MyListView;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.listing.model.Facility;
import com.nataproperti.crm360.view.listing.model.PropertyDescriptionModel;
import com.nataproperti.crm360.view.listing.model.SendToFriendModel;
import com.nataproperti.crm360.view.listing.model.UpdatePromote;
import com.nataproperti.crm360.view.listing.presenter.DescriptionFragmentPresenter;
import com.nataproperti.crm360.view.project.ui.YoutubeActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

import static com.nataproperti.crm360.nataproperty.R.id.linear_maps;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;


public class ListingPropertyDescriptionFragment extends Fragment {
    public static final String TAG = "PropertyDescription";
    public static final String PREF_NAME = "pref";

    String listingTypeName, categoryTypeName, propertyRef, certName, priceTypeName, listingTitle, listingDesc,
            countryName, provinceName, cityName, subLocationName, block, no, postCode, gMap, price,
            buildDimX, buildDimY, landDimX, landDimY, brTypeName, maidBRTypeName, bathRTypeName,
            maidBathRTypeName, garageTypeName, carportTypeName, phoneLineName, furnishTypeName, electricTypeName,
            facingName, viewTypeName, youTube1, youTube2, address, facilityName, cekFacility, memberRefOwner,
            latitude, longitude;

    int imageCount, documentCount;
    String landArea, buildArea;
    private List<Facility> facilities = new ArrayList<>();

    public ListingPropertyDescriptionFragment() {
        // Required empty public constructor
    }

    private View myFragmentView;

    MyListView listView;

    String listingRef, agencyCompanyRef, imageCover, memberRef, listingMemberRef, propertyName, hp, memberNameOwner;

    ArrayList<String> facility = new ArrayList<String>();

    @BindView(R.id.txt_desc)
    TextView txtDesc;
    @BindView(R.id.txt_listing_type)
    TextView txtListingType;
    @BindView(R.id.txt_property_type)
    TextView txtPropertyType;
    @BindView(R.id.txt_property_name)
    TextView txtPropertyName;
    @BindView(R.id.txt_sub_location_name)
    TextView txtSubLocationName;
    /*@Bind(R.id.txt_city_name)
    TextView txtCityName;
    @Bind(R.id.txt_province_name)*/
    TextView txtProvinceName;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.txt_block)
    TextView txtBlock;
    @BindView(R.id.txt_no)
    TextView txtNo;
    @BindView(R.id.txt_post_code)
    TextView txtPostCode;
    @BindView(R.id.txt_price)
    TextView txtPrice;
    @BindView(R.id.txt_price_type_name)
    TextView txtPriceTypeName;
    @BindView(R.id.txt_build_area)
    TextView txtBuildArea;
    @BindView(R.id.txt_land_area)
    TextView txtLandArea;
    @BindView(R.id.txt_br_type_name)
    TextView txtBrTypeName;
    @BindView(R.id.txt_maid_br_type_name)
    TextView txtMaidBrTypeName;
    @BindView(R.id.txt_bath_r_type_name)
    TextView txtBathRTypeName;
    @BindView(R.id.txt_maid_bath_r_type_name)
    TextView txtMaidBathRTypeName;
    @BindView(R.id.txt_garage_type_name)
    TextView txtGarageTypeName;
    @BindView(R.id.txt_carport_type_name)
    TextView txtCarportTypeName;
    @BindView(R.id.txt_phone_line_name)
    TextView txtPhoneLineName;
    @BindView(R.id.txt_furnish_type_name)
    TextView txtFurnishTypeName;
    @BindView(R.id.txt_electric_type_name)
    TextView txtElectricTypeName;
    @BindView(R.id.txt_facing_name)
    TextView txtFacingName;
    @BindView(R.id.txt_view_type_name)
    TextView txtViewTypeName;
    @BindView(R.id.txt_facility)
    TextView txtFacility;
    @BindView(R.id.txt_maps)
    MyTextViewLatoReguler txtMaps;

    @BindView(R.id.linear_info)
    LinearLayout linearLayoutInfo;
    @BindView(R.id.linear_facility)
    LinearLayout linearLayoutFacility;
    @BindView(linear_maps)
    LinearLayout linearLayoutMaps;

    @BindView(R.id.btn_download)
    Button btnDownload;

    @BindView(R.id.btn_video1)
    Button btnVideo1;
    @BindView(R.id.btn_video2)
    Button btnVideo2;
    //@Bind(R.id.btn_send_to_friend)
    Button btnSendToFriend;
    Button btnSendNotif;

    @BindView(R.id.btn_call)
    ImageView btnCall;
    @BindView(R.id.btn_chat)
    ImageView btnMessage;
    @BindView(R.id.btn_favorite)
    ImageView btnFavorit;

    @BindView(R.id.linear_listing)
    LinearLayout linearLayoutListing;
    @BindView(R.id.line)
    View line;

    Typeface font;

    EditText edtEmail;
    AlertDialog alertDialog;
    SharedPreferences sharedPreferences;

    private FragmentActivity myContext;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private DescriptionFragmentPresenter presenter;
    private ProgressDialog progressDialog;

    private YouTubePlayer YPlayer;
    private static final String YoutubeDeveloperKey = "xyz";
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    boolean favorite;
    boolean realFav;
    String isFav;

    @Override
    public void onAttach(Activity activity) {

        if (activity instanceof FragmentActivity) {
            myContext = (FragmentActivity) activity;
        }

        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getActivity().getIntent();
        //agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        imageCover = intent.getStringExtra("imageCover");
        listingRef = intent.getStringExtra("listingRef");
        listingMemberRef = intent.getStringExtra("listingMemberRef");
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_listing_property_description, container, false);
        ButterKnife.bind(this, rootView);
        Log.d("listingRef", "" + listingRef);
        Log.d(TAG, "agencyCompanyRef " + agencyCompanyRef);
        font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Lato-Regular.ttf");

        sharedPreferences = getActivity().getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Log.d("listingMemberRef", "" + listingMemberRef + " " + memberRef);
        service = ((BaseApplication) getActivity().getApplication()).getNetworkService();
        presenter = new DescriptionFragmentPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        btnDownload.setTypeface(font);
        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListingDownloadActivity.class);
                intent.putExtra("listingRef", listingRef);
                intent.putExtra("imageCover", imageCover);
                startActivity(intent);
            }
        });

        btnVideo1.setTypeface(font);
        btnVideo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), YoutubeActivity.class);
                intent.putExtra("urlVideo", youTube1);
                startActivity(intent);

            }
        });

        btnVideo2.setTypeface(font);
        btnVideo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), YoutubeActivity.class);
                intent.putExtra("urlVideo", youTube2);
                startActivity(intent);

            }
        });

        btnSendToFriend = (Button) rootView.findViewById(R.id.btn_send_to_friend);
        btnSendToFriend.setVisibility(View.VISIBLE);
        btnSendToFriend.setTypeface(font);
        btnSendToFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpSendToFriend();
            }
        });
        btnSendNotif = (Button) rootView.findViewById(R.id.btn_send_notif);
        btnSendNotif.setTypeface(font);
        btnSendNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(myContext);
                alertDialogBuilder
                        .setMessage("Send notif to acency's member?")
                        .setCancelable(false)
                        .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sendNotif();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        linearLayoutMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MapsListingPropertyActivity.class);
                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                startActivity(intent);

            }
        });

        return rootView;


    }

    @Override
    public void onResume() {
        super.onResume();
        facility.clear();
        requestPropertyInfo(listingRef);
    }

    public void requestPropertyInfo(final String listingRef) {
        progressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", true);
        presenter.getListingDetail(listingRef, memberRef);

    }

    public void popUpSendToFriend() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_listing_send_to_friend, null);
        dialogBuilder.setView(dialogView);
        edtEmail = (EditText) dialogView.findViewById(R.id.edt_email);
        dialogBuilder.setTitle("Shared this listing");
        dialogBuilder.setPositiveButton(getResources().getString(R.string.btn_send), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {


            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekEmail = edtEmail.getText().toString().trim();
                if (cekEmail.isEmpty()) {
                    edtEmail.setError("Email harus diisi");
                } /*else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(cekEmail).matches()) {
                    edtEmail.setError("Email ini tidak valid");
                } */ else {
                    sendToFriend();
                    edtEmail.setError(null);
                    alertDialog.dismiss();
                }

            }


        });


    }

    public void sendToFriend() {
        progressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", true);
        presenter.sendToFriend(edtEmail.getText().toString().trim(), memberRef, listingRef);
    }

    public void sendNotif() {
        progressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", true);
        presenter.sendNotif(agencyCompanyRef, listingRef, memberRef);
    }

    public void showListingDetailResults(retrofit2.Response<PropertyDescriptionModel> response) {
        listingDesc = response.body().getListingDesc();
        listingTypeName = response.body().getListingTypeName();
        categoryTypeName = response.body().getCategoryTypeName();
        propertyRef = response.body().getPropertyRef();
        certName = response.body().getCertName();
        priceTypeName = response.body().getPriceTypeName();
        listingTitle = response.body().getListingTitle();
        countryName = response.body().getCountryName();
        provinceName = response.body().getProvinceName();
        cityName = response.body().getCityName();
        subLocationName = response.body().getSubLocationName();
        address = response.body().getAddress();
        block = response.body().getBlock();
        no = response.body().getNo();
        postCode = response.body().getPostCode();
        price = response.body().getPrice();
        buildArea = response.body().getBuildArea();
        buildDimX = response.body().getBuildDimX();
        buildDimY = response.body().getBuildDimY();
        landArea = response.body().getLandArea();
        landDimX = response.body().getLandDimX();
        landDimY = response.body().getLandDimY();
        brTypeName = response.body().getBrTypeName();
        maidBRTypeName = response.body().getMaidBRTypeName();
        bathRTypeName = response.body().getBathRTypeName();
        maidBathRTypeName = response.body().getMaidBathRTypeName();
        garageTypeName = response.body().getGarageTypeName();
        carportTypeName = response.body().getCarportTypeName();
        phoneLineName = response.body().getPhoneLineName();
        furnishTypeName = response.body().getFurnishTypeName();
        electricTypeName = response.body().getElectricTypeName();
        facingName = response.body().getFacingName();
        viewTypeName = response.body().getViewTypeName();
        youTube1 = response.body().getYouTube1();
        youTube2 = response.body().getYouTube2();
        imageCount = response.body().getImageCount();
        documentCount = response.body().getDocumentCount();
        memberRefOwner = response.body().getMemberRefOwner();
        agencyCompanyRef = response.body().getAgencyCompanyRefOwner();
        propertyName = response.body().getPropertyName();
        latitude = response.body().getLatitude();
        longitude = response.body().getLongitude();
        facilities = response.body().getFacility();
        favorite = response.body().isFavorite();
        hp = response.body().getHp();
        memberNameOwner = response.body().getMemberNameOwner();

//        Log.d(TAG, "memberRef " + memberRef + " " + memberRefOwner + " " + memberNameOwner);
//        Log.d(TAG, "facilities " + facilities.size());

        if (facilities != null && facilities.size() > 0) {
            for (int i = 0; i < facilities.size(); i++) {
                facility.add(response.body().getFacility().get(i).getFacilityName());
            }
            txtFacility.setText(facility.toString().replace("[", "").replace("]", ""));
        } else {
            txtFacility.setText("-");
        }

        if (listingDesc.equals("")) {
            txtDesc.setText("-");
        } else {
            txtDesc.setText(listingDesc);
        }

        txtListingType.setText(listingTypeName);
        txtPropertyType.setText(categoryTypeName);
        txtPriceTypeName.setText(priceTypeName);
        txtPropertyName.setText(propertyName);
        txtSubLocationName.setText(subLocationName + ", " + cityName + ", " + provinceName);

        if (address.equals("")) {
            txtAddress.setText("-");
        } else {
            txtAddress.setText(address);
        }

        if (block.equals("")) {
            txtBlock.setText("-");
        } else {
            txtBlock.setText(block);
        }

        if (no.equals("")) {
            txtNo.setText("-");
        } else {
            txtNo.setText(no);
        }

        if (postCode.equals("")) {
            txtPostCode.setText("-");
        } else {
            txtPostCode.setText(postCode);
        }

        txtPrice.setText(price);

        if (!buildDimX.equals("0") && !buildDimY.equals("0")) {
            txtBuildArea.setText(Html.fromHtml(String.valueOf(buildArea) + " m<sup><small>2</small></sup>")
                    + " (" + buildDimX + "x" + buildDimY + ")");
        } else {
            txtBuildArea.setText(Html.fromHtml(String.valueOf(buildArea) + " m<sup><small>2</small></sup>"));
        }

        if (!landDimX.equals("0") && !landDimY.equals("0")) {
            txtLandArea.setText(Html.fromHtml(String.valueOf(landArea) + " m<sup><small>2</small></sup>")
                    + " (" + landDimX + "x" + landDimY + ")");
        } else {
            txtLandArea.setText(Html.fromHtml(String.valueOf(landArea) + " m<sup><small>2</small></sup>"));
        }

        txtBrTypeName.setText(brTypeName);
        txtMaidBrTypeName.setText(maidBRTypeName);
        txtBathRTypeName.setText(bathRTypeName);
        txtMaidBathRTypeName.setText(maidBathRTypeName);
        txtGarageTypeName.setText(garageTypeName);
        txtCarportTypeName.setText(carportTypeName);
        txtPhoneLineName.setText(phoneLineName);
        txtFurnishTypeName.setText(furnishTypeName);
        txtElectricTypeName.setText(electricTypeName);
        txtFacingName.setText(facingName);
        txtViewTypeName.setText(viewTypeName);
        txtListingType.setText(listingTypeName);
        //txtPropertyName.setText(carportTypeName);

        if (youTube1.trim().equals("") || youTube2.trim().equals("")) {
            if (youTube1.trim().equals("")) {
                btnVideo1.setVisibility(View.GONE);
            } else {
                btnVideo1.setVisibility(View.VISIBLE);
                btnVideo1.setText("Video");
            }

            if (youTube2.trim().equals("")) {
                btnVideo2.setVisibility(View.GONE);
            } else {
                btnVideo2.setVisibility(View.VISIBLE);
                btnVideo2.setText("Video");
            }
        } else {
            btnVideo1.setVisibility(View.VISIBLE);
            btnVideo2.setVisibility(View.VISIBLE);
        }

        if (imageCount > 0 && memberRefOwner.equals(memberRef)) {
            btnSendNotif.setVisibility(View.VISIBLE);
        } else {
            btnSendNotif.setVisibility(View.GONE);
        }

        if (documentCount == 0) {
            btnDownload.setVisibility(View.GONE);
        } else {
            btnDownload.setVisibility(View.VISIBLE);
        }

        if (latitude.equals("0")) {
            linearLayoutMaps.setVisibility(View.GONE);
        } else {
            linearLayoutMaps.setVisibility(View.VISIBLE);
        }

        //listingan dia atau bukan
        if (memberRef.equals(memberRefOwner)) {
            linearLayoutListing.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
        } else {
            linearLayoutListing.setVisibility(View.VISIBLE);
            line.setVisibility(View.VISIBLE);
        }

        if (favorite) {
            btnFavorit.setImageResource(R.drawable.ic_favorite_pink_500_24dp);
        } else {
            btnFavorit.setImageResource(R.drawable.ic_favorite_border_pink_500_24dp);
        }

        isFav = "";
        realFav = false;
        btnFavorit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFav != listingRef) {
                    realFav = favorite;
                }
                if (realFav) {
                    btnFavorit.setImageResource(R.drawable.ic_favorite_border_pink_500_24dp);
                    unFavorite(listingRef);
                    realFav = false;
                    isFav = listingRef;
                    Log.d("listingRef", listingRef);
                } else {
                    btnFavorit.setImageResource(R.drawable.ic_favorite_pink_500_24dp);
                    favoriteStat(listingRef);
                    realFav = true;
                    isFav = listingRef;
                    Log.d("listingRef", listingRef);
                }

            }
        });

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + hp));
                startActivity(intent);
            }
        });

        btnMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChattingRoomActivity.class);
                intent.putExtra("memberRefSender", memberRef);
                intent.putExtra("memberRefReceiver", memberRefOwner);
                intent.putExtra("name", memberNameOwner);
                startActivity(intent);
            }
        });

        progressDialog.dismiss();

    }

    private void favoriteStat(String listingRef) {
        NataApi service = NataService.getClient().create(NataApi.class);
        final Call<UpdatePromote> call = service.updateFavorite(listingRef, memberRef);
        call.enqueue(new Callback<UpdatePromote>() {
            @Override
            public void onResponse(Call<UpdatePromote> call, retrofit2.Response<UpdatePromote> response) {
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200) {
                    Toast.makeText(getActivity(), "Tambah ke favorite", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Failed. Please check your network", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<UpdatePromote> call, Throwable t) {

            }
        });
    }

    private void unFavorite(String listingRef) {
        NataApi service = NataService.getClient().create(NataApi.class);
        final Call<UpdatePromote> call = service.updateFavorite(listingRef, memberRef);
        call.enqueue(new Callback<UpdatePromote>() {
            @Override
            public void onResponse(Call<UpdatePromote> call, retrofit2.Response<UpdatePromote> response) {
                int status = response.body().getStatus();
                String message = response.body().getMessage();
                if (status == 200) {
                    Toast.makeText(getActivity(), "Hapus dari favorite", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Failed. Please check your network", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<UpdatePromote> call, Throwable t) {

            }
        });
    }

    public void showListingDetailFailure(Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(getContext(), getString(R.string.error_connection), Toast.LENGTH_LONG).show();

    }

    public void showSendToFriendResults(retrofit2.Response<SendToFriendModel> response) {
        progressDialog.dismiss();
        String message = response.body().getMessage();
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

    }

    public void showSendToFriendFailure(Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(getContext(), getString(R.string.error_connection), Toast.LENGTH_LONG).show();
    }

    public void showSendNotifResults(retrofit2.Response<SendToFriendModel> response) {
        progressDialog.dismiss();
        String message = response.body().getMessage();
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    public void showSendNotifFailure(Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(getContext(), getString(R.string.error_connection), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}
