package com.nataproperti.crm360.view.listing.ui;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.listing.adapter.ListingPropertyDetailImageAdapter;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.HackyViewPager;
import com.nataproperti.crm360.view.listing.model.ListingPropertyGalleryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;

public class ListingPropertyDetailImageActivity extends AppCompatActivity {
    public static final String TAG = "PropertyDetailImage";

    public static final String CONTENT_REF = "contentRef";
    public static final String POSISION = "position";
    public static final String PROPERTY_REF = "propertyRef";

    HackyViewPager viewPager;

    private List<ListingPropertyGalleryModel> listGallery = new ArrayList<ListingPropertyGalleryModel>();
    private ListingPropertyDetailImageAdapter adapter;

    String contentRef,propertyRef;

    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail_image);

        Intent intent = getIntent();
        contentRef = intent.getStringExtra(CONTENT_REF);
        position = intent.getIntExtra(POSISION,0);
        propertyRef = intent.getStringExtra(PROPERTY_REF);

        Log.d(TAG,"propertyRef "+propertyRef+" "+contentRef+" "+position);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        listGallery.clear();
        requestPropertyInfo(propertyRef);

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        viewPager = (HackyViewPager) findViewById(R.id.pager);
        adapter = new ListingPropertyDetailImageAdapter(this,listGallery);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());

        Log.d("position",""+String.valueOf(position));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width/0.8;
        Log.d("screen width", result.toString()+"--"+Math.round(result));

        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.width = width;
        params.height = result.intValue() ;
        viewPager.setLayoutParams(params);
        viewPager.requestLayout();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void requestPropertyInfo(final String propertyRef) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListListingPropertyGallery(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    generateGallery(jsonArray);


                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ListingPropertyDetailImageActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", propertyRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "galleryPropertyDetail");

    }

    private void generateGallery(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ListingPropertyGalleryModel propertyGalleryModel = new ListingPropertyGalleryModel();
                propertyGalleryModel.setImageRef(jo.getString("imageRef"));
                propertyGalleryModel.setImageTitle(jo.getString("imageTitle"));
                propertyGalleryModel.setImageFile(jo.getString("imageFile"));

                listGallery.add(propertyGalleryModel);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
        viewPager.setCurrentItem(position);
    }

}
