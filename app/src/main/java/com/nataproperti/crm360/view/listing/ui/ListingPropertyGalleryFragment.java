package com.nataproperti.crm360.view.listing.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.listing.adapter.ListingPropertyGalleryAdapter;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.helper.MyGridView;
import com.nataproperti.crm360.view.listing.model.ListingPropertyGalleryModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.presenter.PropertyGalleryPresenter;

import java.util.ArrayList;
import java.util.List;


public class ListingPropertyGalleryFragment extends Fragment {
    public static final String TAG = "PropertyGalleryFragment";

    public ListingPropertyGalleryFragment() {
        // Required empty public constructor
    }

    String listingRef, agencyCompanyRef, imageLogo;

    private List<ListingPropertyGalleryModel> listGallery = new ArrayList<>();
    private ListingPropertyGalleryAdapter adapter;
    private MyGridView listView;
    private LinearLayout linearLayoutNoData;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private PropertyGalleryPresenter presenter;
    ProgressDialog progressDialog;
    Display display;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getActivity().getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        imageLogo = intent.getStringExtra("imageLogo");
        listingRef = intent.getStringExtra("listingRef");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_listing_property_gallery, container, false);
        service = ((BaseApplication) getActivity().getApplication()).getNetworkService();
        presenter = new PropertyGalleryPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        requestPropertyGallery(listingRef);


        display = getActivity().getWindowManager().getDefaultDisplay();

        linearLayoutNoData = (LinearLayout) rootView.findViewById(R.id.linear_no_data);
        listView = (MyGridView) rootView.findViewById(R.id.list_property_gallery);


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //requestPropertyInfo(propertyRef);
    }


    public void requestPropertyGallery(final String listingRef) {
        progressDialog = ProgressDialog.show(getActivity(), "",
                "Please Wait...", true);
        presenter.getGallery(listingRef);
    }


    public void showGalleryResults(retrofit2.Response<List<ListingPropertyGalleryModel>> response) {
        progressDialog.dismiss();
//        JSONArray jsonArray = new JSONArray(response);
        listGallery = response.body();
        if(listGallery==null){
            getActivity().finish();
        }else {
            if (listGallery.size() != 0) {

//            generateGallery(jsonArray);
                linearLayoutNoData.setVisibility(View.GONE);
                initAdapter();

            } else {
                linearLayoutNoData.setVisibility(View.VISIBLE);
            }
        }

    }

    private void initAdapter() {
//        adapter.notifyDataSetChanged();
        adapter = new ListingPropertyGalleryAdapter(getActivity(), listGallery, display,listingRef);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    public void showGalleryFailure(Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(getContext(), getString(R.string.error_connection), Toast.LENGTH_LONG).show();
    }
}
