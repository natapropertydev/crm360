package com.nataproperti.crm360.view.listing.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.listing.model.InfoMapModel;
import com.nataproperti.crm360.view.listing.presenter.PropertyMapsPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ListingPropertyMapsFragment extends Fragment {
    public static final String TAG = "ListingPropertyMaps";

    public static final String LINK_DETAIL = "linkDetail";
    public static final String PROPERTY_REF = "propertyRef";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private PropertyMapsPresenter presenter;
    GoogleMap googleMap;
    private Marker currentLocMarker;
    ProgressDialog progressDialog;
    int addFrom;

    public static final String PREF_NAME = "pref";

    String linkDetail, propertyRef;

    double longitude, latitude;
    String lat, lng, membeRefAgent;
//    private GoogleMap googleMap;

    public ListingPropertyMapsFragment() {
        // Required empty public constructor
    }

    private static View view;

    String listingRef, agencyCompanyRef, imageLogo, isCobrokePref, memberRef, memberTypeCode;

    @BindView(R.id.txt_no_maps)
    TextView txtNoMaps;
    @BindView(R.id.btn_edit)
    Button btnEdit;

    @BindView(R.id.maps)
    FrameLayout frameLayoutMaps;

    SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        sharedPreferences = getActivity().getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        service = ((BaseApplication) getActivity().getApplication()).getNetworkService();
        presenter = new PropertyMapsPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        Intent intent = getActivity().getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        imageLogo = intent.getStringExtra("imageLogo");
        listingRef = intent.getStringExtra("listingRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        addFrom = intent.getIntExtra("addFrom", 0);
        isCobrokePref = sharedPreferences.getString("isCobroke", "1");

        Log.d(TAG," addFrom = " + addFrom);
        Log.d(TAG," agencyCompanyRef = " + agencyCompanyRef);

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);

        }
        try {
            view = inflater.inflate(R.layout.fragment_listing_property_maps, container, false);
            ButterKnife.bind(this, view);
            requestPropertyInfo(listingRef);
        } catch (InflateException e) {

        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //requestPropertyInfo(propertyRef);

    }

    public void requestPropertyInfo(final String listingRef) {
        progressDialog = ProgressDialog.show(getActivity(), "",
                "Please Wait...", true);
        presenter.getInfoMap(listingRef);
    }

    private void initilizeMap() {

//            SupportMapFragment supportMapFragment = (SupportMapFragment)
//                    getActivity().getSupportFragmentManager().findFragmentById(R.id.map);
//
//            // Getting a reference to the map
//            googleMap = supportMapFragment.getMap();
//            currentLocMarker = googleMap.addMarker(new MarkerOptions().position(
//                    new LatLng(latitude, longitude)));
//            LatLng coordinate = new LatLng(latitude, longitude);
//            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15.0f);
//            googleMap.moveCamera(yourLocation);

        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location_on_indigo_a700_48dp);
//        googleMap = ((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map)).getMap();
        currentLocMarker = googleMap.addMarker(new MarkerOptions().position(
                new LatLng(latitude, longitude)).icon(icon));
        LatLng coordinate = new LatLng(latitude, longitude);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15.0f);
        googleMap.moveCamera(yourLocation);

        // check if map is created successfully or not
        if (googleMap == null) {
            Toast.makeText(getActivity(), "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
        }

        if (this.latitude == 0.0 && this.longitude == 0.0) {
            if (memberRef.equals(membeRefAgent)) {
                txtNoMaps.setText("*Untuk menambahkan info lokasi, silakan klik tombol edit ");
                frameLayoutMaps.setVisibility(View.GONE);
                btnEdit.setVisibility(View.VISIBLE);
            } else {
                txtNoMaps.setText("* Map tidak tersedia.. anda dapat menghubungi agency..");
                frameLayoutMaps.setVisibility(View.GONE);
                btnEdit.setVisibility(View.GONE);
            }
//                if (!memberRefOwner.equals(memberRef)){
//                    txtNoMaps.setText("*Lokasi tidak tersedia");
//                    frameLayoutMaps.setVisibility(View.GONE);
//                    btnEdit.setVisibility(View.GONE);
//                }
        }
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListingAddMapsActivity.class);
                intent.putExtra("listingRef", listingRef);
                intent.putExtra("memberTypeCode", memberTypeCode);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("lat", lat);
                intent.putExtra("lng", lng);
                intent.putExtra("status", "edit");
                intent.putExtra("addFrom", addFrom);
                startActivity(intent);
                //lat,lng
            }
        });

    }

    public void showInfoMapResults(retrofit2.Response<InfoMapModel> response) {
        progressDialog.dismiss();
        String status = response.body().getStatus();
        lat = response.body().getLatitude();
        lng = response.body().getLongitude();
        latitude = Double.parseDouble(lat);
        longitude = Double.parseDouble(lng);
        membeRefAgent = response.body().getMemberRef();
        initilizeMap();

    }

    public void showInfoMapFailure(Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(getContext(), getString(R.string.error_connection), Toast.LENGTH_LONG).show();

    }
}
