package com.nataproperti.crm360.view.listing.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.ybq.endless.Endless;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.model.ListingPropertyModel;
import com.nataproperti.crm360.view.listing.model.ListingPropertyStatus;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.presenter.ListingMemberPresenter;
import com.nataproperti.crm360.view.listing.adapter.RVListingMemberPropertyAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListingPropertyMemberActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    public static final String TAG = "ListingPropertyMember";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private ListingMemberPresenter presenter;
    ProgressDialog progressDialog;
    int setCity = 0;
    private List<ListingPropertyModel> listProperty = new ArrayList<>();
    private List<ListingPropertyModel> listProperty2 = new ArrayList<>();


    RecyclerView rvSearchProperty;
    private SharedPreferences sharedPreferences;

    int countTotal;
    Toolbar toolbar;
    Typeface font;
    Display display;
    RVListingMemberPropertyAdapter adapter;
    Button nextBtn;
    View loadingView;
    private int page = 1;
    Endless endless;
    MyTextViewLatoReguler title;
    private String provinceCode, agencyCompanyRef, cityCode, subLocation, listingTypeRef, categoryType, aboutMe, quotes, memberType,
            imageCover, listingRefpsRef, memberRef, psRef, memberTypeCode;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_member);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListingMemberPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        Intent intent = getIntent();
        psRef = intent.getStringExtra("psRef");
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        imageCover = intent.getStringExtra("imageCover");
        memberTypeCode = intent.getStringExtra("memberTypeCode");

        Log.d(TAG, "agencyCompanyRef " + agencyCompanyRef);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        initWidget();

        listingMember();
        adapter = new RVListingMemberPropertyAdapter(this, listProperty2, display, memberTypeCode, agencyCompanyRef);
        rvSearchProperty.setAdapter(adapter);
        //adapter.notifyDataSetChanged();

        loadingView = View.inflate(this, R.layout.loading, null);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        endless = Endless.applyTo(rvSearchProperty,
                loadingView
        );
        endless.setAdapter(adapter);
        endless.setLoadMoreListener(new Endless.LoadMoreListener() {
            @Override
            public void onLoadMore(int page) {
                presenter.getListingMember(agencyCompanyRef, memberRef, String.valueOf(page), "0");
            }
        });

        /*fab*/
        //1 dari my listing
        //2 dari agency listing
        //3 dari cobroking
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListingPropertyMemberActivity.this, ListingAddPropertyActivity.class);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("listingRef", "");
                intent.putExtra("memberTypeCode", memberTypeCode);
                intent.putExtra("status", "new");
                intent.putExtra("addFrom", 1);
                startActivity(intent);
            }
        });

    }

    private void listingMember() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListingMember(agencyCompanyRef, memberRef, String.valueOf(page), "0");
        Log.d(TAG, "param " + agencyCompanyRef + " " + memberRef + " " + page + " " + 0);
    }


    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        setSupportActionBar(toolbar);
        title.setText("My Listing");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        rvSearchProperty = (RecyclerView) findViewById(R.id.rv_list_search_project);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvSearchProperty.setLayoutManager(llm);
        fab = (FloatingActionButton) findViewById(R.id.fab);

    }


    @Override
    public void onClick(View v) {

    }

    private void initAdapter() {
        progressDialog.dismiss();
        adapter = new RVListingMemberPropertyAdapter(this, listProperty2, display, memberTypeCode, agencyCompanyRef);
        rvSearchProperty.setAdapter(adapter);
//        adapter.notifyDataSetChanged();

    }


    public void showListingMemberResults(Response<ListingPropertyStatus> response, String page) {
        progressDialog.dismiss();
        if (response.isSuccessful() && response != null) {
            int totalPage = response.body().getTotalPage();
            agencyCompanyRef = response.body().getAgencyCompanyRef();
            int count = Integer.parseInt(response.body().getCount());
            listProperty = response.body().getData();

            Log.d(TAG, "bodySize " + listProperty.size());
            Log.d(TAG, "responsePage " + page);
            Log.d(TAG, "count " + count);
            Log.d(TAG, "agencyCompanyRef " + agencyCompanyRef);

            if (Integer.parseInt(page) > totalPage) {
                //Toast.makeText(ListingPropertyActivity.this, "No data available", Toast.LENGTH_LONG).show();
                if (listProperty.size() == 0) {
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(this);
                    builderInner.setMessage("Anda tidak mempunyai listing, silakan klik tombol + untuk membuat listing.");
                    builderInner.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    builderInner.show();
                } else {
                    loadingView.setVisibility(View.GONE);
                }
            } else {
                if (listProperty.size() == 0) {
                    AlertDialog.Builder builderInner = new AlertDialog.Builder(this);
                    builderInner.setMessage("Anda tidak mempunyai listing, silakan klik tombol + untuk membuat listing.");
                    builderInner.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    builderInner.show();
                } else {
                    if (Integer.parseInt(page) == 1) {
                        for (int i = 0; i < listProperty.size(); i++) {
                            ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                            listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                            listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                            listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                            listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                            listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                            listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                            listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                            listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                            listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                            listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                            listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                            listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                            listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                            listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                            listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                            listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                            listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                            listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                            listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                            listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                            listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                            listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                            listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                            listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                            listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                            listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                            listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                            listingFavoritModel.setHp(listProperty.get(i).getHp());
                            listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                            listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                            listingFavoritModel.setDesc(listProperty.get(i).getDesc());
                            listProperty2.add(listingFavoritModel);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        for (int i = 0; i < listProperty.size(); i++) {
                            ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                            listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                            listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                            listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                            listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                            listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                            listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                            listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                            listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                            listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                            listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                            listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                            listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                            listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                            listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                            listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                            listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                            listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                            listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                            listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                            listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                            listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                            listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                            listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                            listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                            listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                            listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                            listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                            listingFavoritModel.setHp(listProperty.get(i).getHp());
                            listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                            listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                            listingFavoritModel.setDesc(listProperty.get(i).getDesc());
                            listProperty2.add(listingFavoritModel);
                        }
                        adapter.notifyDataSetChanged();
                        endless.loadMoreComplete();
                    }
                }

            }
        } else {
            Toast.makeText(ListingPropertyMemberActivity.this, "Terjadi Kesalahan", Toast.LENGTH_LONG).show();
        }

    }

    public void showListingMemberFailure(Throwable t) {
        progressDialog.dismiss();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);

    }
}
