package com.nataproperti.crm360.view.listing.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.helper.LoadingBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ListingPropertyTabsActivity extends AppCompatActivity {
    public static final String TAG = "ListingPropertyTabs";

    //    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    String listingRef, imageCover, listingTitle, agencyCompanyRef, listingMemberRef, closingPrice = "";

    ImageView imageHeader;
    int addFrom;

    String isCobrokePref, subPropertyName, propertyDesc, propertyCountry, propertyProvince, propertyCity, propertyAddress,
            propertyPostCode, launching, finishing, totalFloor, totalLift, totalUnit, subLocation, buildArea, landArea,
            developer, architecture, contractor, pasokanAir, pasokanListrik, clusterList, memberRef, memberRefOwner,
            memberTypeCode;

    double latitude, longitude;
    MyTextViewLatoReguler title;
    Point size;

    Typeface font;
    public static FragmentManager fragmentManager;
    Toolbar toolbar;
    Display display;
    Integer width;
    Double result;
    private SharedPreferences sharedPreferences;
    public static final String PREF_NAME = "pref";
    RelativeLayout rPage;

    private CollapsingToolbarLayout collapsingToolbarLayout;

    private Menu menu;

    int listingStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_property_tabs);
        Intent intent = getIntent();

        listingRef = intent.getStringExtra("listingRef");
        imageCover = intent.getStringExtra("imageCover");
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        listingTitle = intent.getStringExtra("listingTitle");
        listingMemberRef = intent.getStringExtra("listingMemberRef");
//        listingRef = intent.getStringExtra("listingRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        addFrom = intent.getIntExtra("addFrom", 0);
        memberRefOwner = intent.getStringExtra("memberRefOwner");

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        isCobrokePref = sharedPreferences.getString("isCobroke", "1");

        Log.d(TAG, listingRef + "- " + imageCover + " -" + agencyCompanyRef + " - " + listingTitle);
        Log.d(TAG, " listingMemberRef = " + listingMemberRef);
        Log.d(TAG, " addFrom = " + addFrom);
        Log.d(TAG, " agencyCompanyRef " + agencyCompanyRef);
        Log.d(TAG, " memberRef " + memberRef + " " + memberRefOwner);

        initWidget();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        title.setText(listingTitle);
        Glide.with(this).load(imageCover).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(imageHeader);
        fragmentManager = getSupportFragmentManager();

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        imageHeader = (ImageView) findViewById(R.id.img_header);
        rPage = (RelativeLayout) findViewById(R.id.rPage);

        display = getWindowManager().getDefaultDisplay();
        size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ListingPropertyDescriptionFragment(), "Info");
        adapter.addFragment(new ListingPropertyGalleryFragment(), "Gallery");
        adapter.addFragment(new ListingPropertyMapsFragment(), "Peta");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (memberRef.equals(memberRefOwner)){
            getMenuInflater().inflate(R.menu.menu_item_listview, menu);
        } else {
            //getMenuInflater().inflate(R.menu.menu_listing_edit, menu);
        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.btn_delete:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Apakah anda ingin menghapus listing property?");
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteListing(listingRef);

                    }
                });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                return true;

            //addFrom
            //1 dari my listing
            //2 dari agency listing
            //3 dari cobroking
            case R.id.btn_edit:
                Intent intent = new Intent(this, ListingAddPropertyActivity.class);
                intent.putExtra("listingRef", listingRef);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("memberTypeCode", memberTypeCode);
                intent.putExtra("status", "edit");
                intent.putExtra("addFrom", addFrom);
                startActivity(intent);
                return true;

            case R.id.btn_mark_as:

                popupListMarkAs();
                return true;

        }
        return super.onOptionsItemSelected(item);

    }

    private void popupListMarkAs() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.select_dialog_item);
        arrayAdapter.add("Not Active");
        arrayAdapter.add("Sold / Rented");
        arrayAdapter.add("Closed");

        builderSingle.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //listingStatus
                        listingStatus = which + 2;
                        String strName = arrayAdapter.getItem(which);
                        if (strName.equals("Not Active")) {
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(ListingPropertyTabsActivity.this);
                            builderInner.setMessage("Not Active adalah status sementara, dan property ini tidak akan muncul di listing Aplikasi.\n" +
                                    "Silakan mengubah status menjadi Active kembali via Web Dashboard anda.");
                            builderInner.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            updateListingStatus(listingStatus);
                                            dialog.dismiss();
                                        }
                                    });
                            builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderInner.show();

                        } else if (strName.equals("Sold / Rented")) {
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(ListingPropertyTabsActivity.this);
                            builderInner.setMessage("Apakah anda ingin mengubah ke status sold / rented ?");
                            builderInner.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            updateListingStatus(listingStatus);
                                            dialog.dismiss();
                                        }
                                    });
                            builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderInner.show();

                        } else if (strName.equals("Closed")) {
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(ListingPropertyTabsActivity.this);
                            builderInner.setMessage("Dengan mengubah status mennjadi closed, " +
                                    "maka listing ini tidak bisa diaktifkan kembali.");
                            builderInner.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            updateListingStatus(listingStatus);
                                            dialog.dismiss();
                                        }
                                    });
                            builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderInner.show();
                        } else {

                        }


                    }
                });
        builderSingle.show();
    }

    public void updateListingStatus(final int listingStatus) {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.updateStatusListingProperty(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    LoadingBar.stopLoader();
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {
                        Toast.makeText(ListingPropertyTabsActivity.this, message, Toast.LENGTH_LONG).show();

                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(ListingPropertyTabsActivity.this, error, Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingPropertyTabsActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);
                params.put("listingStatus", String.valueOf(listingStatus));
                params.put("closingPrice", closingPrice);
                params.put("memberRef", memberRef);

                Log.d("param", listingRef + " - " + listingStatus + " - " + closingPrice + " - " + memberRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "insertFacility");

    }

    public void deleteListing(final String listingRef) {
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.deleteListingProperty(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    LoadingBar.stopLoader();
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    if (status == 200) {
                        Toast.makeText(ListingPropertyTabsActivity.this, message, Toast.LENGTH_LONG).show();

                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(ListingPropertyTabsActivity.this, error, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingPropertyTabsActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("listingRef", listingRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "deleteListing");

    }
}
