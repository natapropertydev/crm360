package com.nataproperti.crm360.view.listing.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.ybq.endless.Endless;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.helper.MyEditTextLatoReguler;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.adapter.RvContactMessageAdapter;
import com.nataproperti.crm360.view.listing.model.ListingMemberInfoModel;
import com.nataproperti.crm360.view.listing.presenter.ListSearchContactChatPresenter;
import com.nataproperti.crm360.view.profile.model.BaseData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

//import com.arlib.floatingsearchview.FloatingSearchView;

/**
 * Created by nata on 11/17/2016.
 */

public class ListingSearchContactChatActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    private int page = 1;
    private String memberRef, agencyCompanyRef, memberTypeCode;
    private SharedPreferences sharedPreferences;
    private Typeface font;
    private RecyclerView recyclerView;
    private List<ListingMemberInfoModel> listingMemberInfoModels = new ArrayList<>();
    private List<ListingMemberInfoModel> listingMemberInfoModelss = new ArrayList<>();
    private Display display;
    public static final String TAG = "ListingMainActivity";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private ListSearchContactChatPresenter presenter;
    ProgressDialog progressDialog;
    View loadingView;
    RvContactMessageAdapter adapter;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    MyEditTextLatoReguler search;
    ImageView btnSearch, btnGo;
    Endless endless;
    String searchQuery = "";
    String newQuery;
    String keyword = "";
    private EditText edtSearch;
    private TextView txtClose;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_search_contact_chat);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListSearchContactChatPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        //requestContact(agencyCompanyRef, page, keyword);
        initWidget();
        initAdapter();

        edtSearch.setHint("Search..");
        edtSearch.setHintTextColor(getResources().getColor(R.color.colorText));
        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edtSearch.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "Pencarian Tidak Boleh Kosong", Toast.LENGTH_LONG).show();
                    } else {
                        /*if (listingMemberInfoModelss.size() != 0) {
                            listingMemberInfoModelss.clear();
                        }*/
                        requestContact(agencyCompanyRef, page, edtSearch.getText().toString().trim());

                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
                        return true;
                    }
                }
                return false;
            }
        });

        edtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* listingMemberInfoModelss.clear();
                listingMemberInfoModels.clear();
                adapter.notifyDataSetChanged();*/
                int size = listingMemberInfoModelss.size();
                if (size > 0) {
                    for (int i = 0; i < size; i++) {
                        listingMemberInfoModelss.remove(0);
                    }

                    adapter.notifyItemRangeRemoved(0, size);
                }
            }
        });

        loadingView = View.inflate(this, R.layout.loading, null);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        endless = Endless.applyTo(recyclerView, loadingView);
        endless.setAdapter(adapter);
        endless.setLoadMoreListener(new Endless.LoadMoreListener() {
            @Override
            public void onLoadMore(int page) {
                requestContact(agencyCompanyRef, page, edtSearch.getText().toString());
            }
        });

        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        edtSearch = (EditText) findViewById(R.id.edt_search);
        txtClose = (TextView) findViewById(R.id.txt_close);
        display = getWindowManager().getDefaultDisplay();
        recyclerView = (RecyclerView) findViewById(R.id.list_contact);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager
                (ListingSearchContactChatActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    private void requestContact(String agencyCompanyRef, int page, String keyword) {
//        progressDialog = ProgressDialog.show(this, "",
//                "Please Wait...", true);
        presenter.getContactChatKeyword(agencyCompanyRef, page, keyword);
        Log.d(TAG,"param "+agencyCompanyRef+" "+page+" "+keyword);
    }

    private void initAdapter() {

        adapter = new RvContactMessageAdapter(ListingSearchContactChatActivity.this, listingMemberInfoModelss, display, memberTypeCode);
        recyclerView.setAdapter(adapter);

    }

    public void showContactChatKeywordResults(Response<BaseData> response, int pageNo) {
        int totalPage = Integer.parseInt(response.body().getTotalPage());
        listingMemberInfoModels = response.body().getData();
        count = Integer.parseInt(response.body().getCount());

        if (count > 0){
            if (pageNo > totalPage) {
                //Toast.makeText(ListingPropertyActivity.this, "No data available", Toast.LENGTH_LONG).show();
                loadingView.setVisibility(View.GONE);
            } else {
                if (pageNo == 1) {
                    for (int i = 0; i < listingMemberInfoModels.size(); i++) {
                        ListingMemberInfoModel listingMemberInfoModel = new ListingMemberInfoModel();
                        listingMemberInfoModel.setIdAgent(listingMemberInfoModels.get(i).getIdAgent());
                        listingMemberInfoModel.setMemberRef(listingMemberInfoModels.get(i).getMemberRef());
                        listingMemberInfoModel.setAboutMe(listingMemberInfoModels.get(i).getAboutMe());
                        listingMemberInfoModel.setAddress(listingMemberInfoModels.get(i).getAddress());
                        listingMemberInfoModel.setEmail(listingMemberInfoModels.get(i).getEmail());
                        listingMemberInfoModel.setHp1(listingMemberInfoModels.get(i).getHp1());
                        listingMemberInfoModel.setListingMemberRef(listingMemberInfoModels.get(i).getListingMemberRef());
                        listingMemberInfoModel.setName(listingMemberInfoModels.get(i).getName());
                        listingMemberInfoModel.setPosision(listingMemberInfoModels.get(i).getPosision());
                        listingMemberInfoModel.setPsRef(listingMemberInfoModels.get(i).getPsRef());
                        listingMemberInfoModelss.add(listingMemberInfoModel);
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    for (int i = 0; i < listingMemberInfoModels.size(); i++) {
                        ListingMemberInfoModel listingMemberInfoModel = new ListingMemberInfoModel();
                        listingMemberInfoModel.setIdAgent(listingMemberInfoModels.get(i).getIdAgent());
                        listingMemberInfoModel.setMemberRef(listingMemberInfoModels.get(i).getMemberRef());
                        listingMemberInfoModel.setAboutMe(listingMemberInfoModels.get(i).getAboutMe());
                        listingMemberInfoModel.setAddress(listingMemberInfoModels.get(i).getAddress());
                        listingMemberInfoModel.setEmail(listingMemberInfoModels.get(i).getEmail());
                        listingMemberInfoModel.setHp1(listingMemberInfoModels.get(i).getHp1());
                        listingMemberInfoModel.setListingMemberRef(listingMemberInfoModels.get(i).getListingMemberRef());
                        listingMemberInfoModel.setName(listingMemberInfoModels.get(i).getName());
                        listingMemberInfoModel.setPosision(listingMemberInfoModels.get(i).getPosision());
                        listingMemberInfoModel.setPsRef(listingMemberInfoModels.get(i).getPsRef());
                        listingMemberInfoModel.setQuotes(listingMemberInfoModels.get(i).getQuotes());
                        listingMemberInfoModelss.add(listingMemberInfoModel);
                    }
                    adapter.notifyDataSetChanged();
                    endless.loadMoreComplete();
                }
            }
        } else {
            AlertDialog.Builder builderInner = new AlertDialog.Builder(this);
            builderInner.setMessage("Member tidak ditemukan, silakan coba kembali");
            builderInner.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            builderInner.show();
        }

    }

    public void showContactChatKeywordFailure(Throwable t) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }
}
