package com.nataproperti.crm360.view.listing.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.listing.adapter.ListingCityAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingMemberInfoAdapter;
import com.nataproperti.crm360.view.listing.adapter.ListingProvinceAdapter;
import com.nataproperti.crm360.view.project.adapter.LocationAdapter;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.model.ListingMemberInfoModel;
import com.nataproperti.crm360.view.profile.model.CityModel;
import com.nataproperti.crm360.view.profile.model.ProvinceModel;
import com.nataproperti.crm360.view.project.model.LocationModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.presenter.MemberInfoPresenter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListingSearchMemberInfoActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    public static final String TAG = "ListingSearchMember";
    private ListingCityAdapter adapterCity;

    private ListView listView;
    private ArrayList<ListingMemberInfoModel> listMemberInfo = new ArrayList<ListingMemberInfoModel>();
    private ListingMemberInfoAdapter adapter;

    private SharedPreferences sharedPreferences;
    private LocationAdapter locationAdapter;
    private EditText edtSearch;
    private TextView txtClose;
    private LinearLayout linearLayoutNoData;
    private ImageView imageView;
    private TextView txtName, txtEmail, txtHp, txtPosition, txtAddress, txtAboutMe, txtQuotes;
    private AlertDialog alertDialog;
    int setProvince = 0;
    int setCity = 0;
    private ImageView imageProfile;

    private String agencyCompanyRef, psRef, imageLogo,memberRef,provinceCode,cityCode;
    Spinner spinCity,spinLoc;
    private int countTotal;
    private List<ProvinceModel> listProvince = new ArrayList<>();
    private int page = 1;
    Toolbar toolbar;
    TextView title;
    Display display;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private MemberInfoPresenter presenter;
    private String locationRef;
    private List<LocationModel> listLocation = new ArrayList<>();
    private List<CityModel> listCity = new ArrayList<>();
    ProgressDialog progressDialog;
    Button btnNext;
    Typeface font;
    private ListingProvinceAdapter adapterProvince;
    private String name, email, hp, jabatan, listingMemberRef, address, aboutMe, quotes, memberType,memberTypeCode;
    private EditText edtKeyword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_search_member_info);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MemberInfoPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        agencyCompanyRef = getIntent().getStringExtra("agencyCompanyRef");
        imageLogo = intent.getStringExtra("imageLogo");
        psRef = intent.getStringExtra("psRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        initWidget();
        requestLocation();
        btnNext.setOnClickListener(this);

    }

    private void requestLocation() {
        presenter.getProvinsiListing("INA");
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        spinLoc = (Spinner) findViewById(R.id.spn_area);
        spinCity = (Spinner)findViewById(R.id.spn_sub_kota);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Search Member");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        btnNext = (Button) findViewById(R.id.btn_next);
        edtKeyword = (EditText) findViewById(R.id.edt_keyword);
    }

    public void showProvinsiResults(Response<List<ProvinceModel>> response) {
        listProvince = response.body();
        spinProvinsi();

    }

    private void spinProvinsi() {
        adapterProvince = new ListingProvinceAdapter(this, listProvince);
        spinLoc.setAdapter(adapterProvince);
        spinLoc.setSelection(setProvince);
        spinLoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                provinceCode = listProvince.get(position).getProvinceCode();
//                listCity.clear();
                presenter.getCityListing("INA", provinceCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showProvinsiFailure(Throwable t) {

    }

    public void showCityResults(Response<List<CityModel>> response) {
        listCity = response.body();
        spinnerCity();
    }

    private void spinnerCity() {
        adapterCity = new ListingCityAdapter(this, listCity);
        spinCity.setAdapter(adapterCity);
        spinCity.setSelection(setCity);
        spinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cityCode = listCity.get(position).getCityCode();
//                requestSubLocation("INA", provinceCode, cityCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showCityFailure(Throwable t) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                Intent intent = new Intent(this, ListingSearchResultMemberInfoActivity.class);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("psRef",psRef);
                intent.putExtra("provinceCode", provinceCode);
                intent.putExtra("cityCode", cityCode);
                intent.putExtra("keyword", edtKeyword.getText().toString());
                intent.putExtra("memberTypeCode", memberTypeCode);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }
}
