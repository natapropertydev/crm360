package com.nataproperti.crm360.view.listing.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.ybq.endless.Endless;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.model.ListingPropertyModel;
import com.nataproperti.crm360.view.listing.model.ListingPropertyStatus;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.presenter.SearchListingPresenter;
import com.nataproperti.crm360.view.listing.adapter.RVListingSearchPropertyAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListingSearchPropertyDtl extends AppCompatActivity implements View.OnClickListener {
    public static final String PREF_NAME = "pref";
    public static final String TAG = "ListingSearchProperty";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private SearchListingPresenter presenter;
    ProgressDialog progressDialog;
    int setCity = 0;
    private List<ListingPropertyModel> listProperty = new ArrayList<>();
    private List<ListingPropertyModel> listProperty2 = new ArrayList<>();

    RecyclerView rvSearchProperty;
    private SharedPreferences sharedPreferences;

    int countTotal;
    int page = 1;
    Toolbar toolbar;
    Typeface font;
    Display display;
    RVListingSearchPropertyAdapter adapter;
    Button nextBtn;
    View loadingView;
    Endless endless;
    MyTextViewLatoReguler title;
    private String provinceCode, agencyCompanyRef, cityCode, subLocation, listingTypeRef, categoryType, aboutMe, quotes, memberType,
            imageCover,listingRef,memberRef,memberTypeCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_search_property_dtl);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new SearchListingPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberType = sharedPreferences.getString("isMemberType", null);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        Intent intent = getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        provinceCode = intent.getStringExtra("provinceCode");
        cityCode = intent.getStringExtra("cityCode");
        subLocation = intent.getStringExtra("subLocation");
        listingTypeRef = intent.getStringExtra("listingTypeRef");
        categoryType = intent.getStringExtra("categoryType");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        initWidget();
        searchListing();

        loadingView = View.inflate(this, R.layout.loading, null);
        loadingView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        endless = Endless.applyTo(rvSearchProperty,
                loadingView
        );
        endless.setAdapter(adapter);
        endless.setLoadMoreListener(new Endless.LoadMoreListener() {
            @Override
            public void onLoadMore(int page) {
                searchListing();
            }
        });

//        nextBtn.setOnClickListener(this);
    }

    private void searchListing() {
        progressDialog = ProgressDialog.show(this, "",
                "Please Wait...", true);
        presenter.getSearchListing(agencyCompanyRef,"1",String.valueOf(page),provinceCode,cityCode,subLocation,listingTypeRef,categoryType,memberRef);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        setSupportActionBar(toolbar);
        title.setText("Search Listing Property");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        rvSearchProperty = (RecyclerView)findViewById(R.id.rv_list_search_project);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvSearchProperty.setLayoutManager(llm);

    }


    @Override
    public void onClick(View v) {

    }

    public void showSearchResults(Response<ListingPropertyStatus> response) {
        progressDialog.dismiss();
        countTotal = response.body().getTotalPage();

        if (countTotal != 0) {
            if (page <= countTotal) {
                listProperty=response.body().getData();
//                listProperty2.clear();
                for(int i = 0;i<listProperty.size();i++){
                    ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                    listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                    listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                    listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                    listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                    listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                    listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                    listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                    listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                    listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                    listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                    listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                    listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                    listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                    listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                    listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                    listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                    listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                    listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                    listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                    listingFavoritModel.setHp(listProperty.get(i).getHp());
                    listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                    listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                    listProperty2.add(listingFavoritModel);
                }
            }
        }
        else{
            AlertDialog.Builder alertDialogBuilder =
                    new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Data tidak ditemukan, silahkan coba kembali");
            alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    finish();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        initAdapter();


    }

    private void initAdapter() {
        adapter = new RVListingSearchPropertyAdapter(this,listProperty2,display, memberTypeCode,agencyCompanyRef);
        rvSearchProperty.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    public void showSearchFailure(Throwable t) {
        progressDialog.dismiss();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);

    }
}
