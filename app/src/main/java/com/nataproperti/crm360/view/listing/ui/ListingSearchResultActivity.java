package com.nataproperti.crm360.view.listing.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.model.ListingPropertyModel;
import com.nataproperti.crm360.view.listing.model.ListingPropertyStatus;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.presenter.SearchResultListingPresenter;
import com.nataproperti.crm360.view.listing.adapter.RVListingSearchPropertyAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class ListingSearchResultActivity extends AppCompatActivity {
    private static final String TAG = "ListingSearchResult";
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private SearchResultListingPresenter presenter;

    private LinearLayout linearLayoutNoData;
    Toolbar toolbar;
    TextView title;
    Typeface font;
    Display display;
    RecyclerView listView;

    ProgressDialog progressDialog;

    private List<ListingPropertyModel> listProperty = new ArrayList<>();
    private List<ListingPropertyModel> listProperty2 = new ArrayList<>();
    RVListingSearchPropertyAdapter adapter;

    int page = 1, countTotal;
    String agencyCompanyRef, provinceCode, cityCode, locationRef, listingTypeRef, categoryType, memberRef, memberTypeCode, toolbarTitle;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_search_result);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new SearchResultListingPresenter(this, service);

        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        adapter = new RVListingSearchPropertyAdapter(this, listProperty2, display, memberTypeCode, agencyCompanyRef);
        listView.setAdapter(adapter);

        Intent intent = getIntent();
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        provinceCode = intent.getStringExtra("provinceCode");
        cityCode = intent.getStringExtra("cityCode");
        locationRef = intent.getStringExtra("locationRef");
        listingTypeRef = intent.getStringExtra("listingTypeRef");
        categoryType = intent.getStringExtra("categoryType");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        toolbarTitle = intent.getStringExtra("toolbarTitle");

        title.setText(toolbarTitle);

        if (memberTypeCode.equals("2")) {
            requestListProperty();

        } else {
            listingPropertyCobroking();

        }

        Log.d(TAG, "param " + agencyCompanyRef + " " + provinceCode + " " + cityCode + " " + locationRef + " " + listingTypeRef + " " + categoryType);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);

        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();

        listView = (RecyclerView) findViewById(R.id.list_property);
        linearLayoutNoData = (LinearLayout) findViewById(R.id.linear_no_data);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        listView.setLayoutManager(llm);
    }

    public void requestListProperty() {
        //progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getSearchListing(agencyCompanyRef, WebService.listingStatus, String.valueOf(page), provinceCode, cityCode
                , locationRef, listingTypeRef, categoryType, memberRef);
        /*presenter.getSearchListing(agencyCompanyRef, WebService.listingStatus, String.valueOf(page), "All", "All", locationRef, "All", "All");*/

    }

    private void listingPropertyCobroking() {
//        progressDialog = ProgressDialog.show(this, "",
//                "Please Wait...", true);
        presenter.getListingCobroking(agencyCompanyRef, WebService.listingStatus, String.valueOf(page), provinceCode, cityCode
                , locationRef, listingTypeRef, categoryType, memberRef);
    }

    public void showSearchResults(Response<ListingPropertyStatus> response) {
        //progressDialog.dismiss();
        countTotal = response.body().getTotalPage();
        agencyCompanyRef = response.body().getAgencyCompanyRef();
        if (countTotal != 0) {
            if (page <= countTotal) {
                listProperty = response.body().getData();
                Log.d("listProperty ", "" + listProperty.size());
                for (int i = 0; i < listProperty.size(); i++) {
                    ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                    listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                    listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                    listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                    listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                    listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                    listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                    listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                    listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                    listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                    listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                    listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                    listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                    listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                    listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                    listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                    listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                    listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                    listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                    listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                    listingFavoritModel.setHp(listProperty.get(i).getHp());
                    listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                    listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                    listProperty2.add(listingFavoritModel);
                }
                adapter.notifyDataSetChanged();
            }
        } else {
            linearLayoutNoData.setVisibility(View.VISIBLE);
        }
        //initAdapter();

    }

    public void showListingMemberResults(Response<ListingPropertyStatus> response) {
        countTotal = response.body().getTotalPage();
        agencyCompanyRef = response.body().getAgencyCompanyRef();
        if (countTotal != 0) {
            if (page <= countTotal) {
                listProperty = response.body().getData();
                Log.d("listProperty ", "" + listProperty.size());
                for (int i = 0; i < listProperty.size(); i++) {
                    ListingPropertyModel listingFavoritModel = new ListingPropertyModel();
                    listingFavoritModel.setPrice(listProperty.get(i).getPrice());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setAgencyCompanyRef(listProperty.get(i).getAgencyCompanyRef());
                    listingFavoritModel.setBuildArea(listProperty.get(i).getBuildArea());
                    listingFavoritModel.setBuildDimX(listProperty.get(i).getBuildDimX());
                    listingFavoritModel.setBuildDimY(listProperty.get(i).getBuildDimY());
                    listingFavoritModel.setCityName(listProperty.get(i).getCityName());
                    listingFavoritModel.setImageCover(listProperty.get(i).getImageCover());
                    listingFavoritModel.setListingRef(listProperty.get(i).getListingRef());
                    listingFavoritModel.setLandArea(listProperty.get(i).getLandArea());
                    listingFavoritModel.setLandDimX(listProperty.get(i).getLandDimX());
                    listingFavoritModel.setLandDimY(listProperty.get(i).getLandDimY());
                    listingFavoritModel.setListingTitle(listProperty.get(i).getListingTitle());
                    listingFavoritModel.setListingTypeName(listProperty.get(i).getListingTypeName());
                    listingFavoritModel.setMemberName(listProperty.get(i).getMemberName());
                    listingFavoritModel.setMemberRef(listProperty.get(i).getMemberRef());
                    listingFavoritModel.setPriceTypeName(listProperty.get(i).getPriceTypeName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setSubLocationName(listProperty.get(i).getSubLocationName());
                    listingFavoritModel.setProvinceName(listProperty.get(i).getProvinceName());
                    listingFavoritModel.setBrTypeName(listProperty.get(i).getBrTypeName());
                    listingFavoritModel.setMaidBRTypeName(listProperty.get(i).getMaidBRTypeName());
                    listingFavoritModel.setBathRTypeName(listProperty.get(i).getBathRTypeName());
                    listingFavoritModel.setMaidBathRTypeName(listProperty.get(i).getMaidBathRTypeName());
                    listingFavoritModel.setGarageTypeName(listProperty.get(i).getGarageTypeName());
                    listingFavoritModel.setFacility(listProperty.get(i).getFacility());
                    listingFavoritModel.setFavorite(listProperty.get(i).isFavorite());
                    listingFavoritModel.setHp(listProperty.get(i).getHp());
                    listingFavoritModel.setCompanyName(listProperty.get(i).getCompanyName());
                    listingFavoritModel.setCategoryTypeName(listProperty.get(i).getCategoryTypeName());
                    listProperty2.add(listingFavoritModel);
                }
                adapter.notifyDataSetChanged();
            }
        } else {
            linearLayoutNoData.setVisibility(View.VISIBLE);
        }
    }

    public void showListingMemberFailure(Throwable t) {

    }

    public void showSearchFailure(Throwable t) {
        //progressDialog.dismiss();
    }

    private void initAdapter() {
        adapter = new RVListingSearchPropertyAdapter(this, listProperty, display, memberTypeCode, agencyCompanyRef);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}
