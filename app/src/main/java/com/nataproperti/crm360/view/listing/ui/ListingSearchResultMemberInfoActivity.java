package com.nataproperti.crm360.view.listing.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.EndlessScrollListener;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.listing.adapter.ListingMemberInfoAdapter;
import com.nataproperti.crm360.view.listing.model.ListingMemberInfoModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListingSearchResultMemberInfoActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String TAG = "ListingMemberInfo";
    private List<ListingMemberInfoModel> listingMemberInfoModels = new ArrayList<>();

    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<ListingMemberInfoModel> listMemberInfo = new ArrayList<>();
    private ListingMemberInfoAdapter adapter;

    private SharedPreferences sharedPreferences;

    private int count;
    private int countTotal;
    private int page = 1;

    private String agencyCompanyRef, psRef, imageLogo, memberRefSender, memberRefReceiver, cityCode, provinceCode;

    private ImageView imageView;

    private String name, email, hp, jabatan, listingMemberRef, address, aboutMe, quotes, memberType, keyword, memberTypeCode;
    private TextView txtName, txtEmail, txtHp, txtPosition, txtAddress, txtAboutMe, txtQuotes;
    private ImageView imageProfile, imageChat;
    private AlertDialog alertDialog;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_member_info);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberType = sharedPreferences.getString("isMemberType", null);
        memberRefSender = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        agencyCompanyRef = getIntent().getStringExtra("agencyCompanyRef");
        psRef = getIntent().getStringExtra("psRef");
        imageLogo = intent.getStringExtra("imageLogo");
        cityCode = intent.getStringExtra("cityCode");
        provinceCode = intent.getStringExtra("provinceCode");
        keyword = intent.getStringExtra("keyword");
        memberTypeCode = intent.getStringExtra("memberTypeCode");

        initWidget();

        Display display = getWindowManager().getDefaultDisplay();
        listView = (ListView) findViewById(R.id.list_member);
        adapter = new ListingMemberInfoAdapter(this, listMemberInfo, display);
        listView.setAdapter(adapter);
        //listView.setExpanded(true);

        requestMemberInfo(agencyCompanyRef, page);

        listView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                requestMemberInfo(agencyCompanyRef, page);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                psRef = listMemberInfo.get(position).getPsRef();
                name = listMemberInfo.get(position).getName();
                email = listMemberInfo.get(position).getEmail();
                hp = listMemberInfo.get(position).getHp1();
                jabatan = listMemberInfo.get(position).getPosision();
                listingMemberRef = listMemberInfo.get(position).getListingMemberRef();
                memberRefReceiver = listMemberInfo.get(position).getMemberRef();
                address = listMemberInfo.get(position).getAddress();
                aboutMe = listMemberInfo.get(position).getAboutMe();
                quotes = listMemberInfo.get(position).getQuotes();

                if (address.equals("")) {
                    address = "-";
                }

                if (aboutMe.equals("")) {
                    aboutMe = "-";
                }

                if (quotes.equals("")) {
                    quotes = "-";
                }

                popUpMemberInfo();
            }
        });
    }


    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Member Info");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void requestMemberInfo(final String agencyCompanyRef, final int page) {
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getListingMemberInfoSearch(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    countTotal = jsonObject.getInt("totalPage");
                    count = jsonObject.getInt("count");

                    if (count>0){
                        if (page <= countTotal) {
                            JSONArray jsonArray = new JSONArray(jsonObject.getJSONArray("data").toString());
                            generateListProperty(jsonArray);
                            Log.d(TAG, "jsonArray=" + jsonArray.length());
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject data = jsonArray.getJSONObject(i);
//                                Gson gson = new GsonBuilder().serializeNulls().create();
//                                ListingMemberInfoModel listingMemberInfoModel = gson.fromJson("" + data, ListingMemberInfoModel.class);
//                                listingMemberInfoModel.setPsRef(data.optString("psRef"));
//                                listingMemberInfoModel.setName(data.optString("name"));
//                                listingMemberInfoModel.setEmail(data.optString("email"));
//                                listingMemberInfoModel.setHp1(data.optString("hp"));
//                                listingMemberInfoModel.setPosision(data.optString("posision"));
//                                listingMemberInfoModel.setListingMemberRef(data.optString("image"));
//                                listingMemberInfoModel.setAddress(data.optString("address"));
//                                listingMemberInfoModel.setAboutMe(data.optString("aboutMe"));
//                                listingMemberInfoModel.setQuotes(data.optString("quotes"));
//                                listingMemberInfoModel.setMemberRef(data.optString("memberRef"));
//                                listingMemberInfoModel.setIdAgent(i);
//                                ListingMemberInfoModelDao listingMemberInfoModelDao = daoSession.getListingMemberInfoModelDao();
//                                listingMemberInfoModelDao.insertOrReplace(listingMemberInfoModel);
//
//                            }
                        }
                    } else {
                        AlertDialog.Builder builderInner = new AlertDialog.Builder(ListingSearchResultMemberInfoActivity.this);
                        builderInner.setMessage("Member tidak ditemukan, silakan coba kembali");
                        builderInner.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                        builderInner.show();
                    }

                    Log.d(TAG, "count=" + page + " " + countTotal);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

//                        ListingMemberInfoModelDao listingMemberInfoModelDao = daoSession.getListingMemberInfoModelDao();
////                        ProjectTestingModelDao projectTestingModelDao = daoSession.getProjectTestingModelDao();
//
//                        List<ListingMemberInfoModel> listingMemberInfoModels = listingMemberInfoModelDao.queryBuilder().orderAsc(ListingMemberInfoModelDao.Properties.IdAgent)
//                                .list();
//                        Log.d("getProjectLocalDB", "" + listingMemberInfoModels.toString());
//                        int numData = listingMemberInfoModels.size();
//                        if (numData > 0) {
//                            for (int i = 0; i < numData; i++) {
//                                ListingMemberInfoModel listingMemberInfoModel = listingMemberInfoModels.get(i);
//                                listMemberInfo.add(listingMemberInfoModel);
//
//
//                            }
//                        } else {
//                            finish();
//                        }
//                        adapter.notifyDataSetChanged();

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(ListingSearchResultMemberInfoActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(ListingSearchResultMemberInfoActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("agencyCompanyRef", agencyCompanyRef);
                params.put("PageNo", String.valueOf(page));
                params.put("provinceCode", provinceCode);
                params.put("cityCode", cityCode);
                params.put("keyword", keyword.trim());

                Log.d(TAG, "param " + agencyCompanyRef + " " + page + " " + cityCode + " " + provinceCode + " " + keyword);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "memberInfo");

    }

    private void generateListProperty(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                ListingMemberInfoModel listingMemberInfoModel = new ListingMemberInfoModel();
                listingMemberInfoModel.setPsRef(jo.getString("psRef"));
                listingMemberInfoModel.setName(jo.getString("name"));
                listingMemberInfoModel.setEmail(jo.getString("email"));
                listingMemberInfoModel.setHp1(jo.getString("hp"));
                listingMemberInfoModel.setPosision(jo.getString("posision"));
                listingMemberInfoModel.setListingMemberRef(jo.getString("image"));
                listingMemberInfoModel.setAddress(jo.getString("address"));
                listingMemberInfoModel.setAboutMe(jo.getString("aboutMe"));
                listingMemberInfoModel.setQuotes(jo.getString("quotes"));
                listingMemberInfoModel.setMemberRef(jo.getString("memberRef"));

                Log.d("image", "" + jo.getString("image"));

                listMemberInfo.add(listingMemberInfoModel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    public void popUpMemberInfo() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ListingSearchResultMemberInfoActivity.this);
        LayoutInflater inflater = ListingSearchResultMemberInfoActivity.this.getLayoutInflater();
        /*Independent*/
        if (memberType.startsWith("Independent")) {
            final View dialogView = inflater.inflate(R.layout.dialog_listing_member_info_independent, null);
            dialogBuilder.setView(dialogView);
            imageProfile = (ImageView) dialogView.findViewById(R.id.img_profile);
            Glide.with(this).load(WebService.getProfile() + listingMemberRef)
                    .diskCacheStrategy(DiskCacheStrategy.NONE).into(imageProfile);
        } else {
            final View dialogView = inflater.inflate(R.layout.dialog_listing_member_info, null);
            dialogBuilder.setView(dialogView);

            txtName = (TextView) dialogView.findViewById(R.id.txt_name);
            txtEmail = (TextView) dialogView.findViewById(R.id.txt_email);
            txtHp = (TextView) dialogView.findViewById(R.id.txt_mobile);
            txtPosition = (TextView) dialogView.findViewById(R.id.txt_position);
            imageProfile = (ImageView) dialogView.findViewById(R.id.img_profile);
            txtAddress = (TextView) dialogView.findViewById(R.id.txt_address);
            txtAboutMe = (TextView) dialogView.findViewById(R.id.txt_about_me);
            txtQuotes = (TextView) dialogView.findViewById(R.id.txt_quotes);
            imageChat = (ImageView) dialogView.findViewById(R.id.image_chat);

            txtEmail.setText(email.toLowerCase());
            txtHp.setText(hp);
            txtPosition.setText(jabatan);
            txtAddress.setText(address);
            txtAboutMe.setText(aboutMe);
            txtQuotes.setText(quotes);

            imageChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ListingSearchResultMemberInfoActivity.this, ChattingRoomActivity.class);
                    intent.putExtra("memberRefSender", memberRefSender);
                    intent.putExtra("memberRefReceiver", memberRefReceiver);
                    intent.putExtra("name", name);
                    startActivity(intent);
                }
            });

            Glide.with(this).load(WebService.getProfile() + listingMemberRef)
                    .diskCacheStrategy(DiskCacheStrategy.NONE).into(imageProfile);
        }

        dialogBuilder.setTitle(name.toUpperCase());

        dialogBuilder.setPositiveButton(getResources().getString(R.string.listing_property), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dialogBuilder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(ListingSearchResultMemberInfoActivity.this, ListingPropertyActivity.class);
                intent.putExtra("psRef", psRef);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                startActivity(intent);*/
                Intent intent = new Intent(ListingSearchResultMemberInfoActivity.this, ViewListingPropertyActivity.class);
                intent.putExtra("memberRef", memberRefReceiver);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("memberTypeCode", memberTypeCode);
                intent.putExtra("memberName", name);
                startActivity(intent);
                alertDialog.dismiss();

            }

        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_search_icon, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            case R.id.action_search:
                Intent intent = new Intent(ListingSearchResultMemberInfoActivity.this, ListingSearchMemberInfoActivity.class);
                intent.putExtra("agencyCompanyRef", agencyCompanyRef);
                intent.putExtra("imageLogo", imageLogo);
                intent.putExtra("psRef", psRef);
                startActivity(intent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

}
