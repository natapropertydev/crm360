package com.nataproperti.crm360.view.listing.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import java.util.Map;

public class ListingUploadImageActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";

    public static final String TAG = "ListingUploadImage" ;

    String memberRef, imagePath,listingRef,agencyCompanyRef,memberTypeCode,isCobroke;

    SharedPreferences sharedPreferences;
    private Bitmap bitmap;

    CropImageView cropImageView;
    Button upload;

    EditText edtImageTitle;
    AlertDialog alertDialogSave;

    int addFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_upload_image);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Upload Image");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cropImageView = (CropImageView) findViewById(R.id.CropImageView);
        upload = (Button) findViewById(R.id.btn_upload);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        listingRef = intent.getStringExtra("listingRef");
        imagePath = intent.getStringExtra("pathImage");
        agencyCompanyRef = intent.getStringExtra("agencyCompanyRef");
        memberTypeCode = intent.getStringExtra("memberTypeCode");
        addFrom = intent.getIntExtra("addFrom",0);
        isCobroke = intent.getStringExtra("isCobroke");

        Log.d(TAG, "listingRef " + listingRef);
        /*byte[] byteArray = getIntent().getByteArrayExtra("Image");
        bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);*/
        cropImageView.setFixedAspectRatio(true);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                cropImageView.setAspectRatio(370, 300);
            }
        }, 500);
        cropImageView.setImageBitmap(decodeSampledBitmapFromResource(imagePath, 500, 500));

        upload.setTypeface(font);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bitmap = cropImageView.getCroppedImage(500, 500);
                if (bitmap != null)
                    cropImageView.setImageBitmap(bitmap);

                popupImageTitle();
                cropImageView.setVisibility(View.GONE);
                upload.setVisibility(View.GONE);

            }
        });
    }

    public static Bitmap decodeSampledBitmapFromResource(String resId, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void popupImageTitle() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ListingUploadImageActivity.this);
        LayoutInflater inflater = ListingUploadImageActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_insert_image_title, null);
        dialogBuilder.setView(dialogView);

        edtImageTitle = (EditText) dialogView.findViewById(R.id.edt_image_title);
        dialogBuilder.setMessage("Masukkan nama image");
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                upload.setVisibility(View.VISIBLE);
                cropImageView.setVisibility(View.VISIBLE);

            }
        });
        alertDialogSave = dialogBuilder.create();
        alertDialogSave.setCancelable(false);
        alertDialogSave.show();
        Button positiveButton = alertDialogSave.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cekCompanyCode = edtImageTitle.getText().toString();

                if (cekCompanyCode.isEmpty()) {
                    edtImageTitle.setError("Harus diisi");
                } else {
                    uploadImage();
                    cropImageView.setVisibility(View.GONE);
                    upload.setVisibility(View.GONE);
                    alertDialogSave.dismiss();
                }

            }
        });
    }

    private void uploadImage() {
        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebService.insertImagePropertyListing(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TAG", "Upload image " + response.toString());
                        loading.dismiss();

                        try {
                            JSONObject jo = new JSONObject(response);
                            int status = jo.getInt("status");

                            if (status == 200) {
                                //Showing toast message of the response
                                Intent intent = new Intent(ListingUploadImageActivity.this, ListingAddGalleryActivity.class);
                                intent.putExtra("listingRef",listingRef);
                                intent.putExtra("agencyCompanyRef",agencyCompanyRef);
                                intent.putExtra("memberTypeCode",memberTypeCode);
                                intent.putExtra("addFrom",addFrom);
                                intent.putExtra("isCobroke",isCobroke);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                                Toast.makeText(ListingUploadImageActivity.this, "Upload image berhasil", Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(ListingUploadImageActivity.this, "Upload image gagal", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = getStringImage(bitmap);

                Map<String, String> params = new Hashtable<String, String>();
                params.put("listingRef", listingRef);
                params.put("image", image);
                params.put("imageTitle", edtImageTitle.getText().toString());

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_rotate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_rotate:
                cropImageView.rotateImage(90);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
