package com.nataproperti.crm360.view.listing.ui;

/**
 * Created by nata on 12/20/2016.
 */
public class LocationHelper {
    private static Double latitude = 0d;
    private static Double longitude = 0d;

    public static Double getLatitude() {
        return latitude;
    }

    public static void setLatitude(Double latitude) {
        LocationHelper.latitude = latitude;
    }

    public static Double getLongitude() {
        return longitude;
    }

    public static void setLongitude(Double longitude) {
        LocationHelper.longitude = longitude;
    }
    public static boolean isLocationDetected() {
        return latitude != 0d && longitude != 0d;
    }
}
