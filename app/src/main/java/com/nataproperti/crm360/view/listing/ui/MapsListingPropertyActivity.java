package com.nataproperti.crm360.view.listing.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;


public class MapsListingPropertyActivity extends AppCompatActivity {
    public static final String TAG = "MapsListingProperty";

    Toolbar toolbar;
    TextView title;
    Typeface font;

    GoogleMap googleMap;
    double latitude,longitude;
    private Marker currentLocMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_listing_property);
        initWidget();
        Intent intent = getIntent();

        latitude = Double.parseDouble(intent.getStringExtra("latitude"));
        longitude = Double.parseDouble(intent.getStringExtra("longitude"));

        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location_on_indigo_a700_48dp);

//        googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        currentLocMarker = googleMap.addMarker(new MarkerOptions().position(
                new LatLng(latitude, longitude)).icon(icon));
        LatLng coordinate = new LatLng(latitude, longitude);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15.0f);
        googleMap.moveCamera(yourLocation);

        Log.d(TAG,latitude+" "+longitude);
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("Lokasi Property");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
