package com.nataproperti.crm360.view.loginregister.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.LoadingBar;
import com.nataproperti.crm360.helper.MyEditTextLatoReguler;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.before_login.ui.LaunchActivity;
import com.nataproperti.crm360.view.menuitem.ui.ForgotPasswordActivity;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 4/13/2016.
 */
public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {
    public static final String PREF_NAME = "pref";

    public static final String KEY_PASSWORD = "password";
    public static final String KEY_EMAIL = "email";
    public static final String NAMA = "name";
    public static final String ISACTIVE = "isActive";
    public static final String PASSWORD = "password";
    public static final String MOBILE = "mobile";
    public static final String MEMBER_REF = "memberRef";
    public static final String BIRTHDATE = "birthDate";

    public static final String STATUS = "status";
    public static final String STATUS_GOOGLE_SIGN_IN = "statusGoogleSignIn";

    private MyEditTextLatoReguler txtEmail, txtPassword;
    private Button btnLogin;
    private Button btnGoogle;
    private CheckBox cbShowPassword;
    private MyTextViewLatoReguler btnLinkToForgotPassword, btnlinkToRegister;

    private String email;
    private String password;

    SharedPreferences sharedPreferences;

    GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions gso;
    SignInButton signIn_btn;
    private static final int RC_SIGN_IN = 0;
    ProgressDialog progress_dialog;
    GoogleSignInAccount acct;

    String statusGoogleSignIn;
    String emailGet, nameGet, hp1Get;

    String acctName, acctEmail, memberRef;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_login));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        txtEmail = (MyEditTextLatoReguler) findViewById(R.id.txtEmail);
        txtPassword = (MyEditTextLatoReguler) findViewById(R.id.txtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        cbShowPassword = (CheckBox) findViewById(R.id.cbShowPassword);
        btnGoogle = (Button) findViewById(R.id.btnGoogle);

        btnLinkToForgotPassword = (MyTextViewLatoReguler) findViewById(R.id.btnLinkToForgotPassword);
        btnlinkToRegister = (MyTextViewLatoReguler) findViewById(R.id.btnLinkToForgotRegister);

        // cek login
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        boolean state = sharedPreferences.getBoolean("isLogin", false);

        if (state) {
            // open main activity
            Intent intent = new Intent(LoginActivity.this, LaunchActivity.class);
            startActivity(intent);
        }

        btnLogin.setTypeface(font);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  userLoginTest();
                userLogin();

            }
        });

        btnlinkToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,
                        RegisterStepOneActivity.class);
                startActivity(intent);
            }
        });

        btnLinkToForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,
                        ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        cbShowPassword.setTypeface(font);
        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    // show password
                    txtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    txtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        //Google+
        buidNewGoogleApiClient();
        progress_dialog = new ProgressDialog(this);
        progress_dialog.setMessage("Signing in....");

        btnGoogle = (Button) findViewById(R.id.btnGoogle);
        btnGoogle.setTypeface(font);

        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gSignIn();
            }
        });

    }

    private void gSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
        progress_dialog.show();
    }

    /*Google+*/
    private void buidNewGoogleApiClient() {

//        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, RegisterStepOneActivity.this)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail() //Remove these below according to your needs
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, LoginActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {

                progress_dialog.dismiss();

            }
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            getSignInResult(result);
        }
    }

    private void getSignInResult(GoogleSignInResult result) {
        Log.d("gmsauth", "" + result.getStatus());
        if (result.isSuccess() && result != null) {

            acct = result.getSignInAccount();
            acctName = acct.getDisplayName();
            acctEmail = acct.getEmail();

            if (acctName == null) {
                acctName = "Mr/Mrs VIP";
            }

            Log.d("signin google", acctName + " " + acctEmail);
            registerGoogleSignIn(acctEmail);
            progress_dialog.dismiss();
        } else {

        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void userLogin() {
        if (!validate()) {
            onLoginFailed();
            return;
        }

        btnLogin.setEnabled(false);

        email = txtEmail.getText().toString().trim();
        password = txtPassword.getText().toString().trim();
        Log.d(KEY_EMAIL, "email " + email);
        Log.d(KEY_PASSWORD, "password " + password);

//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getLogin(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();
                Log.d("cek", "response login " + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");
                    Log.d("status login", "" + String.valueOf(status));
                    if (status == 200) {
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");

                        SharedPreferences sharedPreferences = LoginActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.commit();

                        Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                        startActivity(intent);

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    } else if (status == 201) {
                        txtEmail.setError(message);
                        btnLogin.setEnabled(true);

                    } else if (status == 202) {
                        txtPassword.setError(message);
                        btnLogin.setEnabled(true);

                    } else if (status == 203) {
                        txtEmail.setError(message);
                        btnLogin.setEnabled(true);

                    } else if (status == 205) {
                        cekEmailPopup();

                    } else if (status == 206) {
                        cekEmailCostumerPopup();

                        //Login dari KALK Biasa
                    } else if (status == 400) {
                        String name = jo.getJSONObject("data").getString("fullName");
                        String email = jo.getJSONObject("data").getString("username");
                        String mobile = jo.getJSONObject("data").getString("hp1");
                        String birthDate = jo.getJSONObject("data").getString("birthDate");
                        Intent intent = new Intent(LoginActivity.this, RegisterStepOneActivity.class);
                        intent.putExtra(KEY_EMAIL, email);
                        intent.putExtra(NAMA, name);
                        intent.putExtra(PASSWORD, txtPassword.getText().toString());
                        intent.putExtra(MOBILE, mobile);
                        intent.putExtra(BIRTHDATE, birthDate);
                        intent.putExtra(ISACTIVE, 1);
                        startActivity(intent);

                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                        btnLogin.setEnabled(true);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        btnLogin.setEnabled(true);
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(LoginActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(LoginActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_EMAIL, email);
                params.put(KEY_PASSWORD, password);

                return params;
            }
        };
        BaseApplication.getInstance().addToRequestQueue(request, "login");

    }

    public boolean validate() {
        boolean valid = true;

        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            txtEmail.setError("enter a valid email address");
            txtEmail.requestFocus();
            valid = false;
        } else {
            txtEmail.setError(null);
        }

        if (password.isEmpty()) {
            txtPassword.setError("enter a password");
            txtPassword.requestFocus();
            valid = false;
        } else {
            txtPassword.setError(null);
        }

        return valid;
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        btnLogin.setEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                /*Intent intent = new Intent(LoginActivity.this, LaunchActivity.class);
                startActivity(intent);*/
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, LaunchActivity.class);
        finish();
        startActivity(intent);
    }

    public void registerGoogleSignIn(final String acctEmail) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.signInGoogle(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jo = new JSONObject(response);
                    int status = jo.getInt("status");
                    final String message = jo.getString("message");
                    final String memberRef = jo.getString("memberRef");
                    final String memberType = jo.getString("memberType");
                    /*200 = sukses*/
                    if (status == 200) {
                        emailGet = jo.getJSONObject("data").getString("email1");
                        nameGet = jo.getJSONObject("data").getString("name");
                        SharedPreferences sharedPreferences = LoginActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", emailGet);
                        editor.putString("isName", nameGet);
                        editor.putString("isMemberType", memberType);
                        editor.commit();
                        Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    /*201 = new*/
                    } else if (status == 201) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Intent intent = new Intent(LoginActivity.this, RegisterStepOneActivity.class);
                                        intent.putExtra(STATUS_GOOGLE_SIGN_IN, "201");
                                        intent.putExtra(KEY_EMAIL, acctEmail);
                                        intent.putExtra(NAMA, acctName);
                                        intent.putExtra(MEMBER_REF, memberRef);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                    /*202 = Register dari calculator*/
                    } else if (status == 300) {
                        final String name = jo.getJSONObject("data").getString("name");
                        final String mobile = jo.getJSONObject("data").getString("hP1");
                        final String birthDate = jo.getJSONObject("data").getString("birthDate");
                        final String password = jo.getJSONObject("data").getString("key");
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Intent intent = new Intent(LoginActivity.this, RegisterStepOneActivity.class);
                                        intent.putExtra(STATUS_GOOGLE_SIGN_IN, "202");
                                        intent.putExtra(KEY_EMAIL, acctEmail);
                                        intent.putExtra(PASSWORD, password);
                                        intent.putExtra(MEMBER_REF, memberRef);
                                        intent.putExtra(NAMA, name);
                                        intent.putExtra(MOBILE, mobile);
                                        intent.putExtra(BIRTHDATE, birthDate);
                                        intent.putExtra(ISACTIVE, 1);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                    /*locked*/
                    } else if (status == 400) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                                    }
                                });
                    } else if (status == 206) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        cekEmailCostumerPopup();
                                    }
                                });
                    } else {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {

                                    }
                                });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(LoginActivity.this, getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(LoginActivity.this, getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", acctEmail);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "productImage");

    }

    private void cekEmailPopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_activate_email, null);
        dialogBuilder.setView(dialogView);

        final TextView txtActivate = (TextView) dialogView.findViewById(R.id.txt_activate);
        //dialogBuilder.setMessage("Special Enquiries");
        txtActivate.setText("please activate your account first, check your email to activate");

        dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                btnLogin.setEnabled(true);
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

        btnLogin.setEnabled(true);
    }

    private void cekEmailCostumerPopup() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_activate_email, null);
        dialogBuilder.setView(dialogView);

        final TextView txtActivate = (TextView) dialogView.findViewById(R.id.txt_activate);
        txtActivate.setText(getResources().getString(R.string.if_email_costumer));

        dialogBuilder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                btnLogin.setEnabled(true);
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

        btnLogin.setEnabled(true);
    }
}
