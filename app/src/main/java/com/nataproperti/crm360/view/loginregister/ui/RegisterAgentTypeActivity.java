package com.nataproperti.crm360.view.loginregister.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.loginregister.adapter.AgenTypeAdapter;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.before_login.model.AgenTypeModel;
import com.nataproperti.crm360.helper.LoadingBar;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 5/14/2016.
 */
public class RegisterAgentTypeActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    public static final String AGENTY_TYPE = "agentType";
    public static final String AGENTY_TYPE_NAME = "agentTypeName";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String MESSAGE = "message";
    public static final String STATUS_GOOGLE_SIGN_IN = "statusGoogleSignIn";

    private List<AgenTypeModel> listAgentType = new ArrayList<AgenTypeModel>();
    private AgenTypeAdapter adapter;

    private ListView listView;

    String fullname, birthDate, phone, email, password;
    String agentType, agentTypeName;

    SharedPreferences sharedPreferences;

    ProgressDialog progressDialog;
    String message, statusGoogleSignIn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_agent_type);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_register_agent_type));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent intent = getIntent();
        fullname = intent.getStringExtra("fullname");
        birthDate = intent.getStringExtra("brithDate");
        phone = intent.getStringExtra("phone");
        email = intent.getStringExtra("email");
        password = intent.getStringExtra("password");
        statusGoogleSignIn = intent.getStringExtra(STATUS_GOOGLE_SIGN_IN);

        Log.d("statusGoogleSignIn", "" + statusGoogleSignIn);

        listView = (ListView) findViewById(R.id.list_agent_type);

        adapter = new AgenTypeAdapter(this, listAgentType);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                agentType = listAgentType.get(position).getMemberType();
                agentTypeName = listAgentType.get(position).getMemberTypeName();

                switch (position) {
                    case 0:
                        Intent intentPropertyAgentcy = new Intent(RegisterAgentTypeActivity.this, RegisterPropertyAgencyActivity.class);
                        SharedPreferences sharedPreferences = RegisterAgentTypeActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("isfullname", fullname);
                        editor.putString("isbirthDate", birthDate);
                        editor.putString("isphone", phone);
                        editor.putString("isemail", email);
                        editor.putString("ispassword", password);
                        editor.putString("isagentType", agentType);
                        editor.putString("isagentTypeName", agentTypeName);
                        editor.putString("statusGoogleSignIn", statusGoogleSignIn);
                        editor.commit();
                        startActivity(intentPropertyAgentcy);
                        break;

                    case 1:
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterAgentTypeActivity.this);
                        alertDialogBuilder.setMessage("Apakah anda " + String.valueOf(agentTypeName) + " ? \n \n \n" +
                                "Dengan mengklik tombol “Ya”, saya setuju dengan Syarat & Ketentuan yang berlaku");
                        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                registerFinish();
                            }
                        });
                        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        alertDialogBuilder.setNeutralButton("Syarat & Ketentuan", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(RegisterAgentTypeActivity.this,RegisterTermActivity.class));
                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                        break;

                    case 2:
                        Intent intentInhouse = new Intent(RegisterAgentTypeActivity.this, RegisterInhouseActivity.class);
                        SharedPreferences sharedPreferencesInhouse = RegisterAgentTypeActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editorInhouse = sharedPreferencesInhouse.edit();
                        editorInhouse.putString("isfullname", fullname);
                        editorInhouse.putString("isbirthDate", birthDate);
                        editorInhouse.putString("isphone", phone);
                        editorInhouse.putString("isemail", email);
                        editorInhouse.putString("ispassword", password);
                        editorInhouse.putString("isagentType", agentType);
                        editorInhouse.putString("isagentTypeName", agentTypeName);
                        editorInhouse.putString("statusGoogleSignIn", statusGoogleSignIn);
                        editorInhouse.commit();
                        startActivity(intentInhouse);
                        break;

                }
            }
        });

        requestMemberType();
    }

    public void requestMemberType() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        JsonArrayRequest request = new JsonArrayRequest(WebService.getMemberType(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        Log.d("memberType", "memberType " + response.toString());
                        generateListMemberType(response);
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                    }
                });

        BaseApplication.getInstance().addToRequestQueue(request, "memberType");
    }

    private void generateListMemberType(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                AgenTypeModel type = new AgenTypeModel();
                type.setMemberType(jo.getString("memberType"));
                type.setMemberTypeName(jo.getString("memberTypeName"));

                listAgentType.add(type);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter.notifyDataSetChanged();
    }

    private void registerFinish() {
//        BaseApplication.getInstance().startLoader(this);
        LoadingBar.startLoader(this);
        StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getRegisterSalesAgent(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                BaseApplication.getInstance().stopLoader();
                LoadingBar.stopLoader();

                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result register", response);
                    int status = jo.getInt("status");

                    if (status == 200) {
                        message = jo.getString("message");
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        SharedPreferences sharedPreferences = RegisterAgentTypeActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.commit();
                        Intent intent = new Intent(RegisterAgentTypeActivity.this, RegisterFinishActivity.class);
                        intent.putExtra(EMAIL, email);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();

                    } else if (status == 500) {
                        message = jo.getString("message");
                        String memberRef = jo.getJSONObject("data").getString("memberRef");
                        String email = jo.getJSONObject("data").getString("username");
                        String name = jo.getJSONObject("data").getString("fullName");
                        String memberType = jo.getJSONObject("data").getString("memberType");
                        SharedPreferences sharedPreferences = RegisterAgentTypeActivity.this.
                                getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", true);
                        editor.putString("isMemberRef", memberRef);
                        editor.putString("isEmail", email);
                        editor.putString("isName", name);
                        editor.putString("isMemberType", memberType);
                        editor.commit();
                        Intent intent = new Intent(RegisterAgentTypeActivity.this, MainMenuActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else if (status == 201) {
                        message = jo.getString("message");
                        Intent intent = new Intent(RegisterAgentTypeActivity.this, RegisterStepOneActivity.class);
                        intent.putExtra(EMAIL, email);
                        intent.putExtra(MOBILE, phone);
                        intent.putExtra(MESSAGE, message);
                        startActivity(intent);
                        finish();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Toast.makeText(RegisterAgentTypeActivity.this, getString(R.string.time_out), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(RegisterAgentTypeActivity.this, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fullname", fullname);
                params.put("username", email);
                params.put("mobile", phone);
                params.put("agentType", agentType);
                params.put("password", password);
                params.put("birthDate", birthDate);
                params.put("status", statusGoogleSignIn);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "registerSales");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
