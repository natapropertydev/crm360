package com.nataproperti.crm360.view.mainmenu.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nataproperti.crm360.config.General;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.commision.ui.CommisionSectionActivity;
import com.nataproperti.crm360.view.mainmenu.model.MainMenuModel;
import com.nataproperti.crm360.view.mybooking.ui.MyBookingSectionActivity;
import com.nataproperti.crm360.view.mynup.ui.MyNupSectionActivity;
import com.nataproperti.crm360.view.project.ui.ListDownloadProjectActivity;
import com.nataproperti.crm360.view.project.ui.MyListingActivity;
import com.nataproperti.crm360.view.project.ui.ProjectActivity;

import java.util.List;

public class MainMenuAdapter extends RecyclerView.Adapter<MainMenuAdapter.MainMenuHolder> {
    public static final String TAG = "MainMenuAdapter";
    private List<MainMenuModel> listMenu;
    private Context context;
    private String usrRef;

    public MainMenuAdapter(Context context, List<MainMenuModel> listMenu) {
        this.context = context;
        this.listMenu = listMenu;
    }

    @Override
    public MainMenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_main_menu, null);

        return new MainMenuHolder(view);
    }

    @Override
    public void onBindViewHolder(MainMenuHolder holder, int position) {

        holder.onBindHolder(listMenu.get(position), position);
    }

    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class MainMenuHolder extends RecyclerView.ViewHolder {

        ImageView menuIcon;
        TextView menuTitle;
        RelativeLayout menuLayout;
        CardView item;

        public MainMenuHolder(View itemView) {
            super(itemView);

            menuIcon = (ImageView) itemView.findViewById(R.id.btn_main_menu);
            menuTitle = (TextView) itemView.findViewById(R.id.btn_main_menu_title);
            menuLayout = (RelativeLayout) itemView.findViewById(R.id.main_menu_layout_item);
            item = (CardView) itemView.findViewById(R.id.item);
        }

        public void onBindHolder(final MainMenuModel menuModel, final int position) {

            menuTitle.setText(menuModel.getMenuTitle());
            menuIcon.setBackground(context.getResources().getDrawable(menuModel.getMenuIcon()));
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = null;
                    switch (menuModel.getMenuKey()) {
                        case General.MenuAllProject:
                            intent = new Intent(context, ProjectActivity.class);
                            context.startActivity(intent);
                            break;
                        case General.MenuMyListing:
                            intent = new Intent(context, MyListingActivity.class);
                            context.startActivity(intent);
                            break;
                        case General.MenuDownload:
                            intent = new Intent(context, ListDownloadProjectActivity.class);
                            context.startActivity(intent);
                            break;
                        case General.MenuCommision:
                            intent = new Intent(context, CommisionSectionActivity.class);
                            context.startActivity(intent);
                            break;
                        case General.MenuMyNup:
                            intent = new Intent(context, MyNupSectionActivity.class);
                            context.startActivity(intent);
                            break;
                        case General.MenuMyBooking:
                            intent = new Intent(context, MyBookingSectionActivity.class);
                            context.startActivity(intent);
                            break;
//                        case General.MenuNews:
//                            intent = new Intent(context, NewsActivity.class);
//                            context.startActivity(intent);
//                            break;
//                        case General.MenuEvent:
//                            intent = new Intent(context, EventActivity.class);
//                            context.startActivity(intent);
//                            break;
//                        case General.MenuTicket:
//                            intent = new Intent(context, MyTicketActivity.class);
//                            context.startActivity(intent);
//                            break;
//                        case General.MenuKalkulator:
//                            intent = new Intent(context, CalculationActivity.class);
//                            context.startActivity(intent);
//                            break;
//                        case General.MenuCobroke:
//                            ((MainMenuActivity)context).requestAgencyInfo();
//                            break;
                    }

                }
            });
        }
    }

}
