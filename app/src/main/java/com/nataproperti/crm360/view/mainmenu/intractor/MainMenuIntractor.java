package com.nataproperti.crm360.view.mainmenu.intractor;

/**
 * Created by Nata
 * on Mar 3/23/2017 19:08.
 * Project : SmartSellingTools
 */
public interface MainMenuIntractor {
    void sendGCMToken(String GCMToken,String memberRef);
    void rxUnSubscribe();
    void requestAgencyInfo(String memberRef);
    void postRegisterAgency(String memberRef,String edtCompanyCode);
    void getCountChat(String memberRef);
}
