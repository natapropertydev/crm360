package com.nataproperti.crm360.view.mainmenu.model;

/**
 * Created by arifcebe
 * on Mar 3/16/17 15:03.
 * Project : wikaAppsAndroid
 */

public class MainMenuModel {

    private int menuIcon;
    private String menuTitle;
    private String menuKey;

    public int getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(int menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public String getMenuKey() {
        return menuKey;
    }

    public void setMenuKey(String menuKey) {
        this.menuKey = menuKey;
    }
}
