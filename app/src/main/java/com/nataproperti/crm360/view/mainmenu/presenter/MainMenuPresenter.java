package com.nataproperti.crm360.view.mainmenu.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.listing.model.ListCountChatModel;
import com.nataproperti.crm360.view.listing.model.ListingAgencyInfoModel;
import com.nataproperti.crm360.view.mainmenu.model.StatusTokenGCMModel;
import com.nataproperti.crm360.view.mainmenu.intractor.MainMenuIntractor;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MainMenuPresenter implements MainMenuIntractor {
    private MainMenuActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public MainMenuPresenter(MainMenuActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void sendGCMToken(String GCMToken,String memberRef) {
        Call<StatusTokenGCMModel> call = service.getAPI().postGCMToken(GCMToken,memberRef);
        call.enqueue(new Callback<StatusTokenGCMModel>() {
            @Override
            public void onResponse(Call<StatusTokenGCMModel> call, Response<StatusTokenGCMModel> responseToken) {
                view.showSendGCMTokenResults(responseToken);
            }

            @Override
            public void onFailure(Call<StatusTokenGCMModel> call, Throwable t) {
                view.showSendGCMTokenFailure(t);

            }


        });

    }

    @Override
    public void requestAgencyInfo(String memberRef) {
        Call<ListingAgencyInfoModel> call = service.getAPI().getAgencyInfo(memberRef);
        call.enqueue(new Callback<ListingAgencyInfoModel>() {
            @Override
            public void onResponse(Call<ListingAgencyInfoModel> call, Response<ListingAgencyInfoModel> response) {
                view.showAgencyResults(response);
            }

            @Override
            public void onFailure(Call<ListingAgencyInfoModel> call, Throwable t) {
                view.showAgencyFailure(t);

            }


        });
    }

    @Override
    public void postRegisterAgency(String memberRef, String edtCompanyCode) {
        Call<ListingAgencyInfoModel> call = service.getAPI().postRegisterAgency(memberRef,edtCompanyCode);
        call.enqueue(new Callback<ListingAgencyInfoModel>() {
            @Override
            public void onResponse(Call<ListingAgencyInfoModel> call, Response<ListingAgencyInfoModel> response) {
                view.showRegisterResults(response);
            }

            @Override
            public void onFailure(Call<ListingAgencyInfoModel> call, Throwable t) {
                view.showRegisterFailure(t);

            }


        });
    }

    @Override
    public void getCountChat(String memberRef) {
        Call<ListCountChatModel> call = service.getAPI().getCountChat(memberRef);
        call.enqueue(new Callback<ListCountChatModel>() {
            @Override
            public void onResponse(Call<ListCountChatModel> call, Response<ListCountChatModel> response) {
                view.showListCountResults(response);
            }

            @Override
            public void onFailure(Call<ListCountChatModel> call, Throwable t) {
                view.showListCountFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }
}
