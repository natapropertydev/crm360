package com.nataproperti.crm360.view.mainmenu.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.material.snackbar.Snackbar;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.General;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.gcm.GCMRegistrationIntentService;
import com.nataproperti.crm360.helper.GridSpacingItemDecoration;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.before_login.ui.LaunchActivity;
import com.nataproperti.crm360.view.listing.model.ListCountChatModel;
import com.nataproperti.crm360.view.listing.model.ListingAgencyInfoModel;
import com.nataproperti.crm360.view.listing.ui.ListingMainActivity;
import com.nataproperti.crm360.view.mainmenu.adapter.MainMenuAdapter;
import com.nataproperti.crm360.view.mainmenu.model.MainMenuModel;
import com.nataproperti.crm360.view.mainmenu.model.StatusTokenGCMModel;
import com.nataproperti.crm360.view.mainmenu.presenter.MainMenuPresenter;
import com.nataproperti.crm360.view.menuitem.ui.AboutUsActivity;
import com.nataproperti.crm360.view.menuitem.ui.ChangePasswordActivity;
import com.nataproperti.crm360.view.menuitem.ui.ContactUsActivity;
import com.nataproperti.crm360.view.menuitem.ui.NotificationActivity;
import com.nataproperti.crm360.view.profile.ui.SectionProfileActivity;
import com.nataproperti.crm360.view.profile.ui.SettingNotificationActivity;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;

/**
 * Created by User on 4/21/2016.
 */
public class MainMenuActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {
    public static final String TAG = "MainMenuActivity";
    public static final String PREF_NAME = "pref";
    public static boolean OFF_LINE_MODE = false;
    private NestedScrollView nestedScrollView;
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private MainMenuPresenter presenter;
    private Context context;
    private SharedPreferences sharedPreferences;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private TextView nameProfile, memberTypeName;
    private CircleImageView imgProfile;
    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    private Tracker mTracker;
    private String email, name;
    private String memberRef, memberType, agencyCompanyRef, imageLogo, memberTypeCode, companyName;
    private boolean state;
    private Button btnRegisterAgency;
    private EditText edtCompanyCode;
    private AlertDialog alertDialogSave;
    private Toolbar toolbar;
    private MyTextViewLatoReguler title;
    private Typeface font;
    private AlertDialog alertDialog;
    private RecyclerView recyclerViewMenu;
    private MainMenuAdapter mainMenuAdapter;
    // list icon menu
    int menuIcon[] = new int[]{
            R.drawable.icon_menu_all_project,
            R.drawable.icon_menu_my_project,
            R.drawable.icon_menu_download,
            R.drawable.icon_menu_komisi,
            R.drawable.icon_menu_nup,
            R.drawable.icon_menu_booking,
            //R.drawable.selector_menu_news,
            //R.drawable.selector_menu_event,
            //R.drawable.selector_menu_ticket,
            //R.drawable.selector_menu_cobroke,
            //R.drawable.selector_menu_calculator,
    };

    // list title menu
    int menuTitle[] = new int[]{
            R.string.menu_all_project,
            R.string.menu_mylisting,
            R.string.menu_download,
            R.string.menu_commision,
            R.string.menu_mynup,
            R.string.menu_mybooking,
            //R.string.menu_news,
            //R.string.menu_event,
            //R.string.menu_ticket,
            //R.string.menu_cobroke,
            //R.string.menu_calculator,
    };
    // list menu key
    String menuKey[] = new String[]{
            General.MenuAllProject,
            General.MenuMyListing,
            General.MenuDownload,
            General.MenuCommision,
            General.MenuMyNup,
            General.MenuMyBooking,
            //General.MenuNews,
            //General.MenuEvent,
            //General.MenuTicket,
            //General.MenuCobroke,
            //General.MenuKalkulator,
    };
    List<MainMenuModel> menulist = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MainMenuPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        context = this;
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        email = sharedPreferences.getString("isEmail", null);
        name = sharedPreferences.getString("isName", null);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberType = sharedPreferences.getString("isMemberType", null);

        //googleAnalitic
        BaseApplication application = (BaseApplication) getApplication();
//        mTracker = application.getDefaultTracker();

        animationButton();
        textSet();
        //contentMenu();
        serviceGCM();
        buidNewGoogleApiClient();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //cekChatting();

        String name = sharedPreferences.getString("isName", null);
        nameProfile.setText(name);

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));

        Glide.with(context).load(WebService.getProfile() + memberRef)
                .asBitmap()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .error(R.drawable.profile_image)
                .placeholder(R.drawable.profile_image)
                .into(imgProfile);

//        mTracker.setScreenName("Email ~ " + email);
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("  " + getString(R.string.app_name));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.drawable.ic_logo_crm);

        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        nameProfile = (TextView) findViewById(R.id.profilName);
        imgProfile = (CircleImageView) findViewById(R.id.profileImage);
        memberTypeName = (TextView) findViewById(R.id.memberTypeName);
        recyclerViewMenu = (RecyclerView) findViewById(R.id.listMenu);

        mainMenuAdapter = new MainMenuAdapter(this, menulist);
        recyclerViewMenu.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewMenu.setHasFixedSize(true);
        recyclerViewMenu.setAdapter(mainMenuAdapter);
        recyclerViewMenu.setNestedScrollingEnabled(false);

        for (int i = 0; i < menuIcon.length; i++) {
            MainMenuModel mm = new MainMenuModel();
            mm.setMenuIcon(menuIcon[i]);
            mm.setMenuTitle(getResources().getString(menuTitle[i]));
            mm.setMenuKey(menuKey[i]);
            menulist.add(mm);
        }
        mainMenuAdapter.notifyDataSetChanged();
        //recyclerViewMenu.addItemDecoration(new GridItemDivider(numColumns, Utils.dpToPx(this, 4), false));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.dimen16);
        recyclerViewMenu.addItemDecoration(new GridSpacingItemDecoration(2, spacingInPixels, true, 0));
    }

    private void contentMenu() {
        if (memberType.startsWith("Inhouse")) {
            menulist.remove(9);
        }
        mainMenuAdapter.notifyDataSetChanged();
    }

    private void textSet() {
        nameProfile.setText(name);
        memberTypeName.setText(memberType);
    }

    private void animationButton() {
        stopService(new Intent(getApplicationContext(), GCMRegistrationIntentService.class));
    }

    private void cekChatting() {
        presenter.getCountChat(memberRef);
    }

    public void showListCountResults(Response<ListCountChatModel> response) {
        //progressDialog.dismiss();
        String status = response.body().getStatus();
        if (status.equals("200")) {
            int countChat = response.body().getMessage();
            if (countChat >= 1) {
                //listingProperty.setBackgroundResource(R.drawable.selector_agency_msg_cobroke);
            } else {
                //listingProperty.setBackgroundResource(R.drawable.selector_menu_cobroke);
            }
        }
    }

    public void showListCountFailure(Throwable t) {
        //progressDialog.dismiss();
        Snackbar snackbar = Snackbar
                .make(nestedScrollView, "OFFLINE MODE", Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(MainMenuActivity.this, LaunchActivity.class));
                        finish();
                    }
                });

        snackbar.setActionTextColor(Color.RED);
        snackbar.show();
    }

    private void serviceGCM() {
        //BroadcastReceiver GCM
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Check type of intent filter
                if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)) {
                    //Registration success
                    String token = intent.getStringExtra("token");
                    Log.d("GCMToken", token);
                    saveGCMTokenToServer(token);


                } else if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)) {

                } else {

                }
            }
        };

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (ConnectionResult.SUCCESS != resultCode) {
            //Check type of error
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                //So notification
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Start service
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);

        }
    }


    //savegcmtoken
    private void saveGCMTokenToServer(final String GCMToken) {
        presenter.sendGCMToken(GCMToken, memberRef);

    }

    public void showSendGCMTokenResults(retrofit2.Response<StatusTokenGCMModel> responseToken) {
        int Status = responseToken.body().getStatus();
        String message = responseToken.body().getMessage();
        if (Status == 200) {
            Log.d("saveToken", message);
        } else {
            Log.d("saveToken", message);
        }
    }

    public void showSendGCMTokenFailure(Throwable t) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_after_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_profile:
                startActivity(new Intent(MainMenuActivity.this, SectionProfileActivity.class));
                return true;

            case R.id.btn_change_password:
                startActivity(new Intent(MainMenuActivity.this, ChangePasswordActivity.class));
                return true;

            case R.id.btn_logout:
                dialogLogout();
                return true;

            case R.id.btn_contact_us:
                startActivity(new Intent(MainMenuActivity.this, ContactUsActivity.class));
                return true;

            case R.id.btn_about_us:
                startActivity(new Intent(MainMenuActivity.this, AboutUsActivity.class));
                return true;

            case R.id.btn_setting:
                startActivity(new Intent(MainMenuActivity.this, SettingNotificationActivity.class));
                return true;

            case R.id.btn_notification:
                startActivity(new Intent(MainMenuActivity.this, NotificationActivity.class));
                return true;

            case R.id.btn_exit:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void dialogLogout() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah anda ingin logout?");
        alertDialogBuilder.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        SharedPreferences sharedPreferences = MainMenuActivity.this.
                                getSharedPreferences(PREF_NAME, 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isLogin", false);
                        editor.putString("isEmail", "");
                        editor.putString("isName", "");
                        editor.putString("isMemberRef", "");
                        editor.putString("isMemberType", "");
                        editor.commit();

                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Intent intent = new Intent(MainMenuActivity.this, LaunchActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();

                                    }
                                });
                    }
                });

        alertDialogBuilder.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void buidNewGoogleApiClient() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, MainMenuActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /*Myagency*/
    public void requestAgencyInfo() {
        presenter.requestAgencyInfo(memberRef);

    }

    public void insertAgecyCode() {
        presenter.postRegisterAgency(memberRef, edtCompanyCode.getText().toString());
    }

    public void showAgencyResults(retrofit2.Response<ListingAgencyInfoModel> response) {
        int status = response.body().getStatus();
        if (status == 200) {
            agencyCompanyRef = response.body().getAgencyCompanyRef();
            imageLogo = response.body().getImageLogo();
            memberTypeCode = response.body().getMemberType();
            companyName = response.body().getCompanyName();
            Intent intent = new Intent(MainMenuActivity.this, ListingMainActivity.class);
            intent.putExtra("agencyCompanyRef", agencyCompanyRef);
            intent.putExtra("imageLogo", imageLogo);
            //intent.putExtra("title", txtMyAgency.getText());
            intent.putExtra("memberTypeCode", memberTypeCode);
            intent.putExtra("companyName", companyName);
            startActivity(intent);
        } else if (status == 201) {
            registerCompany();
        }

    }

    public void showAgencyFailure(Throwable t) {

    }


    public void showRegisterResults(retrofit2.Response<ListingAgencyInfoModel> response) {
        int status = response.body().getStatus();
        String message = response.body().getMessage();
        if (status == 200) {
            agencyCompanyRef = response.body().getAgencyCompanyRef();
            imageLogo = response.body().getImageLogo();
            memberTypeCode = response.body().getMemberType();
            companyName = response.body().getCompanyName();
            Intent intent = new Intent(MainMenuActivity.this, ListingMainActivity.class);
            intent.putExtra("agencyCompanyRef", agencyCompanyRef);
            intent.putExtra("imageLogo", imageLogo);
            //intent.putExtra("title", txtMyAgency.getText());
            intent.putExtra("memberTypeCode", memberTypeCode);
            intent.putExtra("companyName", companyName);
            startActivity(intent);
            alertDialogSave.dismiss();
        } else if (status == 201) {
            edtCompanyCode.setError(message);
        } else {
            edtCompanyCode.setError(message);
        }
    }

    public void showRegisterFailure(Throwable t) {

    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

        if (alertDialogSave != null && alertDialogSave.isShowing()) {
            alertDialogSave.dismiss();
        }
    }

    public void registerCompany() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainMenuActivity.this);
        LayoutInflater inflater = MainMenuActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_insert_company_code, null);
        dialogBuilder.setView(dialogView);

        edtCompanyCode = (EditText) dialogView.findViewById(R.id.edt_company_code);
        btnRegisterAgency = (Button) dialogView.findViewById(R.id.btn_register_agency);

        dialogBuilder.setMessage("Masukkan Company Code");

        btnRegisterAgency.setTypeface(font);
        btnRegisterAgency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(WebService.linkRegisterAgency));
                startActivity(i);

            }
        });
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String cekCompanyCode = edtCompanyCode.getText().toString();

                if (cekCompanyCode.isEmpty()) {
                    edtCompanyCode.setError("Masukkan company code");
                } else {
                    insertAgecyCode();
                }
                dialog.dismiss();

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                dialog.dismiss();
            }
        });
        alertDialogSave = dialogBuilder.create();
        alertDialogSave.setCancelable(false);
        alertDialogSave.show();

    }

}
