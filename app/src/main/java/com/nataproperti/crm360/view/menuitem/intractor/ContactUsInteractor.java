package com.nataproperti.crm360.view.menuitem.intractor;

/**
 * Created by arifcebe
 * on Mar 3/9/17 15:34.
 * Project : adhipersadaproperti
 * <p>
 * Lebih Baik Pulang Nama, Daripada Gagal di Medan Laga
 */

public interface ContactUsInteractor {

    void sendFeedback(String contactName, String contactEmail, String contactSubject, String contactMessage);
}
