package com.nataproperti.crm360.view.menuitem.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterNotificationInteractor {
    void getListNotificationSSTSvc(String memberRef);
    void rxUnSubscribe();

}
