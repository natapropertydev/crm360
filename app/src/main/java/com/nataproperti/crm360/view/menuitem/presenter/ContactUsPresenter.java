package com.nataproperti.crm360.view.menuitem.presenter;


import com.nataproperti.crm360.config.General;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.menuitem.intractor.ContactUsInteractor;
import com.nataproperti.crm360.view.menuitem.model.ContactUsModel;
import com.nataproperti.crm360.view.menuitem.ui.ContactUsActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by arifcebe
 * on Mar 3/9/17 15:42.
 * Project : adhipersadaproperti
 */

public class ContactUsPresenter implements ContactUsInteractor {

    private static final String TAG = ContactUsPresenter.class.getSimpleName();
    private ContactUsActivity view;
    private ServiceRetrofit retrofitProject;

    public ContactUsPresenter(ContactUsActivity view, ServiceRetrofit retrofitProject) {
        this.view = view;
        this.retrofitProject = retrofitProject;
    }

    @Override
    public void sendFeedback(String contactName, String contactEmail, String contactSubject, String contactMessage) {
        Call<ContactUsModel> callContact = retrofitProject.getAPI()
                .saveFeedbackUser(contactName,contactEmail,contactSubject,contactMessage);
        view.onLoading(true);
        callContact.enqueue(new Callback<ContactUsModel>() {
            @Override
            public void onResponse(Call<ContactUsModel> call, Response<ContactUsModel> response) {
                view.onLoading(false);
                if (response.body().getStatus().equals("200")) {
                    view.successResponseFeedback();
                } else{
                    view.failedResponseFeedback(General.NETWORK_ERROR_PERMISSION_ACCESS);
                }
            }

            @Override
            public void onFailure(Call<ContactUsModel> call, Throwable t) {
                view.onLoading(false);
                view.failedResponseFeedback(General.NETWORK_ERROR_CONNCECTION);
            }
        });
    }
}
