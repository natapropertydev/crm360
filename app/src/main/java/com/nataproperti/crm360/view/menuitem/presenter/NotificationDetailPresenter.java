package com.nataproperti.crm360.view.menuitem.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.menuitem.intractor.PresenterNotificationDetailInteractor;
import com.nataproperti.crm360.view.menuitem.model.NotificationDetailModel;
import com.nataproperti.crm360.view.menuitem.ui.NotificationDetailActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class NotificationDetailPresenter implements PresenterNotificationDetailInteractor {
    private NotificationDetailActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public NotificationDetailPresenter(NotificationDetailActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getGCMMessageSvc(String gcmMessageRef,String memberRef) {
        Call<NotificationDetailModel> call = service.getAPI().getGCMMessageSvc(gcmMessageRef,memberRef);
        call.enqueue(new Callback<NotificationDetailModel>() {
            @Override
            public void onResponse(Call<NotificationDetailModel> call, Response<NotificationDetailModel> response) {
                view.showListNotificationDetailResults(response);
            }

            @Override
            public void onFailure(Call<NotificationDetailModel> call, Throwable t) {
                view.showListNotificationDetailFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
