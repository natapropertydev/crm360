package com.nataproperti.crm360.view.menuitem.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.menuitem.intractor.PresenterNotificationInteractor;
import com.nataproperti.crm360.view.menuitem.model.NotificationModel;
import com.nataproperti.crm360.view.menuitem.ui.NotificationActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class NotificationPresenter implements PresenterNotificationInteractor {
    private NotificationActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public NotificationPresenter(NotificationActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListNotificationSSTSvc(String memberRef) {
        Call<ArrayList<NotificationModel>> call = service.getAPI().getListNotificationSSTSvc(memberRef);
        call.enqueue(new Callback<ArrayList<NotificationModel>>() {
            @Override
            public void onResponse(Call<ArrayList<NotificationModel>> call, Response<ArrayList<NotificationModel>> response) {
                view.showListNotificationResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<NotificationModel>> call, Throwable t) {
                view.showListNotificationFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
