package com.nataproperti.crm360.view.menuitem.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.General;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.menuitem.presenter.ContactUsPresenter;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;


/**
 * Created by User on 6/3/2016.
 */
public class ContactUsActivity extends AppCompatActivity implements OnMapReadyCallback {
    public static final String PREF_NAME = "pref";

    SharedPreferences sharedPreferences;

    private GoogleMap googleMap;

    TextView addressContact, phoneContact, emailContact, webContact;
    EditText edtName, edtEmail, edtSubject, edtMassage;
    Button btnSend;

    private double latitude, longitude;

    private String name, email, subject, message;
    private String addressNata, phoneNata, emailNata, webNata;

    ProgressDialog progressDialog;

    private ContactUsPresenter contactUsPresenter;
    private ServiceRetrofit service;

    int status;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_contact_us));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        String email = sharedPreferences.getString("isEmail", null);
        String name = sharedPreferences.getString("isName", null);

        addressContact = (TextView) findViewById(R.id.txt_address);
        phoneContact = (TextView) findViewById(R.id.txt_phone);
        emailContact = (TextView) findViewById(R.id.txt_email);
        webContact = (TextView) findViewById(R.id.txt_web);

        edtName = (EditText) findViewById(R.id.edt_name);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtSubject = (EditText) findViewById(R.id.edt_subject);
        edtMassage = (EditText) findViewById(R.id.edt_message);

        edtName.setText(name);
        edtEmail.setText(email);

        btnSend = (Button) findViewById(R.id.btn_send);
        btnSend.setTypeface(font);

        latitude = General.LATITUDE_CODE;
        longitude = General.LONGITUDE_CODE;

        service = ((BaseApplication) getApplication()).getNetworkService();
        contactUsPresenter = new ContactUsPresenter(this, service);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = edtName.getText().toString();
                String email = edtEmail.getText().toString();
                String message = edtMassage.getText().toString();

                if (name.isEmpty() && email.isEmpty() && message.isEmpty()) {
                    if (name.isEmpty()) {
                        edtName.setError("name a empty");
                    } else {
                        edtName.setError(null);
                    }

                    if (email.isEmpty() || android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        edtEmail.setError("enter a valid email address");
                    } else {
                        edtEmail.setError(null);
                    }

                    if (message.isEmpty()) {
                        edtMassage.setError("message a empty");
                    } else {
                        edtMassage.setError(null);
                    }

                } else {

                    //saveFeedback();
                    contactUsPresenter.sendFeedback(edtName.getText().toString(),
                            edtEmail.getText().toString(),
                            edtSubject.getText().toString(),
                            edtMassage.getText().toString());

                }

            }
        });

        /**
         *maps
         */
        try {
            // Loading map
            initilizeMap();

        } catch (Exception e) {
            e.printStackTrace();
        }

        addressContact.setText(R.string.address);
        phoneContact.setText(R.string.tlpn);
        emailContact.setText(R.string.email);
        webContact.setText(R.string.web);

    }

    private void initilizeMap() {
        /*if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.maps_contact_us)).getMap();
            googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude));
            googleMap.addMarker(marker);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(latitude, longitude)).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }*/

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.maps_contact_us);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ContactUsActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void successResponseFeedback() {
        Toast.makeText(this, "Sukses mengirimkan feedback", Toast.LENGTH_LONG).show();
        edtMassage.setText("");
        edtSubject.setText("");
    }

    public void onLoading(boolean loading) {
        if (loading) {
            progressDialog = new ProgressDialog(ContactUsActivity.this);
            progressDialog.setMessage(getResources().getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    public void failedResponseFeedback(int errorType) {
        switch (errorType) {
            case General.NETWORK_ERROR_CONNCECTION:
                Toast.makeText(this, "Koneksi bermasalah, silahkan cek koneksi internet Anda", Toast.LENGTH_LONG).show();
                break;
            case General.NETWORK_ERROR_PERMISSION_ACCESS:
                Toast.makeText(this, "Gagal mengirimkan feedback, silahkan coba lagi", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude));
        googleMap.addMarker(marker);

        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(latitude, longitude)).zoom(14).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        // check if map is created successfully or not
        if (googleMap == null) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }
    }
}
