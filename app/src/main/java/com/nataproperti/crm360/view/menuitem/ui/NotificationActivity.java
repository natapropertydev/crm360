package com.nataproperti.crm360.view.menuitem.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.snackbar.Snackbar;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;
import com.nataproperti.crm360.view.menuitem.adapter.NotificationAdapter;
import com.nataproperti.crm360.view.menuitem.model.NotificationModel;
import com.nataproperti.crm360.view.menuitem.presenter.NotificationPresenter;

import java.util.ArrayList;

import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {
    public static final String TAG = "NotificationActivity";

    public static final String PREF_NAME = "pref";

    public static final String TITLE = "title";
    public static final String DATE = "date";
    public static final String LINK_DETAIL = "linkDetail";
    public static final String GCM_MESSAGE_REF = "gcmMessageRef";
    public static final String GCM_MESSAGE_TYPE_REF = "gcmMessageTypeRef";
    private static final String EXTRA_RX = "EXTRA_RX";

    SharedPreferences sharedPreferences;

    ListView listView;

    private ArrayList<NotificationModel> listNotification = new ArrayList<NotificationModel>();
    private NotificationAdapter adapter;

    String memberRef, linkDetail, titleGcm, date, gcmMessageRef, gcmMessageTypeRef;

    LinearLayout linearNoData, linearLayout;
    Toolbar toolbar;
    TextView title;
    Typeface font;

    NotificationPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initWidget();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new NotificationPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        requestListNotification();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                linkDetail = listNotification.get(position).getLinkDetail();
                titleGcm = listNotification.get(position).getGcmTitle();
                date = listNotification.get(position).getInputTime();
                gcmMessageRef = listNotification.get(position).getGcmMessageRef();
                gcmMessageTypeRef = listNotification.get(position).getGcmMessageTypeRef();

                Intent intent = new Intent(NotificationActivity.this, NotificationDetailActivity.class);
                intent.putExtra(TITLE, titleGcm);
                intent.putExtra(DATE, date);
                intent.putExtra(LINK_DETAIL, linkDetail);
                intent.putExtra(GCM_MESSAGE_REF, gcmMessageRef);
                intent.putExtra(GCM_MESSAGE_TYPE_REF, gcmMessageTypeRef);
                startActivity(intent);
            }
        });

    }

    public void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_notification));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        linearNoData = (LinearLayout) findViewById(R.id.linear_no_data);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        listView = (ListView) findViewById(R.id.list_notification);
    }

    private void requestListNotification() {
        presenter.getListNotificationSSTSvc(memberRef);
    }

    public void showListNotificationResults(Response<ArrayList<NotificationModel>> response) {
        listNotification = response.body();

        if (listNotification.size() != 0) {
            initAdapter();
            linearNoData.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        } else {
            linearNoData.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }
    }

    public void showListNotificationFailure(Throwable t) {
        Snackbar snackbar = Snackbar.make(linearLayout, getString(R.string.error_connection), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void initAdapter() {
        adapter = new NotificationAdapter(this, listNotification);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NotificationActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
