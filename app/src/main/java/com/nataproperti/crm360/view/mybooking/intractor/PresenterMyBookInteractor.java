package com.nataproperti.crm360.view.mybooking.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyBookInteractor {
    void getListBook(String memberRef);
    void rxUnSubscribe();

}
