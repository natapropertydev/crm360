package com.nataproperti.crm360.view.mybooking.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyBookingProjectPsInteractor {
    void getListBookingProjectPs(String dbMasterRef,String projectRef,String projectPsRef);
    void rxUnSubscribe();

}
