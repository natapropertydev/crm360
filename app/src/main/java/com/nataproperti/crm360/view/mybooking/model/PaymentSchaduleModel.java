package com.nataproperti.crm360.view.mybooking.model;

/**
 * Created by Nata on 1/23/2017.
 */

public class PaymentSchaduleModel {
    String type,dueDate,amount;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
