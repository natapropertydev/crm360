package com.nataproperti.crm360.view.mybooking.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.booking.model.DetailBooking;
import com.nataproperti.crm360.view.booking.model.MemberInfoModel;
import com.nataproperti.crm360.view.mybooking.intractor.PresenterMyBookingInfoInteractor;
import com.nataproperti.crm360.view.mybooking.model.BookingPaymentInfoModel;
import com.nataproperti.crm360.view.mybooking.ui.MyBookingInfoActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyBookingInfoPresenter implements PresenterMyBookingInfoInteractor {
    private MyBookingInfoActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public MyBookingInfoPresenter(MyBookingInfoActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getMemberInfoSvc(String memberRef) {
        Call<MemberInfoModel> call = service.getAPI().getMemberInfoSvc(memberRef);
        call.enqueue(new Callback<MemberInfoModel>() {
            @Override
            public void onResponse(Call<MemberInfoModel> call, Response<MemberInfoModel> response) {
                view.showMemberInfoResults(response);
            }

            @Override
            public void onFailure(Call<MemberInfoModel> call, Throwable t) {
                view.showMemberInfoFailure(t);

            }


        });
    }

    @Override
    public void getBookingDetailSvc(String dbMasterRef, String projectRef, String bookingRef) {
        Call<DetailBooking> call = service.getAPI().getBookingDetailSvc(dbMasterRef,projectRef, bookingRef);
        call.enqueue(new Callback<DetailBooking>() {
            @Override
            public void onResponse(Call<DetailBooking> call, Response<DetailBooking> response) {
                view.showBookingInfoResults(response);
            }

            @Override
            public void onFailure(Call<DetailBooking> call, Throwable t) {
                view.showBookingInfoFailure(t);

            }


        });
    }

    @Override
    public void getBookingPaymentInfoSvc(String bookingRef) {
        Call<BookingPaymentInfoModel> call = service.getAPI().getBookingPaymentInfoSvc(bookingRef);
        call.enqueue(new Callback<BookingPaymentInfoModel>() {
            @Override
            public void onResponse(Call<BookingPaymentInfoModel> call, Response<BookingPaymentInfoModel> response) {
                view.showBookingPaymentInfoResults(response);
            }

            @Override
            public void onFailure(Call<BookingPaymentInfoModel> call, Throwable t) {
                view.showBookingPaymentInfoFailure(t);

            }


        });
    }


    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }


}
