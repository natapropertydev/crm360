package com.nataproperti.crm360.view.mybooking.presenter;

import com.nataproperti.crm360.view.mybooking.intractor.PresenterMyBookingProjectPsInteractor;
import com.nataproperti.crm360.view.mybooking.model.MyBookingModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.mybooking.ui.MyBookingProjectPsActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyBookingProjectPsPresenter implements PresenterMyBookingProjectPsInteractor {
    private MyBookingProjectPsActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public MyBookingProjectPsPresenter(MyBookingProjectPsActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListBookingProjectPs(String dbMasterRef, String projectRef, String projectPsRef) {
        Call<List<MyBookingModel>> call = service.getAPI().getListBookingProjectPs(dbMasterRef,projectRef,projectPsRef);
        call.enqueue(new Callback<List<MyBookingModel>>() {
            @Override
            public void onResponse(Call<List<MyBookingModel>> call, Response<List<MyBookingModel>> response) {
                view.showListBookingProjectPsResults(response);
            }

            @Override
            public void onFailure(Call<List<MyBookingModel>> call, Throwable t) {
                view.showListBookingProjectPsFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
