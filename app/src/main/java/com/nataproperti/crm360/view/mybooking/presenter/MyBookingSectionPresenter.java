package com.nataproperti.crm360.view.mybooking.presenter;

import com.nataproperti.crm360.view.mybooking.intractor.PresenterMyBookingSectionInteractor;
import com.nataproperti.crm360.view.mybooking.model.MyBookingSectionModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.mybooking.ui.MyBookingSectionActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyBookingSectionPresenter implements PresenterMyBookingSectionInteractor {
    private MyBookingSectionActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public MyBookingSectionPresenter(MyBookingSectionActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListBookingSection(String memberRef) {
        Call<List<MyBookingSectionModel>> call = service.getAPI().getListBookingSection(memberRef);
        call.enqueue(new Callback<List<MyBookingSectionModel>>() {
            @Override
            public void onResponse(Call<List<MyBookingSectionModel>> call, Response<List<MyBookingSectionModel>> response) {
                view.showListBookingSectionResults(response);
            }

            @Override
            public void onFailure(Call<List<MyBookingSectionModel>> call, Throwable t) {
                view.showListBookingSectionFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
