package com.nataproperti.crm360.view.mybooking.ui;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.mybooking.adapter.MyBookingAdapter;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.helper.MyListView;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.mybooking.model.MyBookingModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.mybooking.presenter.MyBookPresenter;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;
import com.nataproperti.crm360.view.project.ui.ProjectActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 6/14/2016.
 */
public class MyBookingActivity extends AppCompatActivity {
    public static final String TAG = "MyBookingActivity";

    public static final String PREF_NAME = "pref";

    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_REF = "projectRef";
    public static final String BOOKING_REF = "bookingRef";
    public static final String MEMBER_REF = "memberRef";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private MyBookPresenter presenter;
//    ProgressDialog progressDialog;

    private List<MyBookingModel> listMyBooking = new ArrayList<>();
    private MyBookingAdapter adapter;
    private MyListView listView;

    private SharedPreferences sharedPreferences;

    private String memberRef, dbMasterRef, projectRef, bookingRef;

    ProgressDialog progressDialog;
    AlertDialog alertDialog;

    String result, projectBookingRef;
    LinearLayout itemListNoData;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MyBookPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        initWidget();
        requestMyBooking();
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_my_booking));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        itemListNoData = (LinearLayout) findViewById(R.id.linear_no_data);
        listView = (MyListView) findViewById(R.id.list_mybooking);
    }

    public void requestMyBooking() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListBook(memberRef.toString());
    }

    public void showListBookResults(retrofit2.Response<List<MyBookingModel>> response) {
        progressDialog.dismiss();
        listMyBooking = response.body();

        if (listMyBooking.size() >= 1) {
            initAdapter();

        } else {
            AlertDialog.Builder alertDialogBuilder =
                    new AlertDialog.Builder(MyBookingActivity.this);
            alertDialogBuilder.setMessage("Booking tidak tersedia, Silakan booking terlebih dahulu.");
            alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(MyBookingActivity.this, ProjectActivity.class));
                    finish();
                }
            });
            alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();

                }
            });
            alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        Log.d("jsonArray", String.valueOf(listMyBooking.size()));
    }

    private void initAdapter() {
        adapter = new MyBookingAdapter(MyBookingActivity.this, listMyBooking);
        listView.setAdapter(adapter);
        listView.setExpanded(true);
    }

    public void showListBookFailure(Throwable t) {
        progressDialog.dismiss();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyBookingActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

    }
}
