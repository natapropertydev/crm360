package com.nataproperti.crm360.view.mynup.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyNupProjectPsInteractor {
    void getListNupProjectPs(String dbMasterRef,String projectRef,String projectPsRef,String memberRef,String projectSchemeRef);
    void rxUnSubscribe();

}
