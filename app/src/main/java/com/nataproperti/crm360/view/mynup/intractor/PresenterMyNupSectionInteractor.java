package com.nataproperti.crm360.view.mynup.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface PresenterMyNupSectionInteractor {
    void getListNupSection(String memberRef);
    void rxUnSubscribe();

}
