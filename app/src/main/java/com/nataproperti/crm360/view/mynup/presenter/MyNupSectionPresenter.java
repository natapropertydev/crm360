package com.nataproperti.crm360.view.mynup.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.mynup.intractor.PresenterMyNupSectionInteractor;
import com.nataproperti.crm360.view.mynup.model.MyNupSectionModel;
import com.nataproperti.crm360.view.mynup.ui.MyNupSectionActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class MyNupSectionPresenter implements PresenterMyNupSectionInteractor {
    private MyNupSectionActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public MyNupSectionPresenter(MyNupSectionActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListNupSection(String memberRef) {
        Call<List<MyNupSectionModel>> call = service.getAPI().getListNupSection(memberRef);
        call.enqueue(new Callback<List<MyNupSectionModel>>() {
            @Override
            public void onResponse(Call<List<MyNupSectionModel>> call, Response<List<MyNupSectionModel>> response) {
                view.showListNupSectionResults(response);
            }

            @Override
            public void onFailure(Call<List<MyNupSectionModel>> call, Throwable t) {
                view.showListNupSectionFailure(t);

            }


        });
    }

    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }




}
