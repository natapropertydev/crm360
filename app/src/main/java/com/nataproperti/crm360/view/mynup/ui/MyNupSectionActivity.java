package com.nataproperti.crm360.view.mynup.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.mynup.adapter.MyNupSectionAdapter;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.mynup.model.MyNupSectionModel;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.mynup.presenter.MyNupSectionPresenter;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class MyNupSectionActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";

    Toolbar toolbar;
    TextView title;
    ListView listView;
    SharedPreferences sharedPreferences;
    String memberRef, projectRef, dbMasterRef, projectPsRef, projectSchemeRef;
    ProgressDialog progressDialog;

    MyNupSectionPresenter presenter;
    ServiceRetrofit service;
    boolean rxCallInWorks = false;

    MyNupSectionAdapter adapter;
    List<MyNupSectionModel> listMyNupSection = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_nup_section);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new MyNupSectionPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }
        initWidget();

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        requestSectionNup();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                projectRef = listMyNupSection.get(position).getProjectRef();
                dbMasterRef = listMyNupSection.get(position).getDbMasterRef();
                projectPsRef = listMyNupSection.get(position).getProjectPsRef();
                projectSchemeRef = listMyNupSection.get(position).getProjectSchemeRef();

                if (projectRef.equals("0")) {
                    startActivity(new Intent(MyNupSectionActivity.this, MyNupActivity.class));
                } else {
                    Intent intent = new Intent(MyNupSectionActivity.this, MyNupProjectPsActivity.class);
                    intent.putExtra("dbMasterRef", dbMasterRef);
                    intent.putExtra("projectRef", projectRef);
                    intent.putExtra("projectPsRef", projectPsRef);
                    intent.putExtra("projectSchemeRef", projectSchemeRef);
                    startActivity(intent);
                }
            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("My NUP");
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        listView = (ListView) findViewById(R.id.list_mynup_section);
    }

    public void requestSectionNup() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListNupSection(memberRef.toString());
    }

    public void showListNupSectionResults(Response<List<MyNupSectionModel>> response) {
        progressDialog.dismiss();
        listMyNupSection = response.body();
        initAdapter();
    }

    public void showListNupSectionFailure(Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(MyNupSectionActivity.this, getString(R.string.error_connection), Toast.LENGTH_SHORT).show();
    }

    private void initAdapter() {
        adapter = new MyNupSectionAdapter(MyNupSectionActivity.this, listMyNupSection);
        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyNupSectionActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
