package com.nataproperti.crm360.view.news.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.news.model.NewsDetailModel;

import java.util.List;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by User on 5/11/2016.
 */
public class NewsDetailImageAdapter extends PagerAdapter {
    public static final String TAG = "NewsDetailImageAdapter";

    Context context;
    private List<NewsDetailModel> list;

    PhotoViewAttacher attacher;

    public NewsDetailImageAdapter(Context context, List<NewsDetailModel> list){
        this.context = context;
        this.list = list;
    }

    public Object instantiateItem(ViewGroup container, final int position) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.item_list_news_detail_image, container, false);
        ImageView imageView = (ImageView) viewItem.findViewById(R.id.image_news_detail_image);

        NewsDetailModel image = list.get(position);

        Glide.with(context).load(image.getImageSlider()).into(imageView);

        //zoom
        attacher = new PhotoViewAttacher(imageView);
        attacher.update();

        Log.d(TAG,""+image.getImageSlider());

        ((ViewPager)container).addView(viewItem);

        return viewItem;
    }


    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub

        return view == ((View)object);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }
}
