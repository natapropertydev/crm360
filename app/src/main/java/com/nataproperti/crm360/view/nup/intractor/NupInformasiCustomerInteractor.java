package com.nataproperti.crm360.view.nup.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface NupInformasiCustomerInteractor {
    void getMemberInfoSvc(String memberRef);

    void rxUnSubscribe();

}
