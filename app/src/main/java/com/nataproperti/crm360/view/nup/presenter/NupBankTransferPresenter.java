package com.nataproperti.crm360.view.nup.presenter;


import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.booking.model.SaveBookingModel;
import com.nataproperti.crm360.view.nup.intractor.NupBankTransferInteractor;
import com.nataproperti.crm360.view.nup.model.AccountBankModel;
import com.nataproperti.crm360.view.nup.model.BankModel;
import com.nataproperti.crm360.view.nup.ui.NupBankTransferActivity;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class NupBankTransferPresenter implements NupBankTransferInteractor {
    private NupBankTransferActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public NupBankTransferPresenter(NupBankTransferActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }



    @Override
    public void getProjectAccountBank(String dbMasterRef, String projectRef) {
        Call<ArrayList<AccountBankModel>> call = service.getAPI().getProjectAccountBank(dbMasterRef,projectRef);
        call.enqueue(new Callback<ArrayList<AccountBankModel>>() {
            @Override
            public void onResponse(Call<ArrayList<AccountBankModel>> call, Response<ArrayList<AccountBankModel>> response) {
                view.showListProjectAccountBankResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<AccountBankModel>> call, Throwable t) {
                view.showListProjectAccountBankFailure(t);

            }


        });
    }

    @Override
    public void getListBankSvc() {
        Call<ArrayList<BankModel>> call = service.getAPI().getListBankSvc();
        call.enqueue(new Callback<ArrayList<BankModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BankModel>> call, Response<ArrayList<BankModel>> response) {
                view.showListBankResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<BankModel>> call, Throwable t) {
                view.showListBankFailure(t);

            }


        });
    }

    @Override
    public void savePaymentBankTransferSvc(Map<String, String> fields) {
        Call<SaveBookingModel> call = service.getAPI().savePaymentBankTransferSvc(fields);
        call.enqueue(new Callback<SaveBookingModel>() {
            @Override
            public void onResponse(Call<SaveBookingModel> call, Response<SaveBookingModel> response) {
                view.showSaveNupResults(response);
            }

            @Override
            public void onFailure(Call<SaveBookingModel> call, Throwable t) {
                view.showSaveNupFailure(t);

            }


        });
    }


    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }


}
