package com.nataproperti.crm360.view.nup.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.booking.model.MemberInfoModel;
import com.nataproperti.crm360.view.nup.intractor.NupInformasiCustomerInteractor;
import com.nataproperti.crm360.view.nup.ui.NupInformasiCustomerActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class NupInformasiCustomerPresenter implements NupInformasiCustomerInteractor {
    private NupInformasiCustomerActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public NupInformasiCustomerPresenter(NupInformasiCustomerActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void getMemberInfoSvc(String memberRef) {
        Call<MemberInfoModel> call = service.getAPI().getMemberInfoSvc(memberRef);
        call.enqueue(new Callback<MemberInfoModel>() {
            @Override
            public void onResponse(Call<MemberInfoModel> call, Response<MemberInfoModel> response) {
                view.showMemberInfoResults(response);
            }

            @Override
            public void onFailure(Call<MemberInfoModel> call, Throwable t) {
                view.showMemberInfoFailure(t);

            }

        });
    }


    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }


}
