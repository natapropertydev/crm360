package com.nataproperti.crm360.view.nup.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.booking.model.BookingPaymentMethodModel;
import com.nataproperti.crm360.view.nup.intractor.PresenterNupPaymentMethodInteractor;
import com.nataproperti.crm360.view.nup.model.VeritransNupVTModel;
import com.nataproperti.crm360.view.nup.ui.NupPaymentMethodActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class NupPaymentMethodPresenter implements PresenterNupPaymentMethodInteractor {
    private NupPaymentMethodActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public NupPaymentMethodPresenter(NupPaymentMethodActivity view, ServiceRetrofit service){
        this.view = view;
        this.service = service;
    }


    @Override
    public void GetPaymentMethodSvc(String dbMasterRef, String projectRef) {
        Call<List<BookingPaymentMethodModel>> call = service.getAPI().GetPaymentMethodSvc(dbMasterRef,projectRef);
        call.enqueue(new Callback<List<BookingPaymentMethodModel>>() {
            @Override
            public void onResponse(Call<List<BookingPaymentMethodModel>> call, Response<List<BookingPaymentMethodModel>> response) {
                view.showListNupPaymentMethodResults(response);
            }

            @Override
            public void onFailure(Call<List<BookingPaymentMethodModel>> call, Throwable t) {
                view.showListNupPaymentMethodFailure(t);

            }


        });
    }

    @Override
    public void updatePaymentTypeOrderSvc(String nupOrderRef, String paymentType, String memberRef) {
        Call<VeritransNupVTModel> call = service.getAPI().updatePaymentTypeOrderSvc(nupOrderRef,paymentType, memberRef);
        call.enqueue(new Callback<VeritransNupVTModel>() {
            @Override
            public void onResponse(Call<VeritransNupVTModel> call, Response<VeritransNupVTModel> response) {
                view.showPaymentTypeResults(response);
            }

            @Override
            public void onFailure(Call<VeritransNupVTModel> call, Throwable t) {
                view.showPaymentTypeFailure(t);

            }


        });
    }

    @Override
    public void updatePaymentTypeOrderVTSvc(String nupOrderRef, String paymentType, String dbMasterRef, String projectRef, String memberRef, String nupAmt, String total,final String paymentMethod) {
        Call<VeritransNupVTModel> call = service.getAPI().updatePaymentTypeOrderVTSvc(nupOrderRef,paymentType,dbMasterRef, projectRef,  memberRef,  nupAmt, total);
        call.enqueue(new Callback<VeritransNupVTModel>() {
            @Override
            public void onResponse(Call<VeritransNupVTModel> call, Response<VeritransNupVTModel> response) {
                view.showPaymentTypeOrderVTResults(response,paymentMethod);
            }

            @Override
            public void onFailure(Call<VeritransNupVTModel> call, Throwable t) {
                view.showPaymentTypeOrderVTFailure(t);

            }


        });
    }


    @Override
    public void rxUnSubscribe() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }


}
