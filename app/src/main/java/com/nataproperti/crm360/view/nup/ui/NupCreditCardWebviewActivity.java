package com.nataproperti.crm360.view.nup.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;

public class NupCreditCardWebviewActivity extends AppCompatActivity {
    public static final String TAG = "NupCreditCardWebview";
    public static final String PREF_NAME = "pref";
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String NUP_ORDER_REF = "nupOrderRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String TOTAL = "total";

    String nupOrderRef, projectRef, dbMasterRef, projectName, total, memberRef,link,titlePayment,paymentType="";
    SharedPreferences sharedPreferences;

    WebView webView;
    boolean mylistNup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_credit_card_webview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        MyTextViewLatoReguler title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        Intent intent = getIntent();
        dbMasterRef = intent.getStringExtra(DBMASTER_REF);
        projectRef = intent.getStringExtra(PROJECT_REF);
        projectName = intent.getStringExtra(PROJECT_NAME);
        nupOrderRef = intent.getStringExtra(NUP_ORDER_REF);
        total = intent.getStringExtra(TOTAL);
        link = intent.getStringExtra("link");
        titlePayment = intent.getStringExtra("titlePayment");
        paymentType = intent.getStringExtra("paymentType");
        mylistNup = intent.getBooleanExtra("mylistNup",false);

        Log.d(TAG,"paymentType "+paymentType);

        title.setText(titlePayment);

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(link);
        Log.d("lingWebView",""+link);

        webView.addJavascriptInterface(new JavaScriptInterface(this), "Android");

    }

    public class JavaScriptInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void intentFinish() {
            Intent intent = new Intent(NupCreditCardWebviewActivity.this,NupFinishActivity.class);
            intent.putExtra(DBMASTER_REF,dbMasterRef);
            intent.putExtra(PROJECT_REF,projectRef);
            intent.putExtra(NUP_ORDER_REF,nupOrderRef);
            intent.putExtra("paymentType",paymentType);
            intent.putExtra("mylistNup",mylistNup);

            SharedPreferences sharedPreferences = NupCreditCardWebviewActivity.this.
                    getSharedPreferences(PREF_NAME, 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("isMemberCostumerRef","");
            editor.commit();

            startActivity(intent);
            finish();
        }

        @JavascriptInterface
        public void intentPaymentMethod() {
            Toast.makeText(mContext, "Failed to Pay", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
