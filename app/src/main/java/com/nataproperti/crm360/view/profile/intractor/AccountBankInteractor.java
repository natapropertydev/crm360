package com.nataproperti.crm360.view.profile.intractor;

import java.util.Map;

/**
 * Created by nata on 11/23/2016.
 */

public interface AccountBankInteractor {
    void getListBankSvc();

    void updateAccountBankSvc(Map<String, String> fields);

    void rxUnSubscribe();

}
