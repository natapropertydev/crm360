package com.nataproperti.crm360.view.profile.model;

/**
 * Created by User on 5/15/2016.
 */
public class MaritalStatusModel {
    String maritalStatusRef,maritalStatusName,maritalStatusSorNo;

    public String getMaritalStatusRef() {
        return maritalStatusRef;
    }

    public void setMaritalStatusRef(String maritalStatusRef) {
        this.maritalStatusRef = maritalStatusRef;
    }

    public String getMaritalStatusName() {
        return maritalStatusName;
    }

    public void setMaritalStatusName(String maritalStatusName) {
        this.maritalStatusName = maritalStatusName;
    }

    public String getMaritalStatusSorNo() {
        return maritalStatusSorNo;
    }

    public void setMaritalStatusSorNo(String maritalStatusSorNo) {
        this.maritalStatusSorNo = maritalStatusSorNo;
    }
}
