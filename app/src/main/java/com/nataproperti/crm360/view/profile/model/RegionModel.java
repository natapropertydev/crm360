package com.nataproperti.crm360.view.profile.model;

/**
 * Created by User on 5/15/2016.
 */
public class RegionModel {
    String regionRef;
    String regionName;
    String regionSortNo;

    public String getRegionRef() {
        return regionRef;
    }

    public void setRegionRef(String regionRef) {
        this.regionRef = regionRef;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionSortNo() {
        return regionSortNo;
    }

    public void setRegionSortNo(String regionSortNo) {
        this.regionSortNo = regionSortNo;
    }
}
