package com.nataproperti.crm360.view.profile.presenter;


import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.nup.model.BankModel;
import com.nataproperti.crm360.view.profile.intractor.AccountBankInteractor;
import com.nataproperti.crm360.view.profile.model.ResponeModel;
import com.nataproperti.crm360.view.profile.ui.AccountBankActivity;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class AccountBankPresenter implements AccountBankInteractor {
    private AccountBankActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public AccountBankPresenter(AccountBankActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void getListBankSvc() {
        Call<ArrayList<BankModel>> call = service.getAPI().getListBankSvc();
        call.enqueue(new Callback<ArrayList<BankModel>> () {
            @Override
            public void onResponse(Call<ArrayList<BankModel>>  call, Response<ArrayList<BankModel>>  response) {
                view.showListBankResults(response);
            }

            @Override
            public void onFailure(Call<ArrayList<BankModel>>  call, Throwable t) {
                view.showListBankFailure(t);

            }


        });

    }

    @Override
    public void updateAccountBankSvc(Map<String, String> fields) {
        Call<ResponeModel> call = service.getAPI().updateAccountBankSvc(fields);
        call.enqueue(new Callback<ResponeModel> () {
            @Override
            public void onResponse(Call<ResponeModel>  call, Response<ResponeModel>  responseToken) {
                view.showResponeResults(responseToken);
            }

            @Override
            public void onFailure(Call<ResponeModel>  call, Throwable t) {
                view.showResponeFailure(t);

            }


        });
    }


    @Override
    public void rxUnSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }
}
