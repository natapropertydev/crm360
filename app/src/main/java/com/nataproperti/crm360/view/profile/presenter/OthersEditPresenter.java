package com.nataproperti.crm360.view.profile.presenter;


import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.profile.intractor.OthersEditInteractor;
import com.nataproperti.crm360.view.profile.model.OthersEditModel;
import com.nataproperti.crm360.view.profile.model.ResponeModel;
import com.nataproperti.crm360.view.profile.ui.OthersEditActivity;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class OthersEditPresenter implements OthersEditInteractor {
    private OthersEditActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public OthersEditPresenter(OthersEditActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }

    @Override
    public void getOtherProfileSvc() {
        Call<OthersEditModel> call = service.getAPI().getOtherProfileSvc();
        call.enqueue(new Callback<OthersEditModel> () {
            @Override
            public void onResponse(Call<OthersEditModel> call, Response<OthersEditModel>  response) {
                view.showListResults(response);
            }

            @Override
            public void onFailure(Call<OthersEditModel>  call, Throwable t) {
                view.showListFailure(t);

            }


        });

    }

    @Override
    public void updateOtherProfileSvc(Map<String, String> fields) {
        Call<ResponeModel> call = service.getAPI().updateOtherProfileSvc(fields);
        call.enqueue(new Callback<ResponeModel> () {
            @Override
            public void onResponse(Call<ResponeModel>  call, Response<ResponeModel>  response) {
                view.showResponeResults(response);
            }

            @Override
            public void onFailure(Call<ResponeModel>  call, Throwable t) {
                view.showResponeFailure(t);

            }


        });
    }


    @Override
    public void rxUnSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }
}
