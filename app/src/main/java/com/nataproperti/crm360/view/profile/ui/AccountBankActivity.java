package com.nataproperti.crm360.view.profile.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;
import com.nataproperti.crm360.view.nup.adapter.BankAdapter;
import com.nataproperti.crm360.view.nup.model.BankModel;
import com.nataproperti.crm360.view.profile.model.ResponeModel;
import com.nataproperti.crm360.view.profile.presenter.AccountBankPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Response;

/**
 * Created by User on 6/3/2016.
 */
public class AccountBankActivity extends AppCompatActivity {
    public static final String PREF_NAME = "pref";
    private static final String EXTRA_RX = "EXTRA_RX";
    private ServiceRetrofit service;
    private boolean rxCallInWorks = false;
    private AccountBankPresenter presenter;
    private ArrayList<BankModel> listBank = new ArrayList<BankModel>();
    private BankAdapter adapterBank;

    ProgressDialog progressDialog;
    String message;
    EditText edtBankBranch, edtAccountName, edtAccountNumber;
    Spinner spnBank;
    Button btnSave;
    String bankRef;
    String memberRef, bankBranch, accName, accNo;
    Typeface font;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface fontLight;
    Intent intent;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_bank);
        intent = getIntent();
        bankRef = intent.getStringExtra("bankRef");
        bankBranch = intent.getStringExtra("bankBranch");
        accName = intent.getStringExtra("accName");
        accNo = intent.getStringExtra("accNo");
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new AccountBankPresenter(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        Log.d("Accont", "" + bankRef);
        edtBankBranch.setText(bankBranch);
        edtAccountName.setText(accName);
        edtAccountNumber.setText(accNo);
        requestBank();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAccountBank();
            }
        });
    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.title_account_bank));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        fontLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        edtBankBranch = (EditText) findViewById(R.id.edt_bank_branch);
        edtAccountName = (EditText) findViewById(R.id.edt_account_name);
        edtAccountNumber = (EditText) findViewById(R.id.edt_account_number);
        spnBank = (Spinner) findViewById(R.id.list_bank);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setTypeface(font);
    }

    private void requestBank() {
        presenter.getListBankSvc();
    }

    private void updateAccountBank() {
        String bankBranch = edtBankBranch.getText().toString();
        String accName = edtAccountName.getText().toString();
        String accNo = edtAccountNumber.getText().toString();
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        Map<String, String> fields = new HashMap<>();
        fields.put("memberRef", memberRef);
        fields.put("bankRef", bankRef);
        fields.put("bankBranch", bankBranch);
        fields.put("accName", accName);
        fields.put("accNo", accNo);
        presenter.updateAccountBankSvc(fields);
    }

    public void showListBankResults(Response<ArrayList<BankModel>> response) {
        listBank = response.body();
        adapterBank = new BankAdapter(getApplication(), listBank);
        spnBank.setAdapter(adapterBank);
        spnBank.setSelection(Integer.parseInt(bankRef));
        spnBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                bankRef = listBank.get(position).getBankRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showListBankFailure(Throwable t) {

    }

    public void showResponeResults(Response<ResponeModel> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()){
            int status = response.body().getStatus();
            message = response.body().getMessage();
            if (status == 200) {
                finish();
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        } else {
            finish();
        }

    }

    public void showResponeFailure(Throwable t) {
        progressDialog.dismiss();
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(AccountBankActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
