package com.nataproperti.crm360.view.project.adapter;

import static com.nataproperti.crm360.config.General.PREF_NAME;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.General;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;
import com.nataproperti.crm360.view.mybooking.adapter.MyBookingAdapter;
import com.nataproperti.crm360.view.mybooking.model.MyBookingModel;
import com.nataproperti.crm360.view.project.model.ProjectModelNew;
import com.nataproperti.crm360.view.project.ui.ProjectActivity;
import com.nataproperti.crm360.view.project.ui.ValidateInhouseCode;
import com.nataproperti.crm360.view.projectmenu.ui.ProjectMenuActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GVProjectAdapter extends BaseAdapter {

    private Context context;
    private List<ProjectModelNew> list;
    private Display display;
    private ListProjectHolder holder;

    public GVProjectAdapter(Context context, List<ProjectModelNew> list, Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.include_card_projectrequest, null);
            holder = new ListProjectHolder();
            holder.cv = (CardView) convertView.findViewById(R.id.cv);
            holder.projectName = (TextView) convertView.findViewById(R.id.projectName);
            holder.locationName = (TextView) convertView.findViewById(R.id.locationName);
            holder.subLocationName = (TextView) convertView.findViewById(R.id.subLocationName);
            holder.projectImg = (ImageView) convertView.findViewById(R.id.imgProject);
            holder.btnJoin = (ImageView) convertView.findViewById(R.id.btn_join);
            holder.btnJoined = (ImageView) convertView.findViewById(R.id.btn_joined);
            holder.btnWaiting = (ImageView) convertView.findViewById(R.id.btn_waiting);
            convertView.setTag(holder);
        } else {
            holder = (ListProjectHolder) convertView.getTag();
        }

        holder.sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        holder.state = holder.sharedPreferences.getBoolean("isLogin", false);
        holder.memberRef = holder.sharedPreferences.getString("isMemberRef", null);

        final ProjectModelNew project = list.get(position);
        holder.projectName.setText(project.getProjectName().toUpperCase());
        holder.locationName.setText(project.getLocationName());
        holder.subLocationName.setText(project.getSubLocationName());

        if (project.getIsJoin().equals("0")) {
            if (project.getIsWaiting() != null) {
                if (project.getIsWaiting().equals("1")) {
                    holder.btnJoin.setVisibility(View.GONE);
                    holder.btnJoined.setVisibility(View.GONE);
                    holder.btnWaiting.setVisibility(View.VISIBLE);
                } else {
                    holder.btnJoin.setVisibility(View.VISIBLE);
                    holder.btnJoined.setVisibility(View.GONE);
                    holder.btnWaiting.setVisibility(View.GONE);
                }
            } else {
                holder.btnJoined.setVisibility(View.GONE);
                holder.btnJoin.setVisibility(View.GONE);
                holder.btnWaiting.setVisibility(View.GONE);
            }
        } else {
            holder.btnJoined.setVisibility(View.VISIBLE);
            holder.btnJoin.setVisibility(View.GONE);
            holder.btnWaiting.setVisibility(View.GONE);
        }

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
//        Double result = width / 1.233333333333333;
        Double result = width / 2.0;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = holder.projectImg.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        holder.projectImg.setLayoutParams(params);
        holder.projectImg.requestLayout();
        if (MainMenuActivity.OFF_LINE_MODE) {
            holder.projectImg.setImageResource(R.drawable.ic_default_project1);
        } else {
            Glide.with(context).load(project.getImage()).into(holder.projectImg);
        }

        holder.projectImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProjectMenuActivity.class);
                intent.putExtra(General.PROJECT_REF, project.getProjectRef());
                intent.putExtra(General.DBMASTER_REF, project.getDbMasterRef());
                intent.putExtra(General.PROJECT_NAME, project.getProjectName());
                intent.putExtra(General.LOCATION_NAME, project.getLocationName());
                intent.putExtra(General.LOCATION_REF, project.getLocationRef());
                intent.putExtra(General.SUBLOCATION_NAME, project.getSubLocationName());
                intent.putExtra(General.SUBLOCATION_REF, project.getSublocationRef());
                SharedPreferences sharedPreferences = context.
                        getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("isWaiting", project.getIsWaiting());
                editor.putString("isJoin", project.getIsJoin());
                editor.commit();
                //context.startActivity(intent);
                ((Activity) context).startActivityForResult(intent,1);
            }
        });

        holder.btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View dialogView = inflater.inflate(R.layout.dialog_insert_member_code, null);
                dialogBuilder.setView(dialogView);
                holder.edtMemberCode = (EditText) dialogView.findViewById(R.id.edt_member_code);
                holder.txtTermMemberCode = (TextView) dialogView.findViewById(R.id.txt_term_member_code);
                dialogBuilder.setMessage("Masukkan Member ID");
                holder.txtTermMemberCode.setText("Kosongkan jika tidak memiliki Member ID di " + project.getProjectName());
                dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //subscribeProject();
                    }
                });
                dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                    }
                });
                holder.alertDialogJoin = dialogBuilder.create();
                holder.alertDialogJoin.setCancelable(false);
                holder.alertDialogJoin.show();
                Button positiveButton = holder.alertDialogJoin.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.memberCode = holder.edtMemberCode.getText().toString();
                        holder.progressDialog = ProgressDialog.show(context, "", "Please Wait...", true);
                        final StringRequest request = new StringRequest(Request.Method.POST,
                                WebService.subscribeProject(), new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    holder.progressDialog.dismiss();
                                    JSONObject jo = new JSONObject(response);
                                    int status = jo.getInt("status");
                                    String message = jo.getString("message");

                                    if (status == 200) {
                                        holder.alertDialogJoin.dismiss();
                                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(context, ProjectActivity.class);
                                        ((Activity) context).recreate();
                                        //context.startActivity(intent);
                                    } else if (status == 202) {
                                        holder.alertDialogJoin.dismiss();
                                        Intent intent = new Intent(context, ValidateInhouseCode.class);
                                        intent.putExtra("memberRef", holder.memberRef);
                                        intent.putExtra("dbMasterRef", String.valueOf(project.getDbMasterRef()));
                                        intent.putExtra("projectRef", project.getProjectRef());
                                        intent.putExtra("memberCode", holder.edtMemberCode.getText().toString());
                                        context.startActivity(intent);
                                    } else if (status == 203) {
                                        holder.alertDialogJoin.dismiss();
                                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(context, ProjectActivity.class);
                                        ((Activity) context).recreate();
                                        //context.startActivity(intent);
                                    }  else if (status == 204) {
                                        holder.edtMemberCode.setError(message);
                                    } else {
                                        String error = jo.getString("message");
                                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        holder.progressDialog.dismiss();
                                        NetworkResponse networkResponse = error.networkResponse;
                                        if (networkResponse != null) {
                                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                                        }
                                        if (error instanceof TimeoutError) {
                                            Toast.makeText(context, context.getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                                            Log.d("Volley", "TimeoutError");
                                        } else if (error instanceof NoConnectionError) {
                                            Toast.makeText(context, context.getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                                            Log.d("Volley", "NoConnectionError");
                                        } else if (error instanceof AuthFailureError) {
                                            Log.d("Volley", "AuthFailureError");
                                        } else if (error instanceof ServerError) {
                                            Log.d("Volley", "ServerError");
                                        } else if (error instanceof NetworkError) {
                                            Log.d("Volley", "NetworkError");
                                        } else if (error instanceof ParseError) {
                                            Log.d("Volley", "ParseError");
                                        }
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("memberRef", holder.memberRef);
                                params.put("dbMasterRef", String.valueOf(project.getDbMasterRef()));
                                params.put("projectRef", project.getProjectRef());
                                params.put("memberCode", holder.edtMemberCode.getText().toString());

                                return params;
                            }
                        };

                        BaseApplication.getInstance().addToRequestQueue(request, "project");

                    }

                });

            }


        });

        return convertView;
    }

    private class ListProjectHolder {
        CardView cv;
        TextView projectName, locationName, subLocationName;
        ImageView projectImg, btnJoin, btnJoined, btnWaiting;
        Context context;
        SharedPreferences sharedPreferences;
        private boolean state;
        private String memberRef, dbMasterRef, projectRef, memberCode = "";
        EditText edtMemberCode;
        TextView txtTermMemberCode;
        AlertDialog alertDialogJoin;
        ProgressDialog progressDialog;
    }
}
