package com.nataproperti.crm360.view.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.project.model.ListThreesixtyModel;
import com.nataproperti.crm360.view.project.ui.ListThreesixtyActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListThreesixtyAdapter extends RecyclerView.Adapter<ListThreesixtyAdapter.BalanceViewHolder> {

    private Context context;
    private List<ListThreesixtyModel> listThreesixtyModels;

    public ListThreesixtyAdapter(Context context, List<ListThreesixtyModel> listThreesixtyModels) {
        this.context = context;
        this.listThreesixtyModels = listThreesixtyModels;
    }

    @Override
    public BalanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_threesixty, null);

        return new BalanceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BalanceViewHolder holder, int position) {

        final ListThreesixtyModel lm = listThreesixtyModels.get(position);
        holder.name.setText(lm.getClusterName() + " " + lm.getTitleProduct());

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(context, ThreesixtyActivity.class);
//                i.putExtra("linkThreesixty", lm.getLinkThreesixty());
//                context.startActivity(i);
                ((ListThreesixtyActivity)context).IntentTabCrome(lm.getLinkThreesixty());
            }
        });

    }

    @Override
    public int getItemCount() {
        return listThreesixtyModels.size();
    }

    static class BalanceViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.item)
        CardView item;

        public BalanceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
