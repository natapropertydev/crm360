package com.nataproperti.crm360.view.project.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.LoadingBar;
import com.nataproperti.crm360.view.project.model.ProjectModel2;
import com.nataproperti.crm360.view.project.ui.MyListingActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 4/19/2016.
 */
public class MyListingAdapter extends BaseAdapter {

    public static final String PREF_NAME = "pref";

    private Context context;
    private List<ProjectModel2> list;
    private ListProjectHolder holder;
    private MyListingAdapter adapter;

    private Display display;

    String memberRef, dbMasterRef, projectRef;

    public MyListingAdapter(Context context, List<ProjectModel2> list, Display display) {
        this.context = context;
        this.list = list;
        this.display = display;
        this.adapter = this;
    }

    SharedPreferences sharedPreferences;
    private boolean state;

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        memberRef = sharedPreferences.getString("isMemberRef", null);

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_mylisting, null);
            holder = new ListProjectHolder();
            holder.projectName = (TextView) convertView.findViewById(R.id.projectName);
            holder.locationName = (TextView) convertView.findViewById(R.id.locationName);
            holder.subLocationName = (TextView) convertView.findViewById(R.id.subLocationName);

            holder.projectImg = (ImageView) convertView.findViewById(R.id.imgProject);

            holder.btn_remove = (ImageView) convertView.findViewById(R.id.btnRemove);
            holder.btn_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dbMasterRef = String.valueOf(list.get(position).getDbMasterRef());
                    projectRef = list.get(position).getProjectRef();

                    AlertDialog.Builder alertDialogBuilder =
                            new AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage(R.string.UnSubscribe);
                    alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            unSubscribe(dbMasterRef, projectRef);
                            list.remove(position);
                            notifyDataSetChanged();
                            notifyDataSetInvalidated();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }
            });
            convertView.setTag(holder);
        } else {
            holder = (ListProjectHolder) convertView.getTag();
        }

        ProjectModel2 project = list.get(position);
        holder.projectName.setText(project.getProjectName().toUpperCase());
        holder.locationName.setText(project.getLocationName());
        holder.subLocationName.setText(project.getSubLocationName());

        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / 1.233333333333333;
        Log.d("screen width", result.toString() + "--" + Math.round(result));

        ViewGroup.LayoutParams params = holder.projectImg.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        holder.projectImg.setLayoutParams(params);
        holder.projectImg.requestLayout();

        Glide.with(context)
                .load(WebService.getProjectImage() + project.getDbMasterRef() +
                        "&pr=" + project.getProjectRef()).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.projectImg);

        return convertView;
    }

    private class ListProjectHolder {
        TextView projectName, locationName, subLocationName;
        ImageView projectImg, btn_remove;
    }

    public void unSubscribe(final String dbMasterRef,final String projectRef) {
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.unSubscribeProject(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getListAutoComplite", "" + response.toString());
                try {
                    JSONObject jo = new JSONObject(response);
                    Log.d("result detail project", response);
                    int status = jo.getInt("status");
                    String message = jo.getString("message");

                    if (status == 200) {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        //list.notify();
                        Intent intent = new Intent(context, MyListingActivity.class);
                        ((Activity) context).finish();
                        adapter.notifyDataSetChanged();
                        context.startActivity(intent);
                    } else {
                        String error = jo.getString("message");
                        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        BaseApplication.getInstance().stopLoader();
                        LoadingBar.stopLoader();
                        Toast.makeText(context, context.getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null) {
                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                        }
                        if (error instanceof TimeoutError) {
                            Log.d("Volley", "TimeoutError");
                        } else if (error instanceof NoConnectionError) {
                            Log.d("Volley", "NoConnectionError");
                        } else if (error instanceof AuthFailureError) {
                            Log.d("Volley", "AuthFailureError");
                        } else if (error instanceof ServerError) {
                            Log.d("Volley", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.d("Volley", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.d("Volley", "ParseError");
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("memberRef", memberRef);
                params.put("dbMasterRef", dbMasterRef.toString());
                params.put("projectRef", projectRef.toString());

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "project");

    }
}
