package com.nataproperti.crm360.view.project.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.view.project.model.ProductDetailModel;
import com.nataproperti.crm360.view.project.ui.ProductDetailImageActivity;

import java.util.List;

/**
 * Created by User on 5/11/2016.
 */
public class ProductDetailAdapter extends PagerAdapter {
    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String CLUSTER_REF = "clusterRef";
    public static final String PRODUCT_REF = "productRef";
    public static final String POSISION = "position";

    Context context;
    private List<ProductDetailModel> list;
    //int[] imageId = {R.drawable.image1, R.drawable.image2, R.drawable.image3, R.drawable.image4, R.drawable.image5};

    public ProductDetailAdapter(Context context,List<ProductDetailModel> list){
        this.context = context;
        this.list = list;
    }

    public Object instantiateItem(ViewGroup container, final int position) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.item_list_product_image, container, false);
        ImageView imageView = (ImageView) viewItem.findViewById(R.id.imageView);
        //imageView.setImageResource(imageId[position]);
        ProductDetailModel image = list.get(position);
        Glide.with(context)
                .load(WebService.getProductImageDetail()+image.getQuerystring()).into(imageView);

        imageView.setOnClickListener(new OnImageClickListener(position));

        ((ViewPager)container).addView(viewItem);

        return viewItem;
    }

    class OnImageClickListener implements View.OnClickListener {

        int position;

        // constructor
        public OnImageClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            // on selecting grid view image
            // launch full screen activity
            Intent i = new Intent(context, ProductDetailImageActivity.class);
            i.putExtra(PROJECT_REF,list.get(position).getProjectRef());
            i.putExtra(DBMASTER_REF,list.get(position).getDbMasterRef());
            i.putExtra(PRODUCT_REF,list.get(position).getProductRef());
            i.putExtra(POSISION, position);
            context.startActivity(i);
        }

    }

    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub

        return view == ((View)object);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }
}
