package com.nataproperti.crm360.view.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.project.model.ProductFeaturesModel;

import java.util.List;

/**
 * Created by User on 5/11/2016.
 */
public class ProductFeaturesAdapter extends BaseAdapter {
    private Context context;
    private List<ProductFeaturesModel> list;
    private ListProductFeatureHolder holder;

    public ProductFeaturesAdapter(Context context, List<ProductFeaturesModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_product_features,null);
            holder = new ListProductFeatureHolder();
            holder.featureName = (TextView) convertView.findViewById(R.id.txt_feature);

            convertView.setTag(holder);
        }else{
            holder = (ListProductFeatureHolder) convertView.getTag();
        }


        ProductFeaturesModel project = list.get(position);
        holder.featureName.setText(project.getFeatureName());

        return convertView;
    }

    private class ListProductFeatureHolder {
        TextView featureName;
    }
}
