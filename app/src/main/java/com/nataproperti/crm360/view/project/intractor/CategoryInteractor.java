package com.nataproperti.crm360.view.project.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface CategoryInteractor {
    void getListCategorySvc(String dbMasterRef, String projectRef);

}
