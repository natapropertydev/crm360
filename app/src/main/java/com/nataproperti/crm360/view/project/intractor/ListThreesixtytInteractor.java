package com.nataproperti.crm360.view.project.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface ListThreesixtytInteractor {

    void getListProductThreesixtySvc(String dbMasterRef, String projectRef);

}
