package com.nataproperti.crm360.view.project.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface ProjectInteractor {
    void getLocation(String memberRef);
    void getListProject(String memberRef, String locationRef);
    void rxUnSubscribe();

}
