package com.nataproperti.crm360.view.project.model;

/**
 * Created by nata on 11/25/2016.
 */

public class CommisionStatusModel {
    int status;
    String message;
    CommisionDetailModel data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CommisionDetailModel getData() {
        return data;
    }

    public void setData(CommisionDetailModel data) {
        this.data = data;
    }
}
