package com.nataproperti.crm360.view.project.model;

/**
 * Created by "dwist14"
 * on Jun 6/2/2017 10:33.
 * Project : urbanace
 */
public class ListThreesixtyModel {
    String dbMasterRef, projectRef, clusterRef, productRef, titleProduct, clusterName, linkThreesixty;

    public String getDbMasterRef() {
        return dbMasterRef;
    }

    public void setDbMasterRef(String dbMasterRef) {
        this.dbMasterRef = dbMasterRef;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getClusterRef() {
        return clusterRef;
    }

    public void setClusterRef(String clusterRef) {
        this.clusterRef = clusterRef;
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public String getTitleProduct() {
        return titleProduct;
    }

    public void setTitleProduct(String titleProduct) {
        this.titleProduct = titleProduct;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getLinkThreesixty() {
        return linkThreesixty;
    }

    public void setLinkThreesixty(String linkThreesixty) {
        this.linkThreesixty = linkThreesixty;
    }
}
