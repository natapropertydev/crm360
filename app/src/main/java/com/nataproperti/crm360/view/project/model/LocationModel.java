package com.nataproperti.crm360.view.project.model;

/**
 * Created by User on 10/3/2016.
 */
public class LocationModel {
    long  locationRef;
    String locationName;

    public long getLocationRef() {
        return locationRef;
    }

    public void setLocationRef(long locationRef) {
        this.locationRef = locationRef;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
