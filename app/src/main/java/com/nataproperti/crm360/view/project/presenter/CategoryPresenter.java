package com.nataproperti.crm360.view.project.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.project.intractor.CategoryInteractor;
import com.nataproperti.crm360.view.project.model.CategoryModel;
import com.nataproperti.crm360.view.project.ui.CategoryActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class CategoryPresenter implements CategoryInteractor {
    private CategoryActivity view;
    private ServiceRetrofit service;

    public CategoryPresenter(CategoryActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListCategorySvc(String dbMasterRef, String projectRef) {
        Call<List<CategoryModel>> call = service.getAPI().getListCategorySvc(dbMasterRef,projectRef);
        call.enqueue(new Callback<List<CategoryModel>>() {
            @Override
            public void onResponse(Call<List<CategoryModel>> call, Response<List<CategoryModel>> response) {
               // view.showResults(response);
            }

            @Override
            public void onFailure(Call<List<CategoryModel>> call, Throwable t) {
                //view.showFailure(t);

            }


        });
    }


}
