package com.nataproperti.crm360.view.project.presenter;

import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.project.intractor.ListThreesixtytInteractor;
import com.nataproperti.crm360.view.project.model.ListThreesixtyModel;
import com.nataproperti.crm360.view.project.ui.ListThreesixtyActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ListThreesixtyPresenter implements ListThreesixtytInteractor {
    private ListThreesixtyActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ListThreesixtyPresenter(ListThreesixtyActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }


    @Override
    public void getListProductThreesixtySvc(String dbMasterRef, String projectRef) {
        Call<List<ListThreesixtyModel>> call = service.getAPI().getListProductThreesixtySvc(dbMasterRef,projectRef);
        call.enqueue(new Callback<List<ListThreesixtyModel>>() {
            @Override
            public void onResponse(Call<List<ListThreesixtyModel>> call, Response<List<ListThreesixtyModel>> response) {
                view.showLocationResults(response);
            }

            @Override
            public void onFailure(Call<List<ListThreesixtyModel>> call, Throwable t) {
                view.showLocationFailure(t);

            }


        });
    }


}
