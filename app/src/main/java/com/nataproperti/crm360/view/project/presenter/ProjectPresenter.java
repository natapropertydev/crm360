package com.nataproperti.crm360.view.project.presenter;

import com.nataproperti.crm360.view.project.intractor.ProjectInteractor;
import com.nataproperti.crm360.view.project.model.LocationModel;
import com.nataproperti.crm360.view.project.model.ProjectModelNew;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.view.project.ui.ProjectActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;

/**
 * Created by nata on 11/23/2016.
 */
@SuppressWarnings("unchecked")
public class ProjectPresenter implements ProjectInteractor {
    private ProjectActivity view;
    private ServiceRetrofit service;
    private Subscription subscription;

    public ProjectPresenter(ProjectActivity view, ServiceRetrofit service) {
        this.view = view;
        this.service = service;
    }


    @Override
    public void getLocation(String memberRef) {
        Call<List<LocationModel>> call = service.getAPI().getLocation(memberRef);
        call.enqueue(new Callback<List<LocationModel>>() {
            @Override
            public void onResponse(Call<List<LocationModel>> call, Response<List<LocationModel>> response) {
                view.showLocationResults(response);
            }

            @Override
            public void onFailure(Call<List<LocationModel>> call, Throwable t) {
                view.showLocationFailure(t);

            }


        });
    }

    @Override
    public void getListProject(String memberRef, String locationRef) {
        Call<List<ProjectModelNew>> call = service.getAPI().getListProject(memberRef,locationRef);
        call.enqueue(new Callback<List<ProjectModelNew>>() {
            @Override
            public void onResponse(Call<List<ProjectModelNew>> call, Response<List<ProjectModelNew>> response) {
                view.showListProjectResults(response);
            }

            @Override
            public void onFailure(Call<List<ProjectModelNew>> call, Throwable t) {
                view.showListProjectFailure(t);

            }


        });
    }


    @Override
    public void rxUnSubscribe() {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

    }


}
