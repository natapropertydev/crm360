package com.nataproperti.crm360.view.project.ui;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.General;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.CustomTabActivityHelper;
import com.nataproperti.crm360.helper.WebviewFallback;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.project.adapter.ListThreesixtyAdapter;
import com.nataproperti.crm360.view.project.model.ListThreesixtyModel;
import com.nataproperti.crm360.view.project.presenter.ListThreesixtyPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;


public class ListThreesixtyActivity extends AppCompatActivity {
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_projek)
    ImageView imageView;
    @BindView(R.id.rPage)
    RelativeLayout rPage;

    private List<ListThreesixtyModel> listThreesixtyModels = new ArrayList<>();
    private ListThreesixtyAdapter listThreesixtyAdapter;

    private ServiceRetrofit service;
    private ListThreesixtyPresenter presenter;

    private String dbMasterRef, projectRef;
    private Double result;
    private Integer width;
    private Display display;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        renderView();

        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ListThreesixtyPresenter(this, service);

        Intent i = getIntent();
        dbMasterRef = i.getStringExtra(General.DBMASTER_REF);
        projectRef = i.getStringExtra(General.PROJECT_REF);

        Log.d("TAG", "ListThreesixtyActivity " + dbMasterRef + " " + projectRef);

        listThreesixtyAdapter = new ListThreesixtyAdapter(this, listThreesixtyModels);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(listThreesixtyAdapter);

        presenter.getListProductThreesixtySvc(dbMasterRef, projectRef);

        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;

        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        Glide.with(this).load(WebService.getProjectImage() + String.valueOf(dbMasterRef) +
                "&pr=" + projectRef).into(imageView);

    }

    private void renderView() {
        setContentView(R.layout.activity_list_threesixty);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.listThreesixty));
    }


    public void showLocationResults(Response<List<ListThreesixtyModel>> response) {
        listThreesixtyModels = response.body();
        Log.d("TAG", "ListThreesixtyActivity " + listThreesixtyModels.size());

        if (listThreesixtyModels.size() > 0) {
            listThreesixtyAdapter = new ListThreesixtyAdapter(this, listThreesixtyModels);
            list.setAdapter(listThreesixtyAdapter);
        } else {
            list.setVisibility(View.GONE);
        }

    }

    public void showLocationFailure(Throwable t) {

    }

    public void IntentTabCrome(String link) {
        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                .setToolbarColor(getResources().getColor(R.color.colorPrimary))
                .build();
        CustomTabActivityHelper.openCustomTab(this, customTabsIntent, Uri.parse(link), new WebviewFallback());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
