package com.nataproperti.crm360.view.project.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.LoadingBar;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.before_login.ui.LaunchActivity;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;
import com.nataproperti.crm360.view.project.adapter.MyListingAdapter;
import com.nataproperti.crm360.view.project.adapter.RVMyListAdapter;
import com.nataproperti.crm360.view.project.model.MyLIstingModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User on 5/2/2016.
 */
public class MyListingActivity extends AppCompatActivity {
    public static final String TAG = "MyListingActivity";

    public static final String PREF_NAME = "pref";

    public static final String PROJECT_REF = "projectRef";
    public static final String DBMASTER_REF = "dbMasterRef";
    public static final String PROJECT_NAME = "projectName";
    public static final String LOCATION_NAME = "locationName";
    public static final String LOCATION_REF = "locationRef";
    public static final String SUBLOCATION_NAME = "sublocationName";
    public static final String SUBLOCATION_REF = "sublocationRef";

    public static final String MEMBER_REF = "memberRef";

    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<MyLIstingModel> listProject = new ArrayList<>();
    private MyListingAdapter adapter;

    SharedPreferences sharedPreferences;
    RecyclerView rv_myproject;
    private boolean state;
    private String memberRef;
    private RelativeLayout snackBarBuatan;
    private TextView retry;
    Display display;

    Toolbar toolbar;
    TextView title;
    Typeface font;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mylisting);
        initWidget();
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        String name = sharedPreferences.getString("isName", null);
        requestProject();
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyListingActivity.this, LaunchActivity.class));
                finish();
            }
        });
    }

    public void requestProject() {
//        final DaoSession daoSession = ((BaseApplication) getApplicationContext()).getDaoSession();
        LoadingBar.startLoader(this);
        final StringRequest request = new StringRequest(Request.Method.POST,
                WebService.getMyListing(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                snackBarBuatan.setVisibility(View.GONE);
                LoadingBar.stopLoader();
                Log.d("getListAutoComplite", "" + response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()>0){
                        generateListProject(jsonArray);
//                        for ( int i = 0; i < jsonArray.length();i++) {
//                            JSONObject data = jsonArray.getJSONObject(i);
//                            Gson gson = new GsonBuilder().serializeNulls().create();
//                            MyLIstingModel projectModel = gson.fromJson("" + data, MyLIstingModel.class);
//                            projectModel.setDbMasterRef(data.optLong("dbMasterRef"));
//                            projectModel.setProjectRef(data.optString("projectRef"));
//                            projectModel.setLocationRef(data.optString("locationRef"));
//                            projectModel.setLocationName(data.optString("locationName"));
//                            projectModel.setSublocationRef(data.optString("sublocationRef"));
//                            projectModel.setSubLocationName(data.optString("subLocationName"));
//                            projectModel.setProjectName(data.optString("projectName"));
//                            projectModel.setIsJoin(data.optString("isJoin"));
//                            projectModel.setIsWaiting(data.optString("isWaiting"));
//                            MyLIstingModelDao projectModelDao = daoSession.getMyLIstingModelDao();
//                            projectModelDao.insertOrReplace(projectModel);
//                        }
                    } else {
                        AlertDialog.Builder alertDialogBuilder =
                                new AlertDialog.Builder(MyListingActivity.this);
                        alertDialogBuilder.setMessage("Anda belum join project, pilih project sekarang.");
                        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(MyListingActivity.this, ProjectActivity.class));
                                finish();
                            }
                        });
                        alertDialogBuilder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();

                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        snackBarBuatan.setVisibility(View.VISIBLE);
                        LoadingBar.stopLoader();
//                        MyLIstingModelDao projectModelDao = daoSession.getMyLIstingModelDao();
//                        List<MyLIstingModel> projectModels = projectModelDao.queryBuilder()
//                                .list();
//                        Log.d("getProjectLocalDB", "" + projectModels.toString());
//                        int numData = projectModels.size();
//                        if (numData > 0) {
//                            for (int i = 0; i<numData; i++) {
//                                MyLIstingModel projectModel = projectModels.get(i);
//                                listProject.add(projectModel);
//                                initializeAdapter();
//
//                            }
//                        }
//                        else {
//                            finish();
//                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(MEMBER_REF, memberRef);

                return params;
            }
        };

        BaseApplication.getInstance().addToRequestQueue(request, "inpute");

    }

    private void generateListProject(JSONArray response) {
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject jo = response.getJSONObject(i);
                MyLIstingModel project = new MyLIstingModel();
                project.setDbMasterRef(jo.getLong("dbMasterRef"));
                project.setProjectRef(jo.getString("projectRef"));
                project.setLocationRef(jo.getString("locationRef"));
                project.setLocationName(jo.getString("locationName"));
                project.setSublocationRef(jo.getString("sublocationRef"));
                project.setSubLocationName(jo.getString("subLocationName"));
                project.setProjectName(jo.getString("projectName"));
                project.setIsWaiting(jo.getString("isWaiting"));
                project.setIsJoin(jo.getString("isJoin"));

                listProject.add(project);
                initializeAdapter();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void initWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("My Project");
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        display = getWindowManager().getDefaultDisplay();
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
        rv_myproject = (RecyclerView) findViewById(R.id.rv_myproject);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv_myproject.setLayoutManager(llm);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(MyListingActivity.this, MainMenuActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void initializeAdapter() {
        RVMyListAdapter adapter = new RVMyListAdapter(this, listProject, display);
        rv_myproject.setAdapter(adapter);
    }

}
