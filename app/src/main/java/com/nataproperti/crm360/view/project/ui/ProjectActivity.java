package com.nataproperti.crm360.view.project.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.General;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.view.before_login.ui.LaunchActivity;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;
import com.nataproperti.crm360.view.project.adapter.GVProjectAdapter;
import com.nataproperti.crm360.view.project.adapter.LocationAdapter;
import com.nataproperti.crm360.view.project.adapter.RVProjectAdapter;
import com.nataproperti.crm360.view.project.model.LocationModel;
import com.nataproperti.crm360.view.project.model.ProjectModelNew;
import com.nataproperti.crm360.view.project.presenter.ProjectPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 4/21/2016.
 */
public class ProjectActivity extends AppCompatActivity implements View.OnClickListener {
    public static boolean OFF_LINE_MODE = false;
    private static final String TAG = ProjectActivity.class.getSimpleName();
    private List<ProjectModelNew> listProject = new ArrayList<>();
    private RVProjectAdapter adapter;
    private GVProjectAdapter gv_adapter;
    private ServiceRetrofit service;
    private ProjectPresenter presenter;
    ProgressDialog progressDialog;
    Display display;
    GridView rv_myproject;
    SharedPreferences sharedPreferences;
    private boolean state;
    String memberRef;
    private List<LocationModel> listLocation = new ArrayList<>();
    private LocationAdapter locationAdapter;
    private LocationModel locationModel;
    private String locationRef;
    private RelativeLayout snackBarBuatan;
    private TextView retry;
    private CardView cardViewSpinner;
    Toolbar toolbar;
    MyTextViewLatoReguler title;
    Typeface font;
    Spinner spinner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        initWidget();
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ProjectPresenter(this, service);
        sharedPreferences = getSharedPreferences(General.PREF_NAME, 0);
        state = sharedPreferences.getBoolean("isLogin", false);
//        memberRef = sharedPreferences.getString("isMemberRef", null);
        memberRef = "159";
        requestLocation();
        retry.setOnClickListener(this);
        display = getWindowManager().getDefaultDisplay();
    }

    private void initWidget() {
        snackBarBuatan = (RelativeLayout) findViewById(R.id.main_snack_bar_buatan);
        retry = (TextView) findViewById(R.id.main_retry);
        cardViewSpinner = (CardView) findViewById(R.id.cv_spiner);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText("  " + getString(R.string.app_name));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.drawable.ic_logo_crm);
        rv_myproject = (GridView) findViewById(R.id.rv_myproject);
//        LinearLayoutManager llm = new LinearLayoutManager(this);
//        rv_myproject.setLayoutManager(llm);
    }

    public void requestLocation() {
        presenter.getLocation(memberRef);
    }

    public void showLocationResults(retrofit2.Response<List<LocationModel>> response) {
        MainMenuActivity.OFF_LINE_MODE = false;
        snackBarBuatan.setVisibility(View.GONE);
        cardViewSpinner.setVisibility(View.VISIBLE);
        listLocation = response.body();
        spinnerLoc();
    }

    public void showLocationFailure(Throwable t) {
        MainMenuActivity.OFF_LINE_MODE = true;
        snackBarBuatan.setVisibility(View.VISIBLE);
    }

    private void spinnerLoc() {
        spinner = (Spinner) findViewById(R.id.spinner_nav);
        locationAdapter = new LocationAdapter(this, listLocation);
        spinner.setAdapter(locationAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                locationRef = String.valueOf(listLocation.get(position).getLocationRef());
                listProject.clear();
                requestProject();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void requestProject() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getListProject(memberRef, locationRef);
    }

    public void showListProjectResults(retrofit2.Response<List<ProjectModelNew>> response) {
        progressDialog.dismiss();
        listProject = response.body();
        initializeAdapter();
    }

    public void showListProjectFailure(Throwable t) {
        progressDialog.dismiss();
    }

    private void initializeAdapter() {
//        adapter = new RVProjectAdapter(this, listProject, display);
        gv_adapter = new GVProjectAdapter(this, listProject, display);
        rv_myproject.setAdapter(gv_adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_icon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_search:
                Intent intent = new Intent(ProjectActivity.this, ProjectSearchActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_retry:
                startActivity(new Intent(this, LaunchActivity.class));
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                ProjectActivity.this.recreate();
            }
        }
    }

}
