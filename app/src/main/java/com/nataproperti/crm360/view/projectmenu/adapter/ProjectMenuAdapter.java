package com.nataproperti.crm360.view.projectmenu.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.General;
import com.nataproperti.crm360.config.SessionManager;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.gallery.ui.CategoryGalleryActivity;
import com.nataproperti.crm360.view.ilustration.ui.IlustrationCategoryActivity;
import com.nataproperti.crm360.view.ilustration.ui.IlustrationClusterActivity;
import com.nataproperti.crm360.view.ilustration.ui.IlustrationProductActivity;
import com.nataproperti.crm360.view.nup.ui.NupTermActivity;
import com.nataproperti.crm360.view.project.ui.CategoryActivity;
import com.nataproperti.crm360.view.project.ui.ClusterActivity;
import com.nataproperti.crm360.view.project.ui.CommisionProjectActivity;
import com.nataproperti.crm360.view.project.ui.DownloadActivity;
import com.nataproperti.crm360.view.project.ui.ListThreesixtyActivity;
import com.nataproperti.crm360.view.project.ui.ProductActivity;
import com.nataproperti.crm360.view.project.ui.ProjectDetailActivity;
import com.nataproperti.crm360.view.projectmenu.model.ProjectMenuModel;
import com.nataproperti.crm360.view.projectmenu.model.ProjectVareable;
import com.nataproperti.crm360.view.projectmenu.ui.ProjectMenuActivity;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Nata
 * on Mar 3/16/17 14:59.
 * Project :
 */

public class ProjectMenuAdapter extends RecyclerView.Adapter<ProjectMenuAdapter.MainMenuHolder> {
    public static final String TAG = "ProjectMenuAdapter";
    private List<ProjectMenuModel> listMenu;
    public Context context;
    private long dbMasterRef;
    private String projectRef, projectName, linkDetail, urlVideo, downloadProjectInfo, categoryRef, clusterRef, isShowAvailableUnit,
            isNUP, isBooking, nupAmt, imageLogo, countCategory, countCluster, userRef, isWaiting, isJoin;
    private String latitude, longitude;
    private SharedPreferences sharedPreferences;
    private SessionManager sessionManager;


    public ProjectMenuAdapter(Context context, List<ProjectMenuModel> listMenu, long dbMadterRef, String projectRef, String projectName) {
        this.context = context;
        this.listMenu = listMenu;
        this.dbMasterRef = dbMadterRef;
        this.projectRef = projectRef;
        this.projectName = projectName;

    }

    @Override
    public MainMenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_project_menu, null);

        return new MainMenuHolder(view);
    }

    @Override
    public void onBindViewHolder(MainMenuHolder holder, int position) {

        holder.onBindHolder(listMenu.get(position), position);
    }

    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class MainMenuHolder extends RecyclerView.ViewHolder {

        TextView menu;
        CardView menuLayout;
        TextView name;
        ImageView icon;

        public MainMenuHolder(View itemView) {
            super(itemView);

            menu = (TextView) itemView.findViewById(R.id.btn_main_menu);
            menuLayout = (CardView) itemView.findViewById(R.id.main_menu_layout_item);
            name = (TextView)  itemView.findViewById(R.id.name);
            icon = (ImageView)  itemView.findViewById(R.id.icon);

        }

        public void onBindHolder(final ProjectMenuModel projectMenuModel, final int position) {
            sharedPreferences = context.getSharedPreferences(General.PREF_NAME, 0);
            isWaiting = sharedPreferences.getString("isWaiting", "");
            isJoin = sharedPreferences.getString("isJoin", "");

            sessionManager = new SessionManager(context);
            HashMap<String, String> projectSession = sessionManager.getProjectSession();
            linkDetail = projectSession.get(General.LINK_DETAIL);
            latitude = projectSession.get(General.LATITUDE);
            longitude = projectSession.get(General.LONGITUDE);
            urlVideo = projectSession.get(General.URL_VIDEO);
            downloadProjectInfo = projectSession.get(General.DOWNLOAD_PROJECT_INFO);

            clusterRef = projectSession.get(General.CLUSTER_REF);
            categoryRef = projectSession.get(General.CATEGORY_REF);
            isShowAvailableUnit = projectSession.get(General.IS_SHOW_AVAILABLE_UNIT);
            isBooking = projectSession.get(General.IS_BOOKING);
            isNUP = projectSession.get(General.IS_NUP);
            nupAmt = projectSession.get(General.NUP_AMT);
            imageLogo = projectSession.get(General.IMAGE_LOGO);
            countCategory = projectSession.get(General.COUNT_CATEGORY);
            countCluster = projectSession.get(General.COUNT_CLUSTER);

            userRef = ProjectVareable.getInstance().getUserRef();


            BaseApplication.showLog(TAG,"clusterRef: "+clusterRef);

            icon.setImageResource(listMenu.get(position).getMenuIcon());
            menu.setText(context.getString(projectMenuModel.getMenuName()));
            menuLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (projectMenuModel.getMenuTitle()) {
                        case General.PROJECT_INFORMASI:
                            Intent intent = new Intent(context, ProjectDetailActivity.class);
                            intent.putExtra("projectRef", projectRef);
                            intent.putExtra("dbMasterRef", dbMasterRef);
                            intent.putExtra("projectName", projectName);
                            intent.putExtra("linkDetail", linkDetail);
                            intent.putExtra("latitude", latitude);
                            intent.putExtra("longitude", longitude);
                            intent.putExtra("urlVideo", urlVideo);
                            intent.putExtra("downloadProjectInfo", downloadProjectInfo);
                            context.startActivity(intent);
                            break;

                        case General.PROJECT_SEARCH_UNIT:
                            if (countCategory != null) {
                                if (countCategory.equals("1")) {
                                    if (countCluster.equals("1")) {
                                        intentToProduct();
                                    } else {
                                        intentToCluster();
                                    }
                                } else {
                                    intentToCategory();
                                }
                            }
                            break;

                        case General.PROJECT_DOWNLOAD:
                            Intent intentDownload = new Intent(context, DownloadActivity.class);
                            intentDownload.putExtra("projectRef", projectRef);
                            intentDownload.putExtra("dbMasterRef", dbMasterRef);
                            intentDownload.putExtra("projectName", projectName);
                            intentDownload.putExtra("imageLogo", imageLogo);
                            context.startActivity(intentDownload);
                            break;

                        case General.PROJECT_NUP:
                            BaseApplication.showLog(TAG,"isNup: "+isNUP+" isJoin: "+isJoin+" isWaiting: "+isWaiting);
                            if (!isNUP.equals("0")) {
                                if (isJoin.equals("0")) {
                                    if (isWaiting.equals("1")) {
                                        //sudah join tetapi belum di approv
                                        ((ProjectMenuActivity) context).dialogNotJoin(context.getString(R.string.waitingJoinNup)
                                                .replace("@projectName", projectName));

                                    } else {
                                        //belum join
                                        ((ProjectMenuActivity) context).dialogNotJoin(context.getString(R.string.notJoinNup)
                                                .replace("@projectName", projectName));
                                    }

                                } else {
                                    //jika dia sudah join
                                    Intent intentNup = new Intent(context, NupTermActivity.class);
                                    intentNup.putExtra(General.PROJECT_REF, projectRef);
                                    intentNup.putExtra(General.DBMASTER_REF, String.valueOf(dbMasterRef));
                                    intentNup.putExtra(General.PROJECT_NAME, projectName);
                                    intentNup.putExtra(General.NUP_AMT, nupAmt);
                                    intentNup.putExtra(General.IMAGE_LOGO, imageLogo);
                                    context.startActivity(intentNup);
                                }
                            }
                            break;

                        case General.PROJECT_BOOKING:
                            bookingAvailable();
                            break;

                        case General.PROJECT_KOMISI:
                            Intent intentCommision = new Intent(context, CommisionProjectActivity.class);
                            intentCommision.putExtra(General.PROJECT_REF, projectRef);
                            intentCommision.putExtra(General.DBMASTER_REF, dbMasterRef);
                            intentCommision.putExtra(General.PROJECT_NAME, projectName);
                            intentCommision.putExtra(General.IMAGE_LOGO, imageLogo);
                            context.startActivity(intentCommision);
                            break;

                        case General.PROJECT_GALLERY:
                            Intent intentGallery = new Intent(context, CategoryGalleryActivity.class);
                            intentGallery.putExtra(General.PROJECT_REF, projectRef);
                            intentGallery.putExtra(General.DBMASTER_REF, String.valueOf(dbMasterRef));
                            intentGallery.putExtra(General.PROJECT_NAME, projectName);
                            intentGallery.putExtra(General.IMAGE_LOGO, imageLogo);
                            context.startActivity(intentGallery);
                            break;

                        case General.PROJECT_THREESIXTY:
                            Intent intentNews = new Intent(context, ListThreesixtyActivity.class);
                            intentNews.putExtra(General.PROJECT_REF, projectRef);
                            intentNews.putExtra(General.DBMASTER_REF, String.valueOf(dbMasterRef));
                            context.startActivity(intentNews);
                            break;
                    }
                }
            });
        }
    }

    private void intentToProduct() {
        Intent intentProduct = new Intent(context, ProductActivity.class);
        intentProduct.putExtra("projectRef", projectRef);
        intentProduct.putExtra("dbMasterRef", dbMasterRef);
        intentProduct.putExtra("categoryRef", categoryRef);
        intentProduct.putExtra("clusterRef", clusterRef);
        intentProduct.putExtra("projectName", projectName);
        intentProduct.putExtra("latitude", latitude);
        intentProduct.putExtra("longitude", longitude);
        intentProduct.putExtra("isShowAvailableUnit", isShowAvailableUnit);
        intentProduct.putExtra("isNUP", isNUP);
        intentProduct.putExtra("isBooking", isBooking);
        intentProduct.putExtra("nupAmt", nupAmt);
        intentProduct.putExtra("imageLogo", imageLogo);
        context.startActivity(intentProduct);
    }

    private void intentToCluster(){
        Intent intentCluster = new Intent(context, ClusterActivity.class);
        intentCluster.putExtra(General.PROJECT_REF, projectRef);
        intentCluster.putExtra(General.DBMASTER_REF, dbMasterRef);
        intentCluster.putExtra(General.CATEGORY_REF, categoryRef);
        intentCluster.putExtra(General.CLUSTER_REF, clusterRef);

        intentCluster.putExtra(General.PROJECT_NAME, projectName);
        intentCluster.putExtra(General.IS_NUP, isNUP);
        intentCluster.putExtra(General.IS_BOOKING, isBooking);
        intentCluster.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
        intentCluster.putExtra(General.NUP_AMT, nupAmt);
        intentCluster.putExtra(General.IMAGE_LOGO, imageLogo);
        context.startActivity(intentCluster);
    }

    private void intentToCategory (){
        Intent intentCategory = new Intent(context, CategoryActivity.class);
        intentCategory.putExtra(General.PROJECT_REF, projectRef);
        intentCategory.putExtra(General.DBMASTER_REF, dbMasterRef);
        intentCategory.putExtra(General.PROJECT_NAME, projectName);
        intentCategory.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
        intentCategory.putExtra(General.IS_NUP, isNUP);
        intentCategory.putExtra(General.IS_BOOKING, isBooking);
        intentCategory.putExtra(General.NUP_AMT, nupAmt);
        intentCategory.putExtra(General.IMAGE_LOGO, imageLogo);
        context.startActivity(intentCategory);

    }

    private void bookingAvailable() {
        if (countCategory != null) {
            if (countCategory.equals("1")) {
                if (countCluster.equals("1")) {
                    Intent intentProduct = new Intent(context, IlustrationProductActivity.class);
                    intentProduct.putExtra(General.PROJECT_REF, projectRef);
                    intentProduct.putExtra(General.DBMASTER_REF, dbMasterRef);
                    intentProduct.putExtra(General.CATEGORY_REF, categoryRef);
                    intentProduct.putExtra(General.CLUSTER_REF, clusterRef);

                    intentProduct.putExtra(General.PROJECT_NAME, projectName);
                    intentProduct.putExtra(General.IS_NUP, isNUP);
                    intentProduct.putExtra(General.IS_BOOKING, isBooking);
                    intentProduct.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    intentProduct.putExtra(General.NUP_AMT, nupAmt);

                    intentProduct.putExtra(General.PRODUCT_REF, "");
                    intentProduct.putExtra(General.IMAGE_LOGO, imageLogo);
                    context.startActivity(intentProduct);

                } else {
                    Intent intentCluster = new Intent(context, IlustrationClusterActivity.class);
                    intentCluster.putExtra(General.PROJECT_REF, projectRef);
                    intentCluster.putExtra(General.DBMASTER_REF, dbMasterRef);
                    intentCluster.putExtra(General.CATEGORY_REF, categoryRef);
                    intentCluster.putExtra(General.CLUSTER_REF, clusterRef);

                    intentCluster.putExtra(General.PROJECT_NAME, projectName);
                    intentCluster.putExtra(General.IS_NUP, isNUP);
                    intentCluster.putExtra(General.IS_BOOKING, isBooking);
                    intentCluster.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                    intentCluster.putExtra(General.NUP_AMT, nupAmt);
                    intentCluster.putExtra(General.IMAGE_LOGO, imageLogo);
                    context.startActivity(intentCluster);
                }
            } else {
                Intent intentCategory = new Intent(context, IlustrationCategoryActivity.class);
                intentCategory.putExtra(General.PROJECT_REF, projectRef);
                intentCategory.putExtra(General.DBMASTER_REF, dbMasterRef);

                intentCategory.putExtra(General.PROJECT_NAME, projectName);
                intentCategory.putExtra(General.IS_NUP, isNUP);
                intentCategory.putExtra(General.IS_BOOKING, isBooking);
                intentCategory.putExtra(General.IS_SHOW_AVAILABLE_UNIT, isShowAvailableUnit);
                intentCategory.putExtra(General.NUP_AMT, nupAmt);
                intentCategory.putExtra(General.IMAGE_LOGO, imageLogo);
                context.startActivity(intentCategory);
            }
        } else {

        }
    }

}
