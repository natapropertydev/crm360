package com.nataproperti.crm360.view.projectmenu.intractor;

/**
 * Created by nata on 11/23/2016.
 */

public interface ProjectMenuInteractor {
    void getProjectDtl(String dbMasterRef, String projectRef);
    void getCommision(String dbMasterRef, String projectRef, String memberRef);
}
