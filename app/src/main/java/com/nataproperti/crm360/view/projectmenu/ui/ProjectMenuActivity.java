package com.nataproperti.crm360.view.projectmenu.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.nataproperti.crm360.config.BaseApplication;
import com.nataproperti.crm360.config.General;
import com.nataproperti.crm360.config.ServiceRetrofit;
import com.nataproperti.crm360.config.SessionManager;
import com.nataproperti.crm360.config.WebService;
import com.nataproperti.crm360.helper.MyTextViewLatoReguler;
import com.nataproperti.crm360.nataproperty.R;
import com.nataproperti.crm360.view.before_login.ui.LaunchActivity;
import com.nataproperti.crm360.view.mainmenu.ui.MainMenuActivity;
import com.nataproperti.crm360.view.project.model.CommisionStatusModel;
import com.nataproperti.crm360.view.project.model.DetailStatusModel;
import com.nataproperti.crm360.view.project.ui.ProjectActivity;
import com.nataproperti.crm360.view.project.ui.ValidateInhouseCode;
import com.nataproperti.crm360.view.projectmenu.adapter.ProjectMenuAdapter;
import com.nataproperti.crm360.view.projectmenu.model.ProjectMenuModel;
import com.nataproperti.crm360.view.projectmenu.model.ProjectVareable;
import com.nataproperti.crm360.view.projectmenu.presenter.ProjectMenuPresenter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by UserModel on 5/3/2016.
 */
public class ProjectMenuActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = "ProjectMenuActivity";
    private ImageView imageView;
    private Context context;
    private long dbMasterRef;
    private String projectRef, projectName, locationName, locationRef, sublocationName, sublocationRef;
    private String countCategory, countCluster, salesStatus, isBooking, isNUP, isShowAvailableUnit;
    private String categoryRef, clusterRef, memberCode, projectDescription;
    private String nupAmt, linkDetail, bookingContact = "", downloadProjectInfo;
    private double latitude, longitude;
    private ServiceRetrofit service;
    private ProjectMenuPresenter presenter;
    private ProgressDialog progressDialog;
    static ProjectMenuActivity projectMenuActivity;
    private String imageLogo, urlVideo = "", memberRef, memberTypeCode, userRef;
    private ImageView btnJoin, btnJoined, btnWaiting;
    private SharedPreferences sharedPreferences;
    private String commision, closingFee, description, isWaiting, isJoin;
    private double commissionAmt;
    private Toolbar toolbar;
    private MyTextViewLatoReguler title;
    private Typeface font;
    private RelativeLayout rPage;
    private Display display;
    private Integer width;
    private Double result;
    private Double widthButton, heightButton;
    private EditText edtMemberCode;
    private TextView txtTermMemberCode;
    private AlertDialog alertDialogJoin, b;
    private CoordinatorLayout coordinatorLayout;
    private RecyclerView recyclerViewMenu;
    private ProjectMenuAdapter projectMenuAdapter;
    // list icon menu
    int menuIcon[] = new int[]{
            R.drawable.ic_informasi_project,
            R.drawable.ic_cari_unit,
            R.drawable.ic_download,
            R.drawable.ic_nup,
            R.drawable.ic_cek_harga_dan_booking,
            R.drawable.ic_commision,
            R.drawable.ic_gallary,
            R.drawable.ic_video_360,
    };

    // list title menu
    String menuTitle[] = new String[]{
            General.PROJECT_INFORMASI,
            General.PROJECT_SEARCH_UNIT,
            General.PROJECT_DOWNLOAD,
            General.PROJECT_NUP,
            General.PROJECT_BOOKING,
            General.PROJECT_KOMISI,
            General.PROJECT_GALLERY,
            General.PROJECT_THREESIXTY,
    };

    int menuName[] = new int[]{
            R.string.projectInformasi, //0
            R.string.projectSearchUnit, //1
            R.string.projectDownload, //2
            R.string.projectNup, //3
            R.string.projectBooking, //4
            R.string.projectKomisi, //5
            R.string.projectGallery, //6
            R.string.project360, //6
    };

    private List<ProjectMenuModel> menulist = new ArrayList<>();
    private RecyclerViewHeader header;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projek_menu);
        service = ((BaseApplication) getApplication()).getNetworkService();
        presenter = new ProjectMenuPresenter(this, service);
        projectRef = getIntent().getStringExtra(General.PROJECT_REF);
        dbMasterRef = getIntent().getLongExtra(General.DBMASTER_REF, 0);
        projectName = getIntent().getStringExtra(General.PROJECT_NAME);
        locationName = getIntent().getStringExtra(General.LOCATION_NAME);
        locationRef = getIntent().getStringExtra(General.LOCATION_REF);
        sublocationName = getIntent().getStringExtra(General.SUBLOCATION_NAME);
        sublocationRef = getIntent().getStringExtra(General.SUBLOCATION_REF);

        sessionManager = new SessionManager(getApplicationContext());
        initWidget();

        projectMenuActivity = this;
        context = this;
        initAdapter();
        joinSetup();
        requestProjectDetail();
        requestCommision();
        //logBookSetup();

        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                joinProject();
            }
        });
    }

    private void initWidget() {
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (MyTextViewLatoReguler) toolbar.findViewById(R.id.title);
        title.setText(String.valueOf(projectName));
        font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imageView = (ImageView) findViewById(R.id.image_projek);

        rPage = (RelativeLayout) findViewById(R.id.rPage);
        display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        result = width / 1.233333333333333;

        widthButton = size.x / 3.0;
        heightButton = widthButton / 1.0;
        ViewGroup.LayoutParams params = rPage.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        rPage.setLayoutParams(params);
        rPage.requestLayout();

        btnJoin = (ImageView) findViewById(R.id.btn_join);
        btnJoined = (ImageView) findViewById(R.id.btn_joined);
        btnWaiting = (ImageView) findViewById(R.id.btn_waiting);
        recyclerViewMenu = (RecyclerView) findViewById(R.id.listMenu);

        Glide.with(this).load(WebService.getProjectImage() + String.valueOf(dbMasterRef) +
                "&pr=" + projectRef).into(imageView);
    }

   /* private void logBookSetup() {
        if (memberTypeCode.equals(WebService.inhouse)) {
            ProjectMenuModel mm = new ProjectMenuModel();
            mm.setMenuIcon(R.drawable.selector_project_menu_log_book);
            mm.setMenuTitle(getResources().getString(R.string.projectLookBook));
            menulist.add(mm);
        }
        projectMenuAdapter.notifyDataSetChanged();
    }*/

    private void joinSetup() {
        sharedPreferences = getSharedPreferences(General.PREF_NAME, 0);
        memberRef = sharedPreferences.getString("isMemberRef", null);
        isWaiting = sharedPreferences.getString("isWaiting", null);
        isJoin = sharedPreferences.getString("isJoin", null);
        memberTypeCode = sharedPreferences.getString("isMemberTypeCode", "");

        if (isJoin.equals("0")) {
            if (isWaiting != null) {
                if (isWaiting.equals("1")) {
                    btnJoin.setVisibility(View.GONE);
                    btnJoined.setVisibility(View.GONE);
                    btnWaiting.setVisibility(View.VISIBLE);
                } else {
                    btnJoin.setVisibility(View.VISIBLE);
                    btnJoined.setVisibility(View.GONE);
                    btnWaiting.setVisibility(View.GONE);
                }
            } else {
                btnJoin.setVisibility(View.GONE);
                btnJoined.setVisibility(View.GONE);
                btnWaiting.setVisibility(View.GONE);
            }
        } else {
            btnJoined.setVisibility(View.VISIBLE);
            btnJoin.setVisibility(View.GONE);
            btnWaiting.setVisibility(View.GONE);
        }
    }

    private void initAdapter() {
        projectMenuAdapter = new ProjectMenuAdapter(this, menulist, dbMasterRef, projectRef, projectName);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerViewMenu.setLayoutManager(mLayoutManager);
        recyclerViewMenu.setHasFixedSize(true);
        recyclerViewMenu.setAdapter(projectMenuAdapter);

        for (int i = 0; i < menuIcon.length; i++) {
            ProjectMenuModel projectMenuModel = new ProjectMenuModel();
            projectMenuModel.setMenuIcon(menuIcon[i]);
            projectMenuModel.setMenuTitle(menuTitle[i]);
            projectMenuModel.setMenuName(menuName[i]);
            menulist.add(projectMenuModel);
        }
        projectMenuAdapter.notifyDataSetChanged();

        header = (RecyclerViewHeader) findViewById(R.id.header);
        header.attachTo(recyclerViewMenu);
        //recyclerViewMenu.addItemDecoration(new GridItemDivider(numColumns, Utils.dpToPx(this, 8), false));
    }

    public static ProjectMenuActivity getInstance() {
        return projectMenuActivity;
    }

    private void requestProjectDetail() {
        progressDialog = ProgressDialog.show(this, "", "Please Wait...", true);
        presenter.getProjectDtl(String.valueOf(dbMasterRef), projectRef);
    }

    private void requestCommision() {
        presenter.getCommision(String.valueOf(dbMasterRef), projectRef, memberRef);
    }

    public void showMenuResults(retrofit2.Response<DetailStatusModel> response) {
        progressDialog.dismiss();
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            if (status == 200) {
                projectDescription = response.body().getData().getProjectDescription();
                countCategory = response.body().getData().getCountCategory();
                countCluster = response.body().getData().getCountCluster();
                categoryRef = response.body().getData().getCategoryRef();
                clusterRef = response.body().getData().getClusterRef();
                salesStatus = response.body().getData().getSalesStatus();
                isBooking = response.body().getData().getIsBooking();
                isNUP = response.body().getData().getIsNUP();
                isShowAvailableUnit = response.body().getData().getIsShowAvailableUnit();
                latitude = response.body().getData().getLatitude();
                longitude = response.body().getData().getLongitude();
                nupAmt = response.body().getData().getNupAmt();
                linkDetail = response.body().getData().getLinkProjectDetail();
                urlVideo = response.body().getData().getUrlVideo();
                imageLogo = response.body().getData().getImageLogo();
                bookingContact = response.body().getData().getBookingContact();
                downloadProjectInfo = response.body().getData().getDownloadProjectInfo();

                sessionManager.sessionProject(String.valueOf(dbMasterRef), projectRef, projectName, countCategory, countCluster, clusterRef, salesStatus,
                        isBooking, isNUP, isShowAvailableUnit, Double.toString(latitude), Double.toString(longitude), nupAmt,
                        linkDetail, urlVideo, imageLogo, bookingContact, downloadProjectInfo, categoryRef);

                /*ProjectVareable.getInstance().setLatitude(latitude);
                ProjectVareable.getInstance().setLongitude(longitude);
                ProjectVareable.getInstance().setLinkDetail(linkDetail);
                ProjectVareable.getInstance().setDownloadProjectInfo(downloadProjectInfo);
                ProjectVareable.getInstance().setUrlVideo(urlVideo);

                ProjectVareable.getInstance().setClusterRef(clusterRef);
                ProjectVareable.getInstance().setCategoryRef(categoryRef);
                ProjectVareable.getInstance().setIsShowAvailableUnit(isShowAvailableUnit);
                ProjectVareable.getInstance().setIsBooking(isBooking);
                ProjectVareable.getInstance().setIsNUP(isNUP);
                ProjectVareable.getInstance().setNupAmt(nupAmt);
                ProjectVareable.getInstance().setImageLogo(imageLogo);
                ProjectVareable.getInstance().setCountCategory(countCategory);
                ProjectVareable.getInstance().setCountCluster(countCluster);*/

                if (salesStatus.equals(WebService.salesStatusOpen)) {
                    if (isBooking.equals("0")) {
                        menulist.remove(4);
                        projectMenuAdapter.notifyDataSetChanged();
                    }

                    if (isNUP.equals("0")) {
                        menulist.remove(3);
                        projectMenuAdapter.notifyDataSetChanged();
                    }

                } else {
                    menulist.remove(3);
                    menulist.remove(4);
                    projectMenuAdapter.notifyDataSetChanged();
                }

                projectMenuAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                finish();
                startActivity(getIntent());
            }

        }

    }

    public void showMenuFailure(Throwable t) {
        progressDialog.dismiss();
        MainMenuActivity.OFF_LINE_MODE = true;
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "OFFLINE MODE", Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(ProjectMenuActivity.this, LaunchActivity.class));
                        finish();
                    }
                });

        snackbar.setActionTextColor(Color.RED);
        snackbar.show();
        //btnInformation.setEnabled(false);
    }

    public void dialogNotJoin(String statusJoin) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProjectMenuActivity.this);
        LayoutInflater inflater = ProjectMenuActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_special_enquiries, null);
        dialogBuilder.setView(dialogView);

        final TextView textView = (TextView) dialogView.findViewById(R.id.txt_specialEnquiries);
        dialogBuilder.setMessage("Notifikasi");
        textView.setText(statusJoin);
        dialogBuilder.setPositiveButton("GO TO PROJECT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });
        dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        b = dialogBuilder.create();
        b.show();
    }

    public void showCommisionResults(retrofit2.Response<CommisionStatusModel> response) {
        if (response.isSuccessful()) {
            int status = response.body().getStatus();
            String message = response.body().getMessage();
            if (status == 200) {
                commision = response.body().getData().getCommision();
                closingFee = response.body().getData().getClosingFee();
                description = response.body().getData().getDescription();
                commissionAmt = response.body().getData().getCommissionAmt();
                if (Double.parseDouble(commision) == 0) {
                    if (commissionAmt == 0.0) {
                        menulist.remove(5);
                        projectMenuAdapter.notifyDataSetChanged();
                    }
                } else if (commissionAmt == 0.0) {
                    if (Double.parseDouble(commision) == 0) {
                        menulist.remove(5);
                        projectMenuAdapter.notifyDataSetChanged();
                    }
                }

//                int statusReport = response.body().getDataReport().getStatus();
//                int isView = response.body().getDataReport().getIsView();
//                userRef = response.body().getDataReport().getUserRef();
//                ProjectVareable.getInstance().setUserRef(userRef);
//
//                //Add btn report
//                if (statusReport == 200 && isView > 0 && memberTypeCode.equals(WebService.inhouse) && isJoin.equals("1")) {
//                    ProjectMenuModel mm = new ProjectMenuModel();
//                    mm.setMenuIcon(R.drawable.selector_project_menu_report);
//                    mm.setMenuTitle(getResources().getString(R.string.projectReport));
//                    menulist.add(mm);
//                    projectMenuAdapter.notifyDataSetChanged();
//                }

            } else {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }

        }

    }

    public void showCommisionFailure(Throwable t) {
        //btnInformation.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_join:
                joinProject();
                break;
        }
    }

    private void joinProject() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProjectMenuActivity.this);
        LayoutInflater inflater = ProjectMenuActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_insert_member_code, null);
        dialogBuilder.setView(dialogView);
        edtMemberCode = (EditText) dialogView.findViewById(R.id.edt_member_code);
        txtTermMemberCode = (TextView) dialogView.findViewById(R.id.txt_term_member_code);
        dialogBuilder.setMessage("Masukkan Member ID");
        txtTermMemberCode.setText("Kosongkan jika tidak memiliki Member ID di " + projectName);
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //subscribeProject();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        alertDialogJoin = dialogBuilder.create();
        alertDialogJoin.setCancelable(false);
        alertDialogJoin.show();
        Button positiveButton = alertDialogJoin.getButton(DialogInterface.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                memberCode = edtMemberCode.getText().toString();
                progressDialog = ProgressDialog.show(context, "", "Please Wait...", true);
                final StringRequest request = new StringRequest(Request.Method.POST,
                        WebService.subscribeProject(), new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog.dismiss();
                            JSONObject jo = new JSONObject(response);
                            int status = jo.getInt("status");
                            String message = jo.getString("message");

                            if (status == 200) {
                                alertDialogJoin.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                ProjectMenuActivity.this.recreate();
                                SharedPreferences sharedPreferences = context.
                                        getSharedPreferences(General.PREF_NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("isWaiting", "0");
                                editor.putString("isJoin", "1");
                                editor.commit();
                            } else if (status == 202) {
                                alertDialogJoin.dismiss();
                                Intent intent = new Intent(context, ValidateInhouseCode.class);
                                intent.putExtra("memberRef", memberRef);
                                intent.putExtra("dbMasterRef", String.valueOf(dbMasterRef));
                                intent.putExtra("projectRef", projectRef);
                                intent.putExtra("memberCode", edtMemberCode.getText().toString());
                                context.startActivity(intent);
                            } else if (status == 203) {
                                alertDialogJoin.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                ProjectMenuActivity.this.recreate();
                                SharedPreferences sharedPreferences = context.
                                        getSharedPreferences(General.PREF_NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("isWaiting", "1");
                                editor.putString("isJoin", "0");
                            } else if (status == 204) {
                                edtMemberCode.setError(message);
                            } else {
                                String error = jo.getString("message");
                                Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.dismiss();
                                NetworkResponse networkResponse = error.networkResponse;
                                if (networkResponse != null) {
                                    Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
                                }
                                if (error instanceof TimeoutError) {
                                    Toast.makeText(context, context.getResources().getString(R.string.time_out), Toast.LENGTH_LONG).show();
                                    Log.d("Volley", "TimeoutError");
                                } else if (error instanceof NoConnectionError) {
                                    Toast.makeText(context, context.getResources().getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                                    Log.d("Volley", "NoConnectionError");
                                } else if (error instanceof AuthFailureError) {
                                    Log.d("Volley", "AuthFailureError");
                                } else if (error instanceof ServerError) {
                                    Log.d("Volley", "ServerError");
                                } else if (error instanceof NetworkError) {
                                    Log.d("Volley", "NetworkError");
                                } else if (error instanceof ParseError) {
                                    Log.d("Volley", "ParseError");
                                }
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("memberRef", memberRef);
                        params.put("dbMasterRef", String.valueOf(dbMasterRef));
                        params.put("projectRef", projectRef);
                        params.put("memberCode", edtMemberCode.getText().toString());

                        return params;
                    }
                };

                BaseApplication.getInstance().addToRequestQueue(request, "project");

            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ProjectMenuActivity.this, ProjectActivity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (alertDialogJoin != null && alertDialogJoin.isShowing()) {
            alertDialogJoin.dismiss();
        }

        if (b != null && b.isShowing()) {
            b.dismiss();
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

}